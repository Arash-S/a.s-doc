# Docker

## Table of content
1. [Docker Overview](#Docker-Overview)
 1. [Docker Engine](#Docker-Engine)
 1. [Docker Objects](#Docker-Objects)
 1. [Docker Underlying Technology](#Docker-Underlying-Technology)
1. [Install Docker On Ubuntu](#Install-Docker-On-Ubuntu)
1. [Docker Basic Commands](#Docker-Basic-Commands)

## List of Figures
1. [Docker Engine Components Flow](#Figure-Docker-Engine-Components-Flow)
1. [Docker Architecture](#Figure-Docker-Architecture)

## List of Code
1. [Remove Old Docker Versions](#Code-Remove-Old-Docker-Versions)
1. [Install Pre-requirements](#Code-Install-Pre-requirements)
1. [Setup Docker official Repository](#Code-Setup-Docker-official-Repository)
1. [Install Docker Engine](#Code-Install-Docker-Engine)
1. [Verify Docker Engine Installation](#Code-Verify-Docker-Engine-Installation)
1. [Configure docker to start on boot](#Code-Configure-docker-to-start-on-boot)
1. [Docker help](#Code-Docker-help)

# Docker Overview
Docker is a set of **Platform as Service** (**PaaS**) products that uses
OS-level virtualization to deliver software in packages called **containers**.
Containers are isolated from one another and bundle their own software,
libraries and configuration files. they can communicate with each other through
well-defined channels. All containers are run by single Operating system kernel
and therfore use fewer resources than virtual machine.

The service has both free and premium tiers. The software that hosts the
containers is called **Docker Engine**.

## Docker Engine 
Docker engine have modular design and client-server application Architecture
with these major components:
* **Daemon**:

  A *Server* which is a type of long-running program called a *Daemon* process.
  `dockerd` is the persistent process that manages containers. Docker uses
  different binaries for the daemon and client. To run daemon you type `dockerd`.
  The Docker daemon handles *Building*, *Running* and *Distributing* Docker
  containers.

* **REST API**:

  A REST API whih specifies interfaces that programs can use to talk to the
  daemon and instruct it what to do. This can be done using UNIX sockets or
  Network Interface.

* **Command Line Interface**:

   The `docker` command.

###### Figure-Docker Engine Components Flow
```ASCII
                                                                                           
         +-------------+                Manages                       +-------------+      
         |  Container  |<------------------+------------------------->|    Image    |      
         +-------------+                   |                          +-------------+      
                                           |                                               
                    +------------------------------------------------+                     
                    | Client Docker CLI                              |                     
                    |                                                |                     
         +----------|                                                |---------+           
         |          | +---------------------------------------------+|         |           
         v          | | REST API                                    ||         v           
  +-------------+   | |                                             ||   +--------------+  
  |   Network   |   | |                                             ||   | Data Volumes |  
  +-------------+   | |                                             ||   +--------------+  
                    | |+-------------------------------------------+||                     
                    | || Server (Docker Daemon)                    |||                     
                    | ||                                           |||                     
                    | ||                                           |||                     
                    | |+-------------------------------------------+||                     
                    | +---------------------------------------------+|                     
                    +------------------------------------------------+                     
```

The Docker client talks to the Docker daemon, which does the heavy lifting of
building, running, and distributing your Docker containers. The Docker client
and daemon can run on the same system, or you can connect a Docker client to a
remote Docker daemon. The Docker client and daemon communicate using a REST
API, over UNIX sockets or a network interface.

###### Figure-Docker Architecture
```ASCII
                                                                                       
                                                                                       
  Client              Docker Host                                  Registery           
  +--------------+    +----------------------------------------+   +---------------+   
  | docker build |    |   +---------------------------------+  |   |  +--------+   |   
  |              |------->| Docker Daemon                   |-------> | Ubuntu |   |   
  | docker pull  |    |   +---------------------------------+  |   |  +--------+   |   
  |              |    |                                        |   |      |        |   
  | docker run   |    |   +------------+        +-----------+ <------------        |   
  +--------------+    |   | Containers |<-------|  Images   |  |   +---------------+   
                      |   +------------+        +-----------+  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   |            |        |           |  |                       
                      |   +------------+        +-----------+  |                       
                      +----------------------------------------+                       
                                               


                   +---------------+           
                   | Docker Client |           
                   +---------------+           
                           ^                   
                           |                   
                           v                   
                   +---------------+           
                   | Docker Daemon |           
                   +---------------+           
                           ^                   
                           |                   
                           v                   
                   +---------------+           
                   |   Containerd  |  /* Manage Container Lifecycle */         
                   +---------------+           
                           |                   
               +-----------+----------+        
               v           v          v        
            +------+    +------+   +------+    
            | Shim |    | Shim |   | Shim |  /* Daemonless Containers. It's */
            +------+    +------+   +------+  /* basically sits as the parent of */  
               |           |          |      /* the container's process */  
            +------+    +------+   +------+     
            | Runc |    | Runc |   | Runc |  /* runc can be seen as component */  
            +------+    +------+   +------+  /* containerd. It is a command */ 
               |           |          |      /* line client for runnging application */ 
            +------+    +------+   +------+  /* packaged according to the OCI */   
            |      |    |      |   |      |  /* format and is a compliant implementation */ 
            +------+    +------+   +------+  /* of the OCI spec */   
```

A **Docker registry** stores Docker images. Docker Hub is a public registry that
anyone can use, and Docker is configured to look for images on Docker Hub by
default. You can even run your own private registry. If you use Docker
Datacenter (DDC), it includes Docker Trusted Registry (DTR).

When you use the `docker pull` or `docker run` commands, the required images
are pulled from your configured registry. When you use the `docker push`
command, your image is pushed to your configured registry.

## Docker Objects
Docker objects are various entities used to assemble an application in Docker.
Docker objects are Images, Containers, Networks, Volumes, Plugins, and other
objects.

* **Images**:

 An image is a read-only template with instructions for creating a Docker
 container. Often, an image is based on another image, with some additional
 customization. For example, you may build an image which is based on the ubuntu
 image, but installs the Apache web server and your application, as well as the
 configuration details needed to make your application run.
 
 You might create your own images or you might only use those created by others
 and published in a registry. To build your own image, you create a Dockerfile
 with a simple syntax for defining the steps needed to create the image and run
 it. Each instruction in a Dockerfile creates a layer in the image. When you
 change the Dockerfile and rebuild the image, only those layers which have
 changed are rebuilt. This is part of what makes images so lightweight, small,
 and fast, when compared to other virtualization technologies.

* **Containers**
  
  A container is a runnable instance of an image. You can connect a container
  to one or more networks, attach storage to it, or even create a new image based
  on its current state. A container is defined by its image as well as any
  configuration options you provide to it when you create or start it. When a
  container is removed, any changes to its state that are not stored in
  persistent storage disappear.

* **Services**

 Services allow you to scale containers across multiple Docker daemons, which
 all work together as a swarm with multiple managers and workers. Each member of
 a swarm is a Docker daemon, and all the daemons communicate using the Docker
 API. A service allows you to define the desired state, such as the number of
 replicas of the service that must be available at any given time. By default,
 the service is load-balanced across all worker nodes. To the consumer, the
 Docker service appears to be a single application. Docker Engine supports swarm
 mode in Docker 1.12 and higher.

## Docker Underlying Technology
Docker takes advantage of several Features of the Linux Kernel to deliver its
functionality.

* **Namespaces**
 
 Docker uses `namespaces` to provide the isolated workspace called the
 container. When you run a container, Docker creates a set of namespaces for
 that container. These namespaces provide a layer of isolation. Each aspect of
 a container runs in a separate namespace and its access is limited to that
 namespace.

 Docker Engine uses namespaces such as the following on Linux:
 * **pid**
 * **net**
 * **ipc**
 * **mnt**
 * **uts**

* **Control Groups**

 Docker Engine on Linux also relies on another technology called control groups
 (**cgroups**). A cgroup limits an application to a specific set of resources.
 Control groups allow Docker Engine to share available hardware resources to
 containers and optionally enforce limits and constraints. For example, you can
 limit the memory available to a specific container.

* **Union File Systems**
 
 Union file systems, or UnionFS, are file systems that operate by creating
 layers, making them very lightweight and fast. Docker Engine uses UnionFS to
 provide the building blocks for containers. Docker Engine can use multiple
 UnionFS variants, including AUFS, btrfs, vfs, and DeviceMapper.

* **Container Format**

 Docker Engine combines the namespaces, control groups, and UnionFS into a
 wrapper called a container format. The default container format is
 `libcontainer`. In the future, Docker may support other container formats by
 integrating with technologies such as BSD Jails or Solaris Zones.

# Install Docker On Ubuntu
To install docker engine first of all we need to uninstall old versions.

###### Code-Remove Old Docker Versions
```bash
$ sudo apt remove docker docker-engine docker.io containerd runc
```

The contents of `/var/lib/docker/`, including images, containers, volumes, and
networks, are preserved.

Now we install pre-requirements packages. And then setup official Repository.

###### Code-Install Pre-requirements
```bash
$ sudo apt update

$ sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
```

###### Code-Setup Docker official Repository
```bash
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

$ sudo add-apt-repository "deb [arch=amd64] \
  https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

Now we first update the system and then install the Docker Engine and
containerd.

###### Code-Install Docker Engine
```bash
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

To verify that Docker Engine is installed correctly, run the followning
command.

###### Code-Verify Docker Engine Installation
```bash
$ sudo docker run hello-world
```

###### Code-Configure docker to start on boot
```bash
$ sudo systemctl enable docker
$ echo manual | sudo tee /etc/init/docker.override
```

# Docker Basic Commands
Now we installed Docker, it is time to try out the basic commands that you can
do via the docker client program.

###### Code-Docker help
```bash
$ sudo docker help
$ sudo docker <COMMAND> --help
```


