# Process Credentials

## Table of Contents
1. [Introduction](#TC_Intro)
1. [Understanding Process Credentials](#TC_UPC)
	1. [Real UID and GID](#TC_RUGID)
	1. [Effective UID and GID](#TC_EUGID)
		1. [Set UID and set GID programs](#TC_SUGIDP)
	1. [Saved Set-UID and Saved Set-GID](#TC_SSUGID)
	1. [File-System User ID and File-System Group ID](#TC_FSUGID)
	1. [Supplementary Group IDs](#TC_SGID)
1. [Retrieving and Modifiying Process Credentials](#TC_RAMPC)
	1. [Retrieving real end effective IDs](#TC_RRAEIDS)
	1. [Modifying effective IDs ](#TC_MEIDS)
		1. [Preferred method for drop all privileges](#TC_PMFDAP)
	1. [Set effective IDs](#TC_SEIDS)
		1. [Preferred method for temporarily drop and regain privileges](#TC_PMFTDARP)
	1. [Modifying real and effective IDs](#TC_MREIDS)
	1. [Retrieving real, effective, and saved set IDs](#TC_RREASSIDS)
	1. [Modifying real, effective, and saved set IDs](#TC_MREASSIDS)

## List of Codes
1. [Example of set-UID and set-GID](#CO_EXOFSUGIDP)
1. [Retrieving real and effective IDs](#CO_RRAEIDS)
1. [Modifying effective IDs](#CO_MEIDS)
1. [Preferred method for drop all privileges](#CO_PMFDAP)
1. [Set effective IDs](#CO_SEIDS)
1. [Preferred method for drop and regain privileges](#CO_PMFSUGIDPTDR)
1. [Modifying real and effective IDs](#CO_MRAEIDS)
1. [Permanently drop the privilege using setreuid()](#CO_PDTPUS)

<a name="TC_Intro"></a>
## Introduction
Every process has a set of associated numeric **user identifiers**
(**UIDs**) and **group identifiers** (**GIDs**). Sometimes, these are
referred to as process credentials.

Process credentials identifiers are:
* Real UID and GID
* Effective UID and GID
* Saved set UID and GID
* File system UID and GUID (**Linux specific**)
* Supplementary GIDs

<a name="TC_UPC"></a>
## Understanding Process Credentials
In this section we describe process credentials in details.

<a name="TC_RUGID"></a>
### Real UID and GUID
The real user ID and group ID identify the user and group to which the
process belongs. As part of the login process, a login shell gets its real
user and group IDs from the third and fourth fields of the user’s password
record in the /etc/passwd file. When a new process is created it inherits
these identifiers from its parent.

<a name="TC_EUGID"></a>
### Effective UID and GID
On most UNIX implementations (Linux is a little different), the effective
user ID and group ID, in conjunction with the supplementary group IDs, are
used to determine the permissions granted to a process when it tries to
perform various operations (i.e., system calls).  For example, these
identifiers determine the permissions granted to a process when it accesses
resources such as files and System V interprocess communication (IPC)
objects, which themselves have associated user and group IDs determining to
whom they belong.

A process whose effective user ID is 0 (the user ID of root) has all of the
privileges of the superuser. Such a process is referred to as a privileged
process. Certain system calls can be executed only by privileged processes.

Normally, the effective user and group IDs have the same values as the
corresponding real IDs.

There are two ways in which the effective IDs can assume different values:
1. Use of system call (Like, setuid and etc)
1. Through the execution of set UID and set GID programs.

<a name="TC_SUGIDP"></a>
#### Set UID and set GID programs
A set-user-ID program allows a process to gain privileges it would not
normally have, by setting the process’s effective user ID to the same value
as the user ID (owner) of the executable file. A set-group-ID program
performs the analogous task for the process’s effective group ID.

Like any other file, an executable program file has an associated user ID
and group ID that define the ownership of the file. In addition, an
executable file has two special permission bits: the set-user-ID and
set-group-ID bits. (In fact, every file has these two permission bits, but
it is their use with executable files that interests us here.) These
permission bits are set using the chmod command. An unprivileged user can
set these bits for files that they own. A privileged user ( CAP_FOWNER )
can set these bits for any file.

<a name="CO_EXOFSUGIDP"></a>
```ASCII
$ su
password:

# ls -l program
-rwxr-xr-x 	1 	root 	root 	302585 Jun 26 15:05 program

# chmod u+s program
# chmod g+s program 

# ls -l program
-rwsr-sr-x 	1 	root	root 	302585 Jun 26 15:05 program
```
**Code**: Turn on Set-UID and Set-GID permission bit.

When a set-user-ID program is run (i.e., loaded into a process’s memory by
an exec()), the kernel sets the effective user ID of the process to be the
same as the user ID of the executable file. Running a set-group-ID program
has an analogous effect for the effective group ID of the process. Changing
the effective user or group ID in this way gives a process (in other words,
the user executing the program) privileges it would not normally have. For
example, if an executable file is owned by root (superuser) and has the
set-user-ID permission bit enabled, then the process gains superuser
privileges when that program is run.

Set-user-ID and set-group-ID programs can also be designed to change the
effective IDs of a process to something other than root. For example, to
provide access to a protected file (or other system resource), it may
suffice to create a special-purpose user (group) ID that has the privileges
required to access the file, and create a set-user-ID (set-group-ID)
program that changes the effective user (group) ID of a process to that ID.
This permits the program to access the file without allowing it all of the
privileges of the superuser.

**Note**: Set-UID and Set-GID permission bits don't have any effect for
shell scripts on Linux.

<a name="TC_SSUGID"></a>
### Saved Set-UID and Saved Set-GID
The saved set-user-ID and saved set-group-ID are designed for use with
set-user-ID and set-group-ID programs. 

When a program is executed, the following steps (among many others) occur:
* If the set-user-ID (set-group-ID) permission bit is enabled on the
  executable, then the effective user (group) ID of the process is made the
  same as the owner of the executable. If the set-user-ID (set-group-ID)
  bit is not set, then no change is made to the effective user (group) ID
  of the process.
* The values for the saved set-user-ID and saved set-group-ID are copied
  from the corresponding effective IDs. This copying occurs regardless of
  whether the set-user-ID or set-group-ID bit is set on the file being
  executed.

As an example of the effect of the above steps, suppose that a process
whose real user ID, effective user ID, and saved set-user-ID are all 1000
execs a set-user-ID program owned by root (user ID 0). After the exec, the
user IDs of the process will be changed as follows:

```ASCII
real=1000 effective=0 saved=0
```

Various system calls allow a set-user-ID program to switch its effective
user ID between the values of the real user ID and the saved set-user-ID.
Analogous system calls allow a set-group-ID program to modify its effective
group ID. In this manner, the program can temporarily drop and regain
whatever privileges are associated with the user (group) ID of the execed
file. (In other words, the program can move between the states of
potentially being privileged and actually operating with privi- lege.)

It is secure programming practice for set-user-ID and set-group-ID programs
to operate under the unprivileged (i.e., real) ID whenever the program
doesn’t actually need to perform any operations associated with the
privileged (i.e., saved set) ID.

**Note**: The saved set IDs are a System V invention adopted by POSIX. They
were not provided on releases of BSD prior to 4.4. The initial POSIX.1
standard made support for these IDs optional, but later standards (starting
with FIPS 151-1 in 1988) made support mandatory.

<a name="TC_FSUGID"></a>
### File-System User ID and File-System Group ID
On Linux, it is the file-system user and group IDs, rather than the
effective user and group IDs, that are used (in conjunction with the
supplementary group IDs) to determine permissions when performing
file-system operations such as opening files, changing file ownership, and
modifying file permissions. (The effective IDs are still used, as on other
UNIX implementations, for the other purposes described earlier.)

Normally, the file-system user and group IDs have the same values as the
corresponding effective IDs (and thus typically are the same as the
corresponding real IDs). Furthermore, whenever the effective user or group
ID is changed, either by a system call or by execution of a set-user-ID or
set-group-ID program, the corresponding file-system ID is also changed to
the same value. Since the file-system IDs follow the effective IDs in this
way, this means that Linux effectively behaves just like any other UNIX
implementation when privileges and permissions are being checked. The
file-system IDs differ from the corresponding effective IDs, and hence
Linux differs from other UNIX implementations, only when we use two
Linux-specific system calls, setfsuid() and setfsgid(), to explicitly make
them different.

Why does Linux provide the file-system IDs and in what circumstances would
we want the effective and file-system IDs to differ? The reasons are
primarily historical. The file-system IDs first appeared in Linux 1.2. In
that kernel version, one process could send a signal to another if the
effective user ID of the sender matched the real or effective user ID of
the target process. This affected certain programs such as the Linux NFS
(Network File System) server program, which needed to be able to access
files as though it had the effective IDs of the corresponding client
process. However, if the NFS server changed its effective user ID, it would
be vulnerable to signals from unprivileged user processes. To prevent this
possibility, the separate file-system user and group IDs were devised. By
leaving its effective IDs unchanged, but changing its file-system IDs, the
NFS server could masquerade as another user for the purpose of accessing
files without being vulnerable to signals from user processes.

From kernel 2.0 onward, Linux adopted the SUSv3-mandated rules regarding
permission for sending signals, and these rules don’t involve the effective
user ID of the target process. Thus, the file-system ID feature is no
longer strictly necessary (a process can nowadays achieve the desired
results by making judicious use of the system calls described later in this
chapter to change the value of the effective user ID to and from an
unprivileged value, as required), but it remains for compatibility with
existing software.

<a name="TC_SGID"></a>
### Supplementary Group IDs
The supplementary group IDs are a set of additional groups to which a
process belongs. A new process inherits these IDs from its parent. A login
shell obtains its supplementary group IDs from the system group file. As
noted above, these IDs are used in conjunction with the effective and
file-system IDs to determine permissions for accessing files, System V IPC
objects, and other system resources.

<a name="TC_RAMPC"><a/>
## Retrieving and Modifiying Process Credentials
As an alternative to using the system calls, the credentials of any process
can be found by examining the Uid , Gid , and Groups lines provided in the
Linux-specific ```/proc/PID/status``` file. The Uid and Gid lines list the
identifiers in the order real, effective, saved set, and file system.

In the following sections, we use the traditional definition of a
privileged process as one whose effective user ID is 0. However, Linux
divides the notion of superuser privileges into distinct capabilities. Two
capabilities are relevant for our discussion of all of the system calls
used to change process user and group IDs:

* The CAP_SETUID capability allows a process to make arbitrary changes to
  its user IDs.
* The CAP_SETGID capability allows a process to make arbitrary changes to
  its group IDs.

<a name="TC_RRAEIDS"></a>
### Retrieving real and effective IDs
The **getuid()** and **getgid()** system calls return the real IDs. The
**geteuid()** and **getegid()** system calls return the effective IDs.
These system calls are always successful.

<a name="CO_RRAEIDS"></a>
```C
#include <unistd.h>

uid_t getuid(void);     /* Returns real user ID of calling process */
uid_t geteuid(void);    /* Returns effective user ID of calling process */
gid_t getgid(void);     /* Return real group ID of calling process */
gid_t getegid(void);    /* Returns effective group ID of calling process */
```
**Code**: Retrieving real and effective IDs.

<a name="TC_MEIDS"></a>
### Modifying effective IDs
These system call changes the effective ID and possibly the real ID and the
saved set ID of the calling process to the value given as argument.

<a name="CO_MEIDS"></a>
```C
#include <unistd.h>

int setuid(uid_t uid);  /* Return 0 on success, or -1 on error */
int setgid(gid_t gid);  /* Return 0 on success, or -1 on error */
```
**Code**: Modifying effective IDs

The rules about what changes a process can make to its credentials using
these system calls depend on whether the process is privileged or not.

The following **rules** apply to **setuid()**:
1. When an unprivileged process calls setuid(), only the effective user ID
   of the process is changed. Furthermore, it can be changed only to the
   same value as either the real user ID or saved set-user-ID. (Attempts to
   violate this constraint yield the error EPERM .) This means that, for
   unprivileged users, this call is useful only when executing a
   set-user-ID program, since, for the execution of normal programs, the
   process’s real user ID, effective user ID, and saved set-user-ID all
   have the same value. On some BSD-derived implementations, calls to
   setuid() or setgid() by an unprivileged process have different semantics
   from other UNIX implementations: the calls change the real, effective,
   and saved set IDs (to the value of the current real or effective ID).

1. When a privileged process executes setuid() with a nonzero argument,
   then the real user ID, effective user ID, and saved set-user-ID are all
   set to the value spec- ified in the uid argument. This is a one-way
   trip, in that once a privileged process has changed its identifiers in
   this way, it loses all privileges and therefore can’t subsequently use
   setuid() to reset the identifiers back to 0.

The rules governing the changes that may be made to group IDs using setgid() are
similar, but it has a the condition:
* **Rule** 1 applies exactly as stated.
* **Rule** 2, changing the group IDs doesn't cause a process to lose privileges
  (which are determined bby the effective user ID), privileged programs can
  use setgid() to freely change the group IDs to any desired values.

<a name="TC_PMFDAP"></a>
#### Preferred method for drop all privileges
The following call is the preferred method for a set-user-ID-root program
whose effective user ID is currently 0 to irrevocably drop all privileges
(by setting both the effective user ID and saved set-user-ID to the same
value as the real user ID).

<a name="CO_PMFDAP"><a/>
```C
if (-1 == setuid(getuid())) {
	/*
	 * In this case exit with error.
	 */
}
```
**Code**: Preferred method for drop all privileges.

A set-user-ID program owned by a user other than root can use setuid() to
switch the effective user ID between the values of the real user ID and
saved set-user-ID. However, seteuid() is preferable for this purpose, since
it has the same effect, regardless of whether the set-user-ID pro- gram is
owned by root.

<a name="TC_SEIDS"></a>
### Set effective IDs
A process can use these system calls to change its effective ID.

<a name="CO_SEIDS"></a>
```C
#include <unistd.h>

int seteuid(uid_t euid);     /* Return 0 on success, or -1 on error */
int setegid(git_t egid);     /* Return 0 on success, or -1 on error */
```
**Code**: Set effective IDs.

The following rules govern the changes that a process may make to its
effective IDs:
1. An unprivileged process can change an effective ID only to the same
   value as the corresponding real or saved set ID. Except for the BSD
   portability issues noted earlier.
1. A privileged process can change an effective ID to any value. If a
   privileged process uses seteuid() to change its effective user ID to a
   nonzero value, then it ceases to be privileged (but may be able to
   regain privilege via the previous rule).

<a name="TC_PMFTDARP"></a>
#### Preferred method for temporarily drop and regain privileges
Using seteuid() is the preferred method for set-user-ID and set-group-ID
programs to temporarily drop and later regain privileges.

<a name="CO_PMFSUGIDPTDR"></a>
```C
/*
 * Save initial effective user ID (which is same as
 * saved set-user-ID 
 */
euid = geteuid();

/* 
 *Drop Privileges 
 */
if (-1 == seteuid(getuid())) {
	/*
	 * In this case exit with error.
	 */
}

/*
 * Regain Privileges
 */
if (-1 == seteuid(euid)) {
	/*
	 * In this case exit with error.
	 */
}
```
**Code**: Preferred method for drop and regain privileges.

**Note**: **seteuid()** and **setegid()** are now specified in SUSv3 and
appear on most UNIX implementations.

**Note**: In older versions of the GNU C library (glibc 2.0 and earlier),
seteuid(euid) is implemented as setreuid(–1, euid). In modern versions of
glibc, seteuid(euid) is implemented as setresuid(–1, euid, –1). Both
implementations permit us to specify euid as the same value as the current
effective user ID (i.e., no change). However, SUSv3 doesn’t specify this
behavior for seteuid(), and it is not possible on some other UNIX
implementations. Generally, this potential variation across implementations
is not apparent, since, in normal circumstances, the effective user ID has
the same value as either the real user ID or the saved set-user-ID. (The
only way in which we can make the effective user ID differ from both the
real user ID and the saved set-user-ID on Linux is via the use of the
nonstandard setresuid() system call.)

**Note**: In all versions of glibc (including modern ones), setegid(egid)
is implemented as setregid(–1, egid). As with seteuid(), this means that we
can specify egid as the same value as the current effective group ID,
although this behavior is not specified in SUSv3. It also means that
setegid() changes the saved set-group-ID if the effective group ID is set
to a value other than the current real group ID. (A similar remark applies
for the older implementation of seteuid() using setreuid().) Again, this
behavior is not specified in SUSv3.

<a name="TC_MREIDS"></a>
### Modifying real and effective IDs
The setreuid() system call allows the calling process to independently
change the values of its real and effective user IDs. The setregid() system
call performs the analogous task for the real and effective group IDs. If
we want to change only one of the identifiers, then we can specify –1 for
the other argument.

<a name="CO_MRAEIDS"></a>
```C
#include <unistd.h>

int setreuid(uid_t ruid, uid_t euid);     /* Return 0 on success, or -1 on error */
int setregid(gid_t rgid, gid_t egid);     /* Return 0 on success, or -1 on error */
```
**Code**: Modifying real and effective IDs.

Rules govern the changes that we can make using setreuid() and setregid()
system calls:
1. An unprivileged process can set the real user ID only to the current
   value of the real (i.e., no change) or effective user ID. The effective
   user ID can be set only to the current value of the real user ID,
   effective user ID (i.e., no change), or saved set-user-ID.
1. A privileged process can make any changes to the IDs.
1. For both privileged and unprivileged processes, the saved set-user-ID is
   also set to the same value as the (new) effective user ID if either of
   the following is true:
	1. ruid is not –1 (i.e., the real user ID is being set, even to the
	   same value it already had), or
	1. the effective user ID is being set to a value other than the
	   value of the real user ID prior to the call.

The third rule provides a way for a set-user-ID program to permanently drop
its privilege, using a call such as the following:

<a name="CO_PDTPUS"></a>
```C
setreuid(getuid(), getuid());
```
**Code**: : Permanently drop the privilege using setreuid().

**Note**: These system calls are now specified in SUSv3 and are available
on most UNIX implementations. SUSv3 says that it is unspecified whether an
unprivileged process can use setreuid() to change the value of the real
user ID to the current value of the real user ID, effective user ID, or
saved set-user-ID, and the details of precisely what changes can be made to
the real user ID vary across implementations.  SUSv3 describes slightly
different behavior for setregid(): an unprivileged process can set the real
group ID to the current value of the saved set-group-ID or set the
effective group ID to the current value of either the real group ID or the
saved set-group-ID. Again, the details of precisely what changes can be
made vary across implementations.

**Note**: SUSv3 doesn’t specify the effect of setreuid() and setregid() on
the saved set IDs, but SUSv4 specifies the behavior described here.

<a name="TC_RREASSIDS"></a>
### Retrieving real, effective, and saved set IDs
On most UNIX implementations, a process can’t directly retrieve (or update)
its saved set-user-ID and saved set-group-ID. However, Linux provides two
(nonstandard) system calls allowing us to do just that: getresuid() and
getresgid().

```C
#define _GNU_SOURCE
#include <unistd.h>

int getresuid(uid_t *ruid, uid_t *euid, uid_t *suid); /* Return 0 on success, or -1 on error */
int getresgid(gid_t *rgid, git_t *egid, git_t *sgid); /* Return 0 on success, or -1 on error */
```

<a name="TC_MREASSIDS"></a>
### Modifying real, effective, and saved set IDs
The setresuid() system call allows the calling process to independently
change the values of all three of its user IDs. The new values for each of
the user IDs are specified by the three arguments to the system call.

```C
```
