# Shellshock Attack

## Table of Contents
1. [Introduction](#Introduction)
1. [The Shellshock Vulnerability](#The-Shellshock-Vulnerability)
    1. [Exploiting the Shellshock vulnerability](#Exploiting-the-Shellshock-vulnerability)
        1. [Shellshock Attack on Set-UID Programs](#Shellshock-Attack-on-Set-UID-Programs)

## List of Codes
1. [Code-How to define and use shell functions](#Code-How-to-define-and-use-shell-functions)

## Introduction
The Shellshock vulnerability in bash involves functions definedd inside the
shell, which are called shell functions. 

A defined shell function can be printed using the ```declare``` command.

```bash
$ foo() { echo "Inside function"; }
$ declare -f foo
foo ()
{
    echo "Inside function"
}
$ foo
Inside function
$ unset -f foo
$ declare -f foo
```
##### Code-How to define and use shell functions

The shellshock vunerability described in this document passing a functtion
definition to a child shell process.

There are two ways for a child shell process to get a function definition from
its parent.
1. Define a function in the parent shell, export it, and then the child process
   will have it. The ```export``` command is used with a special flag to export
   the shell function for child processes, that is, when the shell process (the
   parent) forks a child process and runs a shell command in the child process,
   the function definition will be passed down to the child shell process. It
   should be noted that this method is only applicable if the parent is also a
   shell.
   ```bash
   $ foo() { echo "Hello world"; }
   $ declare -f foo
   foo ()
   {
       echo "Hello world"
   }
   $ foo
   Hello world
   $ export -f foo
   $ bash
   (child) $ declare -f foo
   foo ()
   {
       echo "Hello world"
   }
   (child) $ foo
   Hello world
   ```
1. Define a shell variable with special contents. This vulnerability is patched
   and it didn't work any more. it was a bug when bash sess an environment
   variable whose value starts with a pair of parentheses, it converts the
   variable into a shell function, instead of a shell variable.
   ```bash
   $ foo=' () { echo "Hello world"; }'
   $ echo $foo
   () { echo "Hello world"; }
   $declare -f foo
   $export foo
   $ bash
   (child) $ echo $foo
   (child) $ declare -f foo
   foo ()
   {
       echo "Hello world"
   }
   (child) $ foo
   Hello world
   ```

Although the two methods of passing a function definition to the child shell
seem to be different, they are actually the same. They both use environment
variables. In the first method, when the parent shell creates a new process, it
passes each exported function definition as an environment variable to the
child process. If the child process runs bash, the bash program will turn the
environment variable back to a function definition, just like what is described
in the second method..

The second method described above does not require the parent process to be a
shell process. Any process that needs to pass a function definition to its
child bash process just needs to pass the function definition via an
envrionment variable.

## The Shellshock Vulnerability
When bash in the child process converts the value of an environment variable to
a function, it is supposed to parse the commands contained in the variable, not
to execute them. However, due to a bug in its parsing logic, bash executes some
of the command contained in the variable.

### Exploiting the Shellshock vulnerability
Real example to show how the Shellshock attack works:
Requirement:
1. Target process should run bash.
1. Target process hould get some environment variables from outside, in
   praticular, from the user who is not trusted.

#### Shellshock Attack on Set-UID Programs
A Set-UID root program will start a bash process, when it executes the program
```/bin/ls``` via the system() function, the envrionment set by the attacker
will lead to unauthorized commands being executed with the root privilege.

##
