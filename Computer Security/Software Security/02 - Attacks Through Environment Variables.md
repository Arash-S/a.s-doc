# Attacks Through Environment Variables
1. [How a process Gets Its Environment Variables](#How-a-process-Gets-Its-Environment-Variables)
1. [Memory Location for Environment Variables](#Memory-Location-for-Environment-Variables)
1. [Shell Variables and Environment Variables](#Shell-Variables-and-Environment-Variables)
    1. [Shell Variables and Environment Variables are Different](#Shell-Variables-and-Environment-Variables-are-Different)
    1. [Shell Variables Affect the Environment Variables of Child Process](#Shell-Variables-Affect-the-Environment-Variables-of-Child-Process)
    1. [The proc File System](#The-proc-File-System)
1. [Attack Surface Caused by Environment Variable](#Attack-Surface-Caused-by-Environment-Variable)
    1. [Category of Attack surface by Environment Variables](#Category-of-Attack-surface-by-Environment-Variables)
    1. [Linker](#Linker)
        1. [Static Linking](#Static-Linking)
        1. [Dynamic Linking](#Dynamic-Linking)
        1. [LD_PRELOAD and LD_LIBRARY_PATH](#LD_PRELOAD-and-LD_LIBRARY_PATH)
    1. [Library](#Library)
    1. [External Program](#External-Program)
        1. [Two Typical Ways to Invoke External Programs](#Two-Typical-Ways-to-Invoke-External-Programs)
    1. [Application Code](#Application-Code)
1. [Set-UID Approach versus Service Approach](#Set-UID-Approach-versus-Service-Approach)

# Lists of Figure
1. [Figure-Memory Location for Environment Variables](#Figure-Memory-Location-for-Environment-Variables)
1. [Figure-Shell variables and envrionment variables](#Figure-Shell-variables-and-envrionment-variables)
1. [Figure-Attack surface created by environment variable](#Figure-Attack-surface-created-by-environment-variable)
1. [Dynamic Linking](#Dynamic-Linking)

# Lists of Codes
1. [Execute Program](#Code-Execute-Program)
1. [How execve can decide the environment variables of a process](#Code-How-execve-can-decide-the-environment-variables-of-a-process)
1. [Difference of Environment and Shell Variable](#Code-Difference-of-Environment-and-Shell-Variable)
1. [Attack using Dynamic Linker_Main function](#Code-Attack-using-Dynamic-Linker_Main-function)
1. [Code-Attack using Dynamic Linker_Infected library](#Code-Attack-using-Dynamic-Linker_Infected-library)
1. [Code-Attack using Dynamic Linker_Makefile](#Code-Attack-using-Dynamic-Linker_Makefile)

## How a process Gets Its Environment Variables
A process initially gets its environment variables through one of the two ways.
1. If a process is a new one, i.e., it is created using the ```fork()``` system
   call, the child process's memory is a duplicate of the parent's memory.
   Basically, the child process inherits all the parent process's environment
   variables.
1. If a process runs a new program in itself, rather than in a child process,
   it typically use the ```execve()``` system call. This system call overwrites
   the current process's memory with the data provided by the new program;
   therefore, all the environment variables stored inside the process are lost.
   If the process wants to pass environment variables to the new program, it
   has to specifically do that when invoking the ```execve()``` system call.

```C
#include <unistd.h>

/*
 * On sucess, execve() does not return, on error -1 is returned, and errno is
 * set appropriately.
 */
int execve(const char *filename, char *const argv[], char *const envp[]);
```
##### Code-Execute Program

If the process wants to pass its environment variables to the new program, it
can simply pass ```environ``` to ```execve()```. If a process does not want to
pass any environment variable, it can set the third argument to NULL.

```C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

extern char ** environ;

void main(int argc, char *argv[])
{
        char condition = '\0';
        char *v[2] = {NULL};
        char *newenv[3] = {NULL};
        const char * const help = "Program need an arguments.\n" \
                                "1 - Passing no argument variable\n" \
                                "2 - Passing a new set of environment variable\n" \
                                "3 - Passing all the envrionment variables\n" \
                                "\nPlease specify the number only.\n";

        if (2 == argc && ('1' == argv[1][0] ||
                '2' == argv[1][0] || '3' == argv[1][0])) {
                condition = argv[1][0];
        } else {
                write(STDERR_FILENO, help, strlen(help));
                exit(EXIT_FAILURE);
        }


        /*
         * Construct the argument array.
         */
        v[0] = "/usr/bin/env";
        v[1] = NULL;

        /*
         * Construct the envrionment variable array.
         */
        newenv[0] = "AAA=aaa";
        newenv[1] = "BBB=bbb";
        newenv[2] = NULL;

        switch (condition) {
                case '1':
                        execve(v[0], v, NULL);
                case '2':
                        execve(v[0], v, newenv);
                case '3':
                        execve(v[0], v, environ);
                default:
                        execve(v[0], v, NULL);
        }
}
```
##### Code-How execve can decide the environment variables of a process

## Memory Location for Environment Variables
Environment variables are stored on the stack. The following figure shows the
content of the stack when a program starts. Before the program's main function
is invoked, three blocks of data are pushed into the stack.

```ASCII
 Stack      |                                      |    
   |        |--------------------------------------|
   |        |                                  [1] |    
   |    --->|  Strings for environment variables   |    
   |    |   |     (e.g., "SHELL=/bin/bash")        |    
   |    |   |            Strings for argv[]        |<---
   |    |   |                                      |   |
   |    |   |--------------------------------------|   |
   |    |   |                                      |   |
   v    |   |--------------------------------------|   |
        |   |  envp[n]                         [2] |   |
        |   |  .                                   |   |
        |---|  .     Each array entry is a pointer |   |
            |  .                                   |   |
environ---->|  envp[0]                             |   |
        |   |--------------------------------------|   |
        |   |                                      |   |
        |   |--------------------------------------|   |
        |   |  argv[m]                         [3] |   |
        |   |  .                                   |   |
        |   |  .     Each array entry is a pointer |---|
        |   |  .                                   |    
        |   |  argv[0]                             |<---
        |   |--------------------------------------|   |
        |   |                                      |   |
        |   |--------------------------------------|   |
        |---|           envp (a pointer)           |   |
            |--------------------------------------|   |
            |           argv (a pointer)       [4] |---|
            |--------------------------------------|    
            |             argc (an int)            |    
            |--------------------------------------|    
            |                                      |
```
##### Figure-Memory Location for Environment Variables

The place marked by **[2]** stores an array of pointers, each pointing to a
place in the area marked by **[1]**; that is where the actual strings of
environment variables are stored (each string has the form of
```name=value```). The last element of the array contains a ```NULL``` pinter,
marking the end of the environment variable array.

The area marked by **[3]** contains another array of pointers (also ended by a
```NULL``` pointer). This is for the arguments passed to the program. The
actual argument strings are also stored in the area marked by **[1]**. The area
marked by **[4]** is the stack frame for them ```main()``` function. The
```argv``` argument points to the beginning of the argument array, and the envp
argument points to the beginning of the environment variable array. The global
variable ```environ``` also points to the beginning of the environment variable
array.

It should be noted that if changes need to be made to the environment
variables, such as adding or deleting a environment variable, or modifying the
value of an existing one, there may not be enough space in the areas marked by
**[1]** and **[2]**. In that case, the entire environment variable block may
change to a different location (usually in the heap). When this change happens,
the global variable ```environ``` needs to change accordingly, so it always
points to the newly updated environment variable array. On the other hand, the
```main``` function's third argument ```envp``` will not change, so it always
points to the original copy of the environment variables, not the most recent
one. That is why it is recommended that when referring to the environment
variables, always use the global variable ```environ```. A program can change
their environment variables using ```putenv()```, ```setenv()```, etc. These
functions may lead to the location change.

## Shell Variables and Environment Variables
Many people often mistakenly think that environment variables and shell
variables are the same thing. They are actually two very different but related
concepts.

Shell variables are internal variables maintained by a shell program. They
affect shell's behaviors, and they can also be used in shell scripts. Shell
provides built-in commands to allow users to create, assign, and delete
variables.

### Shell Variables and Environment Variables are Different
The main reason why people are confused by shell variables and environment
variables is that shell variables can become environment variables, and
environment variables can become shell variables. When a shell program starts,
it defines a shell variable for each of the enviroment variables of the
process, using the same names and copying their values. From then on, the shell
can easily get the value of the environment variables by referring to its own
shelll variables. Since they are different, whatever changes made to a shell
variable will not affect the environment variable of the same name, and vice
versa.

```bash
arash [~]:
>> strings /proc/$$/environ | grep LOGNAME
LOGNAME=arash
arash [~]:
>> echo $LOGNAME
arash
arash [~]:
>> LOGNAME=arastoo
arash [~]:
>> echo $LOGNAME
arastoo
arash [~]:
>> strings /proc/$$/environ | grep LOGNAME
LOGNAME=arash
arash [~]:
>> unset LOGNAME
arash [~]:
>> echo $LOGNAME

arash [~]:
>> strings /proc/$$/environ | grep LOGNAME
LOGNAME=arash
arash [~]:
>> 
```
##### Code-Difference of Environment and Shell Variable

### Shell Variables Affect the Environment Variables of Child Process
The most common use of shell is to execute programs. When we type a program
name in the shell prompt, shell will execute the program in a child process.
This is usually achieved by using ```fork()``` followed by ```execve()``` (or
one of the variants). When executing the new program in the new process, the
shell program explicitly sets the envrionment variables for the new program.
For example, bash uses ```execve()``` to start a new program, and when doing
that, bash compiles an array of name-value pairs from its shell variables, and
sets the third argument (```envp```) of ```execve()``` using this array.

Not all shell variables are included in the array. In the case of bash, only
the following two types of shell variables will be provided to the new program.
* **Shell variables copied from the environment variables**:

  If a shell variable comes from an environment variable, it will be included,
  and becomes an environment variable of the child process running the new
  program. However, if this shell variable is deleted using ```unset```, it
  will not appear in the child process.

* **User-defined shell variables marked for export**:
  
  Users can define new shell variables, but only those that are _exported_ will
  be given to the child process. This can be done using the ```export```
  command in bash, dash, zsh, and other shells. It should be noted that export
  is shell's built-in command.

```ASCII
                                                                                  
          +----------------------------+           +----------------------------+
          |                            |           |                            | 
          |+--------------------------+|           |+--------------------------+| 
          || Environment variables    ||       --->||   Environment variables  || 
          |+--------------------------+|       |   |+--------------------------+| 
          |             |              |       |   |                            | 
          |-------------|--------------|       |   |                            | 
          |             v              |       |   |                            | 
          |+--------------------------+|       |   |                            | 
          ||                          ||       |   |                            | 
          || Shell variables copied   ||       |   |                            | 
          || from environment         ||---|   |   |                            | 
          || variables                ||   |   |   |                            | 
          ||                          ||   |   |   |                            | 
          |+--------------------------+|   |   |   |                            | 
          |                            |   |---|   |                            | 
          |+--------------------------+|   |       |                            | 
          ||                          ||   |       |                            | 
          || User-defined shell       ||---|       |                            | 
          || variables (exported)     ||           |                            | 
          ||                          ||           |                            | 
          |+--------------------------+|           |                            | 
          |                            |           |                            | 
          |+--------------------------+|           |                            | 
          ||                          ||           |                            | 
          || Predefined shell         ||           |                            | 
          || variables (not exported) ||           |                            | 
          ||                          ||           |                            | 
          |+--------------------------+|           |                            | 
          | Shell's internal variables |           | Running a new program      | 
          |      (shell variables)     |           | started from the shell     | 
          |                            |           | program                    | 
          +----------------------------+           +----------------------------+ 
           Parent Process running shell             Child process
```
##### Figure-Shell variables and envrionment variables

### The proc File System
/proc is a virtual file system in Linux. The files listed in proc act as an
interface to the internal data structures in the kernel. They are used to
obtain system information or change kernel parameters at runtime.

The /proc file system contains a directory for each process, using the process
ID as the name of the directory. Inside shell, $$ is a special bash variable
containing the process ID of the current shell process, so if we want to access
the information of the current process, we just need to use /proc/$$ in the
shell.

Each process directory has a virtual file called ```environ```, which contains
the environment of the process. Since all the environment variables are
text-based, we can use strings to print out the text in this virtual file.

## Attack Surface Caused by Environment Variable
Although environment variables already reside in the memory of a process, the
do not *magically* change the behavior of the process; the must be used, as
inputs, by the process in order to have an effect.

Since environment variables can be set by users (who can be malicious), they
become paart of the attack surface to privileged ```Set-UID``` programs.

### Category of Attack surface by Environment Variables
We categorize the attack surface into two major categories:
* Linker/Loader
* Application
  * Library
  * External program
  * Application code

```ASCII
     +-----------------------------------+                       
     |Environment Variable Attack Surface|                       
     +-----------------------------------+                       
         v                         v                             
     +------+                +-----------+                       
     |Linker|       |--------|Application|-------------|         
     +------+       |        +-----------+             |         
                    v              v                   v         
                +-------+  +----------------+  +----------------+
                |Library|  |External Program|  |Application Code|
                +-------+  +----------------+  +----------------+
```
##### Figure-Attack surface created by environment variable

### Linker
A linker is used to find the external library functions used by a program. This
stage of the program is out of developer's control. Linkers in most operating
systems use envrionment variables to find where the libraries are, so they
create an opportunity for attackers to get privileged program to *find* their
malicious libraries.

Linking can be done whe a program is compiled or during the runtime: they are
called
* Static Linking
* Dynamic Linking

Dynamic linking uses environment variables, which become part of the attack
surface.

#### Static Linking
When static linking is utilized, the linker combines the program's  code and
the library code containing the function and all the functions it depends on.
The executable is self-contained, without any missing code. We can ask the
```gcc``` compiler to use static linking by specifying the -static option. The
size of generated program is 100 times larger than the size of program compiled
using dynamic linking.

Static linking waste a lot of memory. Moreover, if one of the library functions
is updated, all these executables using the affected library function need to
be patched. These disadvantages make static linking an undesirable approach in
practice.

Before a program compiled with dynamic linking is run, its executable is loaded
into memory first. This step is referred to as ```loading```. Linux ELF
executables, a standard file format for executables, contains a ```.interp```
section that specifies the name of the dynamic linker, which itself is a shared
library on Linux systems (ld-linux.so). After the executable is loaded into
memory, the loader passes the control to the dynamic linker, which finds the
implementation of function from a set of shared libraries, and links the
invocation of the functions from the executable to the actual implementation
code of the function.

#### Dynamic Linking
Dynamic linking solves the problems associated with static linking by not
including the library code in the program's binary; the linking to the library
code is conducted during runtime. Libraries supporting dynamic linking are
called ```shared libraries```. On most UNIX systems their names have a ```.so```
suffix; Micorsoft refers to them as DLLs(Dynamic Link Libraries).

```ASCII
                                                                                       
 +------------------+                                                                  
 |   Executable     |    Environment Variable                                          
 |(Partially Linked)|             |                                                    
 +------------------+             |                                                    
      |                           |                                                    
      v                           v                                                    
 +--------+             +-------------------+        +--------------+                  
 |Loader  |             |   Dynamic Linker  |        |Jump to main()|                  
 |(execve)|------------>|(ld.so/ld-linux.so)|------->|   Function   |                  
 +--------+             +-------------------+        +--------------+                  
                     (Link printf() definition)    (Fully Linked Executable in Memory)
```
##### Figure-Dynamic Linking

We can use the ```ldd``` command to see what shared libraries a program depends
on.

#### LD_PRELOAD and LD_LIBRARY_PATH
During the linking stage, the Linux dynamic linker searches some default folders
for the library functions used by the program. Users can specify additional
search places using the ```LD_PRELOAD``` and ```LD_LIBRARY_PATH``` environment
variables.

The ```LD_PRELOAD``` environment variable contains a list of shared libraries,
which will be searched first by the dynamic linker. That is why it is called
*preload*. If not all functions are found, the dynamic linker will search among
several lists of folders, including the list specified in the
```LD_LIBRARY_PATH``` environment variable.

Because these two environment variables can be set by users, they provide an
opportunity for users to control the outcome of the dynamic linking process, in
particular, allowing users to decide what implementation code of a function
should be used. If a program is a privileged *Set-UID* program, the use of
these environment variables by the dynamic linker may lead to security
breaches.

```C
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
	sleep(10);
	return 0;
}
```
##### Code-Attack using Dynamic Linker_Main function

```C
#include <stdio.h>

void sleep(int sec)
{
	printf("I'm not sleeping!\n");
}
```
##### Code-Attack using Dynamic Linker_Infected library

```makefile
CC=gcc
SH_FLAG=-shared

all: Attack Library Clean

Attack: main.o
	$(CC) main.o -o atacktest

main.o: main.c
	$(CC) -c main.c

Library: sleep.o
	$(CC) $(SH_FLAG) -o attacklib.so.1.0.1 sleep.o

sleep.o: sleep.c
	$(CC) -c sleep.c

Clean:
	-rm sleep.o main.o
	@echo ""
	@echo "+------------------------------------------------------------+"
	@echo "|You should export attacklib.so.1.0.1 as shown below         |"
	@echo "|export LD_PRELOAD=./attacklib.so.1.0.1                      |"
	@echo "|And after executing program you should unset this variable  |"
	@echo "|unset LD_PRELOAD                                            |"
	@echo "+------------------------------------------------------------+"
```
##### Code-Attack using Dynamic Linker_Makefile

For Set-UID programs: If the above technique works for Set-UID programs, it
will be dangerous, because attackers can use this method to get Set-UID
programs to run arbitrary code. Let us try it. We trun the above program into a
Set-UID root program. We can see that our ```sleep()``` function was not
invoked by the Set-UID root program. This is due to the countermeasure
implemented by the dynamic linker (ld.so or ld-linux.so), which ignores the
```LD_PRELOAD``` environment variable when the process's real and effective
user IDs differ, or the real and effective group IDs are different. The
```LD_LIBRARY_PATH``` environment variable is also ignored for the same reason.

## Library 
Most programs invoke functions from external libraries. When these functions
were developed, they were not developed for privileged programs, and therefore
may not sanitize the values of the environment variables. If these functions
are invoked by a privileged program, the environment variables used by these
functions immediatly become part of the attack surface, and must be analyzed
throughly to identify their potential risks.

## External Program
A program may choose to invoke external programs for certain functionalities,
such as sending emails, processing data, etc. When an external program is
invoked, its code runs with the calling process's privilege. The external
program may use some environment variables that are not used by the caller
program, and therefore, the entire program's attack surface is expanded, and
the risk is increased.

### Two Typical Ways to Invoke External Programs
There are two typical ways to invoke an external program from inside a program.
* exec() family function
  This function ultimately call the execve() system call to load the
  external program into memory and execute it.
* system() function
  This function first forks a child process, and then uses execl() to run the
  external program; the execl() function eventually calls execve().

Although both approaches eventually use ```execve()```, their attack surfaces
are very different.

Shell programs take a lot of inputs from outside, so their attack surface is
much broader than typical programs.

Shell programs behaviors are affected by many environment variables. The most
common one is the PATH environment variable. When a shell program runs a
command, if the location of the command is not provided, the shell program
searches for the command using the PATH environment variable. This environment
variable consists of a list of directories, from which the command is searched.

## Application Code
A program may use environment variables in its code, but may developers do not
fully understand how an environment variable gets into their program, and have
thus made incorrect assumptions on environment variables. These assumptions can
lead to incorrect sanitization of the environment variables, resulting in
security flows.

Developers may also choose to use a more secure version of ```getenv()```, such
as ```secure_getenv()``` provided by glibc.

## Set-UID Approach versus Service Approach
In the Set-UID approach, normal users run a special program to gain the root
privilege temporarily; they can then conduct the privileged operations. in the
service approach, normal users have to request a privileged service to conduct
the privileged operations for them. This service, usually called daemons or
services, are started by a privileged users or the OS. Both approaches are
similar; from the performance perspective, the set-UID approach may be better,
because it does not require a running background process. This advantage may be
significant in old days when the memory was a precious resource and computers
were not very powerful.
From the security perspective, the Set-UID approach has a much broader attack
surface than the service approach.

Due to this reason, the Android operating system, which is built on top of the
linux kernel, completely removed the Set-UID and Set-GID mechanisms.
