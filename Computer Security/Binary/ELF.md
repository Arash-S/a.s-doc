# Executable and Linkable Format (ELF)

# Table of content
1. [Executable and Linable Format Overview](#Executable-and-Linable-Format-Overview)
1. [Executable Header](#Executable-Header)
    1. [e_ident](#E_IDENT)
    1. [e_type](#E_TYPE)
    1. [e_machine](#E_MACHINE)
    1. [e_version](#E_VERSION)
    1. [e_entry](#E_ENTRY)
    1. [e_phoff](#E_PHOFF)
    1. [e_shoff](#E_SHOFF)
    1. [e_flags](#E_FLAGS)
    1. [e_ehszie](#E_EHSIZE)
    1. [e_phentsize and e_shentsize and e_phnum and e_shnum](#E_PHENTSIZE-and-E_SHENTSIZE-and-E_PHNUM-and-E_SHNUM)
    1. [e_shstrndx](#E_SHSTRNDX)
1. [Program Headers](#Program-Headers)
    1. [p_type](#p_type)
    1. [p_flags](#p_flags)
    1. [p_offset, p_vaddr, p_paddr, p_filesz, and p_memsz](#p_offset-p_vaddr-p_paddr-p_filesz-and-p_memsz)
    1. [p_align](#p_align)
1. [Sections](#Sections)
    1. [.init and .fini](#init-and-fini)
    1. [.text](#text)
    1. [.bss, .data, and .rodata](#bss-data-and-rodata)
    1. [.plt, .got, and .got.plt](#plt-got-and-gotplt)
      1. [Dynamically Resolving a Library Function Using The PLT](#Dynamically-Resolving-a-Library-Function-Using-The-PLT)
    1. [.rel.\* and .rela.\*](#rel-and-rela)
    1. [.dynamic](#dynamic)
    1. [.init_array and .fini_array](#init_array-and-fini_array)
    1. [.shstrtab, .symtab, .strtab, .dynsym, and .dynstr](#shstrtab-symtab-strtab-dynsym-and-dynstr)
1. [Section Headers](#Section-Headers)
    1. [sh_name](#SH_NAME)
    1. [sh_type](#SH_TYPE)
    1. [sh_flags](#SH_FLAGS)
    1. [sh_addr and sh_offset and sh_size](#SH_ADDR-and-SH_OFFSET-and-SH_SIZE)
    1. [sh_link](#SH_LINK)
    1. [sh_info](#SH_INFO)
    1. [sh_addralign](#SH_ADDRALIGN)
    1. [sh_entsize](#SH_ENTSIZE)

# List of Figures
1. [ELF binary at a glance](#Figure-ELF-binary-at-a-glance)
1. [Loading an ELF Binary on a Unix-like Operating Systems](#Figure-Loading-an-ELF-Binary-on-a-Unix-like-Operating-Systems)
1. [e_ident](#Figure-E_IDENT)
1. [Calling a shared library function via the PLT](#Figure-Calling-a-shared-library-function-via-the-PLT)

# List of Code
1. [Executable Header Structure](#Code-Executable-Header-Structure)
1. [EI_CLASS](#Code-EI_CLASS)
1. [EI_DATA](#Code-EI_DATA)
1. [EI_VERSION](#Code-EI_VERSION)
1. [EI_OSABI and EI_ABIVERSION](#Code-EI_OSABI-and-EI_ABIVERSION)
1. [e_type](#Code-E_TYPE)
1. [e_version](#Code-E_VERSION)
1. [Struct Elf64_Phdr](#Code-Struct-Elf64_Phdr)
1. [A typical program header](#Coede-A-typical-program-header)
1. [p_type](#Code-p_type)
1. [p_flags](#Code-p_flags)
1. [A listing of sections in the example binary](#Code-A-listing-of-sections-in-the-example-binary)
1. [Example of Section Header String Table](#Code-Example-of-Section-Header-String-Table)
1. [Disassembel Machine Instruction](#Code-Disassembel-Machine-Instruction)
1. [The relocation sections in the example binary](#Code-The-relocation-sections-in-the-example-binary)
1. [Contents of the dynamic section](#Code-Contents-of-the-dynamic-section)
1. [.init_array section](#Code-init_array-section)
1. [Section Header Structure](#Code-Section-Header-Structure)
1. [Explore .plt Section](#Code-Explore-plt-Section)
1. [sh_type](#Code-sh_type)
1. [Struct Elf64_Rel](#Code-Struct-Elf64_Rel)
1. [Struct Elf64_Rela](#Code-Struct-Elf64_Rela)
1. [Struct Elf64_Dyn](#Code-Struct-Elf64_Dyn)
1. [sh_flags](#Code-SH_FLAGS)

## Executable and Linable Format Overview
Executable and Linable Format (ELF) standard binary file format for Unix and
Unix-like systems. ELF is used for executable files, object files, shared
libraries, and core dumps. 

In general ELF binaries consist of four types of components:
* **Executable Header**
* **Program Headers** (Optional)
* **Sections**
* **Section Headers** (Optional)

###### Figure-ELF binary at a glance
```ASCII
               +-------------------+
               | Executable Header |
               +-------------------+
               |                   |
               |                   |
               |   Program Header  |
               |                   |
               |                   |
               +-------------------+ ---> +---------------------+
               |                   |      | Important Sections: |
               |                   |      |   .interp           |
               |                   |      |   .init             |
               |                   |      |   .plt              |
               |                   |      |   .text             |
               |      Section      |      |   .fini             |
               |                   |      |   .rodata           |
               |                   |      |   .data             |
               |                   |      |   .bss              |
               |                   |      |   .shstrtab         |
               |                   |      |                     |
               +-------------------+      +---------------------+
               |                   |
               |                   |
               |                   |
               |   Setion Header   |
               |                   |
               |                   |
               |                   |
               +-------------------+
```

###### Figure-Loading an ELF Binary on a Unix-like Operating Systems
```ASCII
               +-------------------+
               |+-----------------+|
               ||      Kernel     ||
               |+-----------------+|
               |                   |      +-------------------+
               |+-----------------+| ---> |    Environment    |
               ||      Stack      ||      +-------------------+
               |+-----------------+|      |     Arguments     |
               |         |         |      +-------------------+
               |         |         |
               |         v         |      +-------------------+
 +-------------|+-----------------+|      |    Interpreter    |
 |      +----->|| Memory Mapping  || ---> +-------------------+
 |      | +--->||      area       ||      |      lib1.so      |
J|      | | +->|+-----------------+|      +-------------------+
u|      | | |  |         ^         |      |      lib2.so      |
m|      | | |  |         |         |      +-------------------+
p|    R | | |  |         |         |
 |    e | | |  |+-----------------+|
t|    l | | |  ||       Heap      ||
o|    o | | |  |+-----------------+|
 |    c | | |  |                   |      
e|    a | | |  |                   |      
n|    t | | |  |+-----------------+| ---> +-------------------+
t|    i | | +--||                 ||      |       Header      |
r|    o | |    ||                 ||      +-------------------+
y|    n | |    ||       Data      ||      |   Data Section 1  |
 |    s | |    ||                 ||      +-------------------+
p|      | |    ||                 ||      |   Data Section 2  |
o|      | |    |+-----------------+|      +-------------------+
i|      | |    |+-----------------+| ---> +-------------------+
n|      | +----||                 ||      |   Code Section 1  |
t|      |      ||                 ||      +-------------------+
 |      +------||       Code      ||      |   Code Section 2  |
 |             ||                 ||      +-------------------+
 +------------>||                 ||              Binary
               |+-----------------+|
               |                   |
  Address 0x0  +-------------------+
                   Virtual Memory
```

# Executable Header
Every ELF file start with an executable header, which is just a structured
series of bytes telling you that it's an ELF file, what kind of ELF file it is,
and where in the file to find all the other contents.

###### Code-Executable Header Structure
```C
typedef struct {
        unsigned char e_ident[16]; /* Magic number and other info */
        uint16_t e_type;           /* Object type */                        
        uint16_t e_machine;        /* Architecture */                       
        uint32_t e_version;        /* Object file version */                
        uint64_t e_entry;          /* Entry point virtual address */        
        uint64_t e_phoff;          /* Program header table file offset */   
        uint64_t e_shoff;          /* Section header table file offset */   
        uint32_t e_flags;          /* Proessor-specific flags */            
        uint16_t e_ehsize;         /* ELF header size in bytes */           
        uint16_t e_phentsize;      /* Program header table entry size */    
        uint16_t e_phnum;          /* Program header table entry count */   
        uint16_t e_shentsize;      /* Section header table entry size */    
        uint16_t e_shnum;          /* Section header table entry count */   
        uint16_t e_shstrndx;       /* Section header string table index */  
} Elf64_Ehdr;                                                               
```

## E_IDENT

###### Figure-E_IDENT
```ASCII
0      1   2   3   4          5         6            7          8               9        15
+------+---+---+---+----------+---------+------------+----------+---------------+--------+ 
| ox7f | E | L | F | EI_CLASS | EI_DATA | EI_VERSION | EI_OSABI | EI_ABIVERSION | EI_PAD | 
+------+---+---+---+----------+---------+------------+----------+---------------+--------+
```

- [EI_CLASS](#EI_CLASS)
- [EI_DATA](#EI_DATA)
- [EI_VERSION](#EI_VERSION)
- [EI_OSABI and EI_ABIVERSION](#EI_OSABI-and-EI_ABIVERSION)

### EI_CLASS
Denotes what the ELF Specification refers to as the binary's "Class". This is
a bit of a misnomer since the word class is so generic, it could mean almost
anything. What the byte really denotes is whether the binary is for a 32-bit or
64-bit architecture.                                                        

###### Code-EI_CLASS
```C
#define EI_CLASS          4       /* File class byte index */               
#define ELFCLASSNONE      0       /* Invalid class */                       
#define ELFCLASS32        1       /* 32-bit objecs */                       
#define ELFCLASS64        2       /* 64-bit objects */                      
#define ELFCLASSNUM       3                                                 
```                                                                              

### EI_DATA
Indicated the endianness of the binary and architecture.                  

###### Code-EI_DATA
```C
#define EI_DATA           5       /* Data encoding byte index */          
#define ELFDATANONE       0       /* Invalid data encoding */             
#define ELFDATA2LSB       1       /* 2's complement, Little endian */     
#define ELFDATA2MSB       2       /* 2's complement, Big endian */        
#define ELFDATANUM        3                                               
```                 

### EI_VERSION 
Version of the ELF specification used when creating the binary.           

###### Code-EI_VERSION
```C
#define EV_NONE           0       /* Invalid ELF Version */               
#define EV_CURRENT        1       /* Current Version */                   
#define EV_NUM            2                                               
```                 

### EI_OSABI and EI_ABIVERSION
Denote information regarding the application binary interface (ABI) and
operating system (OS) for which the binary was compiled.

###### Code-EI_OSABI and EI_ABIVERSION
```C
#define EI_OSABI            7     /* OS ABI identification */             
#define ELFOSABI_NONE       0     /* UNIX System V ABI */                 
#define ELFOSABI_SYSV       0     /* Alias */                             
#define ELFOSABI_HPUX       1     /* HP-UX */                             
#define ELFOSABI_NETBSD     2     /* NetBSD */                            
#define ELFOSABI_GNU        3     /* Object uses GNU ELF extensions */    
#define ELFOSABI_LINUX      ELFOSABI_GNU     /* Compatibility alias */    
#define ELFOSABI_SOLARIS    6     /* Sun Solaris */                       
#define ELFOSABI_AIX        7     /* IBM AIX */                           
#define ELFOSABI_IRIX       8     /* SGI Irix */                          
#define ELFOSABI_FREEBSD    9     /* FreeBSD */                           
#define ELFOSABI_TRU64      10    /* Compaq TRU64 UNIX */                 
#define ELFOSABI_MODESTO    11    /* Novell Modesto */                    
#define ELFOSABI_OPENBSD    12    /* OpenBSD */                           
#define ELFOSABI_ARM_AEABI  64    /* ARM EABI */                          
#define ELFOSABI_ARM        97    /* ARM */                               
#define ELFOSABI_STANDALONE 255   /* Standalone (embedded) application */ 
                                                                          
#define EI_ABIVERSION       8     /* ABI version */
```                 

## E_TYPE
Specifies the type of binary.

###### Code-E_TYPE
```C
/* Legal values for e_type (object file type) */

#define ET_NONE    0       /* No file type */           
#define ET_REL     1       /* Realocatable file */      
#define ET_EXEC    2       /* Executeable file */       
#define ET_DYN     3       /* Shared Object file */     
#define ET_CORE    4       /* Core file */              
#define ET_NUM     5       /* Number of defined types */
#define ET_LOOS    0xfe00  /* OS-specific range start */
#define ET_HIOS    0xfeff  /* OS-specific range end */  
#define ET_LOPROC  0xff00  /* Processor-specific range start */ 
#define ET_HIPROC  0xffff  /* Processor-specific range end */   
```

## E_MACHINE
Denotes the architecture that the binary is intended to run on (e.g, `EM_ARM`, 
`EM_386`, `EM_x86_64`). For mor information see ```/usr/include/elf.h``` 
header file.

## E_VERSION
The e_version field serves the same role as the `EI_VERSION` byte in the
e_ident array.

###### Code-E_VERSION
```C
/* Legal values for e_version (version). */
 
#define EV_NONE         0               /* Invalid ELF version */ 
#define EV_CURRENT      1               /* Current version */     
#define EV_NUM          2                                          
```

## E_ENTRY
Denotes the entry point of the binary. This is the virtual address at which
execution should start, right after the interpreter `ld-linux.so` finished
loading the binary into virtual memory.

## E_PHOFF
Indicate the file offset to the beginning of the program header table. The
offsets can also be set to zero to indicate that the file does not contain a
program header. This field is file offsets.

## E_SHOFF
Indicate the file offset to the beginning of the section header table. The
offsets can also be set to zero to indicate that the file does not contain a
program header. This field is file offsets.

## E_FLAGS
The e_flags field provides room for flags specific to the architecture for
which the binary is compiled. For x86 binaries, this field is typically set to
zero and thus not of interest.

## E_EHSIZE
Specifies the size of the executable header, in bytes. For 64-bit it is always
64 bytes and for 32-bit it is 52 bytes.

## E_PHENTSIZE and E_SHENTSIZE and E_PHNUM and E_SHNUM
The `e_phoff` and `e_shoff` fields point to the file offsets where the program
header and section header tables begin. But for the linker or loader (or
another program handling an ELF binary) to actually traverse these tables,
additional information is needed. Specifically, they need to know the size of
the individual program or section headers in the tables, as well as the number
of headers in each table. This information is provided by the `e_phentsize` and
`e_phnum` fields for the program header table and by the `e_shentsize` and
`e_shnum` fields for the section header table.

## E_SHSTRNDX
The `e_shstrndx` field contains the index of the header associated with a
special string table section, called `.shstrtab`. This is a dedicated section
that contains a table of null-terminated ASCII strings, which store the names
of all the sections in the binary. It is used by ELF processing tools such as
*readelf* to correctly show the names of sections.

###### Code-Example of Section Header String Table
```bash
$ readelf -x .shstrtab a.out

Hex dump of section '.shstrtab':
  0x00000000 002e7379 6d746162 002e7374 72746162 ..symtab..strtab
  0x00000010 002e7368 73747274 6162002e 696e7465 ..shstrtab..inte
  0x00000020 7270002e 6e6f7465 2e676e75 2e627569 rp..note.gnu.bui
  0x00000030 6c642d69 64002e6e 6f74652e 4142492d ld-id..note.ABI-
  0x00000040 74616700 2e676e75 2e686173 68002e64 tag..gnu.hash..d
  0x00000050 796e7379 6d002e64 796e7374 72002e67 ynsym..dynstr..g
  0x00000060 6e752e76 65727369 6f6e002e 676e752e nu.version..gnu.
  0x00000070 76657273 696f6e5f 72002e72 656c612e version_r..rela.
  0x00000080 64796e00 2e72656c 612e706c 74002e69 dyn..rela.plt..i
  0x00000090 6e697400 2e746578 74002e66 696e6900 nit..text..fini.
  0x000000a0 2e726f64 61746100 2e65685f 6672616d .rodata..eh_fram
  0x000000b0 655f6864 72002e65 685f6672 616d6500 e_hdr..eh_frame.
  0x000000c0 2e696e69 745f6172 72617900 2e66696e .init_array..fin
  0x000000d0 695f6172 72617900 2e64796e 616d6963 i_array..dynamic
  0x000000e0 002e676f 74002e67 6f742e70 6c74002e ..got..got.plt..
  0x000000f0 64617461 002e6273 73002e63 6f6d6d65 data..bss..comme
  0x00000100 6e7400                              nt.

```

# Program Headers
The *program header table* provides a *segment view* of the binary, The segment
view is used by the operating system and dynamic linker when loading an ELF
into a process for execution to locate the relevant code and data and decide
what to load into virtual memory.

An ELF segment encompasses zero or more sections, essentially bindling these
into a single chunk. Since segments provide an execution view, they are needed
only for executable ELF files.

The program header table encodes the segment view using program headers of type
`struct Elf64_Phdr`.

###### Code-Struct Elf64_Phdr
```C
typedef struct {
  Elf64_Word    p_type;                 /* Segment type */
  Elf64_Word    p_flags;                /* Segment flags */
  Elf64_Off     p_offset;               /* Segment file offset */
  Elf64_Addr    p_vaddr;                /* Segment virtual address */
  Elf64_Addr    p_paddr;                /* Segment physical address */
  Elf64_Xword   p_filesz;               /* Segment size in file */
  Elf64_Xword   p_memsz;                /* Segment size in memory */
  Elf64_Xword   p_align;                /* Segment alignment */
} Elf64_Phdr;
```

**Note**: The section-to-segment mapping at the bottom of the readelf output,
which clearly illustrates that segments are simply a bunch of sections bundled
together. 

###### Coede-A typical program header
```C
>> readelf --wide --segments not_striped 

Elf file type is DYN (Shared object file)
Entry point 0x1040
There are 11 program headers, starting at offset 64

Program Headers:
  Type           Offset   VirtAddr           PhysAddr           FileSiz  MemSiz   Flg Align
  PHDR           0x000040 0x0000000000000040 0x0000000000000040 0x000268 0x000268 R   0x8
  INTERP         0x0002a8 0x00000000000002a8 0x00000000000002a8 0x00001c 0x00001c R   0x1
      [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
  LOAD           0x000000 0x0000000000000000 0x0000000000000000 0x000558 0x000558 R   0x1000
  LOAD           0x001000 0x0000000000001000 0x0000000000001000 0x0001e5 0x0001e5 R E 0x1000
  LOAD           0x002000 0x0000000000002000 0x0000000000002000 0x000120 0x000120 R   0x1000
  LOAD           0x002de8 0x0000000000003de8 0x0000000000003de8 0x000248 0x000250 RW  0x1000
  DYNAMIC        0x002df8 0x0000000000003df8 0x0000000000003df8 0x0001e0 0x0001e0 RW  0x8
  NOTE           0x0002c4 0x00000000000002c4 0x00000000000002c4 0x000044 0x000044 R   0x4
  GNU_EH_FRAME   0x002014 0x0000000000002014 0x0000000000002014 0x000034 0x000034 R   0x4
  GNU_STACK      0x000000 0x0000000000000000 0x0000000000000000 0x000000 0x000000 RW  0x10
  GNU_RELRO      0x002de8 0x0000000000003de8 0x0000000000003de8 0x000218 0x000218 R   0x1

 Section to Segment mapping:
  Segment Sections...
   00     
   01     .interp 
   02     .interp .note.gnu.build-id .note.ABI-tag .gnu.hash .dynsym .dynstr .gnu.version .gnu.version_r .rela.dyn .rela.plt 
   03     .init .plt .text .fini 
   04     .rodata .eh_frame_hdr .eh_frame 
   05     .init_array .fini_array .dynamic .got .got.plt .data .bss 
   06     .dynamic 
   07     .note.gnu.build-id .note.ABI-tag 
   08     .eh_frame_hdr 
   09     
   10     .init_array .fini_array .dynamic .got 
```

## p_type
This field indicate the type of the segment. 

Important values for this field inlude:
* **`PT_LOAD`**

  Segments of type `PT_LOAD` are intended to be loaded into memory when setting
  up the process. The size of the loadable chunk and the address to load it at
  are described in the rest of the program header. 

* **`PT_DYNAMIC`**
  
  Segment of type `PT_DYNAMIC` contains the `.dynamic` section, which tells the
  interpreter how to parse and prepare the binary for execution.

* **`PT_INTERP`**

  Segment of type `PT_INTERP` contains the `.interp` section, which provides the
  name of the interpreter that is to be used to load the binary.

###### Code-p_type
```C
/* Legal values for p_type (segment type).  */

#define PT_NULL         0               /* Program header table entry unused */
#define PT_LOAD         1               /* Loadable program segment */
#define PT_DYNAMIC      2               /* Dynamic linking information */
#define PT_INTERP       3               /* Program interpreter */
#define PT_NOTE         4               /* Auxiliary information */
#define PT_SHLIB        5               /* Reserved */
#define PT_PHDR         6               /* Entry for header table itself */
#define PT_TLS          7               /* Thread-local storage segment */
#define PT_NUM          8               /* Number of defined types */
#define PT_LOOS         0x60000000      /* Start of OS-specific */
#define PT_GNU_EH_FRAME 0x6474e550      /* GCC .eh_frame_hdr segment */
#define PT_GNU_STACK    0x6474e551      /* Indicates stack executability */
#define PT_GNU_RELRO    0x6474e552      /* Read-only after relocation */
#define PT_LOSUNW       0x6ffffffa
#define PT_SUNWBSS      0x6ffffffa      /* Sun Specific segment */
#define PT_SUNWSTACK    0x6ffffffb      /* Stack segment */
#define PT_HISUNW       0x6fffffff
#define PT_HIOS         0x6fffffff      /* End of OS-specific */
#define PT_LOPROC       0x70000000      /* Start of processor-specific */
#define PT_HIPROC       0x7fffffff      /* End of processor-specific */
```

## p_flags
The flags specify the runtime access permissions for the segment. 

###### Code-p_flags
```C
/* Legal values for p_flags (segment flags).  */

#define PF_X            (1 << 0)        /* Segment is executable */
#define PF_W            (1 << 1)        /* Segment is writable */
#define PF_R            (1 << 2)        /* Segment is readable */
#define PF_MASKOS       0x0ff00000      /* OS-specific */
#define PF_MASKPROC     0xf0000000      /* Processor-specific */
```

## p_offset, p_vaddr, p_paddr, p_filesz, and p_memsz
The `p_offset`, `p_vaddr`, `p_filesz` fields, Specify the file offset at which
the segment starts, the virtual address at which it is to be loaded, and the
file size of the segment. For loadable segments, `p_vaddr` must be equal to
`p_offset`.

On operating systems such as Linux, this field is unused and set to zero since
they execute all binaries in virtual memory.

To understand `p_filesz` and `p_memsz`, recall that some sections only indicate
the need to allocate some bytes in memory but don't actually occupy these bytes
in the binary file. For instance, the `.bss` setion contains zero-initialized
data. Since all data in this section is known to be zero anyway. There's no
need to actually include all these zeros in the binary. However, when loading
the segment containing `.bss` into virtual memory, all the bytes in `.bss`
should be alloated. Thus, it's possible for `p_memsz` to be larger than
`p_filesz`. When this happens, the loader adds the extra bytes at the end of
the segment when loading the binary and initializes them to zero.

## p_align
The `p_align` field indiates the required memory alignment (in bytes) for the
segment. An alignment value of 0 or 1 indicates that no particular alignment is
required. If `p_align` isn't set to 0 or 1, then its value must be a power of
2, and `p_vaddr` must be equal to `p_offset`.

# Sections
The code and data in an ELF binary are logically divided into contiguous
nonoverlapping chunks called *sections*.  Sections don't have any predetermined
structure; instead, the structure of each section varies depending on the
contents. In fact, a section may not even have any particular structure at all;
often a section is nothing more than an unstructured blob of code or data. 

Strictly speaking, the division into sections is intended to provide a
convenient organization for use by the linker (or, static binary analysis
tools). This means that not every section is actually needed when setting up a
process and virtual memory to execute the binary. Some sections contain data
that isn't needed for execution at all, such as symbolic or relocation
information. 

To load and execute a binary in a process, you need a different organization of
the code and data in the binary. For this reason, ELF executables specify
another logical organization, called segments, which are used at execution time
(as opposed to sections, which are used at link time). The section view of an
ELF binary, is meant for static linking purposes only.

###### Code-A listing of sections in the example binary
```bash
$ readelf --sections --wide not_striped_binary 
There are 29 section headers, starting at offset 0x3960:

Section Headers:
  [Nr] Name              Type            Address          Off    Size   ES Flg Lk Inf Al
  [ 0]                   NULL            0000000000000000 000000 000000 00      0   0  0
  [ 1] .interp           PROGBITS        00000000000002a8 0002a8 00001c 00   A  0   0  1
  [ 2] .note.gnu.build-id NOTE            00000000000002c4 0002c4 000024 00   A  0   0  4
  [ 3] .note.ABI-tag     NOTE            00000000000002e8 0002e8 000020 00   A  0   0  4
  [ 4] .gnu.hash         GNU_HASH        0000000000000308 000308 00001c 00   A  5   0  8
  [ 5] .dynsym           DYNSYM          0000000000000328 000328 0000a8 18   A  6   1  8
  [ 6] .dynstr           STRTAB          00000000000003d0 0003d0 000082 00   A  0   0  1
  [ 7] .gnu.version      VERSYM          0000000000000452 000452 00000e 02   A  5   0  2
  [ 8] .gnu.version_r    VERNEED         0000000000000460 000460 000020 00   A  6   1  8
  [ 9] .rela.dyn         RELA            0000000000000480 000480 0000c0 18   A  5   0  8
  [10] .rela.plt         RELA            0000000000000540 000540 000018 18  AI  5  22  8
  [11] .init             PROGBITS        0000000000001000 001000 00001b 00  AX  0   0  4
  [12] .plt              PROGBITS        0000000000001020 001020 000020 10  AX  0   0 16
  [13] .text             PROGBITS        0000000000001040 001040 000195 00  AX  0   0 16
  [14] .fini             PROGBITS        00000000000011d8 0011d8 00000d 00  AX  0   0  4
  [15] .rodata           PROGBITS        0000000000002000 002000 000012 00   A  0   0  4
  [16] .eh_frame_hdr     PROGBITS        0000000000002014 002014 000034 00   A  0   0  4
  [17] .eh_frame         PROGBITS        0000000000002048 002048 0000d8 00   A  0   0  8
  [18] .init_array       INIT_ARRAY      0000000000003de8 002de8 000008 08  WA  0   0  8
  [19] .fini_array       FINI_ARRAY      0000000000003df0 002df0 000008 08  WA  0   0  8
  [20] .dynamic          DYNAMIC         0000000000003df8 002df8 0001e0 10  WA  6   0  8
  [21] .got              PROGBITS        0000000000003fd8 002fd8 000028 08  WA  0   0  8
  [22] .got.plt          PROGBITS        0000000000004000 003000 000020 08  WA  0   0  8
  [23] .data             PROGBITS        0000000000004020 003020 000010 00  WA  0   0  8
  [24] .bss              NOBITS          0000000000004030 003030 000008 00  WA  0   0  1
  [25] .comment          PROGBITS        0000000000000000 003030 000020 01  MS  0   0  1
  [26] .symtab           SYMTAB          0000000000000000 003050 000600 18     27  45  8
  [27] .strtab           STRTAB          0000000000000000 003650 000209 00      0   0  1
  [28] .shstrtab         STRTAB          0000000000000000 003859 000103 00      0   0  1
Key to Flags:
  W (write), A (alloc), X (execute), M (merge), S (strings), I (info),
  L (link order), O (extra OS processing required), G (group), T (TLS),
  C (compressed), x (unknown), o (OS specific), E (exclude),
  l (large), p (processor specific)
```

For each section readelf shows the relevant basic information:
* **NR** is the index in the section header table.
* **Address** is the virtual address of the section.
* **Off** is the offset of section in bytes.
* **Size** is the size of the section in bytes.
* **ES** shows the size of each table entry for sections containing a table
  like symbol table and relocation tables.
* **LK** is the linked section index.

As you can see, the output conforms closely to the structure of a section
header. The first entry in the section header table of every ELF file is
defined by the ELF standard to be a `NULL` entry. The type of the entry is
`SHT_NULL`, and all fields in the section header are zeroed out. This means it
has no name and no associated bytes.

## .init and .fini
The `.init` section contains executable code that performs initialization tasks
and needs to run before any other code in the binary is executed. It contains
executable code by the `SHF_EXECINSTR` flag, denoted as an `x` by readelf (in
the **Flag** column).

The `.init` section executes before transferring control to the main entry
point of the binary. From the object-oriented programming view, this of this
section  as a constructor. 

The `.fini` section is analogous to the `.init` section, except that it runs
after the main program complete, essentially functioning as a kind of
destructor.

## .text
The `.text` section is where the main code of the program resides. The `.text`
section has type `SHT_PROGBITS` because it contains user-defined code. Also
note the section flags, which indicate that the section is executable but not
writeable. 

Besides the application-specific code compiled from the program's source, the
`.text` section of a typical binary compiled by `gcc` contains a number of
standard functions that perform initialization and finalization tasks, such as
`_start` (**Most important function of these standard functions**), 
`register _tm_clones`, and `frame_dummy`. 

###### Code-Disassembel Machine Instruction
```bash
$ objdump -M intel -d a.out
```

In C program, there's always a `main` function where your program begins. The
starting point of a binary is not the `main` function, but it is `_start`
section. 

So, how does execution eventually reach `main`? `_start` contains an
instruction that moves the address of `main` into the `rdi` register, which is
one of the registers used to pass parameters for function calls on the *x64*
platform. Then, `_start` calls a function called `__libc_start_main`. It
resides in the `.plt` section, which means the function is part of a shared
library.

## .bss, .data, and .rodata
Because code sections are generally not writeable, variables are kept in one or
more dedicated sections, which are writable. Cconstant data is usually also
kept in its own setion to keep the binary neatly organized, though compilers do
sometimes output constant data in code sections (gcc and clang generally don't
mix code and data, but Visual Studio sometimes does). 

The `.rodata`, *Read Only Data* section, is dedicated to storing constant
values and it's not writable. 

The default values of initialized varables are stored in the `.data` section,
and it's writable.

The `.bss` section reserves space for uninitialized variables. The name
historically stands for **Block started by symbol** referring to the reserving
of blocks of memory for *symbolic* variables.

Unlike `.rodata` and `.data`, which have type `SHT_PROGBITS`, the `.bss`
section has type `SHT_NOBITS`. This is because `.bss` doesn't occupy any bytes
in the binary as it exists on disk - it's simply a directive to allocate a
properly sized block of memory for uninitialized variables when setting up an
execution environment for the binary. Typically, variables that live in `.bss`
are zero initialized, and the section is marked as writable.

## .plt, .got, and .got.plt
When a binary is loaded into a process for execution, the dynamic linker
performs last-minute relocations. For instance, it resolves references to
functions located in shared libraries, where the load address is not yet known
at compile time. Many of this relocations are typically not done right away
when the binary is loaded but are deferred until the first reference to the
unresolved location is actually made. This is known as *Lazy binding*.

Lazy binding ensures that the dynamic linker never needlessly wastes time on
relocations; it only performs those relocations that are truly needed at
runtime. On Linux, lazy binding is the default behavior of the dynamic linker.
It's possible to force the linker to perform all relocations right away by
exporting an environment variable called `LD_BIND_NOW`.

Lazy binding in Linux ELF binaries is implemented with the help of two special
sections, called the **Procedure Linkage Table** `.plt` and the 
**Global Offset Table** `.got`. Though the following discussion focuses on lazy
binding, the GOT is actually used for more than just that. ELF binaries often
contain a separate GOT section called `.got.plt` for use in conjunction with
`.plt` in the lazy binding process. The `.got.plt` setion is analogous to the
regular `.got`.

`.plt` is a code section that contains executable code, just like `.text`,
while `.got.plt` is a data section. The PLT consists entirely of stubs of a
well-defined format, dedicated to directing calls from the `.text` section to
the appropriate library location. 

###### Code-Explore .plt Section
```bash
$ objdump -M intel --section .plt -d a.out
```

The format of the PLT is as follows: First, there is a default stub. After that
comes a series of function stubs, one per library function, all following the
same pattern. Also note that for each consecutive function stub, the value
pushed onto the stack is incremented. 

### Dynamically Resolving a Library Function Using The PLT
Let's say you want to call the `puts` function, which is part of the well-known
`libc` library. Instead of calling it directly, You can make a call to the
corresponding **PLT** stub, `puts@plt` ([Step 1](#Figure-Calling-a-shared-library-function-via-the-PLT)).

The PLT stub begins with an indirect jump instruction, which jumps to an
address stored in the `.got.plt` section ([Step 2](#Figure-Calling-a-shared-library-function-via-the-PLT)).

Initially, before the lazy binding has happened, this address is simply the
address of the next instruction in the function stub, which is a `push`
instruction. Thus, the indirect jump simply transfers control to the
instruction directly after it ([Step 3](#Figure-Calling-a-shared-library-function-via-the-PLT)).

The `push` instruction pushes an integer (in this case, `0x0`) onto the stack.
As mentioned, this integer serves as an identifier for the PLT stub in
question. Subsequently, the next instruction jumps to the common default stub
shared among all PLT function stubs ([Step 4](#Figure-Calling-a-shared-library-function-via-the-PLT)).

The default stub pushes another identifier (taken from the GOT), identifying
the executable itself, and then jumps (indirectly, again through the GOT) to
the dynamic linker ([Step 5](#Figure-Calling-a-shared-library-function-via-the-PLT)).

Using the identifiers pushed by the PLT stubs, the dynamic linker figures out
that it should resolve the address of `puts` and should do so on behalf of the
main executable loaded into the process. This last bit is important because
there may be multiple libraries loaded in the same process as well, each with
their own PLT and GOT. The dynamic linker then looks up the address at which
the `puts` function is located and plugs the address of that function into the
GOT entry associated with `puts@plt` . Thus, the GOT entry no longer points back
into the PLT stub, as it did initially, but now points to the actual address of
puts . At this point, the lazy binding process is complete.

Finally, the dynamic linker satisfies the original intention of calling puts
by transferring control to it. For any subsequent calls to `puts@plt` , the GOT
entry already contains the appropriate (patched) address of puts so that the
jump at the start of the PLT stub goes directly to puts without involving the
dynamic linker ([Step 6](#Figure-Calling-a-shared-library-function-via-the-PLT)).

###### Figure-Calling a shared library function via the PLT
```ascii

                         Code                    ^                                   ^
         +----------------------------------+    |                                   |
         | .plt                             |    |5                                  |6
         |----------------------------------|    |                   Data            |
      +->| <default stub>:                  |    |          +-------------------+    |
      |  |   push QWORD PTR [rip+0x200c12]  |----+          |                   |    |
     4|  |   jmp  QWORD PTR [rip+ox200c14]  |               | .got.plt          |    |
      |  |                                  |               |-------------------|    |
   +--|->| <puts@plt>:                      |      2        | .got.plt[n]:      |    |
   |  |  |   jmp  QWORD PTR [rip+0x200c12]  |-------------->|   <addr>          |----+
   |  +--|   push 0x0                       |<-------+      |-------------------|    |
   |     |   jmp  <default stub>            |        |      |                   |    |
   |     |----------------------------------|        |      |                   |    |
   |     |                                  |        |      |                   |    |
   |     |                                  |        |      +-------------------+    |
  1|     |                                  |        |                               |
   |     |                                  |        +-------------------------------+
   |     |                                  |                         3
   |     | .txt                             |
   |     |----------------------------------|
   |     | <main>:                          |
   |     |   ...                            |
   +-----|   call puts@plt                  |
         |----------------------------------|
         |                                  |
         |                                  |
         |                                  |
         +----------------------------------+
```

## .rel.\* and .rela.\*
As you can see in the readelf dump of the example binary’s section headers,
there are several sections with names of the form `rela.*` . These sections are
of type `SHT_RELA`, meaning that they contain information used by the linker
for performing relocations. Essentially, each section of type `SHT_RELA` is a
table of relocation entries, with each entry detailing a particular address
where a relocation needs to be applied, as well as instructions on how to
resolve the particular value that needs to be plugged in at this address.

###### Code-The relocation sections in the example binary
```bash
$ readelf --relocs a.out
```

There are two important and common relocations type:
* **R_X86_64_GLOB_DAT**:

  They have their offset in the `.got` section. Generally, this type of
  relocation is used to compute the address of a data symbol and plug it into
  the correct offset in `.got`.

* **R_X86_64_JUMP_SLO**: 

  Called jump slots. They have their offset in the `.got.plt` section and
  represent slots where the addresses of library functions can be plugged in.

What all relocation types have in common is that they specify an offset at
which to apply the relocation. The details of how to compute the value to plug
in at that offset differ per relocation type and are sometimes rather involved. 

## .dynamic
The `.dynamic` section functions as a *road map* for the operating system and
dynamic linker when loading and setting up an ELF binary for execution.

The `.dynamic` section contains a table of [Elf64_Dyn](#Code-Struct-Elf64_Dyn)
structures, also referred to as tags. There are different types of tags, each
of which comes with an associated value.

Tags type:
* **DT_NEEDED**

  Inform the Dyanic linker about dependencies of the executable. For instance,
  the binary uses the `puts` function from the `libc.so.6` shared library, so
  it needs to be loaded when executing the binary.

* **DT_VERNEED**
  
  Specify the starting address of the version dependency table.
  
* **DT_VERNEEDNUM**

  Specify the number of entries of the version dependency table.

###### Code-Contents of the .dynamic section
```bash
$ readelf --dynamic a.out

Dynamic section at offset 0x2df8 contains 26 entries:
  Tag        Type                         Name/Value
 0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
 0x000000000000000c (INIT)               0x1000
 0x000000000000000d (FINI)               0x11d8
 0x0000000000000019 (INIT_ARRAY)         0x3de8
 0x000000000000001b (INIT_ARRAYSZ)       8 (bytes)
 0x000000000000001a (FINI_ARRAY)         0x3df0
 0x000000000000001c (FINI_ARRAYSZ)       8 (bytes)
 0x000000006ffffef5 (GNU_HASH)           0x308
 0x0000000000000005 (STRTAB)             0x3d0
 0x0000000000000006 (SYMTAB)             0x328
 0x000000000000000a (STRSZ)              130 (bytes)
 0x000000000000000b (SYMENT)             24 (bytes)
 0x0000000000000015 (DEBUG)              0x0
 0x0000000000000003 (PLTGOT)             0x4000
 0x0000000000000002 (PLTRELSZ)           24 (bytes)
 0x0000000000000014 (PLTREL)             RELA
 0x0000000000000017 (JMPREL)             0x540
 0x0000000000000007 (RELA)               0x480
 0x0000000000000008 (RELASZ)             192 (bytes)
 0x0000000000000009 (RELAENT)            24 (bytes)
 0x000000006ffffffb (FLAGS_1)            Flags: PIE
 0x000000006ffffffe (VERNEED)            0x460
 0x000000006fffffff (VERNEEDNUM)         1
 0x000000006ffffff0 (VERSYM)             0x452
 0x000000006ffffff9 (RELACOUNT)          3
 0x0000000000000000 (NULL)               0x0
```

The `.dynamic` section also contains pointers to other important information
required by the dynamic linker (for instance, the *dynamic string table*, 
*dynamic symbol table*, `.got.plt` section, and *dynamic relocation section* 
pointed to by tags of type `DT_STRTAB`, `DT_SYMTAB`, `DT_PLTGOT`, and
`DT_RELA`, respectively).

## .init_array and .fini_array
The `.init_array` section contains an array of pointers to functions to use as
constructors. Each of these functions is called in turn when the binary is
initialized, before main is called. While the aforementioned `.init` section
contains a single startup function that performs some crucial initialization
needed to start the executable, `.init_array` is a data section that can contain
as many function pointers as you want, including pointers to your own custom
constructors. In gcc , you can mark functions in your **C** source files as
constructors by decorating them with `__attribute__((constructor))`.

###### Code-.init_array section
```bash
$ objdump -d --section .init_array a.out

not_striped:     file format elf64-x86-64


Disassembly of section .init_array:

0000000000003de8 <__frame_dummy_init_array_entry>:
    3de8:       30 11 00 00 00 00 00 00                             0.......

$ objdump -d not_striped | grep "<frame_dummy>"
0000000000001130 <frame_dummy>:
```

The `.fini_array` contains pointers to destructors rather than constructors.

**Note**: Binaries produced by older `gcc` versions may contain sections called
`.ctors` and `.dtors` instead of `.init_array` and `.fini_array`.

## .shstrtab, .symtab, .strtab, .dynsym, and .dynstr
The `.shstrtab` section is simply an array of `NULL`-terminated strings that
contain the names of all the sections in the binary. It's indexed by the
section headers to allow tools like `readelf` to find out the names of the
sections.

The `.symtab` section contains a symbol table, which is a table of `Elf64_Sym`
structures, each of which associates a symbolic name with a piece of code or
data elsewhere in the binary, such as a function or variable. The actual
strings containing the symbolic names are located in the `.strtab` section.
These strings are pointed to by the `Elf64_Sym` structures.

The `.dynsym` and `.dynstr` sections are analogous to `.symtab` and `.strtab`,
except that they contain symbols and strings needed for dynamic linking rather
than static linking. Because the information in these sections is needed during
dynami linking , they cannot be stripped.

# Section Headers
Every section is described by a section header, which denotes the properties of
the section and allows you to locate the bytes belonging to the section.
Because sections are intended to provide a view for the linker only, the
section header table is an optional part of the ELF format. ELF files that
don't need linking aren't required to have a section header table. If no
section header table is present, [e_shoff](#E_SHOFF) field in the executable
header is set to zero.

###### Code-Section Header Structure
```C
typedef struct {
        uint32_t sh_name;       /* Section name (string tbl index) */
        uint32_t sh_type;       /* Section type */
        uint64_t sh_flags;      /* Section flags */
        uint64_t sh_addr;       /* Section virtual addr at execution */
        uint64_t sh_offset;     /* Section file offset */
        uint64_t sh_size;       /* Section size in bytes */
        uint32_t sh_link;       /* Link to another section */
        uint32_t sh_info;       /* Additional section information */
        uint64_t sh_addralign;  /* Section alignment */
        uint64_t sh_entsize;    /* Entry size of section holds table */
} ELF64_Shdr;
```

## SH_NAME
If `sh_name` is set, it contains an index into the section header string table
(`.shstrtab`). If the index is Zero, it means the section doesn't have a name. 

[.shstrtab](#Code-Example-of-Section-Header-String-Table) contains an array of
NULL-terminated strings, One for every section name. The index of the section
header string table (`.shstrtab`) is given in the `e_shstrndx` field of the
[executable header](#E_SHSTRNDX).

In other word, Every ELF header structure contains a member called
`e_shstrndx`. This is an index to the `.shstrtab`. By using this index we can
achieve `.shstrtab`, By reading the `.shstrtab` we can find the name of the
sections.

There is another string table `.strtab` that defines all other references.

## SH_TYPE
Every Section has a type, indicated by an integer field called `sh_type`, that
tells the linker something about the structure of a section's contents.

###### Code-sh_type
```C
/* Legal values for sh_type (section type).  */

#define SHT_NULL          0             /* Section header table entry unused */
#define SHT_PROGBITS      1             /* Program data */
#define SHT_SYMTAB        2             /* Symbol table */
#define SHT_STRTAB        3             /* String table */
#define SHT_RELA          4             /* Relocation entries with addends */
#define SHT_HASH          5             /* Symbol hash table */
#define SHT_DYNAMIC       6             /* Dynamic linking information */
#define SHT_NOTE          7             /* Notes */
#define SHT_NOBITS        8             /* Program space with no data (bss) */
#define SHT_REL           9             /* Relocation entries, no addends */
#define SHT_SHLIB         10            /* Reserved */
#define SHT_DYNSYM        11            /* Dynamic linker symbol table */
#define SHT_INIT_ARRAY    14            /* Array of constructors */
#define SHT_FINI_ARRAY    15            /* Array of destructors */
#define SHT_PREINIT_ARRAY 16            /* Array of pre-constructors */
#define SHT_GROUP         17            /* Section group */
#define SHT_SYMTAB_SHNDX  18            /* Extended section indeces */
#define SHT_NUM           19            /* Number of defined types.  */
#define SHT_LOOS          0x60000000    /* Start OS-specific.  */
#define SHT_GNU_ATTRIBUTES 0x6ffffff5   /* Object attributes.  */
#define SHT_GNU_HASH      0x6ffffff6    /* GNU-style hash table.  */
#define SHT_GNU_LIBLIST   0x6ffffff7    /* Prelink library list */
#define SHT_CHECKSUM      0x6ffffff8    /* Checksum for DSO content.  */
#define SHT_LOSUNW        0x6ffffffa    /* Sun-specific low bound.  */
#define SHT_SUNW_move     0x6ffffffa
#define SHT_SUNW_COMDAT   0x6ffffffb
#define SHT_SUNW_syminfo  0x6ffffffc
#define SHT_GNU_verdef    0x6ffffffd    /* Version definition section.  */
#define SHT_GNU_verneed   0x6ffffffe    /* Version needs section.  */
#define SHT_GNU_versym    0x6fffffff    /* Version symbol table.  */
#define SHT_HISUNW        0x6fffffff    /* Sun-specific high bound.  */
#define SHT_HIOS          0x6fffffff    /* End OS-specific type */
#define SHT_LOPROC        0x70000000    /* Start of processor-specific */
#define SHT_HIPROC        0x7fffffff    /* End of processor-specific */
#define SHT_LOUSER        0x80000000    /* Start of application-specific */
#define SHT_HIUSER        0x8fffffff    /* End of application-specific */
```

Sections with type `SHT_PROGBITS` contain program data, such as machine
instructions or constants. These sections have no particular structure for the
linker to parse.

Section with type `SHT_REL` or `SHT_RELA` are particularly important for the
linker because they contain relocation entries in a well-defined format `struct
ELF64_Rel` and `struct ELF64_Rela`, which the linker can parse to perform the
necessary relocations in other sections.

###### Code-Struct Elf64_Rel
```C
typedef struct {
	Elf64_Addr	r_offset;
	Elf64_Xword	r_info;
} Elf64_Rel;
```

###### Code-Struct Elf64_Rela
```C
typedef struct {
	Elf64_Addr	r_offset;
	Elf64_Xword	r_info;
	Elf64_Sxword	r_addend;
} Elf64_Rela;
```

Each relocation entry tells the linker about a particular location in the
binary where a relocation is needed and which symbol the relocation should be
resolved to. One important note is, the `SHT_REL` and `SHT_RELA` sections are
used for static linking purposes.

Sections of type `SHT_DYNAMIC` contain information needed for dynamic linking.
This information is formatted using `struct Elf64_Dyn`.

###### Code-Struct Elf64_Dyn
```C
typedef struct                                                   
{                                                                
  Elf64_Sxword  d_tag;                  /* Dynamic entry type */ 
  union                                                          
    {                                                            
      Elf64_Xword d_val;                /* Integer value */      
      Elf64_Addr d_ptr;                 /* Address value */      
    } d_un;                                                      
} Elf64_Dyn; 
```

## SH_FLAGS
`sh_flags` field describe additional information about a section. 

###### Code-SH_FLAGS
```C
/* Legal values for sh_flags (section flags).  */

#define SHF_WRITE            (1 << 0)   /* Writable */
#define SHF_ALLOC            (1 << 1)   /* Occupies memory during execution */
#define SHF_EXECINSTR        (1 << 2)   /* Executable */
#define SHF_MERGE            (1 << 4)   /* Might be merged */
#define SHF_STRINGS          (1 << 5)   /* Contains nul-terminated strings */
#define SHF_INFO_LINK        (1 << 6)   /* `sh_info' contains SHT index */
#define SHF_LINK_ORDER       (1 << 7)   /* Preserve order after combining */
#define SHF_OS_NONCONFORMING (1 << 8)   /* Non-standard OS specific handling
                                           required */
#define SHF_GROUP            (1 << 9)   /* Section is member of a group.  */
#define SHF_TLS              (1 << 10)  /* Section hold thread-local data.  */
#define SHF_COMPRESSED       (1 << 11)  /* Section with compressed data. */
#define SHF_MASKOS           0x0ff00000 /* OS-specific.  */
#define SHF_MASKPROC         0xf0000000 /* Processor-specific */
#define SHF_ORDERED          (1 << 30)  /* Special ordering requirement
                                           (Solaris).  */
#define SHF_EXCLUDE          (1U << 31) /* Section is excluded unless
                                           referenced or allocated (Solaris).*/
```

The `SHF_WRITE` flags indicates that the section is writable at runtime. This
makes it easy to distinguish between sections that contain static data (such as
constants) and those that contain variables. 

The `SHF_ALLOC` flag indicates that the contents of the section are to be
loaded into virtual memory when executing the binary (though the actual loading
happens using the segment view of the binary, not the section view).

The `SHF_EXECINSTR` tells you that the section contains executable
instructions, which is useful to know when disassembling a binary.

## SH_ADDR and SH_OFFSET and SH_SIZE 
The `sh_addr`, `sh_offset` and `sh_size` fields describe the virtual address,
file offset, and size of the section, respectively. 

Sections are used only for linking, not for creating and executing a process.
But, the linker sometimes needs to know at which addresses particular pieces of
code and data will end up at runtime to do relocations. The `sh_addr` field
provides this information. Sections that aren't intended to be loaded into
virtual memory when setting up the process have an `sh_addr` value of zero.

## SH_LINK
Sometimes there are relationships between sections that the linker needs to
know about. For instance, an `SHT_SYMTAB`, `SHT_DYNSYM`, or `SHT_DYNAMIC` has
an associated string table section, which contains the symbolic names for the
symbols in question. Similarly, relocation sections (type `SHT_REL` or
`SHT_RELA`) are associated with a symbol table describing the symbols involved
in the relocations. The `sh_link` field makes these relationships explicit by
denoting the index (in the section header table) of the related section.

## SH_INFO
The `sh_info` field contains additional information about the section. The
meaning of the additional information varies depending on the section type.

## SH_ADDRALIGN
Some sections may need to be aligned in memory in a particular way for
efficiency of memory accesses. For example, a section may need to be loaded at
some address that is a multiple of 8 bytes or 16 bytes. These alignment
requirements are specified in the `sh_addralign` field. For instance, if this
field is set to 16, it means the base address of the section (as chosen by the
linker) must be some multiple of 16. The values 0 and 1 are reserved to
indicate no special alignment needs.

## SH_ENTSIZE
Some sections, such as symbol tables or relocation tables, contain a table of
well-defined data structures (such as `Elf64_Sym` or `Elf64_Rela`). For such
sections, the sh_entsize field indicates the size in bytes of each entry in the
table. When the field is unused, it is set to zero.
