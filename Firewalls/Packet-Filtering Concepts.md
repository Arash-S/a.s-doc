# Packet-Filtering Concepts

## Table of Contents
1. [Introduction](#Introduction)
1. [A Packet-Filtering Firewall](#A-Packet-Filtering-Firewall)
1. [Choosing a Default Packet-Filtering Policy](#Choosing-a-Default-Packet-Filtering-Policy)

## List of Figure
1. [Input and Output flowchart](#LF_IOFLOWCHART)

## Introduction
What is a firewall?<br />
A firewall is a device or group of devices that enforces an access control
policy between networks (RFC 2647).This definition is very broad.

A firewall can encompass many layers of the OSI model.

A nonstateful, or stateless, firewall usually performs some packet
filtering based solely on the IP layer (Layer 3) of the OSI model, though
somethimes higher-layer protocols are involved in this type of firewall.

A stateful firewall is one that keeps track of the packets previously seen
within a given session and applies the access policy to packets based on
what has already been seen for the given connection A stateful firewall
implies the basic packet-filtering capabilities of a stateless firewall as
well. A stateful firewall will, for example, keep track of the stages of
the TCP three-way handshake and reject packets that appear out of sequence
for that handshake. Being connectionless, UDP is somewhat trickier to
handle for a stateful firewall because there's no state to speak of.
However, a stateful firewall tracks recent UDP exchanges to ensure that a
packet that has been received relates to a recent outgoing packet.

An **Application-Level Gateway** (**ALG**) or **Application-Layer Gateway**
is yet another form of firewall. Unlike the stateless firewall, which has
knowledge of the Network and possibly Transport layers, an ALG primarily
handles Layer 7 of the OSI model and have deep knowledge of the application
data being passed being passed and can thus look for any deviation from the
normal traffic for the application in question.

An ALG will typically reside between the client and the real server and
will, for all intents and purposes, mimic the behavior of the real server
to the client. In effect, local traffic never leaves the LAN, and remote
traffic never enters the LAN.

ALG sometimes also refers to a module, or piece of software that assists
another firewall. Many firewalls come with and FTP ALG to support FTP's
port mode data channel, where the client tells the server what local port
to connect to so that it can open the data channel. The server initiates
the incoming data channel connection (whereas, usually, the client
initiates all connections). ALGs are frequently required to pass multimedia
protocols through a firewall because multimedia sessions often use multiple
connections initiated from both ends and generally use TCP and UDP
together.

ALG is a proxy. Another form of proxy is a circuit-level proxy.
Circuit-level proxies don't usually have appplication-specific knowledge,
but they can enforce access and authorization policies, and they serve as
termination points in what would otherwise be an end-to-end connection.
SOCKS is an example of a circuit-level proxy. The proxy server acts as a
termination point for both sides of the connection, but the server doesn't
have any application-specific knowledge.

In each of these cases, the firewall's purpose is to enforce the access
control or security policies that you define. Security policies are
essentially about access control--who is and is not allowed to perform
which actions on the servers and networks under your control.

Though not necessarily specific to a firewall, firewalls many times find
themselves performing additional tasks, some of which might include Network
Address Translation (NAT), antivirus checking, event notification, URL
filtering, user authentication, and Network-layer encryption.

## A Packet-Filtering Firewall
At its most basic level, a packet-filtering firewall consists of a list of
acceptance and denial rules. These rules explicitly define which packets
will and will not be allowed through the network interface. The firewall
rules use the packet header fields to decide whether to forward a packet to
its destination, to silently throw away the packet, or to block the packet
and return an error condition to the sending machine. These rules can be
based on a wide array of factors, including the source or destination IP
addresses, the source and destination ports, and portions of invidiual
packets such as the TCP header flags, the types of protocol, the MAC
address, and more.

For a single-machine setup, it meight be helpful to think of the network
interface as an I/O pair. The firewall independently filters what comes in
and what goes out through the interface.The input filtering and the output
filtering can, and likely do, have completely different rules.

<a name="LF_IOFLOWCHART"></a>
```ASCII

             +-------------------+
             | Network Interface |
             +-------------------+
             |                  ^
             |                  |
             v                  |
   +-----------------+ +-----------------+
   | Incoming packet | | Match Rule 3 ?  |
   +-----------------+ +-----------------+
             |                  ^
             |                  |
             v                  |
   +-----------------+       +----+
   | Input chain     |       | No |
   +-----------------+       +----+
             |                  ^
             |                  |
             v                  |
   +-----------------+ +-----------------+
   | Match Rule 1 ?  | | Match Rule 2 ?  |
   +-----------------+ +-----------------+
             |                  ^
             |                  |
             v                  |
           +----+            +----+
           | No |            | No |
           +----+            +----+
             |                  ^         
             |                  |         
             v                  |         
   +-----------------+ +-----------------+
   | Match Rule 2 ?  | | Match Rule 1 ?  |
   +-----------------+ +-----------------+
             |                  ^
             |                  |
             v                  |
           +----+      +-----------------+
           | No |      | Outgoing chain  |
           +----+      +-----------------+
             |                  ^
             |                  |
             v                  |
   +-----------------+ +-----------------+
   | Match Rule 3 ?  | | Outgoing packet |
   +-----------------+ +-----------------+
             |
             |
             v
```
**Figure**: Input and Output flowchart.

This sound pretty powerful, and it is; but it isn't a surefire security
mechannism. It's only part of the story, just one layer in the multilayered
approach to data security. Not all application communication protocols lend
themselves to packet filtering. This type of filtering is too low-level to
allow fine-grained authentication and access control. These security
service must be furnished at higher levels. The only identifying
information available at this level is the source address in the IP packet
header. The source address can be modified with little difficulty. One
level up, neither the Network layer nor the Transport layer can verify that
the application data is correct. Nevertheless, the packet level allows
greter, simpler control over direct port access, packet contents, and
correct communication protocols than can easily or conveniently be done at
higher levels.

Without packet-level filtering, higher-level filtering and proxy security
measures are either crippled or potentially ineffective. To some extent,,
at least, they must rely on the correctness of the underlying communication
protocol. Each layer in the security protocol stack adds another piece that
other layers can't easily provide.

## Choosing a Default Packet-Filtering Policy
There are two basic approaches to a default firewall policy:
* Deny everything by default, and explicitly allow selected packets
  through. (Recommended approach)
* Accept everything by default, and explicitly deny selected packets from
  passing through.


