# Overview of Cryptography

## Table of Contents
1. [Information Security and Cryptography](#Information-Security-and-Cryptography)
1. [Background on Function](#Background-on-Function)
1. [Basic Terminology and Concepts](#Basic-Terminology-and-Concepts)
    1. [Encryption Domains and Codomains](#Encryption-Domains-and-Codomains)
    1. [Encryption and Decryption Transformations](#Encryption-and-Decryption-Transformations)
    1. [Achieving Confidentiality](#Achieving-Confidentiality)
    1. [Security](#Security)
1. [Symmetric-Key Encryption](#Symmetric-Key-Encryption)
    1. [Definition of Block Cipher](#Definition-of-Block-Cipher)
        1. [Substitution Ciphers](#Substitution-Ciphers)
            1. [Simple Substitution Ciphers](#Simple-Substitution-Ciphers)
            1. [Homophonic Substitution Ciphers](#Homophonic-Substitution-Ciphers)
            1. [Polyalphabetic Substitution Ciphers](#Polyalphabetic-Substitution-Ciphers)
        1. [Transposition Ciphers](#Transposition-Ciphers)
        1. [Product Ciphers](#Product-Ciphers)
    1. [Definition of Stream Cipher](#Definition-of-Stream-Cipher)
        1. [Vernam Cipher](#Vernam-Cipher)
1. [Digital Signatures](#Digital-Signatures)
    1. [Signing Procedure](#Signing-Procedure)
    1. [Verfication Procedure](#Verfication-Procedure)
1. [Authentication and Identification](#Authentication-and-Identification)
    1. [Identification](#Identification)
    1. [Data Origin Authentication](#Data-Origin-Authentication)
1. [Public-Key Cryptography](#Public-Key-Cryptography)
    1. [Public-Key Encryption](#Public-Key-Encryption)

## List of Figures
1. [Figure-A taxonomy of Cryptographic Primitives](#Figure-A-taxonomy-of-Cryptographic-Primitives)

## Information Security and Cryptography
To introduce cryptography, an understanding of issues related to **information
security** in general is necessary. Information security manifests itself in
many ways according to the situation and requirement. Regardless of who is
involved, to one degree or another, all parties to a transaction must have
confidence that certain objectives associated with information security have
been met.

Information Security Objectives:
* Privacy or Confidentiality
* Data integrity
* Entity Authentication or Identification
* Message Authentication
* Signature
* Authorization
* Validation
* Access Control
* Certification
* Timestamping
* Witnessing
* Receipt
* Confirmation
* Ownership
* Anonymity
* Non-Repudiation
* Revocation

One of the fundamental tools used in information security is the signature. It
is a building block for many other services such as non-repudiation, data
origin authentication, iden- tification, and witnessing, to mention a few.

With electronic information the concept of a signature needs to be redressed.
Achieving information security in an electronic society requires a vast array
of technical and legal skills. There is, however, no guarantee that all of the
information security objectives deemed necessary can be adequately met.  The
technical means is provided through cryptography.

**Cryptography** is the study of mathematical techniques related to aspects of
information security such as confidentiality, data integrity, entity
authentication, and data origin authentication.

**Note**: Cryptography is not the only means of providing information security,
but rather one set of techniques. 

Of all the information security objectives listed in above, the following four
form a framework upon which the other will be derived:
* **Confidentiality**: is a service used to keep the content of information
  from all but those authorized to have it. Secrecy is a term synonymous with
  confidentiality and privacy.  There are numerous approaches to providing
  confidentiality, ranging from physical protection to mathematical algorithms
  which render data unintelligible.
* **Data integrity**: Data integrity is a service which addresses the
  unauthorized alteration of data. To assure data integrity, one must have the
  ability to detect data manipulation by unauthorized parties. Data
  manipulation includes such things as insertion, deletion, and substitution.
* **Authentication**: Authentication is a service related to identification.
  This function applies to both entities and information itself. Two parties
  entering into a communication should identify each other. Information
  delivered over a channel should be authenticated as to origin, date of
  origin, data content, time sent, etc. For these reasons this aspect of
  cryptography is usually subdivided into two major classes:
  * Entity Authentication
  * Data Origin Authentication: Implicitly provides data integrity.
* **Non-repudiation**: Non-repudiation is a service which prevents an entity
  from denying previous commitments or actions. When disputes arise due to an
  entity denying that certain actions were taken, a means to resolve the
  situation is necessary. For example, one entity may authorize the purchase of
  property by another entity and later deny such authorization was granted. A
  procedure involving a trusted third party is needed to resolve the dispute.


```ASCII
                                                  /----> Aribrary length hash function                        
                      /----> Unkeyed Primitives --|----> One-Way Permutations                                 
                      |                           \----> Random sequences                                     
                      |                                                                                       
                      |                                                                /----> Block Ciphers   
                      |                                 /----> Symmetric-Key Ciphers --|                      
                      |                                 |                              \----> Stream Ciphers  
                      |                                 |----> Arbitrary length hash functions (MACs)         
Security Primitives --|----> Symmetric-Key Primitives --|----> Signatures                                     
                      |                                 |----> Pseudorandom sequences                         
                      |                                 \----> Identification Primitives                      
                      |                                                                                       
                      |                              /----> Public-Key ciphers                                
                      \----> Public-Key Primitives --|----> Signatures                                        
                                                     \----> Identification Primitives                         
```
##### Figure-A taxonomy of Cryptographic Primitives

Cryptographic tools (Primitives) should be evaluted with respect to various
criteria such as:
* **Level of Security**: This is usually difficult to quantify. Often it is
  given in terms of the number of operations required (using the best methods
  currently known) to defeat the intended objective. Typically the level of
  security is defined by an upper bound on the amount of work necessary to
  defeat the objective. This is sometimes called the work factor
* **Functionality**: Primitives will need to be combined to meet various
  information security objectives. Which primitives are most effective for a
  given objective will be determined by the basic properties of the primitives.
* **Methods of Operation**: Primitives, when applied in various ways and with
  various inputs, will typically exhibit different characteristics; thus, one
  primitive could provide very different functionality depending on its mode of
  operation or usage.
* **Performance**: This refers to the efficiency of a primitive in a particular
  mode of operation. (For example, an encryption algorithm may be rated by
  the number of bits per second which it can encrypt.)
* **Ease of implementation**: This refers to the difficulty of realizing the
  primitive in a practical instantiation. This might include the complexity of
  implementing the primitive in either a software or hardware environment.

## Background on Function
A set consists of distinct objects which are called elements of the set. For
example, a set X might consist of the elements a, b, c, and this is denoted X =
{a, b, c}.

**Definition**: A function is defined by two sets X and Y and a rule f which
assigns to each element in X precisely one element in Y . The set X is called
the domain of the function and Y the codomain. If x is an element of X (usually
written x ∈ X) the image of x is the element in Y which the rule f associates
with x; the image y of x is denoted by y = f (x).

Standard notation for a function f from set X to set Y is f : X -→ Y .

If y ∈ Y , then a preimage of y is an element x ∈ X for which f (x) = y. The
set of all elements in Y which have at least one preimage is called the image
of f , denoted Im(f ).

Thinking of a function in terms of the schematic (sometimes called a functional
diagram), each element in the domain X has precisely one arrowed line
originating from it. Each element in the codomain Y can have any number of
arrowed lines incident to it (including zero lines).

**Definition** of 1-1 Function: A function (or transformation) is 1 − 1
(one-to-one) if each element in the codomain Y is the image of **at most** one
element in the domain X.

**Definition** of onto Function: A function (or transformation) is onto if each
element in the codomain Y is the image of **at least** one element in the
domain.  Equivalently, a function f : X -→ Y is onto if Im(f ) = Y .

**Definition** of Bijection Function: If a function f : X −→ Y is 1−1 and Im(f
) = Y , then f is called a bijection.

**Fact**: If f : X −→ Y is 1 − 1 then f : X −→ Im(f ) is a bijection. In
particular, if f : X −→ Y is 1 − 1, and X and Y are finite sets of the same
size, then f is a bijection.

In terms of the schematic representation, if f is a bijection, then each
element in Y has exactly one arrowed line incident with it.

**Definition**: If f is a bijection from X to Y then it is a simple matter to
define a bijection g from Y to X as follows: for each y ∈ Y define g(y) = x
where x ∈ X and f (x) = y. This function g obtained from f is called the
inverse function of f and is denoted by g = f^−1.

**Note** that if f is a bijection, then so is f^−1 . In cryptography bijections
are used as the tool for encrypting messages and the inverse transformations
are used to decrypt. Notice that if the transformations were not bijections
then it would not be possible to always decrypt to a unique message.

**Definition** of One-Way Functions: A function f from a set X to a set Y is
called a one-way function if f (x) is “easy” to compute for all x ∈ X but for
“essentially all” elements y ∈ Im(f ) it is “computationally infeasible” to
find any x ∈ X such that f (x) = y.

**Note**: A rigorous definition of the terms “easy” and “computationally
infeasible” is necessary but would detract from the simple idea that is being
conveyed. For the purpose of this chapter, the intuitive meaning will suffice.

**Note**: The phrase “for essentially all elements in Y ” refers to the fact
that there are a few values y ∈ Y for which it is easy to find an x ∈ X such
that y = f (x). For example, one may compute y = f (x) for a small number of x
values and then for these, the inverse is known by table look-up. An alternate
way to describe this property of a one-way function is the following: for a
random y ∈ Im(f ) it is computationally infeasible to find any x ∈ X such that
f (x) = y.

**Definition** of Trapdoor one-way functions: A trapdoor one-way function is a
one-way function f : X −→ Y with the additional property that given some extra
information (called the trapdoor information) it becomes feasible to find for
any given y ∈ Im(f ), an x ∈ X such that f (x) = y.

It remains to be rigorously established whether there actually are any (true)
one-way functions. That is to say, no one has yet definitively proved the
existence of such func- tions under reasonable (and rigorous) definitions of
“easy” and “computationally infeasi- ble”. Since the existence of one-way
functions is still unknown, the existence of trapdoor one-way functions is also
unknown. However, there are a number of good candidates for one-way and
trapdoor one-way functions. Many of these are discussed in this book, with
emphasis given to those which are practical.

One-way and trapdoor one-way functions are the basis for public-key
cryptography.

**Definition** of Permutations: Let S be a finite set of elements. A
permutation p on S is a bijection from S to itself (i.e., p : S −→ S).

p(1) = 3, p(2) = 5, p(3) = 4, p(4) = 2, p(5) = 1.

Since permutations are bijections, they have inverses. If a permutation is
written as an array (see 1.1), its inverse is easily found by interchanging the
rows in the array and reordering the elements in the new top row if desired
(the bottom row  would have to be reordered correspondignly).

**Definition** of Involutions: Let S be a finite set and let f be a bijection
from S to S (i.e., f : S −→ S).  The function f is called an involution if f =
f −1 . An equivalent way of stating this is f (f (x)) = x for all x ∈ S.

## Basic Terminology and Concepts
In the following section we describe a list of terms and basic concepts.

### Encryption Domains and Codomains
**A** denotes a finite set called the alphabet of definition. For example, A =
{0, 1}, the binary alphabet, is a frequently used alphabet of definition. Note
that any alphabet can be encoded in terms of the binary alphabet. For example,
since there are 32 binary strings of length five, each letter of the English
alphabet can be assigned a unique binary string of length five.

**M** denotes a set called the message space. M consists of strings of symbols
from an alphabet of definition. An element of M is called a plaintext message
or simply a plaintext. For example, M may consist of binary strings, English
text, computer code, etc.

**C** denotes a set called the ciphertext space. C consists of strings of
symbols from an alphabet of definition, which may differ from the alphabet of
definition for M. An element of C is called a ciphertext.

### Encryption and Decryption Transformations
**K** denotes a set called the key space. An element of K is called a key.

Each element e ∈ K uniquely determines a bijection from M to C, denoted by Eе.
Eе is called an encryption function or an encryption transformation. Note that
Eе must be a bijection if the process is to be reversed and a unique plaintext
message recovered for each distinct ciphertext.

For each d ∈ K, Dd denotes a bijection from C to M (i.e., Dd : C −→ M). Dd is
called a decryption function or decryption transformation.

The process of applying the transformation Ee to a message m ∈ M is usually
referred to as encrypting m or the encryption of m.

The process of applying the transformation Dd to a ciphertext c is usually
referred to as decrypting c or the decryption of c.

An encryption scheme consists of a set {Ee : e ∈ K} of encryption
transformations and a corresponding set {Dd : d ∈ K} of decryption
transformations with the prop- erty that for each e ∈ K there is a unique key d
∈ K such that Dd = Ee^−1 ; that is, Dd(Ee(m)) = m for all m ∈ M. An encryption
scheme is sometimes referred to as a cipher.

The keys e and d in the preceding definition are referred to as a key pair and
sometimes denoted by (e, d). Note that e and d could be the same.

To construct an encryption scheme requires one to select a message space M, a
ciphertext space C, a key space K, a set of encryption transformations {Ee :
e ∈ K}, and a corresponding set of decryption transformations {Dd : d ∈ K}.

### Achieving Confidentiality
An encryption scheme may be used as follows for the purpose of achieving
confidentiality.  Two parties Alice and Bob first secretly choose or secretly
exchange a key pair (e, d). At a subsequent point in time, if Alice wishes to
send a message m ∈ M to Bob, she computes c = Ee (m) and transmits this to Bob.
Upon receiving c, Bob computes Dd (c) = m and hence recovers the original
message m.

### Channels
A channel is a means of conveying information from one entity to another.

A physically secure channel or secure channel is one which is not physically
accessible to the adversary.

An unsecured channel is one from which parties other than those for which the
information is intended can reorder, delete, insert, or read.

A secured channel is one from which an adversary does not have the ability to
reorder, delete, insert, or read.

### Security
A fundamental premise in cryptography is that the sets M, C, K, {Ee : e ∈ K},
{Dd : d ∈ K} are public knowledge. When two parties wish to communicate
securely using an encryption scheme, the only thing that they keep secret is
the particular key pair (e, d) which they are using, and which they must
select. One can gain additional security by keeping the class of encryption and
decryption transformations secret but one should not base the security of the
entire scheme on this approach. History has shown that maintaining the secrecy
of the transformations is very difficult indeed.

**Definition** An encryption scheme is said to be breakable if a third party,
without prior knowledge of the key pair (e, d), can systematically recover
plaintext from corresponding ciphertext within some appropriate time frame.

An appropriate time frame will be a function of the useful lifespan of the data
being protected. For example, an instruction to buy a certain stock may only
need to be kept secret for a few minutes whereas state secrets may need to
remain confidential indefinitely.

An encryption scheme can be broken by trying all possible keys to see which one
the communicating parties are using (assuming that the class of encryption
functions is public knowledge). This is called an exhaustive search of the key
space. It follows then that the number of keys (i.e., the size of the key
space) should be large enough to make this approach computationally infeasible.
It is the objective of a designer of an encryption scheme that this be the best
approach to break the system.

A set of requirements for cipher systems:
1. he system should be, if not theoretically unbreakable, unbreakable in
   practice.
1. compromise of the system details should not inconvenience the
   correspondents.
1. the key should be rememberable without notes and easily changed.
1. the cryptogram should be transmissible by telegraph.
1. the encryption apparatus should be portable and operable by a single person.
1. the system should be easy, requiring neither the knowledge of a long list of
   rules nor mental strain.

## Symmetric-Key Encryption
**Definition of Symmetric-Key Encryption**: Consider an encryption scheme
consisting of the sets of encryption and decryption transformations {Ee : e ∈
K} and {Dd : d ∈ K}, respectively, where K is the key space. The encryption
scheme is said to be symmetric-key if for each associated encryption/decryption
key pair (e, d), it is computationally “easy” to determine d knowing only e,
and to determine e from d.

One of the major issues with symmetric-key systems is to find an efficient
method to agree upon and exchange keys securely. This problem is referred to as
the key distribution problem

As has been emphasized several times the only information which should be
required to be kept secret is the key d. However, in symmetric-key encryption,
this means that the key e must also be kept secret, as d can be deduced from e.

There are two classes of symmetric-key encryption schemes which are commonly
distinguished: 
* block ciphers
* stream ciphers

### Definition of Block Cipher
A block cipher is an encryption scheme which breaks up the plaintext messages
to be transmitted into strings (called blocks) of a fixed length t over an
alphabet A, and encrypts one block at a time.

Most well-known symmetric-key encryption techniques are block ciphers. Two
important classes of block ciphers are:
* substitution ciphers
* transposition ciphers

#### Substitution Ciphers
Substitution ciphers are block ciphers which replace symbols (or groups of
symbols) by other symbols or groups of symbols.

##### Simple Substitution Ciphers
Let A be an alphabet of q symbols and M be the set of all strings of length
t over A. Let K be the set of all permutations on the set A. Define for each e ∈ K an
encryption transformation E e as:

```ASCII
Ee(m) = (e(m1),e(m2),···, e(mt)) = (c1,c2,···,ct) = c
```

where m = (m1,m2, ···, mt) ∈ M. In other words, for each symbol in a t-tuple,
replace (substitute) it by another symbol from A according to some fixed
permutation e. To decrypt c = (c1, c2, ···, ct) compute the inverse permutation
d = e^−1 and

```ASCII
Dd(c) = (d(c1), d(c2), ···, d(ct)) = (m1, m2, ···, mt) = m
```

Ee is called a simple substitution cipher or a mono-alphabetic substitution
cipher.

The number of distinct substitution ciphers is q! and is independent of the
block size in the cipher. Simple substitution ciphers over small block sizes
provide inadequate security even when the key space is extremely large.

##### Homophonic Substitution Ciphers
To each symbol a ∈ A, associate a set H(a) of strings of t symbols, with the
restriction that the sets H(a), a ∈ A, be pairwise disjoint. A homophonic
substitution cipher replaces each symbol a in a plaintext message block with a
randomly chosen string from H(a). To decrypt a string c of t symbols, one must
determine an a ∈ A such that c ∈ H(a). The key for the cipher consists of the
sets H(a).

**Example**: (homophonic substitution cipher) Consider A = {a, b}, H(a) = {00,
10}, and H(b) = {01, 11}. The plaintext message block ab encrypts to one of the
following: 0001, 0011, 1001, 1011. Observe that the codomain of the encryption
function (for messages of length two) consists of the following pairwise
disjoint sets of four-element bitstrings:

```ASCII
aa −→ {0000, 0010, 1000, 1010}
ab −→ {0001, 0011, 1001, 1011}
ba −→ {0100, 0110, 1100, 1110}
bb −→ {0101, 0111, 1101, 1111}
```

Any 4-bitstring uniquely identifies a codomain element, and hence a plaintext
message.

Often the symbols do not occur with equal frequency in plaintext messages. A
homophonic cipher can be used to make the frequency of occurrence of ciphertext
symbols more uniform, at the expense of data expansion. Decryption is not as
easily performed as it is for simple substitution ciphers.

##### Polyalphabetic Substitution Ciphers
A polyalphabetic substitution cipher is a block cipher with block length t over
an alphabet A having the following properties:
1. the key space K consists of all ordered sets of t permutations (p1, p2, ...
   , pt), where each permutation pi is defined on the set A;
1. encryption of the message m = (m1, m2, ···, mt) under the key e = (p1 , p2,
   ..., pt) is given by Ee (m) = (p1(m1 ), p2(m2), ···, pt(mt)); and
1. the decryption key associated with e = (p1, p2, ..., pt) is d = (p^−1 1 ,
   p2, ..., pt).

**Example**: Vigenère cipher.

#### Transposition Ciphers
Another class of symmetric-key ciphers is the simple transposition cipher,
which simply permutes the symbols in a block.

Consider a symmetric-key block encryption scheme with block length t. Let K be
the set of all permutations on the set {1, 2, ... , t}. For each e ∈ K define
the encryption function

```ASCII
Ee(m) = (me(1), me(2), ···, me(t))
```

where m = (m1, m2, ···, mt) ∈ M, the message space. The set of all such
transformations is called a simple transposition cipher. The decryption key
corresponding to e is the inverse permutation d = e^−1 . To decrypt c = (c1,
c2, ···, ct), compute Dd(c) = (cd(1), cd(2), ···, cd(t)).

A simple transposition cipher preserves the number of symbols of a given type
within a block, and thus is easily cryptanalyzed.

**Example** of Transposition Cihpers is Composition of ciphers.

#### Product Ciphers
Simple substitution and transposition ciphers individually do not provide a
very high level of security. However, by combining these transformations it is
possible to obtain strong ciphers. Some of the most practical and effective
symmetric-key systems are product ciphers. One example of a product cipher is a
composition of t ≥ 2 transformations Ek1, Ek2, ···, Ekt where each Eki , 1 ≤ i
≤ t, is either a substitution or a transposition cipher. For the purpose of
this introduction, let the composition of a substitution and a transposition be
called a round.

### Definition of Stream Cipher
Stream ciphers form an important class of symmetric-key encryption schemes.
They are, in one sense, very simple block ciphers having block length equal to
one. What makes them useful is the fact that the encryption transformation can
change for each symbol of plaintext being encrypted. In situations where
transmission errors are highly probable, stream ciphers are advantageous
because they have no error propagation.

**Definition**: Let K be the key space for a set of encryption transformations.
A sequence of symbols e1, e2, e3, ···, ei ∈ K, is called a keystream.

**Definition**: Let A be an alphabet of q symbols and let E e be a simple
substitution cipher with block length 1 where e ∈ K. Let m1, m2, m3, ··· be a
plaintext string and let e1, e2, e3, ··· be a keystream from K. A stream cipher
takes the plaintext string and produces a ciphertext string c1, c2, c3, ···
where ci = Eei(mi). If d i denotes the inverse of e i , then Ddi(ci) = mi
decrypts the ciphertext string.

The keystream could be generated at random, or by an algorithm which generates
the keystream from an initial small keystream (called a seed), or from a seed
and previous ciphertext symbols. Such an algorithm is called a keystream
generator.

#### Vernam Cipher
The Vernam Cipher is a stream cipher defined on the alphabet A = {0, 1}. A
binary message m1, m2, ···, mt is operated on by a binary key string k1, k2,
···, kt of the same length to produce a ciphertext string c1, c2, ···, ct where
ci = mi XOR ki, 1 <= i <= t.

If the key string is randomly chosen and never used again, the Vernam cipher is
called a one-time system or a one-time pad.

If the key string is reused there are ways to attack the system. For example,
if c1, c2, ···, ct and c'1, c'2, ···, c't are two ciphertext strings
produced by the same keystream k1, k2, ···, kt then
ci = mi XOR ki, c'i = m'i XOR ki
and ci XOR c'i = mi XOR mi . The redundancy in the latter may permit
cryptanalysis.

**Fact**: A necessary, but usually not sufficient, condition for an encryption
scheme to be secure is that the key space be large enough to preclude
exhaustive search.

## Digital Signatures
A cryptographic primitive which is fundamental in authentication,
authorization, and nonrepudiation is the digital signature. The purpose of a
digital signature is to provide a means for an entity to bind its identity to a
piece of information. The process of signing entails transforming the message
and some secret information held by the entity into a tag called a signature.

**Nomenclature and set-up**
* M is the set of messages which can be signed.
* S is a set of elements called signatures, possibly binary strings of a fixed
  length.
* (S)A is a transformation from the message set M to the signature set S, and
  is called a signing transformation for entity A. The transformation (S)A is
  kept secret by A, and will be used to create signatures for messages from M.
* (V)A is a transformation from the set M × S to the set {true, false}. (V)A is
  called a verification transformation for A’s signatures, is publicly known,
  and is used by other entities to verify signatures created by A.

The transformations (S)A and (V)A provide a digital signature scheme for A.
Occasionally the term digital signature mechanism is used.

### Signing Procedure
Entity A (the signer) creates a signature for a message m ∈ M by doing the
following:
1. Compute s = SA(m)
1. Transmit the pair (m, s). s is called the signature for message m.

### Verfication Procedure
To verify that a signature s on a message m was created by A, an entity B (the
verifier) performs the following steps:
1. Obtain the verification function VA of A.
2. Compute u = VA (m, s).
3. Accept the signature as having been created by A if u = true, and reject the
   signature if u = false.

There are several properties which the signing and verification transformations
must satisfy.
* s is a valid signature of A on message m if and only if VA (m, s) = true.
* It is computationally infeasible for any entity other than A to find, for any
  m ∈ M, an s ∈ S such that VA (m, s) = true.

## Authentication and Identification
Imagine two person Alice and Bob. If Al-
ice and Bob desire assurance of each other’s identity, there are two
possibilities to consider.
1. Alice and Bob could be communicating with no appreciable time delay. That
   is, they are both active in the communication in “real time”.
1. Alice or Bob could be exchanging messages with some delay. That is, messages
   might be routed through various networks, stored, and forwarded at some
   later time.

In the first instance Alice and Bob would want to verify identities in real
time. This might be accomplished by Alice sending Bob some challenge, to which
Bob is the only entity which can respond correctly. Bob could perform a similar
action to identify Alice. This type of authentication is commonly referred to
as entity authentication or more simply identification.

For the second possibility, it is not convenient to challenge and await
response, and moreover the communication path may be only in one direction.
Different techniques are now required to authenticate the originator of the
message. This form of authentication is called data origin authentication.

### Identification
An identification or entity authentication technique assures one party (through
acquisition of corroborative evidence) of both the identity of a second party
involved, and that the second was active at the time the evidence was created
or acquired.

Typically the only data transmitted is that necessary to identify the
communicating parties. The entities are both active in the communication,
giving a timeliness guarantee.

### Data Origin Authentication
Data origin authentication or message authentication techniques provide to one
party which receives a message assurance (through corroborative evidence) of
the identity of the party which originated the message.

Often a message is provided to B along with additional information so that B
can determine the identity of the entity who originated the message. This
form of authentication typically provides no guarantee of timeliness, but is
useful in situations where one of the parties is not active in the
communication.

Data origin authentication implicitly provides data integrity since, if the
message was modified during transmission, A would no longer be the originator.

## Public-Key Cryptography
We start the section with subject of Public-key encryption.

### Public-Key Encryption
Let {E e : e ∈ K} be a set of encryption transformations, and let {D d : d ∈ K}
be the set of corresponding decryption transformations, where K is the key
space. Consider any pair of associated encryption/decryption transformations (E
e , D d ) and suppose that each pair has the property that knowing E e it is
computationally infeasible, given a random ciphertext c ∈ C, to find the
message m ∈ M such that E e (m) = c. This property implies that given e it is
infeasible to determine the corresponding decryption key d. (Of course e and d
are simply means to describe the encryption and decryption functions,
respectively.) E e is be- ing viewed here as a trapdoor one-way function
(Definition 1.16) with d being the trapdoor information necessary to compute
the inverse function and hence allow decryption. This is unlike symmetric-key
ciphers where e and d are essentially the same.

Under these assumptions, consider the two-party communication between Alice and
Bob. Bob selects the key pair (e, d). Bob sends the encryption key e (called
the public key) to Alice over any channel but keeps the decryption key d
(called the private key) secure and secret. Alice may subsequently send a
message m to Bob by apply- ing the encryption transformation determined by
Bob’s public key to get c = E e (m). Bob decrypts the ciphertext c by applying
the inverse transformation D d uniquely determined by d.

**Remark**: (private key vs. secret key) To avoid ambiguity, a common
convention is to use the term private key in association with public-key
cryptosystems, and secret key in associ- ation with symmetric-key
cryptosystems. This may be motivated by the following line of thought: it takes
two or more parties to share a secret, but a key is truly private only when one
party alone knows it.
