# Configure IPSEC VPN using StrongSwan on Debian 9.9

## Setup CA using the strongSwan PKI tool.
In order for the VPN client to verify the authenticity of the VPN server, you
need to generate the VPN server certificate and key. Before you can generate
the server certificate and the key, you have to create a local CA for signing
them. stronSwan provides a PKI utility that eases this process. However you
need to install this utility by running the command below;

``` Bash
apt install strongswan
apt install strongswan-pki 


# Generate a Private key for self signing the CA certificate.
ipsec pki --gen --size 4096 --type rsa --outform pem > vpn-ca.key.pem

# Server CA and self-sign with the key generated above.
ipsec pki --self --in vpn-ca.key.pem --type rsa --dn "CN=VPN Server root CA" \
--ca --lifetime 3650 --outform pem > vpn-ca.cert.pem

# Server private key and issue a matching certificate using the CA create above.
ipsec pki --gen --size 4096 --type rsa --outform pem > vpn-server.key.pem

ipsec pki --pub --in vpn-server.key.pem --type rsa \ |
ipsec pki --issue --lifetime 2750 \
--cacert vpn-ca.cert.pem \
--cakey vpn-ca.key.pem \
--dn "CN=vpnsvpnsvr.example.com" \
--san="vpnsvr.example.com" \
--flag serverAuth --flag ikeIntermediate --outform pem > vpn-server.cert.pem
```

## Install The Certificates

```Bash
mv vpn-ca.cert.pem /etc/ipsec.d/cacerts/
mv vpn-server.cert.pem /etc/ipsec.d/certs/
mv {vpn-ca.key.pem,vpn-server.key.pem} /etc/ipsec.d/private/
```

## Configure StrongSwan
Most configuration are in ```/etc/ipsec.conf``` .

* CONFIG SECTIONS (config setup)
  
  It defines general configuration parameters

* CONN SECTIONS (conn <name>)
  
  A conn section contains a connection specification, defining a network
  connection to be made using IPsec.

* CA SECTION (ca <name>)
  
  It defines a certification authority.


```Bash
cp /etc/ipsec.conf /etc/ipsec.conf.bak
vim /etc/ipsec.conf
config setup
        # The charondebug = <debug list>  parameter defines the charon debug
        # loggin where the debug list can be dmn, mgr, ike, chd, job, cfg, knl, net, asn,
        # enc, lib, esp, tls, tnc, imc, imv, pts. The logging levels can one of -1, 0, 1,
        # 2, 3, 4 (for silent, audit, control, controlmore, raw, private). By default,
        # the level is set to 1 for all types. For a description of the debug lists,
        # check the LOGGER CONFIGURATION section on strongswan.conf(5).
        charondebug="ike 2, knl 2, cfg 2, net 2, esp 2, dmn 2, mgr 2"
        strictcrlpolicy=no
        uniqueids=yes
        cachecrls=no
```
