# Raw Sockets

## Table of Contents
1. [Introduction](#introduction)
1. [Raw Socket Creation](#Raw-Socket-Creation)
1. [Packet Interface On Device Level](#Packet-Interface-On-Device-Level)
1. [Generic Socket Address Structure](#Generic-Socket-Address-Structure)
1. [Reception of The Network Packet](#Reception-of-The-Network-Packet)
1. [Extracting The Ethernet Header](#Extracting-The-Ethernet-Header)
1. [Extracting the IP Header](#Extracting-the-IP-Header)
1. [Extracting The Transport Layer Header](#Extracting-The-Transport-Layer-Header)
1. [Extracting Data](#Extracting-Data)
1. [Final Code](#Final-Code)
1. [Sending Packets With a Raw Socket](#Sending-Packets-With-a-Raw-Socket)
1. [Opening a Raw Socket](#Opening-a-Raw-Socket)
1. [Low Level acess to Linux Network Devices](#Low-Level-Acess-to-Linux-Network-Devices)

## List of Codes
1. [Socket System Call](#Code-Socket-System-Call)
1. [Opening a Raw Socket](#Code-Opening-a-Raw-Socket)
1. [Packet Interface On Device Level](#Code-Packet-Interface-On-Device-Level)
1. [Generic Socket Address Structure](#Code-Generic-Socket-Address-Structure)
1. [Receive a Message from a Socket](#Code-Receive-a-Message-from-a-Socket)
1. [Reception of The Network Packet](#Code-Reception-of-The-Network-Packet)
1. [Definitions for The Ethernet IEEE 802.3 Interface](#Code-Definitions-for-The-Ethernet-IEEE-802-3-Interface)
1. [Extacting the Ethernet Header](#Code-Extacting-the-Ethernet-Header)
1. [Definition of IP Header](#Code-Definition-of-IP-Header)
1. [Linux User Space implements the Internet Protocol](#Code-Linux-User-Space-implements-the-Internet-Protocol)
1. [Extracting the IP Header](#Code-Extracting-the-IP-Header)
1. [Definition of TCP Header](#Code-Definition-of-TCP-Header)
1. [Definition of UDP Header](#Code-Definition-of-UDP-Header)
1. [Extracting User Datagram Protocol](#Code-Extracting-User-Datagram-Protocol)
1. [Extracting Data](#Code-Extracting-Data)
1. [Definition of ifreq Structure](#Code-Definition-of-ifreq-Structure)
1. [Getting Interface Information](#Code-Getting-Interface-Information)

## Introduction
A raw socket is used to receive raw packets. This means packets received at the
Ethernet layer will directly pass to the raw socket. Stating it precisely, a
raw socket bypasses the normal TCP/IP processing and sends the packets to the
specific user application (see [Figure-1](#Figure-1))

```ASCII
  +-----------------------------------------------------------+  
  |                      User Applications                    |  
  |-----------------------------------------------------------|  
  |                                                           |  
  |                              +-------------------------+  |  
  |  Socket Interface            |        Raw Socket       |  |  
  |                              +-------------------------+  |  
  |                                           ^               |  
  |----------------------------+--------------|---------------|  
  | Transport Layer Processing |              |               |  
  |----------------------------|              |               |  
  | Ip Layer Processing        |              |               |  
  |----------------------------|              |               |  
  | Ethernet Layer Processing  |--------------|               |  
  |----------------------------+------------------------------|  
  |                    NIC Card Driver                        |  
  +-----------------------------------------------------------+ 
```
###### Figure-1

Other sockets like stream sockets and data gram sockets receive data from the
transport layer that contains no headers but only the payload. This means that
there is no information about the source IP address and MAC address. If
applications running on the same machine or on different machines are
communicating, then they are only exchanging data.  The purpose of a raw socket
is absolutely different. A raw socket allows an application to directly access
lower level protocols, which means a raw socket receives un-extracted packets.
There is no need to provide the port and IP address to a raw socket, unlike in
the case of stream and datagram sockets.

```
  +-----------------+--------------------------+---------------------------------+------+ 
  | Ethernet Header | Ethernet Protocol Header | Transport Layer Protocol Header | Data | 
  +-----------------+--------------------------+---------------------------------+------+ 
```
###### Figure-2 A Generic Representation of A Network Packet.

```
  +-----------------+-----------+----------------+------+ 
  | Ethernet Header | IP Header | TCP/UDP Header | Data | 
  +-----------------+-----------+----------------+------+ 
```
###### Figure-3 Network Packet for Internet Protocol.

When an application sends data into the network, it is processed by various
network layers. Before sending data, it is wrapped in various headers of the
network layer. The wrapped form of data, which contains all the information
like the source and destination address, is called a network packet. According
to Ethernet protocols, there are various types of network packets
like Internet Protocol packets, Xerox PUP packets, Ethernet Loopback packets,
etc. In Linux, we can see all protocols in the ```if_ether.h``` header file.

**Note**: When we connect to the Internet, we receive network packets, and our
machine extracts all network layer headers and sends data to a particular
application. For example, when we type ```www.google.com``` in our browser, we
receive packets sent from Google, and our machine extracts all the headers of
the network layer and gives the data to our browser.  By default, a machine
receives those packets that have the same destination address as that of the
machine, and this mode is called the non-promiscuous mode. But if we want to
receive all the packets, we have to switch into the promiscuous mode. We can go
into the promiscuous mode with the help of ioctls.

## Raw Socket Creation

```socket()``` creates  an endpoint for communication and returns a file
descriptor that refers to that endpoint.  The file descriptor returned by a
successful call will be the lowest-numbered file descriptor not currently open
for the process.

The socket function creates a raw socket when the second argument 
is ```SOCK_RAW```.  The third argument (the protocol) is normally nonzero.
Only processes with an effective user ID of 0 or the CAP_NET_RAW capability are
allowed to open raw sockets. So, during the execution of the program, you have
to be the root user.

```C
#include <sys/types.h>
#include <sys/socket.h>

/*
 * @param domain
 * The domain argument specifies a communication domain;
 * this selects the protocol family which will be used for communication.
 * These families are defined in <sys/socket.h>.
 *
 * @param type
 * The socket has the indicated type, which specifies the communication
 * semantics.
 *
 * @param protocol
 * The protocol specifies a particular protocol to be used with the socket.
 * Normally only a single protocol exists to support a particular socket type
 * within a given protocol family, in which case protocol can be specified as
 * 0.  However,  it  is  possible that many protocols may exist, in which case
 * a particular protocol must be specified in this manner.  The protocol number
 * to use is specific to the “communication domain” in which communication is
 * to take place; see protocols(5).  See getprotoent(3) on how to map protocol
 * name strings to protocol numbers.
 *
 * @return 
 * On success, a file descriptor for the new socket is returned.
 * On error, -1 is returned, and errno is set appropriately.
 */

int socket(int domain, int type, int protocol);

```
###### Code-Socket System Call

For a raw socket, the socket family is ```AF_PACKET```, the socket type 
is ```SOCK_RAW``` and for the protocol, see the ```if_ether.h``` header file.
To receive all packets, the macro is ```ETH_P_ALL``` and to receive IP packets,
the macro is ```ETH_P_IP``` for the protocol field.

```C
#define MAX_ERR_BUF 1024

int sock_r;
int ret;
char err_buf[MAX_ERR_BUF];

memset(err_buf, 0, MAX_ERR_BUF);

errno = 0;
sock_r = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
if (0 > sock_r) {
        ret = strerror_r(errno, err_buf, MAX_ERR_BUF)
        fprintf(stderr, "%s\n", (0 == ret) ? err_buf : "Socket system call Failed");
        exit(EXIT_FAILURE);
}
```
###### Code-Opening a Raw Socket

## Packet Interface On Device Level
Packet  sockets  are  used  to  receive or send raw packets at the device
driver level (OSI Layer 2).  They allow the user to implement protocol modules
in user  space  on top of the physical layer.

```C
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h> /* The L2 protocols */

packet_socket = socket(AF_PACKET, int socket_type, int protocol);
```
###### Code-Packet Interface On Device Level

The **socket_type** is either ```SOCK_RAW``` or ```SOCK_DGRAM```.
* **SOCK_RAW**:

  This type are using for packets that is included the link-level header. 
  The Physical header is included.

* **SOCK_DGRAM**:
  
  This type are using for packets that is not included the link-level header.
  The Physical header is removed.


The  link-level header  information is available in a common format in 
a ```sockaddr_ll``` structure. protocol is the IEEE 802.3 protocol 
number in network byte order. See the ```<linux/if_ether.h>```  include
file  for a list of allowed protocols.  When protocol is set 
to ```htons(ETH_P_ALL)```, then all protocols are received.  All  incoming
packets  of that  protocol type will be passed to the packet socket before they
are passed to the protocols implemented in the kernel.

By default, all packets of the specified protocol type are passed to a packet
socket. To get packets only from a specific interface use ```bind(2)```
specifying an address in a struct ```sockaddr_ll``` to bind the packet socket
to an interface.  Fields used for  binding are ```sll_family``` (should 
be ```AF_PACKET```), ```sll_protocol```, and ```sll_ifindex```.

he ```connect(2)``` operation is not supported on packet sockets.

When the ```MSG_TRUNC``` flag is passed to ```recvmsg(2)```, ```recv(2)```, 
or ```recvfrom(2)```, the real length of the packet on the wire is always
returned, even when it is longer than  the buffer.

## Generic Socket Address Structure
A socket address structures is always passed by reference when passed as an
argument to any socket functions. But any socket function that takes one of
these pointers as an argument must deal with socket address structures from any
of the supported protocol families.

A problem arises in how to declare the type of pointer that is passed. With
ANSI C, the solution is simple: void * is the generic pointer type. But, the
socket functions predate ANSI C and the solution chosen in 1982 was to define a
generic socket address structure in the ```<sys/socket.h>``` header.

```C
struct sockaddr {
        sa_family_t sa_family; /* Address family */
        char sa_data[]; /* Socket address (variable-length data) */
};
```
###### Code-Generic Socket Address Structure

## Reception of The Network Packet
The ```recv()``` and ```recvfrom()``` calls are used to receive messages from a
socket.  They may be used to receive data on both connectionless and
connection-oriented sockets. 

The only difference between ```recv()``` and ```read(2)``` is the presence of
flags.  With a zero flags argument, ```recv()``` is generally equivalent 
to ```read(2)```.

```C
#include <sys/types.h>
#include <sys/socket.h>

/*
 * @Return Number of bytes received, 
 * or -1 in the case of error errno variable.
 * set appropriately.
 * When a stream socket peer has performed an orderly shutdown, 
 * or Datagram sockets in various domains is recived, 
 * or if the requested number of bytes to receive from a stream socket was 0,
 * The value 0 returned.
 */
ssize_t recv(int sockfd, void *buf, size_t len, int flags);

ssize_t recvfrom(int sockfd, void *buf, size_t len, int flags,
                struct sockaddr *src_addr, socklen_t *addrlen);

```
###### Code-Receive a Message from a Socket

```C
#define NETWORK_PACKET_SIZE 65536
#define MAX_ERR_BUF 1024

int saddr_len; 
int ret = 0;
ssize_t bufflen;
char err_buf[MAX_ERR_BUF];
unsigned char *buffer = (unsigned char *) malloc(NETWORK_PACKET_SIZE);
struct sockaddr saddr;

saddr_len = sizeof(saddr);
memset(buffer, 0, NETWORK_PACKET_SIZE);
memset(err_buf, 0, MAX_ERR_BUF);

/*
 * Receive a network packet and copy in to buffer
 */
errno = 0;
buflen = recvfrom(sock_r, buffer, NETWORK_PACKET_SIZE, 0 &saddr, (socklen_t *)&saddr_len);
if (0 > buflen) {
        ret = strerror_r(errno, err_buf, MAX_ERR_BUF);
        fprintf(stderr, "%s\n", (0 == ret) ? err_buf : "recvfrom system call failed");
        exit(EXIT_FAILURE);
}
```
###### Code-Reception of The Network Packet

In the [above code](#Code-Reception-of-The-Network-Packet), ```saddr``` provide 
the underlying protocol that contains source and destination address of the
packet.

You can find ```ethhdr``` struct at ```linux/include/uapi/linux/if_ether.h```
address in Linux Kernel Source code.

```C
struct ethhdr {
        unsigned char   h_dest[ETH_ALEN];       /* destination eth addr */
        unsigned char   h_source[ETHALEN];      /* Source ether addr    */
        __be16          h_proto;                /* packet type ID field */
} __attribute__((packed));
```
###### Code-Definitions for The Ethernet IEEE 802.3 Interface

## Extracting The Ethernet Header
Now that we have the network packets in our buffer, we will get information
about the Ethernet header. The Ethernet header contains the physical address of
the source and destination, or the MAC address and protocol of the receiving
packet. The ```if_ether.h``` header contains the structure of the Ethernet
header.

Now, we can easily access these fields:

```C
struct ethhdr *eth = (struct ethhdr *)(buffer);
printf("\nEthernet Header\n");
printf("\t|-Source Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n", 
        eth->h_source[0], eth->h_source[1], eth->h_source[2],
        eth->h_source[3], eth->h_source[4], eth->h_source[5]);
printf("\t|-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n", 
        eth->h_dest[0], eth->h_dest[1], eth->h_dest[2],
        eth->h_dest[3], eth->h_dest[4], eth->h_dest[5]);
printf("\t|-Protocol : %d\n", eth->h_proto);
```
###### Code-Extacting the Ethernet Header

The ```h_proto``` gives information about the next layer. If you 
get ```0x800``` ```(ETH_P_IP)```, it means that the next header is the IP
header. Later, we will consider the next header as the IP header.

**Notes**: 
* The physical address is 6 bytes.
* We can also direct the output to a file for better understanding. Use fflush
  to avoid the input-output buffer problem when writing into a file.

## Extracting the IP Header
The IP layer gives various pieces of information like the source and
destination IP address, the transport layer protocol, etc. The structure of the
IP header is defined in the ```ip.h``` header file, You can find ```iphdr ```
struct at ```linux/include/uapi/linux/ip.h``` address in Linux Kernel Source
Code.


```C
#include <linux/ip.h>

struct iphdr {
#if defined(__LITTLE_ENDIAN_BITFIELD)
        __u8    ihl:4,
                version:4;
#elif defined (__BIG_ENDIAN_BITFIELD)
        __u8    version:4,
                ihl:4;
#else
#error	"Please fix <asm/byteorder.h>"
#endif
        __u8    tos;
        __be16  tot_len;
        __be16  id;
        __be16  frag_off;
        __u8    ttl;
        __u8    protocol;
        __sum16 check;
        __be32  saddr;
        __be32  daddr;
        /*The options start here. */
};
```
###### Code-Definition of IP Header

Linux User Space implements the Internet Protocol, version 4, described in 
RFC 791 and RFC 1122.  ```IP``` contains a level 2 multicasting implementation
conforming to RFC 1112.  It also contains an IP router including a packet
filter.

Valid  socket  types  are ```SOCK_STREAM``` to open a ```tcp(7)``` 
socket, ```SOCK_DGRAM``` to open a ```udp(7)``` socket, or ```SOCK_RAW``` to
open a ```raw(7)``` socket to access the IP protocol directly.  protocol is the
IP protocol in the IP header to be received or sent.  The only valid values for
protocol are ```0``` and ```IPPROTO_TCP``` for TCP sockets, and ```0``` 
and ```IPPROTO_UDP``` for UDP sockets.  For ```SOCK_RAW``` you may specify a valid
IANA IP protocol defined in RFC 1700 assigned numbers.

When a process wants to receive new incoming packets or connections, it
should bind a socket to a local interface address using ```bind(2)```.  In
this case, only one IP socket may be bound to any given local (address,
port)  pair. When ```INADDR_ANY``` is specified in the bind call, the
socket will be bound to all local interfaces.  When ```listen(2)``` is called
on an unbound socket, the socket is automatically bound to a random free
port with the local address set to ```INADDR_ANY```.  When ```connect(2)``` is
called on an unbound socket, the socket is automatically bound to a
random free port or to a usable shared port with the local address set
to ```INADDR_ANY```.

A TCP local socket address that has been bound is unavailable for some
time after closing, unless the ```SO_REUSEADDR``` flag has been set.  Care
should be taken when using this flag as it makes TCP less reliable.

```C
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

struct sockaddr_in {
        sa_family_t     sin_family; /* address family: AF_INET */
        in_port_t       sin_port;   /* port in network byte order */
        struct in_addr  sin_addr;   /* internet address */
};

/* Internet address. */
struct in_addr {
        uint32_t        s_addr;     /* address in network byte order */
};

```
###### Code-Linux User Space implements the Internet Protocol

The size of the IP header varies from 20 bytes to 60 bytes. We can calculate
this from the IP header field or ```IHL```. ```IHL``` 
means ```Internet Header Length (IHL)```, which is the number of 32-bit words
in the header. So we have to multiply the IHL by 4 to get the size of the
header in bytes:

```C
unsigned short iphdrlen;
struct iphdr *ip = (struct iphdr*)(buffer + sizeof(struct ethhdr));
iphdrlen = ip->ihl*4;

memset(&source, 0, sizeof(source));
source.sin_addr.s_addr = ip->saddr;
memset(&dest, 0, sizeof(dest));
dest.sin_addr.s_addr = ip->daddr;
 
fprintf(log_txt, "\t|-Version : %d\n", (unsigned int)ip->version); 
fprintf(log_txt , "\t|-Internet Header Length : %d DWORDS or %d Bytes\n", 
                (unsigned int)ip->ihl,((unsigned int)(ip->ihl))*4); 
fprintf(log_txt , "\t|-Type Of Service : %d\n", (unsigned int)ip->tos); 
fprintf(log_txt , "\t|-Total Length : %d Bytes\n", ntohs(ip->tot_len)); 
fprintf(log_txt , "\t|-Identification : %d\n", ntohs(ip->id)); 
fprintf(log_txt , "\t|-Time To Live : %d\n", (unsigned int)ip->ttl); 
fprintf(log_txt , "\t|-Protocol : %d\n", (unsigned int)ip->protocol); 
fprintf(log_txt , "\t|-Header Checksum : %d\n", ntohs(ip->check)); 
fprintf(log_txt , "\t|-Source IP : %s\n", inet_ntoa(source.sin_addr)); 
fprintf(log_txt , "\t|-Destination IP : %s\n", inet_ntoa(dest.sin_addr));

```
###### Code-Extracting the IP Header

## Extracting The Transport Layer Header
There are various transport layer protocols. Since the underlying header was
the IP header, we have various IP or Internet protocols. You can see these
protocols in the ```/etc/protocls``` file. The TCP and UDP protocol structures
are defined in ```tcp.h``` and ```udp.h``` respectively. These structures
provide the port number of the source and destination. With the help of the
port number, the system gives data to a particular application. You can 
find ```tcphdr``` struct at ```linux/include/uapi/linux/tcp.h``` address in
Linux Kernel Source Code.

```C
#include <linux/tcp.h>

struct tcphdr {
	__be16	source;
	__be16	dest;
	__be32	seq;
	__be32	ack_seq;
#if defined(__LITTLE_ENDIAN_BITFIELD)
	__u16	res1:4,
		doff:4,
		fin:1,
		syn:1,
		rst:1,
		psh:1,
		ack:1,
		urg:1,
		ece:1,
		cwr:1;
#elif defined(__BIG_ENDIAN_BITFIELD)
	__u16	doff:4,
		res1:4,
		cwr:1,
		ece:1,
		urg:1,
		ack:1,
		psh:1,
		rst:1,
		syn:1,
		fin:1;
#else
#error	"Adjust your <asm/byteorder.h> defines"
#endif	
	__be16	window;
	__sum16	check;
	__be16	urg_ptr;
};
```
###### Code-Definition of TCP Header

```C
#include <linux/udp.h>

struct udphdr {
	__be16	source;
	__be16	dest;
	__be16	len;
	__sum16	check;
};
```
###### Code-Definition of UDP Header

If your machine is little endian, you have to use ntohs because the network
uses the big endian scheme.

```C
struct iphdr *ip = (struct iphdr *)( buffer + sizeof(struct ethhdr));
/* getting actual size of IP header*/
iphdrlen = ip->ihl*4;
/* getting pointer to udp header*/
struct tcphdr *udp=(struct udphdr*)(buffer + iphdrlen + sizeof(struct ethhdr));

fprintf(log_txt , "\t|-Source Port : %d\n", ntohs(udp->source));
fprintf(log_txt , "\t|-Destination Port : %d\n", ntohs(udp->dest));
fprintf(log_txt , "\t|-UDP Length : %d\n", ntohs(udp->len));
fprintf(log_txt , "\t|-UDP Checksum : %d\n", ntohs(udp->check));
```
###### Code-Extracting User Datagram Protocol

## Extracting Data
After the transport layer header, there is data payload remaining. For this, we
will move the pointer to the data, and then print.

```C
unsigned char * data = (buffer + iphdrlen + sizeof(struct ethhdr) + sizeof(struct udphdr));
int remaining_data = buflen - (iphdrlen + sizeof(struct ethhdr) + sizeof(struct udphdr));
 
for(i=0;i<remaining_data;i++)
{
        if(i!=0 && i%16==0)
                printf("\n");
        printf("%.2X",data[i]);
}
```
###### Code-Extracting Data

## Final Code

```C
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <net/ethernet.h>
#include <arpa/inet.h>
#include <errno.h>

#define IP_HEADER 0x800
#define UDP_HEADER 17
#define TCP_HEADER 6

#define MAX_ERR_BUF_SIZE 1024
#define NET_PKT_SIZE 65536

void errExit(short int condition, int error_number, char * msg)
{
        if (condition) {
                char err_buf[MAX_ERR_BUF_SIZE];
                int ret = 0;
                memset(err_buf, 0, MAX_ERR_BUF_SIZE);
                ret = strerror_r(error_number, err_buf, MAX_ERR_BUF_SIZE);
                fprintf(stderr, "%s\n", (0 == ret) ? err_buf : msg);
                exit(EXIT_FAILURE);
        }
}

int extEthHdr(void **buffer)
{
        struct ethhdr *eth = (struct ethhdr *)(*buffer);

        printf("\nEthernet Header\n");
        printf("\t|-Source Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n", 
                        eth->h_source[0], eth->h_source[1], eth->h_source[2],
                        eth->h_source[3], eth->h_source[4], eth->h_source[5]);
        printf("\t|-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n", 
                        eth->h_dest[0], eth->h_dest[1], eth->h_dest[2],
                        eth->h_dest[3], eth->h_dest[4], eth->h_dest[5]);
        printf("\t|-Protocol : 0x%2X\n", ntohs(eth->h_proto));

        return ntohs(eth->h_proto);
}

unsigned int extIPHdr(void **buffer, unsigned short *header_len)
{
        struct sockaddr_in source;
        struct sockaddr_in destionation;
        struct iphdr *ip = (struct iphdr *)((*buffer) + sizeof(struct ethhdr));

        memset(&source, 0, sizeof(source));
        memset(&destionation, 0, sizeof(destionation));

        /* getting actual size of IP header*/
        *header_len = ip->ihl*4;
        source.sin_addr.s_addr = ip->saddr;
        destionation.sin_addr.s_addr = ip->daddr;

        printf("\nInternet Protecol Header\n");
        printf("\t|-Version : %d\n", (unsigned int)ip->version); 
        printf("\t|-Internet Header Length : %d DWORDS or %d Bytes\n", 
                       (unsigned int)ip->ihl,((unsigned int)(ip->ihl))*4); 
        printf("\t|-Type Of Service : %d\n", (unsigned int)ip->tos); 
        printf("\t|-Total Length : %d Bytes\n", ntohs(ip->tot_len)); 
        printf("\t|-Identification : %d\n", ntohs(ip->id)); 
        printf("\t|-Time To Live : %d\n", (unsigned int)ip->ttl); 
        printf("\t|-Protocol : %d\n", (unsigned int)ip->protocol); 
        printf("\t|-Header Checksum : %d\n", ntohs(ip->check)); 
        printf("\t|-Source IP : %s\n", inet_ntoa(source.sin_addr)); 
        printf("\t|-Destination IP : %s\n", inet_ntoa(destionation.sin_addr));

        return ip->protocol;
}

void extUDPHdr(void **buffer, unsigned short previous_hdr_len)
{
        struct udphdr *udp = (struct udphdr *)((*buffer) + 
                        previous_hdr_len + sizeof(struct ethhdr));

        printf("\nUser Datagram Protocol Header\n");
        printf("\t|-Source Port : %d\n", ntohs(udp->source));
        printf("\t|-Destination Port : %d\n", ntohs(udp->dest));
        printf("\t|-UDP Length : %d\n", ntohs(udp->len));
        printf("\t|-UDP Checksum : %d\n", ntohs(udp->check));
}

void extTCPHdr(void **buffer, unsigned short previous_hdr_len)
{
        struct tcphdr *tcp = (struct tcphdr *)((*buffer) + 
                        previous_hdr_len + sizeof(struct ethhdr));

        printf("\nTransmission Control Protocol Header\n");
        printf("\t|-Source Port : %d\n", ntohs(tcp->source));
        printf("\t|-Destination Port : %d\n", ntohs(tcp->dest));
        printf("\t|-Sequence Number : %d\n", ntohs(tcp->seq));
        printf("\t|-Acknowledge Number : %d\n", ntohs(tcp->ack_seq));
        printf("\t|-Data Offset : %d\n", tcp->doff);
        printf("\t\t-----------FLAGS-----------\n");
        printf("\t\t|-CWR : %d\n", tcp->cwr);
        printf("\t\t|-ECE : %d\n", tcp->ece);
        printf("\t\t|-URG : %d\n", tcp->urg);
        printf("\t\t|-ACK : %d\n", tcp->ack);
        printf("\t\t|-PSH : %d\n", tcp->psh);
        printf("\t\t|-RST : %d\n", tcp->rst);
        printf("\t\t|-SYN : %d\n", tcp->syn);
        printf("\t\t|-FIN : %d\n", tcp->fin);
        printf("\t|-Window Size : %d\n", ntohs(tcp->window));
        printf("\t|Urgent Pointer : %d\n", ntohs(tcp->urg_ptr));
}

void extData(void **buffer, ssize_t buf_len,
                unsigned short prv_hdr_len, int hdr_type)
{
        int i;
        int offset = prv_hdr_len + sizeof(struct ethhdr);
        int remaining_data = 0;
        unsigned char *data = NULL;

        if (TCP_HEADER == hdr_type) {
                offset = offset + sizeof(struct tcphdr);
        } else if (UDP_HEADER == hdr_type) {
                offset = offset + sizeof(struct udphdr);
        }
        
        data = ((*buffer) + offset);
        remaining_data = buf_len - offset;

        printf("\nData : \n");
        for(i = 0; i < remaining_data; i++) {
                if (0 != i && 0 == (i%16)) {
                        printf("\n");
                }
                
                printf(" %.2X ", data[i]);
        }
}

void MALLOC(void **buffer, int size, char type) 
{
        switch (type) {
        case 'C': {     /* Unsigned char */
                *buffer = (unsigned char *) malloc(size);
                errExit((NULL == buffer), ENOMEM, "Memory allocation failed.");
                memset(*buffer, 0, MAX_ERR_BUF_SIZE);
                }
                break;
        }
}

void SOCKET(int domain, int type, int protocol, int *sock_fd)
{
        int sfd;

        errno = 0;
        sfd = socket(domain, type, protocol);
        errExit((0 > (sfd)), errno, "Socket system call failed");

        *sock_fd = sfd;
}

void RECVFROM(int sockfd, void **buffer, size_t len,
                int flags, struct sockaddr *src_addr, ssize_t *buffer_len)
{
        int saddr_len = sizeof(*src_addr);
       

        errno = 0;
        *buffer_len = recvfrom(sockfd, *buffer, len, flags,
                        src_addr, (socklen_t *) &saddr_len);
        errExit((0 > buffer_len), errno, "Recvfrom system call failed");
}


int main(int argc, char *argv[]) 
{
        int sock_r;
        int next_hdr = 0;
        ssize_t buffer_len = 0;
        unsigned short header_len = 0;
        unsigned char *buffer = NULL; 
        struct sockaddr saddr;

        SOCKET(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL), &sock_r);
        MALLOC((void *) &buffer, NET_PKT_SIZE, 'C');
        
        RECVFROM(sock_r, (void *) &buffer, NET_PKT_SIZE, 0, &saddr, &buffer_len);
        
        next_hdr = extEthHdr((void *) &buffer);
        if (IP_HEADER == next_hdr) {
                next_hdr = extIPHdr((void *) &buffer, &header_len);
        }

        if (UDP_HEADER == next_hdr) {
                extUDPHdr((void *) &buffer, header_len);
        } else if (TCP_HEADER == next_hdr) {
                extTCPHdr((void *) &buffer, header_len);
        }
        
        extData((void *) &buffer, buffer_len, header_len, next_hdr);

        free(buffer);
        exit(EXIT_SUCCESS);
}
```

## Sending Packets With a Raw Socket
To send a packet, we first have to know the source and destination IP addresses
as well as the MAC address. 

There are two ways to find out your IP and MAC address:
1. Enter ifconfig and get the IP and MAC for a particular interface.
2. Enter ioctl and get the IP and MAC.

## Opening a Raw Socket
To open a raw socket, you have to know three fields of ```socket``` system
call. **Domain**, **Type** and **Protocol**.

For Sending Low-level packet we use:
* **Domain**: AF_PACKET
* **Type**: SOCK_RAW
* **Protocol**: IPPROTO_RAW

## Low Level Acess to Linux Network Devices
Linux supports some standard ioctls to configure network devices.  They can be
used on any socket's file descriptor regardless of the family or type.  Most of
them pass an ifreq structure. You can find ```ifreq``` struct at
```linux/include/uapi/linux/if.h``` address in Linux Kernel Source Code.

```C
#include <sys/ioctl.h>
#include <net/if.h>

struct ifreq {
#define IFHWADDRLEN	6
	union
	{
		char	ifrn_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	} ifr_ifrn;
	
	union {
		struct  sockaddr        ifru_addr;
		struct  sockaddr        ifru_dstaddr;
		struct  sockaddr        ifru_broadaddr;
		struct  sockaddr        ifru_netmask;
		struct  sockaddr        ifru_hwaddr;
		short                   ifru_flags;
		int                     ifru_ivalue;
		int                     ifru_mtu;
		struct                  ifmap ifru_map;
		char                    ifru_slave[IFNAMSIZ];	/* Just fits the size */
		char                    ifru_newname[IFNAMSIZ];
		void    __user *        ifru_data;
		struct  if_settings     ifru_settings;
	} ifr_ifru;
};

#define ifr_name        ifr_ifrn.ifrn_name      /* interface name       */
#define ifr_hwaddr      ifr_ifru.ifru_hwaddr    /* MAC address          */
#define ifr_addr        ifr_ifru.ifru_addr      /* address              */
#define ifr_dstaddr     ifr_ifru.ifru_dstaddr   /* other end of p-p lnk */
#define ifr_broadaddr   ifr_ifru.ifru_broadaddr /* broadcast address    */
#define ifr_netmask     ifr_ifru.ifru_netmask   /* interface net mask   */
#define ifr_flags       ifr_ifru.ifru_flags     /* flags                */
#define ifr_metric      ifr_ifru.ifru_ivalue    /* metric               */
#define ifr_mtu         ifr_ifru.ifru_mtu       /* mtu                  */
#define ifr_map         ifr_ifru.ifru_map       /* device map           */
#define ifr_slave       ifr_ifru.ifru_slave     /* slave device         */
#define ifr_data        ifr_ifru.ifru_data      /* for use by interface */
#define ifr_ifindex     ifr_ifru.ifru_ivalue    /* interface index      */
#define ifr_bandwidth   ifr_ifru.ifru_ivalue    /* link bandwidth       */
#define ifr_qlen        ifr_ifru.ifru_ivalue    /* Queue length         */
#define ifr_newname     ifr_ifru.ifru_newname   /* New name             */
#define ifr_settings    ifr_ifru.ifru_settings  /* Device/proto settings*/
```
###### Code-Definition of ifreq Structure

```C
const char * const interface = "enp3s0";
struct ifreq ifreq_i;
struct ifreq ifreq_c;
struct ifreq ifreq_ip;

/* Interface Index */
memset(&ifreq_i, 0, sizeof(ifreq_i));
strncpy(ifreq_i.ifr_name, interface, strlen(interface));
if(0 > ioctl(sock_raw, SIOCGIFINDEX, &ifreq_i)) {
        exit(EXIT_FAILURE);
}

/* Interface MAC */
memset(&ifreq_c, 0, sizeof(ifreq_c));
strncpy(ifreq_c.ifr_name, interface, strlen(interface));
if(0 > ioctl(sock_raw, SIOCGIFHADDR, &ifreq_c)) {
        exit(EXIT_FAILURE);
}

/* Interface IP */
memset(&ifreq_ip, 0, sizeof(ifreq_ip));
strncpy(ifreq_ip.ifr_name, interface, strlen(interface));
if(0 > ioctl(sock_raw, SIOCGIFADDR, &ifreq_ip)) {
        exit(EXIT_FAILURE);
}
```
###### Code-Getting Interface Information
