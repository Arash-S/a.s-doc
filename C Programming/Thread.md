# Threads

## Table of Contents
1. [Thread Introduction](#Thread-Introduction)
  1. [Threads and errno](#Threads-and-errno)
  1. [Return value from pthreads functions](#Return-value-from-pthreads-functions)
  1. [Compiling Pthreads programs](#Compiling-Pthreads-programs)
  1. [Thread Creation](#Thread-Creation)
  1. [Thread Termination](#Thread-Termination)
  1. [Thread IDs](#Thread-IDs)
  1. [Joining with a Terminated Thread](#Joining-with-a-Terminated-Thread)
  1. [Detaching a Thread](#Detaching-a-Thread)
  1. [Thread Attributes](#Thread-Attributes)
1. [Thread Synchronization](#Thread-Synchronization)
  1. [Mutexes](#Mutexes)
    1. [Statically Allocated Mutexes](#Statically-Allocated-Mutexes)
    1. [Locking and Unlocking a Mutex](#Locking-and-Unlocking-a-Mutex)
       1. [Trylock](#Trylock)
       1. [Timedlock](#Timedlock)
    1. [Mutex Deadlocks](#Mutex-Deadlocks)
    1. [Dynamically Initializing a Mutex](#Dynamically-Initializing-a-Mutex)
    1. [Mutex Attributes](#Mutex-Attributes)
       1. [Mutex Types](#Mutex-Types)
  1. [Signaling Changes of State: Condition Variables](#Signaling-Changes-of-State-Condition-Variables)
    1. [Statically Allocated Condition Variables](#Statically-Allocated-Condition-Variables)

## Thread Introduction
There are two main Linux threading implementations:
* LinuxThreads
* Native POSIX Threads Library (NPTL)

A single process can contain multiple threads.  All of these threads are
independently executing the same program, and they all share the same global
memory, including the initialized data, uninitialized data, and heap segments.
(A traditional UNIX process is simply a special case of a multithreaded
processes; it is a process that contains just one thread.)

the location of the per-thread stacks may be intermingled with shared libraries
and shared memory regions, depending on the order in which threads are created,
shared libraries loaded, and shared memory regions attached. Furthermore, the
location of the per-thread stacks can vary depending on the Linux distribution.

Besides global memory, threads also share a number of other attributes (i.e.,
these attributes are global to a process, rather than specific to a thread).
These attributes include the following:

* process ID and parent process ID;
* process group ID and session ID;
* controlling terminal;
* process credentials (user and group IDs);
* open file descriptors;
* record locks created using fcntl();
* signal dispositions;
* file system–related information: umask, current working directory, and root directory;
* interval timers (setitimer()) and POSIX timers (timer_create());
* System V semaphore undo (semadj) values; 
* resource limits;
* CPU time consumed (as returned by times());
* resources consumed (as returned by getrusage()); and
* nice value (set by setpriority() and nice()).

Among the attributes that are distinct for each thread are the following:

* thread ID;
* signal mask;
* thread-specific data;
* alternate signal stack (sigaltstack());
* the errno variable;
* floating-point environment (see fenv(3));
* realtime scheduling policy and priority;
* CPU affinity (Linux-specific);
* capabilities (Linux-specific); and
* stack (local variables and function call linkage information).

**Pthreads data types**:

Data Types | Descriptions
-----------|--------------
pthread_t | Thread identifier
pthread_mutex_t | Mutex
pthread_mutexattr_t | Mutex attributes object
pthread_cond_t | Condition variable
pthread_condattr_t | Condition variable attributes object
pthread_key_t | Key for thread-specific data
pthread_once_t | One-time initialization control context
pthread_attr_t | Thread attributes object

SUSv3 doesn’t specify how these data types should be represented, and portable
programs should treat them as opaque data. By this, we mean that a program
should avoid any reliance on knowledge of the structure or contents of a
variable of one of these types. In particular, we can’t compare variables of
these types using the C == operator.

### Threads and errno
In the traditional UNIX API, errno is a global integer variable. However, this
doesn’t suffice for threaded programs. If a thread made a function call that
returned an error in a global errno variable, then this would confuse other
threads that might also be making function calls and checking errno. In other
words, race conditions would result. Therefore, in threaded programs, each
thread has its own errno value. On Linux, a thread-specific errno is achieved
in a similar manner to most other UNIX implementations: errno is defined as a
macro that expands into a function call returning a modifiable lvalue that is
distinct for each thread. (Since the lvalue is modifiable, it is still possible
to write assignment statements of the form errno = value in threaded programs.)

To summarize, the errno mechanism has been adapted for threads in a manner that
leaves error reporting unchanged from the traditional UNIX API.

The original POSIX.1 standard followed K&R C usage in allowing a program to
declare errno as extern int errno. SUSv3 doesn’t permit this usage (the change
actually occurred in 1995 in POSIX.1c). Nowadays, a program is required to
declare errno by including <errno.h> , which enables the implementation of a
per-thread errno.

### Return value from pthreads functions
The traditional method of returning status from system calls and some library
functions is to return 0 on success and –1 on error, with errno being set to
indicate the error. The functions in the Pthreads API do things differently.
All Pthreads functions return 0 on success or a positive value on failure. The
failure value is one of the same values that can be placed in errno by
traditional UNIX system calls.

### Compiling Pthreads programs
On Linux, programs that use the Pthreads API must be compiled with 
the ```cc –pthread``` option. The effects of this option include the following:

* The _REENTRANT preprocessor macro is defined. This causes the declarations of
  a few reentrant functions to be exposed.
* The program is linked with the libpthread library (the equivalent of –lpthread).

### Thread Creation

```C
#include <pthread.h>

int pthread_create(pthread_t * thread , const pthread_attr_t * attr ,
                void *(* start )(void *), void * arg );

Returns 0 on success, or a positive error number on error
```

**Note**:

Strictly speaking, the C standards don’t define the results of casting int to
void * and vice versa. However, most C compilers permit these operations, and
they produce the desired result; that is, int j == (int) ((void *) j).

**Note**:

Caution is required when using a cast integer as the return value of a thread’s
start function. The reason for this is that PTHREAD_CANCELED , the value
returned when a thread is canceled, is usually some implementation-defined
integer value cast to void *. If a thread’s start function returns the same
integer value, then, to another thread that is doing a pthread_join(), it will
wrongly appear that the thread was canceled. In an application that employs
thread cancellation and chooses to return cast integer values from a thread’s
start functions, we must ensure that a normally terminating thread does not
return an integer whose value matches PTHREAD_CANCELED on that Pthreads
implementation. A portable application would need to ensure that normally
terminating threads don’t return integer values that match PTHREAD_CANCELED on
any of the implementations on which the application is to run.

* **thread**:
  
  The thread argument points to a buffer of type pthread_t into which the
  unique identifier for this thread is copied before ```pthread_create()```
  returns.  This identifier can be used in later Pthreads calls to refer to the
  thread.

  SUSv3 explicitly notes that the implementation need not initialize the buffer
  pointed to by thread before the new thread starts executing; that is, the new
  thread may start running before pthread_create() returns to its caller. If
  the new thread needs to obtain its own ID, then it must do so using
  pthread_self().

* **attr**:
  
  The attr argument is a pointer to a pthread_attr_t object that specifies
  various attributes for the new thread. If attr is specified as NULL , then
  the thread is created with various default attributes.

  After a call to pthread_create(), a program has no guarantees about which
  thread will next be scheduled to use the CPU (on a multiprocessor system,
  both threads may simultaneously execute on different CPUs). Programs that
  implicitly rely on a particular order of scheduling are open to the same
  sorts of race conditions.

### Thread Termination
The execution of a thread terminates in one of the following ways:

* The thread’s start function performs a return specifying a return value for
  the thread.

* The thread calls pthread_exit().

* The thread is canceled using pthread_cancel().

* Any of the threads calls exit(), or the main thread performs a return (in the
  main() function), which causes all threads in the process to terminate
  immediately.

The ```pthread_exit()``` function terminates the calling thread, and specifies
a return value that can be obtained in another thread by 
calling ```pthread_join()```.


```C
#include <pthread.h>

void pthread_exit(void * retval );
```

Calling pthread_exit() is equivalent to performing a return in the thread’s
start function, with the difference that pthread_exit() can be called from
any function that has been called by the thread’s start function.

The retval argument specifies the return value for the thread. The value
pointed to by retval should not be located on the thread’s stack, since the
contents of that stack become undefined on thread termination. (For example,
that region of the process’s virtual memory might be immediately reused by the
stack for a new thread.) The same statement applies to the value given to a
return statement in the thread’s start function.

If the main thread calls pthread_exit() instead of calling exit() or performing
a return , then the other threads continue to execute.

### Thread IDs

```C
#include <pthread.h>

pthread_t pthread_self(void);

Returns the thread ID of the calling thread
```

Thread IDs are useful within applications for the following reasons:

* Various Pthreads functions use thread IDs to identify the thread on which
  they are to act. Examples of such functions include pthread_join(),
  pthread_detach(), pthread_cancel(), and pthread_kill().

* In some applications, it can be useful to tag dynamic data structures with
  the ID of a particular thread. This can serve to identify the thread that
  created or “owns” a data structure, or can be used by one thread to identify
  a specific thread that should subsequently do something with that data
  structure.

```C
#include <pthread.h>

int pthread_equal(pthread_t t1 , pthread_t t2 );

Returns nonzero value if t1 and t2 are equal, otherwise 0
```

The pthread_equal() function is needed because the pthread_t data type must be
treated as opaque data. On Linux, pthread_t happens to be defined as an
unsigned long, but on other implementations, it could be a pointer or a
structure.

In NPTL, pthread_t is actually a pointer that has been cast to unsigned long.

SUSv3 doesn’t require pthread_t to be implemented as a scalar type; it could be
a structure. Therefore, we can’t portably use code such as the following to
display a thread ID (though it does work on many implementations, including
Linux, and is sometimes useful for debugging purposes):

```C
pthread_t thr;

printf("Thread ID = %ld\n", (long) thr); /* WRONG! */
```

In the Linux threading implementations, thread IDs are unique across processes.
However, this is not necessarily the case on other implementations, and SUSv3
explicitly notes that an application can’t portably use a thread ID to identify
a thread in another process. SUSv3 also notes that an implementation is
permitted to reuse a thread ID after a terminated thread has been joined with
pthread_join() or after a detached thread has terminated.

**Note**:

POSIX thread IDs are not the same as the thread IDs returned by the
Linux-specific gettid() system call. POSIX thread IDs are assigned and
maintained by the threading implementation. The thread ID returned by gettid()
is a number (similar to a process ID) that is assigned by the kernel. Although
each POSIX thread has a unique kernel thread ID in the Linux NPTL threading
implemen- tation, an application generally doesn’t need to know about the
kernel IDs (and won’t be portable if it depends on knowing them).

### Joining with a Terminated Thread
The ```pthread_join()``` function waits for the thread identified by thread to
terminate. (If that thread has already terminated, ```pthread_join()``` returns
immediately.) This operation is termed joining.

```C
#include <pthread.h>

int pthread_join(pthread_t thread , void ** retval );

Returns 0 on success, or a positive error number on error
```

If retval is a non-NULL pointer, then it receives a copy of the terminated
thread’s return value—that is, the value that was specified when the thread
performed a return or called pthread_exit().

Calling pthread_join() for a thread ID that has been previously joined can lead
to unpredictable behavior; for example, it might instead join with a thread
created later that happened to reuse the same thread ID.

If a thread is not detached, then we must join with it using pthread_join(). If
we fail to do this, then, when the thread terminates, it produces the thread
equivalent of a zombie process. Aside from wasting system resources, if enough
thread zombies accumulate, we won’t be able to create additional threads.

The task that pthread_join() performs for threads is similar to that performed
by waitpid() for processes. However, there are some notable differences:

* Threads are peers. Any thread in a process can use pthread_join() to join
  with any other thread in the process. For example, if thread A creates thread
  B, which creates thread C, then it is possible for thread A to join with
  thread C, or vice versa. This differs from the hierarchical relationship
  between processes.  When a parent process creates a child using fork(), it is
  the only process that can wait() on that child. There is no such relationship
  between the thread that calls pthread_create() and the resulting new thread.

* There is no way of saying “join with any thread” (for processes, we can do
  this using the call waitpid(–1, &status, options)); nor is there a way to do
  a nonblocking join (analogous to the waitpid() WNOHANG flag). There are ways
  to achieve similar functionality using condition variables.

**Note**:

The limitation that pthread_join() can join only with a specific thread ID is
intentional. The idea is that a program should join only with the threads that
it “knows” about. The problem with a “join with any thread” operation stems
from the fact that there is no hierarchy of threads, so such an operation could
indeed join with any thread, including one that was privately created by a
library function.  As a consequence, the library would no longer be able to
join with that thread in order to obtain its status, and it would erroneously
try to join with a thread ID that had already been joined. In other words, a
“join with any thread” operation is incompatible with modular program design.

```C
#include "general_hdr.h"
#include <pthread.h>

static void * threadFunc(void *arg)
{
        char *s = (char *) arg;

        printf("%s", s);

        return (void *) strlen(s);
}

int main(int argc, char *argv[])
{
        pthread_t t1;
        void *res;
        int s;

        s = pthread_create(&t1, NULL, threadFunc, "Hello World\n");
        if (0 != s) {
                errExitEN(s, "pthread_create");
        }

        printf("Message From main()\n");
        s = pthread_join(t1, &res);
        if (0 != s) {
                errExitEN(s, "pthread_join");
        }

        printf("Thread returned %ld\n", (long) res);

        exit(EXIT_SUCCESS);
}
```

### Detaching a Thread
By default, a thread is joinable, meaning that when it terminates, another
thread can obtain its return status using ```pthread_join()```. Sometimes, we don’t
care about the thread’s return status; we simply want the system to
automatically clean up and remove the thread when it terminates. In this case,
we can mark the thread as detached, by making a call to ```pthread_detach()```
specifying the thread’s identifier in thread.

```C
#include <pthread.h>

int pthread_detach(pthread_t thread );

Returns 0 on success, or a positive error number on error
```

As an example of the use of pthread_detach(), a thread can detach itself using
the following call:

```C
pthread_detach(pthread_self());
```

Once a thread has been detached, it is no longer possible to 
use ```pthread_join()``` to obtain its return status, and the thread can’t be
made joinable again.  Detaching a thread doesn’t make it immune to a call 
to ```exit()``` in another thread or a return in the main thread. In such an
event, all threads in the process are immediately terminated, regardless of
whether they are joinable or detached. To put things another 
way, ```pthread_detach()``` simply controls what happens after a thread
terminates, not how or when it terminates.

### Thread Attributes
These attributes include information such as the location and size of
the thread’s stack, the thread’s scheduling policy and priority and whether the
thread is joinable or detached.

```C
pthread_t thr;
pthread_attr_t attr;
int s;

s = pthread_attr_init(&attr);
if (s != 0)
        errExitEN(s, "pthread_attr_init");

/* Assigns default values */
s = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
if (s != 0)
        errExitEN(s, "pthread_attr_setdetachstate");

s = pthread_create(&thr, &attr, threadFunc, (void *) 1);
if (s != 0)
        errExitEN(s, "pthread_create");

s = pthread_attr_destroy(&attr);
if (s != 0)
        errExitEN(s, "pthread_attr_destroy");
```

## Thread Synchronization
Threads can use two tools to synchronize their actions:

* **Mutexes**:
  
  Mutexes allow threads to synchronize their use of a shared resource, so that,
  for example, one thread doesn’t try to access a shared variable at the same
  time as another thread is modifying it.

* **Condition Variables**:
  
  Condition variables perform a complementary task: they allow threads to
  inform each other that a shared variable (or other shared resource) has
  changed state.

### Mutexes
**Critical section** is section of code that accesses a shared resource and
whose execution should be atomic; that is, its execu- tion should not be
interrupted by another thread that simultaneously accesses the same shared
resource.

A mutex has two states: locked and unlocked. At any moment, at most one thread
may hold the lock on a mutex. Attempting to lock a mutex that is already locked
either blocks or fails with an error, depending on the method used to place the
lock.

When a thread locks a mutex, it becomes the owner of that mutex. Only the mutex
owner can unlock the mutex. This property improves the structure of code that
uses mutexes and also allows for some optimizations in the implementation of
mutexes. Because of this ownership property, the terms **acquire** and
**release** are sometimes used synonymously for lock and unlock.

In general, we employ a different mutex for each shared resource (which may
consist of multiple related variables).

#### Statically Allocated Mutexes
A mutex can either be allocated as a static variable or be created dynamically
at run time (for example, in a block of memory allocated via malloc()).

A mutex is a variable of the type ```pthread_mutex_t```. Before it can be used,
a mutex must always be initialized. For a statically allocated mutex, we can do
this by assigning it the value ```PTHREAD_MUTEX_INITIALIZER``` , as in the
following example:

```C
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
```

**Note**:

According to SUSv3, applying the operations to a copy of a mutex yields results
that are undefined. Mutex operations should always be performed only on the
original mutex that has been statically initialized 
using ```PTHREAD_MUTEX_INITIALIZER``` or dynamically initialized 
using ```pthread_mutex_init()```.

### Locking and Unlocking a Mutex
After initialization, a mutex is unlocked. To lock and unlock a mutex, we use
the ```pthread_mutex_lock()``` and ```pthread_mutex_unlock()``` functions.

```C
#include <pthread.h>

int pthread_mutex_lock(pthread_mutex_t * mutex );
int pthread_mutex_unlock(pthread_mutex_t * mutex );

Both return 0 on success, or a positive error number on error
```

If the mutex is currently unlocked, this call locks the mutex and returns
immediately. If the mutex is currently locked by another thread, 
then ```pthread_mutex_lock()``` blocks until the mutex is unlocked, at
which point it locks the mutex and returns.

If the calling thread itself has already locked the mutex given 
to ```pthread_mutex_lock()```, then, for the default type of mutex, one of two
implementation-defined possibilities may result: the thread deadlocks, blocked
trying to lock a mutex that it already owns, or the call fails, returning the
error EDEADLK . On Linux, the thread deadlocks by default.

The pthread_mutex_unlock() function unlocks a mutex previously locked by the
calling thread. It is an error to unlock a mutex that is not currently locked,
or to unlock a mutex that is locked by another thread.

If more than one other thread is waiting to acquire the mutex unlocked by a
call to pthread_mutex_unlock(), it is indeterminate which thread will succeed
in acquiring it.

#### Trylock

```C
#include <pthread.h>

int pthread_mutex_trylock(pthread_mutex_t *mutex);

Return 0 on success, or error EBUSY
```

#### Timedlock

```C
#include <pthread.h>
#include <time.h>

int pthread_mutex_timedlock(pthread_mutex_t *restrict mutex, 
                                const struct timespec *restrict abstime);

Return 0 on success, or error number for indicating the error.
```

If the mutex is already locked, the calling thread shall block until the mutex
becomes available. If the mutex cannot be locked without waiting for another
thread to unlock the mutex, this wait shall be terminated when the specified
timeout expires. The timeout shall expire when the absolute time specified by
abstime has already been passed at the time of the call. The timeout shall be
based on the ```CLOCK_REALTIME``` clock.

**Note**: Performance impact of using a mutex is not significant in most
applications. On Linux mutexes are implemented using **futexes** (an acronym
derived from fast user space mutexes).

### Mutex Deadlocks
Sometimes, a thread needs to simultaneously access two or more different shared
resources, each of which is governed by a separate mutex. When more than one
thread is locking the same set of mutexes, deadlock situations can arise.
[Figure-1](#Figure-1) shows an example of a deadlock in which each thread
successfully locks one mutex, and then tries to lock the mutex that the other
thread has already locked. Both threads will remain blocked indefinitely.

```
                   Thread A                        Thread B
         +--------------------------+    +---------------------------+
         |                          |    |                           |
         |                          |    |                           |
         |                          |    |                           |
         |--------------------------|    |---------------------------|
         |pthread_mutex_lock(mutex1)|    |pthread_mutex_lock(mutex2) |
         |--------------------------|    |---------------------------|
         |pthread_mutex_lock(mutex2)|    |pthread_mutex_lock(mutex1) |
         |--------------------------|    |---------------------------|
         |                          |    |                           |
         |                          |    |                           |
         |                          |    |                           |
         +--------------------------+    +---------------------------+ 
```
###### Figure-1

The simplest way to avoid such deadlocks is to define a mutex hierarchy. When
threads can lock the same set of mutexes, they should always lock them in the same
order. For example, in the scenario in [Figure-1](#Figure-1), the deadlock
could be avoided if the two threads always lock the mutexes in the order mutex1
followed by mutex2.  Sometimes, there is a logically obvious hierarchy of
mutexes. However, even if there isn’t, it may be possible to devise an
arbitrary hierarchical order that all threads should follow.

An alternative strategy that is less frequently used is **try, and then back
off.** In this strategy, a thread locks the first mutex using
```pthread_mutex_lock()```, and then locks the remaining mutexes using
```pthread_mutex_trylock()```. If any of the ```pthread_mutex_trylock()```
calls fails (with ```EBUSY```), then the thread releases all mutexes, and then tries
again, perhaps after a delay interval. This approach is less efficient than a
lock hierarchy, since multiple iterations may be required. On the other hand,
it can be more flexible, since it doesn’t require a rigid mutex hierarchy.

### Dynamically Initializing a Mutex
The static initializer value ```PTHREAD_MUTEX_INITIALIZER``` can be used only
for initializing a statically allocated mutex with default attributes. In all
other cases, we must dynamically initialize the mutex using
```pthread_mutex_init()``` .

```C
#include <pthread.h>

int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr);
int pthread_mutex_destroy(pthread_mutex_t *mutex);

Return 0 on success, or a positive error number on error
```

If attr is specified as NULL , then the mutex is assigned various default
attributes.

SUSv3 specifies that initializing an already initialized mutex results in
undefined behavior; we should not do this.

Among the cases where we must use ```pthread_mutex_init()``` rather than a
static initializer are the following:

* The mutex was dynamically allocated on the heap. For example, suppose that we
  create a dynamically allocated linked list of structures, and each structure
  in the list includes a pthread_mutex_t field that holds a mutex that is used
  to protect access to that structure.
* The mutex is an automatic variable allocated on the stack.
* We want to initialize a statically allocated mutex with attributes other than the
  defaults.

When an automatically or dynamically allocated mutex is no longer required, it
should be destroyed using ```pthread_mutex_destroy()``` . (It is not necessary
to call ```pthread_mutex_destroy()``` on a mutex that was statically
initialized using ```PTHREAD_MUTEX_INITIALIZER``` .)

It is safe to destroy a mutex only when it is unlocked, and no thread will
subsequently try to lock it. If the mutex resides in a region of dynamically
allocated mem- ory, then it should be destroyed before freeing that memory
region. An automatically allocated mutex should be destroyed before its host
function returns.  

A mutex that has been destroyed with ```pthread_mutex_destroy()``` can
subsequently be reinitialized by ```pthread_mutex_init()``` .

### Mutex Attributes
As noted earlier, the ```pthread_mutex_init()``` attr argument can be used to
specify a ```pthread_mutexattr_t``` object that defines the attributes of a
mutex. Various Pthreads functions can be used to initialize and retrieve the
attributes in a ```pthread_mutexattr_t``` object. We won’t go into all of the
details of mutex attributes or show the prototypes of the various functions
that can be used to initialize the attributes in a ```pthread_mutexattr_t```
object. However, we’ll describe one of the attributes that can be set for a
mutex: its type.

#### Mutex Types
Behavior of mutexes:
* A single thread may not lock the same mutex twice.
* A thread may not unlock a mutex that it doesn’t currently own (i.e., that it
  did not lock).
* A thread may not unlock a mutex that is not currently locked.

Precisely what happens in each of these cases depends on the type of the mutex.

SUSv3 defines the following mutex types:

* **PTHREAD_MUTEX_NORMAL**

  (Self-)deadlock detection is not provided for this type of mutex. If a thread
  tries to lock a mutex that it has already locked, then deadlock results.
  Unlocking a mutex that is not locked or that is locked by another thread
  produces undefined results. (On Linux, both of these operations succeed for
  this mutex type.)

* **PTHREAD_MUTEX_ERRORCHECK**

  Error checking is performed on all operations. All three of the above
  scenarios cause the relevant Pthreads function to return an error. This type
  of mutex is typically slower than a normal mutex, but can be useful as a
  debugging tool to discover where an application is violating the rules about
  how a mutex should be used.

* **PTHREAD_MUTEX_RECURSIVE**
  
  A recursive mutex maintains the concept of a lock count. When a thread first
  acquires the mutex, the lock count is set to 1. Each subsequent lock
  operation by the same thread increments the lock count, and each unlock
  operation decrements the count. The mutex is released (i.e., made avail- able
  for other threads to acquire) only when the lock count falls to 0.  Unlocking
  an unlocked mutex fails, as does unlocking a mutex that is currently locked
  by another thread.

In addition to the above mutex types, SUSv3 defines 
the ```PTHREAD_MUTEX_DEFAULT``` type, which is the default type of mutex if we
use ```PTHREAD_MUTEX_INITIALIZER``` or specify attr as ```NULL``` in a call 
to ```pthread_mutex_init()```. The behavior of this mutex type is deliberately
undefined in all three of the scenarios described at the start of this section,
which allows maximum flexibility for efficient implementation of mutexes. On
Linux, a ```PTHREAD_MUTEX_DEFAULT``` mutex behaves like 
a ```PTHREAD_MUTEX_NORMAL``` mutex.

```C
pthread_mutex_t mtx;
pthread_mutexattr_t mtxAttr;
int s, type;

s = pthread_mutexattr_init(&mtxAttr);
if (0 != s) {
        errExitEN(s, "pthread_mutexattr_init");
}

s = pthread_mutexattr_settype(&mtxAttr, PTHREAD_MUTEX_ERRORCHECK);
if (0 != s) {
        errExitEN(s, "pthread_mutexattr_settype");
}

s = pthread_mutex_init(mtx, &mtxAttr);
if (0 != s) {
        errExitEN(s, "pthread_mutex_init");
}

s = pthread_mutexattr_destroy(&mtxAttr); /* No longer needed */
if (0 != s) {
        errExitEN(s, "pthread_mutexattr_destroy");
}
```

### Signaling Changes of State: Condition Variables
A mutex prevents multiple threads from accessing a shared variable at the same
time. A condition variable allows one thread to inform other threads about
changes in the state of a shared variable (or other shared resource) and allows
the other threads to wait (block) for such notification.

```C
#include <time.h>
#include <pthread.h>
#include "general_hdr.h"

#define LOOP for(;;)

#define BRK_LOOP_IF(condition) \
        if (condition) break;

#define ITERATE(iterator, number) \
        for (int iterator = 0; iterator < number; iterator++)

#define FOREACH_STR(list, item) \
        for (char **item = list; *item; item++)

#define EXIT_IF_ERR(condition, error, msg) \
        if (condition) { \
                errExitEN(error, msg); \
        }

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

static int avail = 0;

static void *thread_main(void *arg)
{
        int result;
        int cnt = atoi((char *) arg);

        ITERATE(i, cnt) {
                sleep(1);

                /* Code to produce a unit ommitted */
                result = pthread_mutex_lock(&mtx);
                EXIT_IF_ERR((0 != result), result, "Thread mutex lock failed");

                avail++;

                result = pthread_mutex_unlock(&mtx);
                EXIT_IF_ERR((0 != result), result, "Thread mutex unlock failed");
        }

        return NULL;
}

int main(int argc, char *argv[])
{
        int result;
        /* Total number of units that all threads will produce */
        int totRequired;
        /* Total units so far consumed */
        int numConsumed;
        time_t t;
        Boolean done;
        pthread_t tid;

        t = time(NULL);

        /* Create all threads */
        totRequired = 0;

        FOREACH_STR(&argv[1], arg) {
                totRequired += atoi(*arg);

                result = pthread_create(&tid, NULL, thread_main, *arg);
                EXIT_IF_ERR((0 != result), result, "Thread creation failed");
        }

        /* Use a polling loop to check for available units */
        numConsumed = 0;
        done = FALSE;

        LOOP {
                result = pthread_mutex_lock(&mtx);
                EXIT_IF_ERR((0 != result), result, "Thread mutex lock failed");

                /* Consume all available units */
                while (0 < avail) {
                        /* Do something with produced unit */
                        numConsumed++;
                        avail--;

                        printf("Time = %ld: number of consumed = %d\n",
                                        (long) (time(NULL) - t),
                                        numConsumed);
                        done = (numConsumed >= totRequired);
                }

                result = pthread_mutex_unlock(&mtx);
                EXIT_IF_ERR((0 != result), result, "Thread mutex unlock failed");

                BRK_LOOP_IF(done);
        }

        exit(EXIT_SUCCESS);
}
```

A previous simple example above, didnt use condition variables. The reason that
didn't use condition variables is to demonstrate why they are useful.
The above code works, but it wastes CPU time, because the main thread
continually loops, checking the state of the variable ```avail```.

A condition variable is always used in conjunction with a mutex. The mutex
provides mutual exclusion for accessing the shared variable, while the
condition variable is used to signal changes in the variable’s state. (The use
of the term signal here has nothing to do with the signals, rather, it is used
in the sense of indicate.)

#### Statically Allocated Condition Variables
As with mutexes, condition variables can be allocated statically or
dynamically.

A condition variable has the type pthread_cond_t. As with a mutex, a condition
variable must be initialized before use. For a statically allocated condition
variable, this is done by assigning it the value PTHREAD_COND_INITIALIZER , as
in the following example:

```C
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
```
