# Understanding extern keyword in C

## Introduction
Declaration of a variable/function simply declares that the variable/function
exists somewhere in the program but the memory is not allocated for them.

Definition, when we define a variable/function, apart from the role of the
declaration, it also allocates memory for that variable/function. Therefore, we
can think of definition as a superset of the declaration.

From this explanation, it should be obvious that a variable/function  can be
declared any number of times but it can be defined only once.

## Extern function
By default, the declaration and definition of a C function have “extern”
prepended with them. It means even though we don’t use extern with the
declaration/definition of C functions, it is present there. For example, when
we write.

```C
int foo(int arg1, char arg2);
```

There’s an extern present at the beginning which is hidden and the compiler
treats it as below.

```C
extern int foo(int arg1, char arg2);
```

Same is the case with the definition of a C function (Definition of a C
function means writing the body of the function). Therefore whenever we define
a C function, an extern is present there in the beginning of the function
definition. Since the declaration can be done any number of times and
definition can be done only once, we can notice that declaration of a function
can be added in several C/H files or in a single C/H file several times. But we
notice the actual definition of the function only once (i.e. in one file only).
And as the extern extends the visibility to the whole program, the functions
can be used (called) anywhere in any of the files of the whole program provided
the declaration of the function is known. (By knowing the declaration of the
function, C compiler knows that the definition of the function exists and it
goes ahead to compile the program). So that’s all about extern with C
functions.

## Extern variables
```C
extern int var;
```
Here, an integer type variable called var has been declared (remember no
definition i.e. no memory allocation for var so far). And we can do this
declaration as many times as needed. 

Now how would you define a variable? Now I agree that it is the most trivial
question in programming and the answer is as follows.

```C
int var;
```

Here, an integer type variable called var has been declared as well as defined.
(remember that definition is the superset of declaration). Here the memory for
var is also allocated. Now here comes the surprise, when we declared/defined a
C function, we saw that an extern was present by default. While defining a
function, we can prepend it with extern without any issues. But it is not the
case with C variables. If we put the presence of extern in a variable as
default then the memory for them will not be allocated ever, they will be
declared only. Therefore, we put extern explicitly for C variables when we want
to declare them without defining them. Also, as the extern extends the
visibility to the whole program, by using the extern keyword with a variable we
can use the variables anywhere in the program provided we know the declaration
of them and the variable is defined somewhere.
