# Android Development with Kivy

## Table of Contents
1. [Introduction](#Introduction)
1. [Basic of Kivy](#Basic-of-Kivy)

## Introduction
A Kivy language file creates widgets, and defines their behavior. These have
the extension .kv. They help separate the logic, from the presentation.
Howevver, it is possible to do everything with Python, at the expense of having
more code, and harder to follow logic.

A main Kivy file is always searched. It has the same name as the App subclass,
except there is no ending App in it, and everything is lowercase. Additional kv
files may be loaded, with the help of the Builder class.

Example:
* **App name**: HelloWorldApp
* **Main kivy file**: helloworld.kv
* Addition kivy files may be loaded with the help of the Builder class.

In the Python file, note we are importing only one user interface element,
BoxLayout. We will create a class which will inherit the functionality of
BoxLayout. This class is called the root class. The reason we are not importing
a Button class, even though we display buttons, is that all button code has
been moved to the Kivy file and thus, the Python file does not need to have the
Button name, in its namespace.

```Python
#helloworld.py
from kivy.uix.boxlayout import BoxLayout
from kivy.app import App #This statement must import in every app
import time

class MyBox(BoxLayout):
    """
    This is the root class, which inherits from the BoxLayout class.
    This class has two functions, which will be called from the Kivy file,
    after a button is clicked.
    Note: The word return is not necessary.
    """
    def hello(self, *args):
        print("--> Hello at time %s" % time.ctime())
        return
    def world(self, *args):
        print("--> World! at time %s" % time.ctime())
        return

class HelloWorldApp(App):
    """
    Application class.
    The name of class should always end with App.
    and this class should always inherit from App class.
    we use build method of App class and overwrite it.
    """
    def build(self):
        return MyBox()

if __name__=="__main__":
    myApp = HelloWorldApp()
    print("The name of your app is %s" % myApp.name)
    myApp.run()
```

The Kivy file defines a rule for our root class. All rules are in the angle
brackets. Since MyBox class implements the functionality of BoxLayout, the
Buttons are arranged horizontally, the default orientation. Buttons have
different properties. We define 2 here, the ```text``` and ```on_press```. The
text indicate what is displayed. The ```on_press``` is called whenever a click
occurs, on the particular widget. The right-hand side calls either the
```hello()``` or the ```word()``` function of the root class. In fact, the
right-hand side could be any Python code.

```Python
<MyBox>:
        Button:
                text: 'Hello'
                on_press: root.hello(*args)
        Button:
                text: 'World!'
                on_press: root.world(*args)
```

## Basic of Kivy
This simple program show basic of Kivy, in simple manner.

```Python
#!/usr/bin/python
import kivy
kivy.require('1.10.1')

from kivy.app import App
from kivy.uix.label import Label

class MyDemoApp(App):
    def build(self):
        return Label(text = "I am Programmer!!")

if __name__ == "__main__":
    MyDemoApp().run()
```
