 DPDK User Guide

## Table of Contets
1. [Introduction](#Introduction)
    1. [Documentation Roadmap](#Documentation-Roadmap)
1. [System Requirements](#System-Requirements)
    1. [BIOS setting Prerequisite on x86](#BIOS-setting-Prerequisite-on-x86)
    1. [Compilation of the DPDK](#Compilation-of-the-DPDK)
    1. [Running DPDK Applications](#Running-DPDK-Applications)
        1. [System Software](#System-Software)
        1. [Use of Huge Pages in the Linux Environment](#Use-of-Huge-Pages-in-the-Linux-Environment)
        1. [Reserving Hugepages for DPDK Use](#Reserving-Hugepages-for-DPDK-Use)
            1. [Alternative](#Alternative)
            1. [Configure hugepages in Kernel](#Configure-hugepages-in-Kernel)
        1. [Using Hugepages with the DPDK](#Using-Hugepages-with-the-DPDK)
1. [Compiling the DPDK Target from Source](#Compiling-the-DPDK-Target-from-Source)
    1. [Install the DPDK and Browse Source](#Install-the-DPDK-and-Browse-Source)
    1. [Installation of DPDK Target Environments](#Installation-of-DPDK-Target-Environments)
    1. [Browsing the Installed DPDK Environment Target](#Browsing-the-Installed-DPDK-Environment-Target)
1. [Linux Drivers](#Linux-Drivers)
    1. [UIO](#UIO)
    1. [VFIO](#VFIO)
    1. [Bifurcated Driver](#Bifurcated-Driver)
    1. [Binding and Unbinding Network Ports to/from the Kernel Modules](#Binding-and-Unbinding-Network-Ports-to-from-the-Kernel-Modules)
1. [Compiling and Running Sample Applications](#Compiling-and-Running-Sample-Applications) 
    1. [Compiling a Sample Application](#Compiling-a-Sample-Application)
    1. [Running a Sample Application](#Running-a-Sample-Application)

## Introduction
This document contains instructions for installing and configuring the Data
Plane Development Kit (DPDK) software. The document describes how to compile
and run a DPDK application in a linux application (linuxapp) environment,
without going deeply into detail.

### Documentation Roadmap
The following is a list of DPDK documents in the suggested reading order:
* Release Note (**Please google it**)
* User Guide
* Programmer's Guide
    * The software architecture and how to use it specifically in a Linux
      application environment.
    * The content of the DPDK, the build system and guidelines for porintg
      application.
    * Optimizations used in the software and those that should be
      considered for new development.
* API Reference
* Sample Application User Guide

## System Requirements
This chapter describes the packages required to compile the DPDK.

### BIOS setting Prerequisite on x86
For the majority of platforms, no special BIOS settings are needed to use basic
DPDK functionality. However, for additional **High Precision Event Timer**
(HPET) timer and power management functionality, and high performance of small
packets, BIOS setting changes may be needed.

**NOTE**: The HPET is a hardware timer used in personal computers.

**NOTE**: If UEFI secure boot is enabled, the Linux kernel may disallow the use
of UIO on the system. Therefore, devices for use by DPDK should be bound to the
```vfio-pci``` kernel module rather than ```igb_uio``` or ```uio_pci_generic```.

### Compilation of the DPDK
Required Tools and Libraries:
* GNU make.
* coreutils: cmp, sed, grep, arch, etc.
* gcc: versions 4.9 or later is recommended. on some distributions, some
  specific compiler flags and linker flags are enabled by default and affect
  performance (```-fstack-protector```, for example). Please refer to the
  documentation of your distribution and to ```gcc -dumpspecs```.
* libc headers, often packaged as ```gcc-multilib``` (```glibc-devel.i686``` /
  ```glibc6-dev-i386```; ```glibc-devel.x86_64``` / ```libc6-dev``` for 64-bit
  compilation on Intel architecture; ```glibc-devel.ppc64``` for 64 bit IBM
  Power architecture;)
* Linux kernel headers or sources required to build kernel modules.
  (```kernel-devel.x86_64```; ```kernel-devel.ppc64```)
* Additional packages required for 32-bit compilation on 64-bit systems are:
  * ```glibc.i686```, ```libgcc.i686```, ```libstdc++.i686``` and
    ```glibc-devel.i686``` for Intel ```i686/x86_64```;
  * ```glibc.ppc64```, ```libgcc.ppc64```, ```libstdc++.ppc64``` and
    ```glibc-devel.ppc64``` for IBM ```ppc_64```;
* ```libnuma-devel``` - library for handling NUMA (Non Uniform Memory Access).
* Python, version 2.7+ or 3.2+, to use various helper scripts included in the
  DPDK package.
* libpcap headers and libraries (```libpcap-devel```) to compile and use the
  ```libpcap-based``` pool-mode driver. This driver is disabled by default and
  can be enabled by setting ```CONFIG_RTE_LIBRTE_PMD_PCAP=y```

### Running DPDK Applications
To run an DPDK application, some customization may be required on the target
machine.

#### System Software
Required:
* Kernel version >= 3.2
* glibc >= 2.7
* Kernel configuration. These options should be enabled in kernel
  configuration:
  * HUGETLBFS
  * PROC_PAGE_MONITOR
  * HPET
  * HPET_MMAP

#### Use of Huge Pages in the Linux Environment
Before we start talking about how to use of huge pages, let's talk about what
is it.

What is the Huge Page in Linux? Huge pages are helpful in virtual memory
management in Linux system. As name suggests, they help is managing huge size
pages in memory in addition to standard 4KB page size. You can define as huge
as 1GB page size using huge pages.

During system boot, you reserve your memory portion with huge pages for your
application. This memory portion i.e. these memory occupied by huge pages is
never swapped out of memory. It will stick there until you change your
configuration.

Why use huge page? In virtual memory management, kernel maintains table in
which it has mapping of virtual memory address to physical address. For every
page transaction, kernel needs to load related mapping. If you have small size
pages then you need to load more numbers of pages resulting kernel to load more
mapping tables. This decreases performance.

Using huge pages, means you will need fewer pages. This decreases number of
mapping tables to load by kernel to great extent. This increases your kernel
level performance which ultimately benefits your application.

In short, by enabling huge pages, system has fewer page tables to deal with and
hence less overhead to access / maintain them!

Hugepage support is required for the large memory pool allocation used for
packet buffers (the HUGETLBFS option must be enabled in the running kernel as
indicated the previous section). By using hugepage allocations, performance is
increased since fewer pages are needed, and therefore less Translation
Lookaside Buffers (TLBs, high speed translation caches), which reduce the time
it takes to translate a virtual page address to a physical page address.
Without hugepages, high TLB miss rates would occur with the standard 4k page
size, slowing performance.

#### Reserving Hugepages for DPDK Use
The allocation of hugepages should be done at boot time or as soon as possible
after system boot to prevent memory from being fragmented in physical memory.
To reserve hugepages at boot time, a parameter is passed to the Linux kernel on
the kernel command line.

For 2 MB pages, just pass the hugepages option to the kernel. For example, to
reserve 1024 pages of 2 MB, use:

```Bash
hugepages=1024
```

For other hugepage sizes, for example 1G pages, the size must be specified
explicitly and can also be optionally set as the default hugepage size for the
system. For example, to reserve 4G of hugepage memory in the form of four 1G
pages, the following options should be passed to the kernel:

```Bash
default_hugepagesz=1G hugepagesz=1G hugepages=4
```

**Note**: The hugepage sizes that a CPU supports can be determined from the CPU
flags on Intel architecture. If pse exists, 2M hugepages are supported; if
pdpe1gb exists, 1G hugepages are supported. On IBM Power architecture, the
supported hugepage sizes are 16MB and 16GB.

**Note**: For 64-bit applications, it is recommended to use 1 GB hugepages if
the platform supports them.

In the case of a dual-socket NUMA system, the number of hugepages reserved at
boot time is generally divided equally between the two sockets (on the
assumption that sufficient memory is present on both sockets).

See the Documentation/kernel-parameters.txt file in your Linux source tree for
further details of these and other kernel options.

##### Alternative
For 2 MB pages, there is also the option of allocating hugepages after the
system has booted. This is done by echoing the number of hugepages required to
a nr_hugepages file in the ```/sys/devices/``` directory. For a single-node
system, the command to use is as follows (assuming that 1024 pages are
required):

```Bash
echo 1024 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
```

On a NUMA machine, pages should be allocated explicitly on separate nodes:

```Bash
echo 1024 > /sys/devices/system/node/node0/hugepages/hugepages-2048kB/nr_hugepages
echo 1024 > /sys/devices/system/node/node1/hugepages/hugepages-2048kB/nr_hugepages
```

**Note**: For 1G pages, it is not possible to reserve the hugepage memory after
the system has booted.On IBM POWER system, the nr_overcommit_hugepages should
be set to the same value as nr_hugepages. For example, if the required page
number is 128, the following commands are used:

```Bash
echo 128 > /sys/kernel/mm/hugepages/hugepages-16384kB/nr_hugepages
echo 128 > /sys/kernel/mm/hugepages/hugepages-16384kB/nr_overcommit_hugepages
```

##### Configure hugepages in Kernel
Add below value in ```/etc/sysctl.conf``` and reload configuration by issuing
```sysctl -p``` command.

```Bash
vm.nr_hugepages=126
```

Notice that we added 2 extra pages in kernel since we want to keep couple of
pages spare than actual required number.

Now, huge pages has been configured in kernel but to allow your application to
use them you need to increase memory limits as well. New memory limit should be
126 pages x 2 MB each = 252 MB i.e. 258048 KB.

You need to edit below settings in ```/etc/securitylimits.conf```

```Bash
soft memlock 258048
hard memlock 258048
```

Now reastart your OS. For checking current huge pages details.

```Bash
# grep Huge /proc/meminfo
```

#### Using Hugepages with the DPDK
Once the hugepage memory is reserved, to make the memory available for DPDK
use, perform the following steps:

```Bash
mkdir /mnt/huge
mount -t hugetlbfs nodev /mnt/huge
```

The mount point can be made permanent across reboots, by adding the following
line to the /etc/fstab file:

```Bash
nodev /mnt/huge hugetlbfs defaults 0 0
```

For 1GB pages, the page size must be specified as a mount option:

```Bash
nodev /mnt/huge_1GB hugetlbfs pagesize=1GB 0 0
```

## Compiling the DPDK Target from Source
In this section we describe how to compile DPDK from source.

### Install the DPDK and Browse Source

```Bash
tar xJf dpdk-<version>.tar.xz
cd dpdk-<version>
```
The DPDK is composed of several directories:
* lib: Source code of DPDK libraries
* drivers: Source code of DPDK poll-mode drivers
* app: Source code of DPDK applications (automatic tests)
* examples: Source code of DPDK application examples
* config, buildtools, mk: Framework-related makefiles, scripts and configuration


### Installation of DPDK Target Environments
The format of a DPDK target is:

```ASCII
ARCH-MACHINE-EXECENV-TOOLCHAIN
```

where:
* ```ARCH``` can be: ```i686```, ```x86_64```, ```ppc_64```, ```arm64```
* ```MACHINE``` can be: ```native```, ```power8```, ```armv8a```
* ```EXECENV``` can be: ```linuxapp```, ```bsdapp```
* ```TOOLCHAIN``` can be: ```gcc```, ```icc```

The targets to be installed depend on the 32-bit and/or 64-bit packages and
compilers installed on the host. Available targets can be found in the
DPDK/config directory. The defconfig_ prefix should not be used.

**Note**: Configuration files are provided with the ```RTE_MACHINE```
optimization level set. Within the configuration files, the ```RTE_MACHINE```
configuration value is set to native, which means that the compiled software is
tuned for the platform on which it is built.

When using the Intel® C++ Compiler (icc), one of the following commands should
be invoked for 64-bit or 32-bit use respectively. Notice that the shell scripts
update the ```$PATH``` variable and therefore should not be performed in the
same session. Also, verify the compiler’s installation directory since the path
may be different:

```Bash
source /opt/intel/bin/iccvars.sh intel64
source /opt/intel/bin/iccvars.sh ia32
```

To install and make targets, use the ```make install T=<target>``` command in
the top-level DPDK directory.

For example, to compile a 64-bit target using icc, run:

```Bash
make install T=x86_64-native-linuxapp-icc
```

To compile a 32-bit build using gcc, the make command should be:

```Bash
make install T=i686-native-linuxapp-gcc
```

To prepare a target without building it, for example, if the configuration
changes need to be made before compilation, use the ```make config T=<target>``` 
command:

```Bash
make config T=x86_64-native-linuxapp-gcc
```
**Warning**: Any kernel modules to be used, e.g. ```igb_uio```, ```kni```, must be
compiled with the same kernel as the one running on the target. If the DPDK is
not being built on the target machine, the ```RTE_KERNELDIR``` environment
variable should be used to point the compilation at a copy of the kernel
version to be used on the target machine.

Once the target environment is created, the user may move to the target
environment directory and continue to make code changes and re-compile. The
user may also make modifications to the compile-time DPDK configuration by
editing the .config file in the build directory. (This is a build-local copy of
the defconfig file from the top- level config directory).

```Bash
cd x86_64-native-linuxapp-gcc
vi .config
make
```

In addition, the make clean command can be used to remove any existing compiled
files for a subsequent full, clean rebuild of the code.

### Browsing the Installed DPDK Environment Target
Once a target is created it contains all libraries, including poll-mode
drivers, and header files for the DPDK environment that are required to build
customer applications. In addition, the test and testpmd applications are built
under the build/app directory, which may be used for testing. A kmod directory
is also present that contains kernel modules which may be loaded if needed.

## Linux Drivers
Different Poll Mode Drivers (PMDs) may require different kernel drivers in
order to work properly. Depends on the PMD being used, a corresponding kernel
driver should be load and bind to the network ports.

### UIO
UIO is a small kernel module to set up the device, map device memory to
user-space and register interrupts. In many cases, the standard
```uio_pci_generic``` module included in the Linux kernel can provide the uio
capability. This module can be loaded using the command:

```Bash
sudo modprobe uio_pci_generic
```

**Note**: ```uio_pci_generic``` module doesn't support the creation of virtual
fucntions.

As an alternative to the ```uio_pci_generic```, the DPDK also includes the
igb_uio module which can be found in the kmod subdirectory referred to above.
It can be loaded as shown below:

```Bash
sudo modprobe uio
sudo insmod kmod/igb_uio.ko
```

**Note**: For some devices which lack support for legacy interrupts, e.g.
virtual function (VF) devices, the ```igb_uio``` module may be needed in place
of ```uio_pci_generic```.

**Note**: If UEFI secure boot is enabled, the Linux kernel may disallow the use
of UIO on the system. Therefore, devices for use by DPDK should be bound to the
vfio-pci kernel module rather than igb_uio or uio_pci_generic.

### VFIO
A more robust and secure driver in compare to the ```UIO```, relying on IOMMU
protection. To make use of VFIO, the ```vfio-pci``` module must be loaded:

```Bash
sudo modprobe vfio-pci
```

Note that in order to use VFIO, your kernel must support it. VFIO kernel
modules have been included in the Linux kernel since version 3.6.0 and are
usually present by default, however please consult your distributions
documentation to make sure that is the case.

Also, to use VFIO, both kernel and BIOS must support and be configured to use
IO virtualization (such as Intel® VT-d).

**Note**: ```vfio-pci``` module doesn't support the creation of virtual
functions.

For proper operation of VFIO when running DPDK applications as a non-privileged
user, correct permissions should also be set up. This can be done by using the
DPDK setup script (called dpdk-setup.sh and located in the usertools
directory).

**Note**: VFIO can be used without IOMMU. While this is just as unsafe as using
UIO. it does make it possible for the user to keep the degree of device access
and programming that VFIO has in situations where IOMMU is not available.

```ASCII
     +-----------------------------------------------------+ 
     |Physical Address                                     | 
     |    +-------------------------------------------+    | 
     |    |                                           |    | 
     |    |                Main Memory                |    | 
     |    |                                           |    | 
     |    +-------------------------------------------+    | 
     |             ^                          ^            | 
     |             |                          |            | 
     | +----------------------+   +----------------------+ | 
     |-|         IOMMU        |---|          MMU         |-| 
     | +----------------------+ | +----------------------+ | 
     |Device       ^            |Virtual      ^            | 
     |Address      |            |Address      |            | 
     | +----------------------+ | +----------------------+ | 
     | |                      | | |                      | | 
     | |        Device        | | |          CPU         | | 
     | |                      | | |                      | | 
     | |                      | | |                      | | 
     | +----------------------+ | +----------------------+ | 
     +-----------------------------------------------------+ 
```

### Bifurcated Driver
Poll mode drivers (PMDs) which use the bifurcated driver co-exists with the
device kernel driver. On such model the NIC is controlled by the kernel, while
the data path is performed by the PMD directly on top of the device.

Such model has the following benefits:
* It is secure and robust, as the memory management and isolation is done by
  the kernel.
* It enables the user to use legacy linux tools such as ethtool or ifconfig
  while running DPDK application on the same network ports.
* It enables the DPDK application to filter only part of the traffic, While the
  rest will be directed and handled by the kernel driver.

### Binding and Unbinding Network Ports to-from the Kernel Modules
**Note**: PMDs Which use the bifurcated driver should not be unbind from their
kernel drivers. this section is for PMDs which use the UIO or VFIO drivers.

As of release 1.4, DPDK applications no longer automatically unbind all
supported network ports from the kernel driver in use. Instead, in case the PMD
being used use the UIO or VFIO drivers, all ports that are to be used by an
DPDK application must be bound to the ```uio_pci_generic```, ```igb_uio``` or
```vfio-pci``` module before the application is run. For such PMDs, any network
ports under Linux* control will be ignored and cannot be used by the
application.

To bind ports to the ```uio_pci_generic```, ```igb_uio``` or ```vfio-pci```
module for DPDK use, and then subsequently return ports to Linux* control, a
utility script called dpdk-devbind.py is provided in the usertools
subdirectory. This utility can be used to provide a view of the current state
of the network ports on the system, and to bind and unbind those ports from the
different kernel modules, including the uio and vfio modules. The following are
some examples of how the script can be used. A full description of the script
and its parameters can be obtained by calling the script with the ```--help```
or ```--usage``` options. Note that the uio or vfio kernel modules to be used,
should be loaded into the kernel before running the ```dpdk-devbind.py```
script.

**Warning**: Due to the way VFIO works, there are certain limitations to which
devices can be used with VFIO. Mainly it comes down to how IOMMU groups work.
Any Virtual Function device can be used with VFIO on its own, but physical
devices will require either all ports bound to VFIO, or some of them bound to
VFIO while others not being bound to anything at all.

If your device is behind a PCI-to-PCI bridge, the bridge will then be part of
the IOMMU group in which your device is in. Therefore, the bridge driver should
also be unbound from the bridge PCI device for VFIO to work with devices behind
the bridge.

**Warning**: While any user can run the dpdk-devbind.py script to view the
status of the network ports, binding or unbinding network ports requires root
privileges.

To see the status of all network ports on the system:

```Bash
./usertools/dpdk-devbind.py --status

Network devices using DPDK-compatible driver
============================================
0000:82:00.0 '82599EB 10-GbE NIC' drv=uio_pci_generic unused=ixgbe
0000:82:00.1 '82599EB 10-GbE NIC' drv=uio_pci_generic unused=ixgbe

Network devices using kernel driver
===================================
0000:04:00.0 'I350 1-GbE NIC' if=em0  drv=igb unused=uio_pci_generic *Active*
0000:04:00.1 'I350 1-GbE NIC' if=eth1 drv=igb unused=uio_pci_generic
0000:04:00.2 'I350 1-GbE NIC' if=eth2 drv=igb unused=uio_pci_generic
0000:04:00.3 'I350 1-GbE NIC' if=eth3 drv=igb unused=uio_pci_generic

Other network devices
=====================
<none>
```

To bind device ```eth1```, ``04:00.1``, to the ```uio_pci_generic``` driver:

```Bash
./usertools/dpdk-devbind.py --bind=uio_pci_generic 04:00.1
```

or, alternatively,

```Bash
./usertools/dpdk-devbind.py --bind=uio_pci_generic eth1
```

To restore device ```82:00.0``` to its original kernel binding:

```Bash
./usertools/dpdk-devbind.py --bind=ixgbe 82:00.0
```

## Compiling and Running Sample Applications
In this section we are going to describes how to compile and run applications
in an DPDK environment. It also provides a pointer to where sample applications
are stored.

### Compiling a Sample Application
Once an DPDK target environment directory has been created (such as
```x86_64-native-linuxapp-gcc```), it contains all libraries and header files
required to build an application.

When **compiling** an application in the Linux* environment on the DPDK, the
following variables must be exported:

* ```RTE_SDK```: Points to the DPDK installation directory.
* ```RTE_TARGET```: Points to the DPDK target environment directory.


The following is an example of creating the ```helloworld``` application, which
runs in the DPDK Linux environment. This example may be found in the
```${RTE_SDK}/examples``` directory.

The directory contains the ```main.c``` file. This file, when combined with the
libraries in the DPDK target environment, calls the various functions to
initialize the DPDK environment, then launches an entry point (dispatch
application) for each core to be utilized. By default, the binary is generated
in the build directory.

```C
/*
 * file         main.c
 * brief        Sample Application Using DPDK
 * Copyright(c) 2010-2014 Intel Corporation
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <sys/queue.h>

#include <rte_memory.h>
#include <rte_launch.h>
#include <rte_eal.h>
#include <rte_per_lcore.h>
#include <rte_lcore.h>
#include <rte_debug.h>

/**
 * Function that run on every available lcore.
 */
static int lcore_hello(__attribute__((unused)) void *arg)
{
	unsigned lcore_id;
	lcore_id = rte_lcore_id();
	printf("hello from core %u\n", lcore_id);
	return 0;
}

/**
 * Main function.
 * 
 * @param int argc, char *argv[]
 * @return EXIT_SUCCESS on successfull termination or EXIT_FAILURE otherwise.
 */
int main(int argc, char *argv[])
{
        int ret;
        unsigned lcore_id;

        /**
         * The Environment Abstraction Layer (EAL) is responsible for gainning
         * access to low-level resources such as hardware and memory space. It
         * provides a generic interface that hides the envrionment specifics
         * from the applications and libraries. It is the responsibility of the
         * initialization routine to decide how to allocate these resources.
         *
         * This call finishes the initialization process that was started
         * before main() is called.
         *
         * This function is to be executed on the MASTER lcore only, as soon as
         * possible in the application's main() function.
         *
         * The function finishes the initialization process before main() is
         * called. It puts the SLAVE lcores in the WAIT state.
         *
         * @param argc, argv
         * @return
         *     On Sucess, the number of parsed arguments, which is greater or
         *     equal to zero. all arguments may have been modified by this
         *     function call and shoud not be further interpreted by the
         *     application.
         *     On Failure, -1 and rte_errno is set to value indicating the
         *     cause for failure. In some instances, the application will need
         *     to be restarted as part of clearing the issue.
         */
        ret = rte_eal_init(argc, argv);
        if (0 > ret) {
                /**
                 * Provide notification of a critical non-recoverable error and
                 * terminate execution abnormally.
                 * @param printf-like argument string.
                 * @return Dumps the stack and calls abort() resulting in a
                 * core dump if enabled.
                 */
                rte_panic("Cannot init EAL\n");
        }

        /**
         * Macro to brwse all running lcores except the master lcore.
         */
        RTE_LCORE_FOREACH_SLAVE(lcore_id) {
                /**
                 * Launch a function on another lcore. These function to be
                 * executed on the master lcore only,
                 * @param lcore_function_t *function. The function to be
                 * called.
                 * @param void *arg. The argument for the fuction.
                 * @param unsigned slave_id. The identifier of the lcore on
                 * which the function should be executed.
                 *
                 * @return 0 on Success, -EBUSY on Failure. -EBUSY indicate
                 * remote lcore is not in a WAIT state.
                 */
                rte_eal_remote_launch(lcore_hello, NULL, lcore_id);
        }

        lcore_hello(NULL);

        /**
         * Wait until an lcore finishes its job. This function to be executed
         * on the MASTER lcore only
         * @param slave_id. The identifier of the lcore.
         *
         * @return 0 on success. The lcore identified by the slave_id is in a
         * wait state. Otherwise the value that was returned by the previous
         * remote launch function call if the lcore identified by the slave_id
         * was in a FINISHED or RUNNING state. In this case, it changes the
         * state of the lcore to WAIT. 
         */
        rte_eal_mp_wait_lcore();

        return EXIT_SUCCESS;
}
```

### Running a Sample Application
Before running the application make sure:
* Hugepages setup is done
* Any Kernel Driver being used is loaded
* In case needed, ports being used by the application should be bound to the
  corresponding Kernel driver.

The application is linked with the DPDK target environment’s Environmental
Abstraction Layer (EAL) library, which provides some options that are generic
to every DPDK application.

The following is the list of options that can be given to the EAL:
```Bash
./rte-app [-c COREMASK | -l CORELIST] [-n NUM] [-b <domain:bus:devid.func>] [--socket-mem=MB,...] \
          [-d LIB.so|DIR] [-m MB] [-r NUM] [-v] [--file-prefix] [--proc-type <primary|secondary|auto>]
```

The EAL options are as follows:
* ```-c COREMASK``` or ```-l CORELIST```:
