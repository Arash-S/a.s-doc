# The Linux Boot Process

## Table of contents
1. [Introduction](#Introduction)

## List of Figures
1. [Over view of the Linux Boot Process](#F_INTROOverViewOfLinuxBootProcess)

# Introduction
In the early days, bootstrapping a computer meant feeding a paper tape
containing a boot program or manually loading a boot program using the
front panel address/data/control switches. Today's computers are equipped
with facilities to simplify the boot process, but that doesn't necessarily
make it simple.

Let's start with a high-level view of Linux boot so you can see the entire
landscape. Then we'll review what's going on at each of the individual
steps. Source references along the way will help you navigate the kernel
tree and dig in further.

<a name="F_INTROOverViewOfLinuxBootProcess"></a>
```ASCII
 Power-up / Reset                                           
          |      +---------------------+                    
          |      |    System startup   | BIOS / Bootmonitor 
          |      +---------------------+                    
          |                                                 
          |      +---------------------+                    
          |      |  Stage 1 Bootloader | Master Boot Record 
          |      +---------------------+                    
          |                                                 
          |      +---------------------+                    
          |      |  Stage 2 Bootloader | LILO, GRUB, etc    
          |      +---------------------+                    
          |                                                 
          |      +---------------------+                    
          |      |        Kernel       | Linux              
          |      +---------------------+                    
          |                                                 
          |      +---------------------+                    
          |      |         Init        | User-space         
          v      +---------------------+                    
       Operation                                            
```
**Figure**: Over view of the Linux Boot Process

When a system is first booted, or is reset, the processor executes code at
a well-known location. In a personal computer (PC), this location is in the
basic input/output system (BIOS), which is stored in flash memory on the
motherboard. The central processing unit (CPU) in an embedded system
invokes the reset vector to start a program at a known address in
flash/ROM. In either case, the result is the same. Because PCs offer so
much flexibility, the BIOS must determine which devices are candidates for
boot. We'll look at this in more detail later.

When a boot device is found, the first-stage boot loader is loaded into RAM
and executed. This boot loader is less than 512 bytes in length (a single
sector), and its job is to load the second-stage boot loader.

When the second-stage boot loader is in RAM and executing, a splash screen
is commonly displayed, and Linux and an optional initial RAM disk
(temporary root file system) are loaded into memory. When the images are
loaded, the second-stage boot loader passes control to the kernel image and
the kernel is decompressed and initialized. At this stage, the second-stage
boot loader checks the system hardware, enumerates the attached hardware
devices, mounts the root device, and then loads the necessary kernel
modules. When complete, the first user-space program (init) starts, and
high-level system initialization is performed.


