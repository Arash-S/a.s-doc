# How to compile linux kernel

## Table Of Content
1. [Requirements for building and using the kernel](#Requirements-for-building-and-using-the-kernel)
	1. [Tools to use the Kernel](#Tools-to-use-the-Kernel)
		1. [Util-linux](#Util-linux)
		1. [Module-init-tools](#Module-init-tools)
		1. [Filesystem-Specific Tools](#Filesystem-Specific-Tools)
		1. [Other Tools](#Other-Tools)

## Requirements for building and using the kernel
This chapter describes the programs you need to configure a kernel, build
it, and successfully boot it.

Tool to build the Linux Kernel:
* gcc (Compiler)<br />
 Be warned that getting the most recent gcc version is not always a good
 idea. Some of the newest gcc releases don’t build the kernelproperly, so
 unless you wish to help debug compiler bugs, it is not recommended that
 you try them out.
* build-essential 
* libncurses5-dev
* fakeroot
* bzip2
* bison
* flex

### Tools to use the Kernel
While the version of the kernel that is running does not usually affect any
user application, there are a small number of program for which the kernel
version is important. This section describes a number of tools that are
probably already installed on your Linux system. If you upgrade your kernel
to a version different from the one that came with your distribution, some
of these packages may also need to be upgraded in order for the system to
work properly.

#### Util-linux
The util-linux is a collection of small utilites that do a wide range of
different tasks. To determine which version of the util-linux package you
have on your system, run the following command:```$ fdformat --version```

#### Module-init-tools
The module-init-tools package is needed if you wish to use Linux Kernel
modules. It is useful to compile device drivers as modules and then load
only the ones that correspond to the hardware present in the system.
Modules save memory by loading just the code that is needed to control
the machine properly. The kernel module loading process underwent a
radical change in the 2.6 kernel release. The linker for the module (the
code that resolves all symbols and figures out how to put the pieces
together in memory) is now built into the kernel, which makes the
userspace tools quite small. You can blacklisting modules to prevent them
from being automatically loaded by the udev package. To determine which
version of the module-init-tools package you have on your system, run the
following command: ```$ depmod -V```

#### Filesystem-Specific Tools
* ext2/ext3/ext4<br />
  To work with any of these filesystems, you must have the e2fsprogs
  package. To determine which version of e2fsprogs you have on your system,
  run the following command: ```$ tune2fs```
* JFS<br />
  To use the JFS filesystem from IBM, you must have the jfsutils package.
* ReiserFS<br />
  To use the RiserFS filesystem, you must have the rieserfsprogs package.
* XFS<br />
  To use the XFS filesystem from SGI, you must have the xfsprogs package.
* Quotas<br />
 A disk quota is a limit set by a system administrator that restricts
 certain aspects of file system usage on modern operating systems. The
 function of setting quotas to disks is to allocate limited disk-space in a
 reasonable way. To use the quota functionality of the kernel, you must
 have the quota-tools package.

#### Other Tools
There are a few other important programs that are closely tied to the
kernel version. These programs are not usually required in order for the
kernel to work properly, but they enable access to different types of
hardware and functions.

* udev<br />
 udev is a program that enables Linux to provide a persistent device-naming
 system in the /dev directory. It also provides a dynamic /dev, much like
 the one provided by the older (and now removed) devfs filesystem. Almost
 all Linux distributions use udev to manage the /dev directory, so it is
 required in order to properly boot the machine.<br />
 Unfortunately, udev relies on the structure of /sys, which has been known
 to change from time to time with kernel releases. Some of these changes in
 the past have been known to break udev, so that your machine will not boot
 properly.<br />
 To determine which version of udev you have on your system, run the
 following command:```$ udevinfo -V```
* Process tools
  The package procps includes the commonly used tools ps and top, as well
  as many other handy tools for managing and monitoring processes running
  on the system.
* PCMCIA tools<br />
  In order to properly use PCMCIA devices with Linux, a userspace helper
  program must be used to set up the devices. pcmciautils is the package
  that properly manage PCMCIA devices.
