# Cryptographic Protection of TCP Streams

## Table of Contents
1. [Introduction](#Introduction)
    1. [Opportunistic Encryption](#Opportunistic-Encryption)
    1. [Goals of Tcpcrypt](#Goals-of-Tcpcrypt)
1. [Idealized Protocol](#Idealized-Protocol)
    1. [Stages of the protocol](#Stages-of-the-protocol)
        1. [The setup phase](#The-setup-phase)
        1. [The Encrypting state](#The-Encrypting-state)
            1. [Understanding TCP RST Control Bit](#Understanding-TCP-RST-Control-Bit)
                1. [The TCP Reset Function](#The-TCP-Reset-Function)
                1. [Handling Reset Segments](#Handling-Reset-Segments)
                1. [Idle Connection Management and KEEPALIVE messages](#Idle-Connection-Management-and-KEEPALIVE-messages)
        1. [The Disabled state](#The-Disabled-state)
            1. [Understanding TCP Active and Passive Opens](#Understanding-TCP-Active-and-Passive-Opens)
    1. [Cryptographic Algorithms](#Cryptographic-Algorithms)
    1. [C and S roles](#C-and-S-roles)
    1. [Key exchange protocol](#Key-exchange-protocol)

## Lists of Code
1. [Code-HKDF Extract and Expand function](#Code-HKDF-Extract-and-Expand-function)

## Lists of Figure
1. [TCP Header Format](#Figure-TCP-Header-Format)
1. [Key Exchange Protocl_When C connect to S](#Figure-Key-Exchange-Protocl_When-C-connect-to-S)

## Introduction 
In computer networking, tcpcrypt is a transport layer communication encryption
protocol. Unlike prior protocols like TLS (SSL), tcpcrypt is implemented as a
TCP extension. 

Tcpcrypt has been published as an [Internet Draft](https://tools.ietf.org/html/draft-bittau-tcpinc-01).
Experimental user-space implementations are available for Linux, Mac OS X,
FreeBSD and Windows. There is also a Linux Kernel implementations.

Tcpcrypt provides opportunistic encryption. Tcpcrypt also provides encryption
to any application using TCP, even ones that do not know about encryption. This
enables incremental and seamless deployment.

Unlike TLS, tcpcrypt itself does not do any authentication, but passes a unique
"session ID" down to the application; the application can then use this token
for further authentication. This means that any authentication scheme can be
used, including passwords or certificates. It also does a larger part of the
public-key connection initiation on the client side, to reduce load on servers
and mitigate DoS attacks.

### Opportunistic Encryption
Opportunistic encryption (OE) refers to any system that, when connecting to
another system, attempts to encrypt the communications channel, otherwise
falling back to unencrypted communications. This method requires no
pre-arrangment between the two systems.

Opportunistic encryption can be used to combat passive wiretapping. (An active
wiretapper, on the other hand, can disrupt encryption negotiation to either
force an unencrypted channel or perform a man-in-the-middle attack on the
encrypted link.) It does not provide a strong level of security as
authentication may be difficult to establish and secure communications are not
mandatory. Yet, it does make the encryption of most Internet traffic easy to
implement, which removes a significant impediment to the mass adoption of
Internet traffic security.

### Goals of Tcpcrypt
Tcpcrypt was designed to meet the following goals:
* Maintain confidentiality of communications against a passive adversary.
  Ensure that an adversary must actively intercept and modify the traffic to
  eavesdrop, either by re-encrypting all traffic or by forcing a downgrade to
  an unencrypted session.
* Minimize computational cost, particularly on servers.
* Provide interfaces to higher-level software to facilitate end-to- end
  security, either in the application level protocol or after the fact.  (E.g.,
  client and server log session IDs and can compare them after the fact; if
  there was no tampering or eavesdropping, the IDs will match.)
* Be compatible with further extensions that allow authenticated resumption of
  TCP connections when either end changes IP address.
* Facilitate multipath TCP by identifying a TCP stream with a session ID
  independent of IP addresses and port numbers.
* Provide for incremental deployment and graceful fallback, even in the
  presence of NATs and other middleboxes that might remove unknown options, and
  traffic normalizers.

## Idealized Protocol
This section describes the tcpcrypt protocol at an abstract level, without
reference to particular cryptographic algorithms or data encodings.

### Stages of the protocol
A tcpcrypt endpoint goes through multiple stages. It begins in a *SETUP
PHASE* and ends up in one of two states, *ENCRYPTING* or *DISABLED*,
before applications may send or receive data. The *ENCRYPTING* and *DISABLED*
status are definitive and mutually exclusive; an endpoint that has been in one
of the two states *MUST NOT* enter the other, nor ever re-enter the setup
phase.

#### The setup phase
The setup phase negotiates use of the tcpcrypt extension.  During this phase,
two hosts agree on a suite of cryptographic algorithms and establish shared
secret session keys.

```ASCII
 0                   1                   2                   3   
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           Source Port         |        Destination Port       |
|-------------------------------+-------------------------------|
|                          Sequence Number                      |
|---------------------------------------------------------------|
|                       Acknowledgment Number                   |
|---------+---------+-----------+-------------------------------|
| Data    |         |U|A|P|R|S|F|                               |
| Offset  |Reserved |R|C|S|S|Y|I|            Window             |
|         |         |G|K|H|T|N|N|                               |
|---------+---------+-----------+-------------------------------|
|            Checksum           |         Urgent Pointer        |
|-------------------------------+---------------+---------------|
|                        Options                |    Padding    |
|-----------------------------------------------+---------------|
|                             Data                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
```
##### Figure-TCP Header Format

The setup phase uses the Data portion of TCP segments to exchange cryptographic
keys.  Implementations MUST NOT include application data in TCP segments during
setup and MUST NOT allow applications to read or write data.  System calls MUST
behave the same as for TCP connections that have not yet entered the
ESTABLISHED state; calls to read and write SHOULD block or return temporary
errors, while calls to poll or select SHOULD consider connections not ready.

When setup succeeds, tcpcrypt enters the ENCRYPTING state.  Importantly, a
successful setup also produces an important value called the _Session ID_.  The
Session ID is tied to the negotiated algorithms and cryptographic keys, and is
unique over all time with overwhelming probability.

Operating systems MUST make the Session ID available to applications.  To
prevent man-in-the-middle attacks, applications MAY authenticate the session ID
through any protocol that ensures both endpoints of a connection have the same
value.  Applications MAY alternatively just log Session IDs so as to enable
attack detection after the fact through comparison of the values logged at both
ends.

The setup phase can also fail for various reasons, in which case tcpcrypt
enters the DISABLED state.

Applications MAY test whether setup succeeded by querying the operating system
for the Session ID.  Requests for the Session ID MUST return an error when
tcpcrypt is not in the ENCRYPTING state.  Applications SHOULD authenticate the
returned Session ID.  Applications relying on tcpcrypt for security SHOULD
authenticate the Session ID and SHOULD treat unauthenticated Session IDs the
same as connections in the DISABLED state.

#### The Encrypting state
When the setup phase succeeds, tcpcrypt enters the ENCRYPTING state.  Once in
this state, applications may read and write data with the expected semantics of
TCP connections.

In the ENCRYPTING state, a host MUST encrypt the Data portion of all TCP
segments transmitted and MUST include a Message Authentication Code (MAC) in
all segments transmitted.  A host MUST furthermore ignore any TCP segments
received without the RST bit set, unless those segments also contain a valid
MAC option.

A host SHOULD accept RST segments without valid MACs by default.  However, the
application SHOULD be allowed to force unMACed RST segments to be dropped by
enabling the TCP_CRYPT_RSTCHK option on the connection.

Once in the ENCRYPTING state, an endpoint MUST NOT directly or indirectly
transition to the DISABLED state under any circumstances.

##### Understanding TCP RST Control Bit
Once both of the devices in a TCP connection have completed connection setup
and have entered the ESTABLISHED state, the TCP software is in its normal
operating mode. Bytes of data will be packaged into segments for transmission
using the mechanisms of message formatting and data transfer. The sliding
windows scheme will be used to control segment size and to provide flow
control, congestion handling and retransmissions as needed.

Once in this mode, both devices can remain there indefinitely. Some TCP
connections can be very long-lived indeed--in fact, some users maintain certain
connections like Telnet sessions for hours or even days at a time. There are
two circumstances that can cause a connection to move out of the ESTABLISHED
state:
* **Connection Termination**:

  Either of the devices decides to terminate the connection.
* **Connection Disruption**:
  
  A problem of some sort occurs that causes the connection to be interrupted.

###### The TCP Reset Function
To allow TCP to live up to its job of being a reliable and robust protocol, it
includes intelligence that allows it to detect and respond to various problems
that can occur during an established connection. One of the most common is the
half-open connection. This situation occurs when due to some sort of problem,
one device closes or aborts the connection without the other one knowing about
it. This means one device is in the ESTABLISHED state while the other may be in
the CLOSED state (no connection) or some other transient state. This could
happen if, for example, one device had a software crash and was restarted in
the middle of a connection, or if some sort of glitch caused the states of the
two devices to become unsynchronized.

To handle half-open connections and other problem situations, TCP includes a
special reset function. A reset is a TCP segment that is sent with the RST flag
set to one in its header. Generally speaking, a reset is generated whenever
something happens that is “unexpected” by the TCP software. Some of the most
common specific cases in which a reset is generated include:
* Receipt of any TCP segment from any device with which the device receiving
  the segment does not currently have a connection (other than a SYN requesting
  a new connection.)
* Receipt of a message with an invalid or incorrect Sequence Number or
  Acknowledgment Number field, indicating the message may belong to a prior
  connection or is spurious in some other way.
* Receipt of a SYN message on a port where there is no process listening for
  connections.

###### Handling Reset Segments
When a device receives a segment with the RST bit sent, it tells the device to
reset the connection so it can be re-established. Like all segments, the reset
itself must be checked to ensure that it is valid (by looking at the value of
its Sequence Number field). This prevents a spurious reset from shutting down a
connection. Assuming the reset is valid, the handling of the message depends on
the state of the device that receives it:
* If the device is in the LISTEN state, the reset is ignored and it remains in
  that state.
* If the device is in the SYN-RECEIVED state but was previously in the LISTEN
  state (which is the normal course of events for a server setting up a new
  connection), it returns to the LISTEN state.
* In any other situation, the reset causes the connection to be aborted and the
  device returns to the CLOSED state for that connection. The device will
  advise the higher-layer process using TCP that the connection has been
  closed.

###### Idle Connection Management and KEEPALIVE messages
A TCP session that is active but that has no data being transmitted by either
device for a prolonged period of time. The TCP standard specifies that the
appropriate action to take in this situation is… nothing. The reason is that,
strictly speaking, there is no need to do anything to maintain an idle
connection in TCP. The protocol is perfectly happy to allow both devices to
stop transmitting for a very long period of time, and then simply resume
transmissions of data and acknowledgment segments when either has data to send.

However, just as many people become “antsy” when they are on a telephone call
and they don’t hear anything for a long while, there was concern on the part of
some TCP implementors that a TCP connection that was idle for a very long while
might mean that the connection had been broken.

Thus, TCP software often includes an “unofficial” feature that allows a device
with a TCP link to periodically send a null segment containing no data to its
peer on the connection. If the connection is still valid, the other device
responds with a segment containing an acknowledgment; if it is not, the other
device will reply with a connection reset segment as described above. These
segments sometimes called TCP “keepalive” messages, or just “keepalives”. 

Worse, sending a “keepalive” can in theory cause a good TCP session to be
unnecessarily broken. This may happen if the “keepalive” is sent during a time
when there is an intermittent failure between the client and server, a failure
that might otherwise have corrected itself by the time the next piece of “real”
data must be sent. In addition, some TCP implementations may not properly deal
with the receipt of these segments.

Those in favor of using “keepalives” point out that each TCP connection
consumes a certain amount of resources, and this can be an issue especially for
busy servers. If many clients connect to such a server and don’t terminate the
TCP connection properly, the server may sit for a long time with an idle
connection, using system memory and other resources that could be applied
elsewhere.

Since there is no wide acceptance on the use of this feature, devices
implementing it include a way to disable it if necessary. Devices are also
programmed so they will not terminate a connection simply as a result of not
receiving a response to a single “keepalive”. They may do so if they do not
receive a reply after several such messages have been sent over a period of
time.

#### The Disabled state
When setup fails, tcpcrypt enters the DISABLED state.  In this case, the host
MUST continue just as TCP would without tcpcrypt, unless network conditions
would cause a plain TCP connection to fail as well.  Entering the DISABLED
state prohibits the endpoint from ever entering the ENCRYPTING state.

An implementation MUST behave identically to ordinary TCP in the DISABLED
state, except that the first segment transmitted after entering the DISABLED
state MAY include a TCP CRYPT option with a DECLINE suboption (and optionally
other suboptions such as UNKNOWN) to indicate that tcpcrypt is supported but
not enabled.

Operating systems MUST allow applications to turn off tcpcrypt by setting the
state to DISABLED before opening a connection.  An active opener with tcpcrypt
disabled MUST behave identically to an implementation of TCP without tcpcrypt.
A passive opener with tcpcrypt disabled MUST also behave like normal TCP,
except that it MAY optionally respond to SYN segments containing a CRYPT option
with SYN-ACK segments containing a DECLINE suboption, so as to indicate that
tcpcrypt is supported but not enabled.

##### Understanding TCP Active and Passive Opens

The server does a passive open. This creates a listening port. It does not
initiate any protcol on the wire. The client does an active open to a listening
port on the server. This does start the TCP protocol. If the client open's
against a non-listening port, there will either be no response, or there will
be an error response. An error response is preferable because the client will
not have to timeout.

### Cryptographic Algorithms
The setup phase employs three types of cryptographic algorithms:
* A _Public key cipher_ is used with a short-lived public key to exchange (or
  agree upon) a random, shared secret.
* An _extract function_ is used to generate a pseudo-random key from some
  initial keying material, typically the output of the public key cipher.
  The notation Extract (S, IKM) denotes the output of the extract function with
  salt S and initial keying material IKM.
* A _collision-resistant pseudo-random function (CPRF)_ is used to generate
  multiple cryptographic keys from a pseudo-random key, typically the output of
  the extract function.  We use the notation CPRF (K, TAG, L) to designate the
  output of L bytes of the pseudo- random function identified by key K on TAG.
  A collision-resistant function is one on which, for sufficiently large L, an
  attacker cannot find two distinct inputs K_1, TAG_1 and K_2, TAG_2 such that
  CPRF (K_1, TAG_1, L) = CPRF (K_2, TAG_2, L).  Collision resistance is
  important to assure the uniqueness of Session IDs, which are generated using
  the CPRF.

The Extract and CPRF functions used by default are the Extract and Expand
functions of HKDF [RFC 5869](https://tools.ietf.org/html/rfc5869). These are
defined as follows:

```ASCII
HKDF-Extract(salt, IKM) -> PRK
    PRK = HMAC-Hash(salt, IKM)

HKDF-Expand(PRK, TAG, L) -> OKM
    T(0) = empty string (zero length)
    T(1) = HMAC-Hash(PRK, T(0) | TAG | 0x01)
    T(2) = HMAC-Hash(PRK, T(1) | TAG | 0x02)
    T(3) = HMAC-Hash(PRK, T(2) | TAG | 0x03)
    ...
    0KM = first L octets of T(1) | T(2) | T(3) | ...
```
##### Code-HKDF Extract and Expand function

**Note**: The symbol | denotes concatenation, and the counter concatenated with
TAG is a single octet.

Because the public key cipher, the extract function, and the expand function
all make use of cryptographic hashes in their constructions, the three
algorithms are negotiated as a unit employing a single hash function.  For
example, the OAEP+-RSA [RFC 2437](https://tools.ietf.org/html/rfc2437) cipher,
which uses a SHA-256-based mask-generation function, is coupled with HKDF [RFC
5869](https://tools.ietf.org/html/rfc5869) using HMAC-SHA256 [RFC
2104](https://tools.ietf.org/html/rfc2104).

The encrypting phase employs an _authenticated encryption mode_ to encrypt all
application data.  This mode authenticates both application data and most of
the TCP header (excepting header fields commonly modified by middleboxes).

Note that public key generation, public key encryption, and shared secret
generation all require randomness.  Other tcpcrypt functions may also require
randomness depending on the algorithms and modes of operation selected.  A weak
pseudo-random generator at either host will defeat tcpcrypt's security.  Thus,
any host implementing tcpcrypt MUST have a cryptographically secure source of
randomness or pseudo-randomness.

### C and S roles
Tcpcrypt transforms a single pseudo-random key (PRK) into cryptographic session
keys for each direction.  Doing so requires an asymmetry in the protocol, as
the key derivation function must be perturbed differently to generate different
keys in each direction.  Tcpcrypt includes other asymmetries in the roles of
the two hosts, such as the process of negotiating algorithms (e.g., proposing
vs.  selecting cipher suites).

We use the terms "C" and "S" to denote the distinct roles of the two hosts in
tcpcrypt's setup phase.  In the case of key transport, "C" is the host that
supplies a public key, while "S" is the host that encrypts a pre-master secret
with the key belonging to "C".  Which role a host plays can have performance
implications, because for some public key algorithms encryption is much faster
than decryption.  For instance, on a machine at the time of writing, encryption
with a 2,048-bit RSA-3 key is over two orders of magnitude faster than
decryption.

### Key exchange protocol
Every machine C has a short-lived public encryption key or key agreement
parameter, PK_C, which gets refreshed periodically and SHOULD NOT ever be
written to persistent storage.

When a host C connects to S, the two engage in the following protocol:

```ASCII
C -> S: HELLO
S -> C: PKCONF, pub-cipher-list
C -> S: INIT1, sym-cipher-list, N_C, pub-cipher, PK_C
S -> C: INIT2, sym-cipher, KX_S
```
##### Figure-Key Exchange Protocl_When C connect to S

The parameters are defined as follows:
* **Pub-cihper-list**:

  A list of public key ciphers and parameters acceptable to S.

* **Sym-cipher-list**:
  
  A list of symmetric cipher suites acceptable to C.

  NOTE: Symmetric-Key algorithms are algorithms for cryptography that use the
  same cryptographic keys for both encryption of plaintext and decryption of
  ciphertext. The keys may be identical or there may be a simple transformation
  to go between the two keys. The keys, in practice, represent a shared secret
  between two or more parties that can be used to maintain a private
  informatino link. This requirement that both parties have access to the
  secret key is one of the main drawbacks of symmetric key encryption, in
  comparison to public-key encryption.

* **N_C**:
  
  Nonce chosen at random by C.

* **pub-cipher**:
  
  The type of PK_C.

* **PK_C**:
  
  C's public key or key agreement parameter.

* **Sym-ciper**:
  
  The symmetric cipher selected by S

* **KX_S**:
  
  Key exchange information produced by S. KX_S will depend on whether key
  transport is being done or key agreement.
