# Assembly Language X86_64

# Table of Content
1. [Introduction](#Introduction)
1. [Setup Development Environment](#Setup-Development-Environment)
1. [How Invoke System calls with syscall](#How-Invoke-System-calls-with-syscall)
	1. [int 0x80](#int-0x80)
	1. [Syscall](#Syscall)
1. [Data type](#Data-type)
	1. [Integer](#Integer)
	1. [Floating point Numbers](#Floating-point-Numbers)
1. [Reserving storage space for variables](#Reserving-storage-space-for-variables)
	1. [Initialized Data](#Initialized-Data)
	1. [Uninitialized Data](#Uninitialized-Data)
1. [Data instruction](#Data-instruction)
	1. [Data movement instruction](#Data-movement-instruction)
		1. [Move](#Move)
		1. [Move data from string to string](#Move-data-from-string-to-string)
		1. [Move with zero extend](#Move-with-zero-extend)
		1. [Move sign extend](#Move-sign-extend)
		1. [Scan string](#Scan-string)
		1. [Store string](#Store-string)
		1. [Compare](#Compare)
		1. [Compare string](#Compare-string)
		1. [Compare and exchange](#Compare-and-exchange)
		1. [Data swap](#Data-swap)
		1. [Load effective address](#Load-effective-address)
		1. [Load string](#Load-string)
	1. [Arithmetic and Logical instruction](#Arithmetic-and-Logical-instruction)
		1. [Addition](#Addition)
		1. [Subtraction](#Subtraction)
		1. [Multiples](#Multiples)
		1. [Divides](#Divides)
		1. [Increment and Decrement](#Increment-and-Decrement)
		1. [Arithmetic Flags Instruction](#Arithmetic-Flags-Instruction)
			1. [Complement Carry Flag](#Complement-Carry-Flag)
			1. [Clear Carry Flag](#Clear-Carry-Flag)
			1. [Set Carry Flag](#Set-Carry-Flag)
		1. [Logical Operation](#Logical-Operation)
		1. [Shift and Rotate](#Shift-and-Rotate)
			1. [Logical Shift](#Logical-Shift)
			1. [Arithmetic Shift](#Arithmetic-Shift)
			1. [Double Shift](#Double-Shift)
			1. [Rotate](#Rotate)
			1. [Rotate With Carry](#Rotate-With-Carry)
	1. [Control Flow instruction](#Control-Flow-instruction)

# Table of Code
1. [Syntax of allocating storage space for Initialized Data](#TC_InitVar)
1. [Example of allocating storage space for Initialized Data](#TC_ALLOCSSFINITDATAEXAMPLE)
1. [Example of allocating storage space for Uninitialized Data](#TC_EXAMPLEOFALLOCSSFUNINITDATA)
1. [Move instruction](#TC_DMIMOVE)
1. [Moves instruction](#TC_MDIMDFSTS)
1. [Moves instruction example](#TC_MDIMDFSTSE)
1. [Move with zero extend instruction](#TC_DMIMWZE)
1. [movsx instructon](#TC_DMIMSE)
1. [Scan string instruction](#TC_DMISS)
1. [Scan string instruction example](#TC_DMISSE)
1. [Store string instruction](#TC_DMISTS)
1. [Store string instruction example](#TC_DMISTS)
1. [Compare instruction](#TC_DMIC)
1. [Compare string instruction](#TC_DMICSO)
1. [Compare and exchange instruction](#TC_DMICAE)
1. [Data swap instruction](#TC_MDIDS)
1. [Lea instruction](#TC_DMILEA)
1. [Lea instruction example](#TC_DMILEAE)
1. [Load string instruction](#TC_DMILS)
1. [Load string instruction example](#TC_DMILSE)
1. [Add instruction](#TC_AALIADD)
1. [Sub instruction](#TC_AALISUB)
1. [Mul instruction](#TC_AALIMUL)
1. [Mul instruction example](#TC_AALIMULE)
1. [Div instruction](#TC_AALIDIV)
1. [Increment and decrement instruction](#TC_AALINCDEC)
1. [Complement Carry Flag instruction](#TC_AALIAFICCF)
1. [Clear Carry Flag instruction](#TC_AALIAFCLCF)
1. [Set Carry Flag instruction](#TC_AALIAFSCF)
1. [Logical shift instruction](#TC_AALISARLSI)
1. [Arithmetic shift instruction](#TC_AALISARASESI)
1. [Rotate Instruction](#TC_AALISARR)
1. [Rotate with carry instruction](#TC_AALISARRWC)

# List of Figure
1. [Correlation Assembly language with High Level Languages](#TF_CWHLLs)
1. [When two bytes are multiplied](#TF_AALIMUL1)
1. [When two one-word values are multiplied](#TF_AALIMUL2)
1. [When two double values are multiplied](#TF_AALIMUL3)
1. [When the divisor is 1 byte](#TF_AALIDIV1)
1. [When the divisor is 1 word](#TF_AALIDIV2)
1. [When the divisor is doubleword](#TF_AALIDIV3)
1. [Logical shift](#TF_AALISARLSI)
1. [Arithmetic shift right with positive operand](#TF_AALIASPO)
1. [Arithmetic shift right with negative operand](#TF_AALIASPN)
1. [SHLD and SHRD Instruction Operation](#TF_AALIASDS)
1. [Rotate Instruction Operation](#TF_AALISARR)
1. [Rotate with carry instruction operation](#TF_AALISARRWC)

# List of Table
1. [Invoke a syscall by calling interrupt 0x80](#LT_INT0x80)
1. [Invoke a syscall by calling sycall](#LT_SYSCALL)
1. [Directive used for define initialized data](#LT_DDFID)
1. [Directive used for allocating storage space for uninitialized data](#LT_RESUNINITDATA)
1. [Logiacl Operand instruction](#LT_AALILO)

## Introduction
Assembly Language is a low-level programming language that communicate with
microprocessor. Every processor family has specific assembly language rule.
Assembly language translated to machine language code, zero and one's, with
Assembler and Linker.

<a name="TF_CWHLLs"></a>
```ASCII
         +--------------------+
         | Source Code File - |
         | hello.c, hello.cpp |
         +--------------------+
                   |
                   | C preprocessor
                   v
         +--------------------+
         | Preprocessed Code  |
         | File - hello.i     |
         +--------------------+
                   |
                   | C Compiler
                   v
         +--------------------+
         | Assembly code      |                  
         | file - hello.s     |                  
         +--------------------+      
                   |           
                   | Assembler 
                   v           
         +--------------------+
         | Object code file   |
         | - hello.o          |
         +--------------------+
                   |
                   | Linker/link editor
                   |
                   |          +----------------------------------+
                   |<---------|Relocation object code information|
                   |          +----------------------------------+
                   |
                   |          +----------------------------------+
                   |<---------|Other objects file/modules        |
                   |          +----------------------------------+
                   |
                   |          +----------------------------------+
                   |<---------|Library files                     |
                   |          +----------------------------------+
                   |
                   |
                   |
                   v
         +--------------------+
         | Executable code -  |
         | hello, hello.exe   |
         +--------------------+
                  |||
                  |||
                  \|/
                   v
         +--------------------+
         | Stored in secondary|
         | storage such as    |
         | hard disk (hdd) as |
         | an executable image|
         +--------------------+
                  |||
                  ||| When running/execute the program (a process)
                  |||          +-------------------------------------+
                  |||<---------|Run time objects/ modules / libraries|
                  \|/          +-------------------------------------+
                   v
         |                    |
         |--------------------|
         | Process Address    |
         | Space              |
         |--------------------|
         | Primary memory e.g.|
         |        RAM         |
         |--------------------|
         |                    |
```
**Figure**: Correlation Assembly language with High Level Languages.

## Setup Development Environment
For writing assembly program we should have the following program:
* NASM
* LD

**Note**: 
* In this document we describe Intel syntax.
* Nasm is case sensitive.

## How Invoke System calls with syscall
Syscalls are the interface between user programs and the Linux kernel. They
are used to let the kernel perform various system tasks, such as file
access, process management and networking. In the C programming language,
you would normally call a wrapper function which executes all required
steps or even use high-level features such as the standard IO library.

On Linux, there are several ways to make a syscall. This page will focus on
making syscalls by calling a software interrupt using ```int $0x80``` or
```syscall```.  This is an easy and intuitive method of making syscalls in
assembly-only programs. 

For making a syscall using an interrupt, you have to pass all required
information to the kernel by copying them into general purpose registers.

Each syscall has a fixed number (note: the numbers differ between ```int
$0x80``` and ```syscall```!). You specify the syscall by writing the number
into the eax/rax register. 

Most syscalls take parameters to perform their task. Those parameters are
passed by writing them in the appropriate registers before making the
actual call. Each parameter index has a specific register. 

Parameters are passed in the order they appear in the function signature of
the corresponding C wrapper function. You may find syscall functions and
their signatures in every Linux API documentation, like the reference
manual (type man 2 open to see the signature of the ```open``` syscall).

After everything is set up correctly, you call the interrupt using ```int
$0x80``` or ```syscall``` and the kernel performs the task.

The return / error value of a syscall is written to ```eax/rax```.

The kernel uses its own stack to perform the actions. The user stack is not
touched in any way. 

### int 0x80
On both Linux x86 and Linux x86_64 systems you can make a syscall by
calling interrupt 0x80 using the ```int $0x80``` command. Parameters are
passed by setting the general purpose registers as following: 

<a name="LT_INT0x80"></a>

Syscall # | Param 1 | Param 2 | Param 3 | Param 4 | Param 5 | Param 6
----------|---------|---------|---------|---------|---------|--------
eax | ebx | ecx | edx | esi | edi | ebp

Return Value |
-------------|
eax |

**Table**: Invoke a syscall by calling interrupt 0x80.

### Syscall
The x86_64 architecture introduced a dedicated instruction to make a
```syscall```. It does not access the interrupt descriptor table and is
faster.  Parameters are passed by setting the general purpose registers as
following:

<a name="LT_SYSCALL"></a>

Syscall # | Param 1 | Param 2 | Param 3 | Param 4 | Param 5 | Param 6
----------|---------|---------|---------|---------|---------|--------
rax | rdi | rsi | rdx | r10 | r8 | r9

Return Value |
-------------|
rax |

**Table**: Invoke a syscall by calling sycall.

The syscall numbers are described in the Linux generated file
```$build/usr/include/asm/unistd_64.h``` .

All registers, except ```rcx``` and ```r11``` (and the return value,
```rax```), are preserved during the syscall.

## Data type
Strictly speaking, assembly has no predefined data types like higher-level
programming languages. Any general purpose register can hold any sequence
of two or four bytes, whether these bytes represent numbers, letters, or
other data. In the same way, there are no concrete types assigned to blocks
of memory – you can assign to them whatever value you like.

That said, one can group data in assembly into two categories: integer and
floating point. While you could load a floating point value into a register
and treat it like an integer, the results would be unexpected, so it is
best to keep them separate.

### Integer
An integer represents a whole number, either positive or negative. Under
the 8086 architecture, it originally came in 8-bit and 16-bit sizes, which
served the most basic operations. Later, starting with the 80386, the data
bus was expanded to support 32-bit operations and thus allow operations on
integers of that size. The newest systems under the x86 architecture
support 64-bit instructions; however, this requires a 64-bit operating
system for optimal effect. 

Some assembly instructions behave slightly differently in regards to the
sign bit; as such, there is a minor distinction between signed and unsigned
integers.

### Floating point Numbers
Floating point numbers are used to approximate the real numbers that
usually contain digits before and after the decimal point (like π,
3.14159...). Unlike integers where the decimal point is understood to be
after all digits, in floating point numbers the decimal floats anywhere in
the sequence of digits. The precision of floating point numbers is limited
and thus a number like π can only be represented approximately.

Originally, floating point was not part of the main processor, requiring
the use of emulating software. However, there were floating point
coprocessors that allowed operations on this data-type, and starting with
the 486DX, were integrated directly with the CPU.

As such, floating point operations are not necessarily compatible with all
processors - if you need to perform this type of arithmetic, you may want
to use a software library as a backup code path.

## Reserving storage space for variables
NASM provides various define directives for reserving storage space for
variables. The define assembler directive is used for allocation of storage
space. It can be used to reserve as well as initialize one or more bytes.

Reserving storage space for variables are done in the data section
```section .data```.

### Initialized Data
The syntax for storage allocation statement for initialized data is:

<a name="TC_InitVar"></a>

```ASCII
[variable-name] 	define-directive 	initial-value [,initial-value]
```

**Code**: Syntax of allocating storage space for Initialized Data.

There are five basic forms of the define directive:
<a name="LT_DDFID"><a/>

Directive | Purpose | Storage Space
----------|---------|---------------
DB | Define Byte | Allocates 1 byte
DW | Define Word | Allocates 2 bytes
DD | Define Doubleword | Allocates 4 bytes
DQ | Define QuadWord | Allocates 8 bytes
DT | Define Ten bytes | allocates 10 bytes

**Table**: Directive used for define initialized data.

**Note**:
* Each byte of character is stored as its ASCII value in hexadecimal.
* Each decimal value is automatically converted to its 16-bit binary
  equivalent and stored as a hexadecimal number.
* Processor uses the little-endian byte ordering.
* Negative numbers are converted to its 2's complement representation.
* Short and long floating-point numbers are represented using 32 or 64
  bits, respectively.

<a name="TC_ALLOCSSFINITDATAEXAMPLE"></a>
```nasm
global _start

section .text
_start:
	mov rax, msg    ; Move msg address to rax.
	mov rax, [msg]  ; Move msg value to rax.

	; Print Hello World message to the screen.
	mov rax, 1
	mov rdi, 1
	mov rsi, msg
	mov rdx, len
	syscall

	; Exit program gracefully.
	mov rax, 60
	mov rdi, 0
	syscall

section .data
	msg: db "Hello World!", 0xa
	; Special Token $
	;   $  Evalute to the current line.
	;   $$ Evalute to the beginning of current section.
	len: equ $-msg
	; The times directive allows multiple initializations to the same
	; value.
	star: times 10 db "*"
```
**Code**: Example of allocating storage space for Initialized Data.

### Uninitialized Data
The reserve directives are used for reserving space for uninitialized data.
The reserve directives take a single operand that specifies the number of
units of space to be reserved. Each define directive has a related reserve
directive.

Reserving storage space for variables are done in the bss section
```section .bss```.

There are five basic forms of the reserve directive:
<a name="LT_RESUNINITDATA"></a>

Directive | Purpose
----------|---------
RESB | Reserve a Byte
RESW | Reserve a Word
RESD | Reserve a Doubleword
RESQ | Reserve a Quadword
REST | Reserve a Ten Bytes

**Table**: Directive used for allocating storage space for uninitialized data.

<a name="TC_EXAMPLEOFALLOCSSFUNINITDATA"></a>
```nasm
global _start

section .data
        Prompt: db "Enter : "
        PromptLen: equ $-Prompt
        BuffLen: db 100

section .bss
        Buff: resb 100

section .text
_start:
        mov al, 1
        mov dil, 1
        mov esi, Prompt
        mov dl, PromptLen
        syscall

        mov rax, 0
        mov rdi, 0
        mov rsi, Buff
        mov rdx, BuffLen
        syscall

        mov rdx, rax
        mov rax, 1
        mov rdi, 1
        mov rsi, Buff
        syscall

        mov rax, 60
        mov rdi, 0
        syscall
```
**Code**: Example of allocating storage space for Uninitialized Data.

## Data instruction
Machine instructions generally fall into three categories: data movement,
arithmetic/logic, and control-flow. In this section, we will look at
important examples of x86_64 instructions from each category. This section
should not be considered an exhaustive list of x86_64 instructions, but
rather a useful subset. For a complete list, see Intel's instruction set
reference.

### Data movement instruction

#### Move
The mov instruction copies the data item referred to by its second operand
(i.e. register contents, memory contents, or a constant value) into the
location referred to by its first operand (i.e. a register or memory).
While register-to-register moves are possible, direct memory-to-memory
moves are not. In cases where memory transfers are desired, the source
memory contents must first be loaded into a register, then can be stored to
the destination memory address.

The moveabs instruction move immediate value to register.

**Modified flags**: No FLAGS are modified by this instruction.

<a name="TC_DMIMOVE"></a>
```nasm
mov dest, src
movabs dest, src
```
**Code**: Move instruction.

#### Move data from string to string
Moves the byte, word, or doubleword specified with the second operand
(source operand) to the location specified with the first operand
(destination operand). Both the source and destination operands are located
in memory. The address of the source operand is read from the DS:ESI or the
DS:SI registers (depending on the address-size attribute of the
instruction, 32 or 16, respectively). The address of the destination
operand is read from the ES:EDI or the ES:DI registers (again depending on
the address-size attribute of the instruction). The DS segment may be
overridden with a segment override prefix, but the ES segment cannot be
overridden.

At the assembly-code level, two forms of this instruction are allowed: the
“explicit-operands” form and the “no-operands” form. The explicit-operands
form (specified with the MOVS mnemonic) allows the source and destination
operands to be specified explicitly. Here, the source and destination
operands should be symbols that indicate the size and location of the
source value and the destination, respectively. This explicit-operands form
is provided to allow documentation; however, note that the documentation
provided by this form can be misleading. That is, the source and
destination operand symbols must specify the correct type (size) of the
operands (bytes, words, or doublewords), but they do not have to specify
the correct location. The locations of the source and destination operands
are always specified by the DS:(E)SI and ES:(E)DI registers, which must be
loaded correctly before the move string instruction is executed.

The no-operands form provides “short forms” of the byte, word, and
doubleword versions of the MOVS instructions. Here also DS:(E)SI and
ES:(E)DI are assumed to be the source and destination operands,
respectively. The size of the source and destination operands is selected
with the mnemonic: MOVSB (byte move), MOVSW (word move), or MOVSD
(doubleword move).

After the move operation, the (E)SI and (E)DI registers are incremented or
decremented automatically according to the setting of the DF flag in the
EFLAGS register. (If the DF flag is 0, the (E)SI and (E)DI register are
incremented; if the DF flag is 1, the (E)SI and (E)DI registers are
decremented.) The registers are incremented or decremented by 1 for byte
operations, by 2 for word operations, or by 4 for doubleword operations.

<a name="TC_MDIMDFSTS"></a>
```nasm
movs
movsb
movsw
movsd
movsq
```
**Code**: Moves instruction.

<a name="TC_MDIMDFSTSE"></a>
```nasm
global _start

section .text
_start:
        push rbp
        mov rbp, rsp

        ; Clear direction flag
        cld
        lea rsi, [HelloWorld]
        lea rdi, [Copy]
        ; Move qword from address (R|E)SI to (R|E)DI
        movsq

        cld 
        lea rsi, [HelloWorld]
        xor rax, rax
        mov qword [Copy], rax
        lea rdi, [Copy]
        mov rcx, len
        rep movsb

        mov rax, 0x0123456789abcdef
        lea rdi, [var1]
        stosq

        xor rax, rax
        lea rsi, [var1]
        lodsq

        leave
        mov rax, 60
        mov rdi, 0
        syscall

section .data
        HelloWorld: db "Hello World"
        len: equ $-HelloWorld

section .bss
        Copy: resb len
        var1: resb 8

```
**Code**: Moves instruction example.

#### Move with zero extend
The movz instruction copies the src operand in the dest operand and pads
the remaining bits not provided by src with zeros (0).

This instruction is useful for copying a small, unsigned value to a bigger
register.

Source operands can be register or memory but destination operand must to
be a register.

**Modified flags**: No FLAGS are modified by this instruction.

<a name="TC_DMIMWZE"></a>
```nasm
movzx dest, src
```
**Code**: Move with zero extend instruction.

#### Move sign extend
The movs instruction copies the src operand in the dest operand and pads
the remaining bits not provided by src with the sign bit (the MSB) of src.

This instruction is useful for copying a signed small value to a bigger
register. 

Source operand can be register or memory but destination operand must to be
a register.

**Modified flags**: no FLAGS are modified by this instruction.

<a name="TC_DMIMSE"></a>
```nasm
movsx dest, src
```
**Code**: movsx instructon.
#### Scan string
In non-64-bit modes and in default 64-bit mode: this instruction compares a
byte, word, doubleword or quadword specified using a memory operand with
the value in AL, AX, or EAX. It then sets status flags in EFLAGS recording
the results. The memory operand address is read from ES:(E)DI register
(depending on the address-size attribute of the instruction and the current
operational mode). Note that ES cannot be overridden with a segment
override prefix.

At the assembly-code level, two forms of this instruction are allowed. The
explicit-operand form and the no-operands form. The explicit-operand form
(specified using the SCAS mnemonic) allows a memory operand to be specified
explicitly. The memory operand must be a symbol that indicates the size and
location of the operand value. The register operand is then automatically
selected to match the size of the memory operand (AL register for byte
comparisons, AX for word comparisons, EAX for doubleword comparisons). The
explicit-operand form is provided to allow documentation. Note that the
documentation provided by this form can be misleading. That is, the memory
operand symbol must specify the correct type (size) of the operand (byte,
word, or doubleword) but it does not have to specify the correct location.
The location is always specified by ES:(E)DI.

The no-operands form of the instruction uses a short form of SCAS. Again,
ES:(E)DI is assumed to be the memory operand and AL, AX, or EAX is assumed
to be the register operand. The size of operands is selected by the
mnemonic: SCASB (byte comparison), SCASW (word comparison), or SCASD
(doubleword comparison).

After the comparison, the (E)DI register is incremented or decremented
automatically according to the setting of the DF flag in the EFLAGS
register. If the DF flag is 0, the (E)DI register is incremented; if the DF
flag is 1, the (E)DI register is decremented. The register is incremented
or decremented by 1 for byte operations, by 2 for word operations, and by 4
for doubleword operations.

SCAS, SCASB, SCASW, SCASD, and SCASQ can be preceded by the REP prefix for
block comparisons of ECX bytes, words, doublewords, or quadwords. Often,
however, these instructions will be used in a LOOP construct that takes
some action based on the setting of status flags. See “REP/REPE/REPZ
/REPNE/REPNZ—Repeat String Operation Prefix” in this chapter for a
description of the REP prefix.


In 64-bit mode, the instruction’s default address size is 64-bits, 32-bit
address size is supported using the prefix 67H. Using a REX prefix in the
form of REX.W promotes operation on doubleword operand to 64 bits. The
64-bit no-operand mnemonic is SCASQ. Address of the memory operand is
specified in either RDI or EDI, and AL/AX/EAX/RAX may be used as the
register operand. After a comparison, the destination register is
incremented or decremented by the current operand size (depending on the
value of the DF flag).

**Modified flags**: The OF, SF, ZF, AF, PF, and CF flags are set according to
the temporary result of the comparison.

<a name="TC_DMISS"></a>
```nasm
; Compare AL/AX/EAX/RAX with byte/word/doubleword/quadword at DI/EDI/RDI,
; then set status flags.
scas
scasb ;byte
scasw ;word
scasd ;doubleword
scasq ;quadword
```
**Code**: Scan string instruction.

<a name="TC_DMISSE"></a>
```nasm
global _start			

section .text
_start:

	; scasb/w/d/q 
	; Compare memory to register 

	cld
	mov rax, 0x1234567890abcdef
	lea rdi, [var1]
	scasq

	lea rdi, [var2]
	scasq


	; cmpsb/w/d/q
	; Compare memory to memory

	cld
	lea rsi, [var1]
	lea rdi, [var3]
	cmpsq
	
		
	; exit the program gracefully  

	mov rax, 0x3c
	mov rdi, 0		
	syscall


section .data

	var1:	dq	0x1234567890abcdef
	var2:	dq	0xfedcba1234567890
	var3:	dq	0x1234567890abcdef

```
**Code**: Scan string instruction example.

#### Store string
In non-64-bit and default 64-bit mode; stores a byte, word, or doubleword
from the AL, AX, or EAX register (respectively) into the destination
operand. The destination operand is a memory location, the address of which
is read from either the ES:EDI or ES:DI register (depending on the
address-size attribute of the instruction and the mode of operation). The
ES segment cannot be overridden with a segment override prefix.

At the assembly-code level, two forms of the instruction are allowed: the
“explicit-operands” form and the “no-operands” form. The explicit-operands
form (specified with the STOS mnemonic) allows the destination operand to
be specified explicitly. Here, the destination operand should be a symbol
that indicates the size and location of the destination value. The source
operand is then automatically selected to match the size of the destination
operand (the AL register for byte operands, AX for word operands, EAX for
doubleword operands). The explicit-operands form is provided to allow
documentation; however, note that the documentation provided by this form
can be misleading. That is, the destination operand symbol must specify the
correct type (size) of the operand (byte, word, or doubleword), but it does
not have to specify the correct location. The location is always specified
by the ES:(E)DI register. These must be loaded correctly before the store
string instruction is executed.

The no-operands form provides “short forms” of the byte, word, doubleword,
and quadword versions of the STOS instructions. Here also ES:(E)DI is
assumed to be the destination operand and AL, AX, or EAX is assumed to be
the source operand. The size of the destination and source operands is
selected by the mnemonic: STOSB (byte read from register AL), STOSW (word
from AX), STOSD (doubleword from EAX).

After the byte, word, or doubleword is transferred from the register to the
memory location, the (E)DI register is incremented or decremented according
to the setting of the DF flag in the EFLAGS register. If the DF flag is 0,
the register is incremented; if the DF flag is 1, the register is
decremented (the register is incremented or decremented by 1 for byte
operations, by 2 for word operations, by 4 for doubleword operations).

In 64-bit mode, the default address size is 64 bits, 32-bit address size is
supported using the prefix 67H. Using a REX prefix in the form of REX.W
promotes operation on doubleword operand to 64 bits. The promoted
no-operand mnemonic is STOSQ. STOSQ (and its explicit operands variant)
store a quadword from the RAX register into the destination addressed by
RDI or EDI. See the summary chart at the beginning of this section for
encoding data and limits.

The STOS, STOSB, STOSW, STOSD, STOSQ instructions can be preceded by the
REP prefix for block loads of ECX bytes, words, or doublewords. More often,
however, these instructions are used within a LOOP construct because data
needs to be moved into the AL, AX, or EAX register before it can be stored.

**Note**: To improve performance, more recent processors support
modifications to the processor’s operation during the string store
operations initiated with STOS and STOSB. See Section 7.3.9.3 in the Intel®
64 and IA-32 Architectures Software Developer’s Manual, Volume 1 for
additional information on fast-string operation.

<a name="TC_DMISTS"></a>
```nasm
; Store string from register to memory.
stos
stosb
stosw
stosd
stosq
```
**Code**: Store string instruction.

<a name="TC_DMISTS"></a>
```nasm
section	.text
   global _start        ;must be declared for using gcc
	
_start:	                ;tell linker entry point
   mov    ecx, len
   mov    esi, s1
   mov    edi, s2
	
loop_here:
   lodsb
   or      al, 20h
   stosb
   loop    loop_here	
   cld
   rep	movsb
	
   mov	edx,20	        ;message length
   mov	ecx,s2	        ;message to write
   mov	ebx,1	        ;file descriptor (stdout)
   mov	eax,4	        ;system call number (sys_write)
   int	0x80	        ;call kernel
	
   mov	eax,1	        ;system call number (sys_exit)
   int	0x80	        ;call kernel
	
section	.data
s1 db 'HELLO, WORLD', 0 ;source
len equ $-s1

section	.bss
s2 resb 20              ;destination
```
**Code**: Store string instruction example.

#### Compare
Compare the values of the two specified operands, setting the condition
codes in the machine status word appropriately. This instruction is
equivalent to the sub instruction, except the result of the subtraction is
discarded instead of replacing the first operand.

<a name="TC_DMIC"></a>
```nasm
cmp <reg>, <reg>
cmp <reg>, <mem>
cmp <mem>, <reg>
cmp <reg>, <con>
```
**Code**: Compare instruction.

#### Compare string
Compares the byte, word, doubleword, or quadword specified with the first
source operand with the byte, word, doubleword, or quadword specified with
the second source operand and sets the status flags in the EFLAGS register
according to the results.

Both source operands are located in memory. The address of the first source
operand is read from DS:SI, DS:ESI or RSI (depending on the address-size
attribute of the instruction is 16, 32, or 64, respectively). The address
of the second source operand is read from ES:DI, ES:EDI or RDI (again
depending on the address-size attribute of the instruction is 16, 32, or
64). The DS segment may be overridden with a segment override prefix, but
the ES segment cannot be overridden.

At the assembly-code level, two forms of this instruction are allowed: the
“explicit-operands” form and the “no-operands” form. The explicit-operands
form (specified with the CMPS mnemonic) allows the two source operands to
be specified explicitly. Here, the source operands should be symbols that
indicate the size and location of the source values. This explicit-operand
form is provided to allow documentation. However, note that the
documentation provided by this form can be misleading. That is, the source
operand symbols must specify the correct type (size) of the operands
(bytes, words, or doublewords, quadwords), but they do not have to specify
the correct location. Locations of the source operands are always specified
by the DS:(E)SI (or RSI) and ES:(E)DI (or RDI) registers, which must be
loaded correctly before the compare string instruction is executed.

The no-operands form provides “short forms” of the byte, word, and
doubleword versions of the CMPS instructions. Here also the DS:(E)SI (or
RSI) and ES:(E)DI (or RDI) registers are assumed by the processor to
specify the location of the source operands. The size of the source
operands is selected with the mnemonic: CMPSB (byte comparison), CMPSW
(word comparison), CMPSD (doubleword comparison), or CMPSQ (quadword
comparison using REX.W).

After the comparison, the (E/R)SI and (E/R)DI registers increment or
decrement automatically according to the setting of the DF flag in the
EFLAGS register. (If the DF flag is 0, the (E/R)SI and (E/R)DI register
increment; if the DF flag is 1, the registers decrement.) The registers
increment or decrement by 1 for byte operations, by 2 for word operations,
4 for doubleword operations. If operand size is 64, RSI and RDI registers
increment by 8 for quadword operations.

The CMPS, CMPSB, CMPSW, CMPSD, and CMPSQ instructions can be preceded by
the REP prefix for block comparisons. More often, however, these
instructions will be used in a LOOP construct that takes some action based
on the setting of the status flags before the next comparison is made. See
“REP/REPE/REPZ /REPNE/REPNZ—Repeat String Operation Prefix” in Chapter 4 of
the Intel® 64 and IA-32 Architectures Software Developer’s Manual, Volume
2B, for a description of the REP prefix.

In 64-bit mode, the instruction’s default address size is 64 bits, 32 bit
address size is supported using the prefix 67H. Use of the REX.W prefix
promotes doubleword operation to 64 bits (see CMPSQ). 

<a name="TC_DMICSO"></a>
```nasm
cmps
cmpsb
cmpsw
cmpsd
cmpsq
```
**Code**: Compare string instruction.

#### Compare and exchange
The cmpxchg instruction has two implicit operands AL/AX/EAX(depending on
the size of arg1) and ZF(zero) flag. The instruction compares arg1 to
AL/AX/EAX and if they are equal sets arg1 to arg2 and sets the zero flag,
otherwise it sets AL/AX/EAX to arg1 and clears the zero flag. Unlike xchg
there is not an implicit lock prefix and if the instruction is required to
be atomic then lock must be prefixed.

Operand one should be a register or memory but Operand two must to be a
registers.

**Modified flags**: The ZF flag is modified by this instruction.

<a name="TC_DMICAE"></a>
```nasm
cmpxchg arg1, arg2
```
**Code**: Compare and exchange instruction.

#### Data swap
The xchg instruction swaps the src operand with the dest operand. It's like
doing three move operations: from dest to a temporary (another register),
then from src to dest, then from the temporary to src, except that no
register needs to be reserved for temporary storage. 

If one of the operands is a memory address, then the operation has an
implicit LOCK prefix, that is, the exchange operation is atomic. This can
have a large performance penalty.

It's also worth noting that the common NOP (no op) instruction, 0x90, is
the opcode for ```xchgl eax, eax```.

Both operands can be either register or memory, but with one condition only
one operand can be in memory: the other must be a register.

**Modified flags**: No FLAGS are modified by this instruction.

<a name="TC_MDIDS"></a>
```nasm
xchg dest, src
```
**Code**: Data swap instruction.

#### Load effective address
The lea instruction calculates the address of the src operand and loads it
into the dest operand.

Source operand can be immediate, register or memory but destination operand
must to be a register.

**Modified flags**: No FLAGS are modified by this instruction.

<a name="TC_DMILEA"></a>
```nasm
lea dest, src
```
**Code**: Lea instruction.

**Note**: Load Effective Address calculates its src operand in the same way
as the mov instruction does, but rather than loading the contents of that
address into the dest operand, it loads the address itself.  lea can be
used not only for calculating addresses, but also general-purpose unsigned
integer arithmetic (with the caveat and possible advantage that FLAGS are
unmodified). This can be quite powerful, since the src operand can take up
to 4 parameters: base register, index register, scalar multiplier and
displacement, e.g. [eax + edx*4 -4] (Intel syntax) or -4(%eax, %edx, 4)
(GAS syntax). The scalar multiplier is limited to constant values 1, 2, 4,
or 8 for byte, word, double word or quad word offsets respectively. This by
itself allows for multiplication of a general register by constant values
2, 3, 4, 5, 8 and 9, as shown below:

<a name="TC_DMILEAE"></a>
```nasm
lea ebx, [ebx*2]      ; Multiply ebx by 2
lea ebx, [ebx*8+ebx]  ; Multiply ebx by 9, which totals ebx*18
```
**Code** Lea instruction example.

#### Load string
Loads a byte, word, or doubleword from the source operand into the AL, AX,
or EAX register, respectively. The source operand is a memory location, the
address of which is read from the DS:ESI or the DS:SI registers (depending
on the address-size attribute of the instruction, 32 or 16, respectively).
The DS segment may be overridden with a segment override prefix.

At the assembly-code level, two forms of this instruction are allowed: the
“explicit-operands” form and the “no-operands” form. The explicit-operands
form (specified with the LODS mnemonic) allows the source operand to be
specified explicitly. Here, the source operand should be a symbol that
indicates the size and location of the source value. The destination
operand is then automatically selected to match the size of the source
operand (the AL register for byte operands, AX for word operands, and EAX
for doubleword operands). This explicit-operands form is provided to allow
documentation; however, note that the documentation provided by this form
can be misleading. That is, the source operand symbol must specify the
correct type (size) of the operand (byte, word, or doubleword), but it does
not have to specify the correct location. The location is always specified
by the DS:(E)SI registers, which must be loaded correctly before the load
string instruction is executed.

The no-operands form provides “short forms” of the byte, word, and
doubleword versions of the LODS instructions. Here also DS:(E)SI is assumed
to be the source operand and the AL, AX, or EAX register is assumed to be
the destination operand. The size of the source and destination operands is
selected with the mnemonic: LODSB (byte loaded into register AL), LODSW
(word loaded into AX), or LODSD (doubleword loaded into EAX).

After the byte, word, or doubleword is transferred from the memory location
into the AL, AX, or EAX register, the (E)SI register is incremented or
decremented automatically according to the setting of the DF flag in the
EFLAGS register. (If the DF flag is 0, the (E)SI register is incremented;
if the DF flag is 1, the ESI register is decremented.) The (E)SI register
is incremented or decremented by 1 for byte operations, by 2 for word
operations, or by 4 for doubleword operations.

In 64-bit mode, use of the REX.W prefix promotes operation to 64 bits.
LODS/LODSQ load the quadword at address (R)SI into RAX. The (R)SI register
is then incremented or decremented automatically according to the setting
of the DF flag in the EFLAGS register.

The LODS, LODSB, LODSW, and LODSD instructions can be preceded by the REP
prefix for block loads of ECX bytes, words, or doublewords. More often,
however, these instructions are used within a LOOP construct because
further processing of the data moved into the register is usually necessary
before the next transfer can be made.

<a name="TC_DMILS"></a>
```nasm
; load string from memory to register.
loads
loadsb
loadsw
loadsd
loadsq
```
**Code**: Load string instruction.

<a name="TC_DMILSE"></a>
```nasm
global _start

section .text
_start:
	push rbp
	mov rbp, rsp

	; Clear direction flag
	cld
	lea rsi, [HelloWorld]
	lea rdi, [Copy]
	; Move qword from address (R|E)SI to (R|E)DI
	movsq

	cld 
	lea rsi, [HelloWorld]
	xor rax, rax
	mov qword [Copy], rax
	lea rdi, [Copy]
	mov rcx, len
	rep movsb

	mov rax, 0x0123456789abcdef
	lea rdi, [var1]
	stosq

	xor rax, rax
	lea rsi, [var1]
	lodsq

	leave
	mov rax, 60
	mov rdi, 0
	syscall

section .data
	HelloWorld: db "Hello World"
	len: equ $-HelloWorld

section .bss
	Copy: resb len
	var1: resb 8
```
**Code**: Load string instruction example.


### Arithmetic and Logical instruction
Arithmetic instructions take two operands: a destination and a source. The
destination must be a register or a memory location. The source may be
either a memory location, a register, or a constant value. Note that at
least one of the two must be a register, because operations may not use a
memory location as both a source and a destination.

#### Addition

<a name="TC_AALIADD"></a>
```nasm
add dest, src
adc dest, src ;plus carry flag
```
**Code**: Add instruction.

#### Subtraction

<a name="TC_AALISUB"></a>
```nasm
sub dest, src
sbb dest, src ; subtract with borrow
```
**Code**: Sub instruction.

#### Multiples
There are two instructions for multiplying binary data. The MUL (Multiply)
instruction handles unsigned data and the IMUL (Integer Multiply) handles
signed data. Both instructions affect the Carry and Overflow flag.

<a name="TC_AALIMUL"></a>
```nasm
mul multiplier       ; This multiplies arg by the value of corresponding byte-length in the AX register.
imul multiplier      ; As mul, only signed.
imul dest, src       ; This multiplies src by dest.
imul dest, src, aux  ; This multiplies src by aux and places it into dest.
```
**Code**: Mul instruction.

Multiplicand in both cases will be in an accumulator, depending upon the
size of the multiplicand and the multiplier and the generated product is
also stored in two registers depending upon the size of the operands.
Following section explains MUL instructions with three different cases.
* When two bytes are multiplied: <br />
  The multiplicand is in the AL register, and the multiplier is a byte in
  the memory or in another register. The product is in AX. High-order 8
  bits of the product is stored in AH and the low-order 8 bits are stored
  in AL.

<a name="TF_AALIMUL1"></a>
```ASCII
 +----+   +--------------+   +----+ +----+
 | AL | x | 8 Bit Source | = | AH | | AL |
 +----+   +--------------+   +----+ +----+ 
```
**Figure**: When two byte are multiplied.

* When two one-word values are multiplied: <br />
  The multiplicand should be in the AX register, and the multiplier is a
  word in memory or another register. For example, for an instruction like
  MUL DX, you must store the multiplier in DX and the multiplicand in
  AX.<br /> The resultant product is a doubleword, which will need two
  registers. The high-order (leftmost) portion gets stored in DX and the
  lower-order (rightmost) portion gets stored in AX.


<a name="TF_AALIMUL2"></a>
```ASCII
 +----+   +---------------+   +----+ +----+
 | AX | x | 16 Bit Source | = | DX | | AX |
 +----+   +---------------+   +----+ +----+ 
```
**Figure**: When two one-word values are multiplied.

* When two doubleword values are multiplied: <br />
  When two doubleword values are multiplied, the multiplicand should be in
  EAX and the multiplier is a doubleword value stored in memory or in
  another register. The product generated is stored in the EDX:EAX
  registers, i.e., the high order 32 bits gets stored in the EDX register
  and the low order 32-bits are stored in the EAX register.

<a name="TF_AALIMUL3"></a>
```ASCII
 +-----+   +---------------+   +-----+ +-----+
 | EAX | x | 32 Bit Source | = | EDX | | EAX |
 +-----+   +---------------+   +-----+ +-----+ 
```
**Figure**: When two double values are multiplied.

<a name="TC_AALIMULE"></a>
```nasm
MOV AL, 10
MOV DL, 25
MUL DL
...
MOV DL, 0FFH	; DL= -1
MOV AL, 0BEH	; AL = -66
IMUL DL
```
**Code**: Mul instruction example.

#### Divides
The division operation generates two elements - a quotient and a remainder.
In case of multiplication, overflow does not occur because double-length
registers are used to keep the product. However, in case of division,
overflow may occur. The processor generates an interrupt if overflow
occurs.

The DIV (Divide) instruction is used for unsigned data and the IDIV
(Integer Divide) is used for signed data.

<a name="TC_AALIDIV"></a>
```nasm
div divisor
idiv divisor
```
**Code**: Div instruction.

The dividend is in an accumulator. Both the instructions can work with
8-bit, 16-bit or 32-bit operands. The operation affects all six status
flags. Following section explains three cases of division with different
operand size:
* When the divisor is 1 byte: <br />
  The dividend is assumed to be in the AX register (16 bits). After
  division, the quotient goes to the AL register and the remainder goes to
  the AH register.

<a name="TF_AALIDIV1"></a>
```ASCII
    16 bit divident
        +----+
        | AX |          Quotient   Remainder
        +----+          +----+     +----+
    _________________ = | AL | And | AH |
    +---------------+   +----+     +----+
    | 8 bit Divisor |
    +---------------+ 
```
**Figure**: When the divisor is 1 byte.

* When the divisor is 1 word: <br />
  The dividend is assumed to be 32 bits long and in the DX:AX registers.
  The high-order 16 bits are in DX and the low-order 16 bits are in AX.
  After division, the 16-bit quotient goes to the AX register and the
  16-bit remainder goes to the DX register.

<a name="TF_AALIDIV2"></a>
```ASCII
    32 bit divident                         
    +----+  +----+                         
    | DX |  | AX |       Quotient   Remainder
    +----+  +----+       +----+     +----+   
    __________________ = | AX | And | DX |  
    +----------------+   +----+     +----+  
    | 16 bit Divisor |                      
    +----------------+
```
**Figure**: When the divisor is 1 word.

* When the divisor is doubleword: <br />
  The dividend is assumed to be 64 bits long and in the EDX:EAX registers.
  The high-order 32 bits are in EDX and the low-order 32 bits are in EAX.
  After division, the 32-bit quotient goes to the EAX register and the
  32-bit remainder goes to the EDX register.

<a name="TF_AALIDIV3"></a>
```ASCII
    64 bit divident                         
    +-----+  +-----+                         
    | EDX |  | EAX |     Quotient    Remainder
    +-----+  +-----+     +-----+     +-----+   
    __________________ = | EAX | And | EDX |  
    +----------------+   +-----+     +-----+  
    | 32 bit Divisor |                      
    +----------------+
```
**Figure**: When the divisor is doubleword.

#### Increment and Decrement

<a name="TC_AALINCDEC"></a>
```nasm
inc arg  ; Increments the register value in the argument by 1
dec arg  ; Decrements the register value in the argument by 1
```
**Code**: Increment and decrement instruction.

#### Arithmetic Flags Instruction
The status flag control instructions operate on the bits in the EFLAGS
register.

##### Complement Carry Flag
Reverses the setting of the carry flag; affects no other flags

<a name="TC_AALIAFICCF"></a>
```nasm
cmc
```
**Code**: Complement Carry Flag instruction.

##### Clear Carry Flag
Sets the carry flag to zero; affects no other flags.

<a name="TC_AALIAFCLCF"></a>
```nasm
clc
```
**Code**: Clear Carry Flag instruction.

##### Set Carry Flag
Sets the carry flag to 1.

<a name="TC_AALIAFSCF"></a>
```nasm
stc
```
**Code** Set Carry Flag instruction.

#### Logical Operation
The processor instruction set provides the instructions AND, OR, XOR, TEST,
and NOT Boolean logic, which tests, sets, and clears the bits according to
the need of the program.

<a name="LT_AALILO"></a>

Instruction | Format
------------|--------
AND | AND operand1, operand2
OR  | OR operand1, operand2
XOR | XOR operand1, operand2
TEST | TEST operand1, operand2
NOT | NOT operand1

**Table**: Logiacl Operand instruction.

The first operand in all the cases could be either in register or in
memory.  The second operand could be either in register/memory or an
immediate (constant) value. However, memory-to-memory operations are not
possible.  These instructions compare or match bits of the operands and set
the CF, OF, PF, SF and ZF flags.

#### Shift and Rotate
The bit shifts are sometimes considered bitwise operations, because they
treat a value as a series of bits rather than as a numerical quantity. In
these operations the digits are moved, or shifted, to the left or right.
Registers in a computer processor have a fixed width, so some bits will be
"shifted out" of the register at one end, while the same number of bits are
"shifted in" from the other end; the differences between bit shift
operators lie in how they determine the values of the shifted-in bits.

##### Logical Shift
In a logical shift instruction (also referred to as unsigned shift), the
bits that slide off the end disappear (except for the last, which goes into
the carry flag), and the spaces are always filled with zeros. Logical
shifts are best used with unsigned numbers.

<a name="TF_AALISARLSI"></a>
```ASCII
                 Logical Shift Left (SHL)                                  
  +----+   +-----------------------------------+                           
  | CF |<--|                                   |<-- Bit                    
  +----+   +-----------------------------------+                           
           ^                                   ^                           
           |                                   |                           
          High Significant Bit (HSB)          Lowest Significant Bit (LSB) 
                                                                           
                                                                           
                 Logical Shift Right (SHR)                                 
           +-----------------------------------+   +----+                 
    Bit -->|                                   |-->| CF |                 
           +-----------------------------------+   +----+                 
           ^                                   ^                           
           |                                   |                           
          HSB                                 LSB                          
```
**Figure**: Logical shift.

<a name="TC_AALISARLSI"></a>
```nasm
shr dest, src ; Shift right
shl dest, src ; Shift left
```
**Code**: Logical shift instruction.

##### Arithmetic Shift
In an arithmetic shift (also referred to as signed shift), like a logical
shift, the bits that slide off the end disappear (except for the last,
which goes into the carry flag). But in an arithmetic shift, the spaces are
filled in such a way to preserve the sign of the number being slid. For
this reason, arithmetic shifts are better suited for signed numbers in
two's complement format.

As explained the behavior of arithmetic shift right (SAR) is different base
on operand:
* **Positive operand**:
<a name="TF_AALIASPO"></a>

```ASCII
Initial State.                                                              
    +---------------------------------------------------------------+       
    |0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0 0|1|0|0|0|1|1|1|       
    +---------------------------------------------------------------+       
                                                                            
After one bit shift arithmetic right (SAR) instruction.                 CF  
    +---------------------------------------------------------------+  +---+
  ->|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0 0|1|0|0|0|1|1|->| 1 |
  | +---------------------------------------------------------------+  +---+
  |  |                                                                      
  |__|                                                                      
  Because it's positive operand 0 is filled up.
```
**Figure**: Arithmetic shift right with positive operand.

* **Negative operand**:

<a name="TF_AALIASPN"></a>
```ASCII
Initial State.                                                              
    +---------------------------------------------------------------+       
    |0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0 0|1|0|0|0|1|1|1|       
    +---------------------------------------------------------------+       
                                                                            
After one bit shift arithmetic right (SAR) instruction.                 CF  
    +---------------------------------------------------------------+  +---+
  ->|1|1|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0|0|1|0|0 0|1|0|0|0|1|1|->| 1 |
  | +---------------------------------------------------------------+  +---+
  |  |                                                                      
  |__|                                                                      
  Because it's negative operand 1 is filled up.
```
**Figure**: Arithmetic shift right with negative operand.

<a name="TC_AALISARASESI"></a>
```nasm
sar dest, src  ; Shift Arithmetic Right
sal dest, src  ; Shift Arithmetic Left
```
**Code**: Arithmetic shift instruction.

##### Double Shift
The SHLD (shift left double) and SHRD (shift right double) instructions
shift a specified number of bits from one operand to another They are
provided to facilitate operations on unaligned bit strings. They can also
be used to implement a variety of bit string move operations.

<a name="TF_AALIASDS"></a>
```ASCII
SHLD Instruction:                                          
            31                                  0          
   +----+   +-----------------------------------+          
   | CF |<--|  Destination (Memory or Register) |<--|      
   +----+   +-----------------------------------+   |      
                                                    |      
          __________________________________________|      
         |                                                 
         |  31                                  0          
         |  +-----------------------------------+          
         |__|           Source (Register)       |          
            +-----------------------------------+          
                                                           
SHRD Instruction:                                          
            31                                  0          
            +-----------------------------------+          
            |           Source (Register)       |___       
            +-----------------------------------+   |      
                                                    |      
          __________________________________________|      
         |                                                 
         |  31                                  0          
         |  +-----------------------------------+   +----+ 
         |->|  Destination (Memory or Register) |-->| CF | 
            +-----------------------------------+   +----+
```
**Figure**: SHLD and SHRD Instruction Operation.

The SHLD instruction shifts the bits in the destination operand to the left
and fills the empty bit positions (in the destination operand) with bits
shifted out of the source operand. The destination and source operands must
be the same length (either words or doublewords). The shift count can range
from 0 to 31 bits. The result of this shift operation is stored in the
destination operand, and the source operand is not modified. The CF flag is
loaded with the last bit shifted out of the destination operand.

The SHRD instruction operates the same as the SHLD instruction except bits
are shifted to the right in the destination operand, with the empty bit
positions filled with bits shifted out of the source operand.

##### Rotate
In a rotate instruction, the bits that slide off the end of the register
are fed back into the spaces.

<a name="TF_AALISARR"></a>
```ASCII
Rotate Right (ROR)                                        
          ________________________________________        
         |                                        |       
         |  31                                  0 |       
         |  +-----------------------------------+ | +----+
         |->|                                   |-->| CF |
            +-----------------------------------+   +----+
                                                      
Rotate Left (ROL)                                     
                                                      
            31                                  0
   +----+   +-----------------------------------+     
   | CF |<--|                                   |<--| 
   +----+ | +-----------------------------------+   | 
          |                                         | 
          |_________________________________________| 
```
**Figure**: Rotate Instruction Operation.

<a name="TC_AALISARR"></a>
```nasm
ror dest, src  ; Rotate Right
rol dest, src  ; Rotate Left
```
**Code**: Rotate Instruction.

#### Rotate With Carry
Like with shifts, the rotate can use the carry bit as the "extra" bit that
it shifts through.

<a name="TF_AALISARRWC"></a>
```ASCII
RCR:
          ___________________________________________________  
         |                                                   | 
         |  31                                  0            | 
         |  +-----------------------------------+   +----+   | 
         |->|                                   |-->| CF |---| 
            +-----------------------------------+   +----+     
                                                               

RCL:
                      31                                  0    
             +----+   +-----------------------------------+    
         |---| CF |<--|                                   |<--|
         |   +----+   +-----------------------------------+   |
         |                                                    |
         |____________________________________________________|
```
**Figure**: Rotate with carry instruction operation.

<a name="TC_AALISARRWC"></a>
```nasm
rcr dest, src
rcl dest, src
```
**Code**: Rotate with carry instruction.

### Control Flow instruction
