# Memory Model

## Table of Contents
1. [Introduction](#Introduction)
1. [How Memory Addressing Work Under Real Mode Flat Model](#HRealModeFlatModel)
1. [How Memory Addressing Work Under Real Mode Segmented Model](#HRealModeSegmentedModel)
	1. [Backward Compatibility](#BackwardCompatibility)
	1. [16-Bit blinders](#16-BitBlinders)
	1. [The Nature of Segments](#TheNatureofSegments)
	1. [Making 20-Bit Addresses out of 16-Bit Registers](#Making20-BitAddreeses)
1. [16-Bit and 32-Bit Registers](#16BitAnd32BitRegisters)
	1. [General-Purpose Register](#GeneralPurposeRegister)
	1. [The Instruction Pointer](#TheInstructionPointer)
	1. [The Flags Register](#TheFlagsRegister)
1. [Real Mode Flat Model](#RealModeFlatModel)
1. [Real Mode Segmented Model](#RealModeSegmentedModel)
1. [Protected Model Flat Model](#ProtectedModelFlatModel)
## Table of Figure
1. [8080 Memory Addressing Scheme](#F_8080MemoryAddrScheme)
1. [8080 Memory Model](#F_8080MemoryModel)
1. [The 8080 Memory Model Inside an 8086 Memory System](#F_8080MemoryModelInsideAn8086)
1. [Seeing a megabyte through 64K blinders](#F_SeeingAMBThrough64KBlinders)
1. [Memory Addresses versus segment addresses](#F_MemoryAddrVersusSegmentAddr)
1. [Segment Position in Real Mode](#F_SegmentPositionInRealMode)
1. [Generating 20-bit physical address in real mode](#F_Generating20BitPhysicalAddressInRealMode)
1. [Segments and offsets](#F_SegmentsAndOffsets)
1. [Initializing The Data Segment Register](#F_InitializingTheDataSegmentRegister)
1. [The six segments of the memory system](#F_TheSixSegmentsOfTheMemorySystem)
1. [Extending 16-bit general-purpose registers](#F_Extending16BitGeneralPurposeRegisters)
1. [8-Bit, 16-Bit, and 32-Bit registers](#F_8-16-32-BitRegisters)
1. [Real Mode Flat Model](#F_RealModeFlatModel)
1. [Real Mode Segmented Model](#F_RealModeSegmentedModel)
1. [Protected Model Flat Model](#F_ProtectedModeFlatModel)

## List of Tables
1. [Terms of Memory](#T_TermsOfMemory)
1. [FLAGS](#T_FlagsRegister)

<a name="Introduction"></a>
## Introduction
Different ways to address memory in the x86 CPU family called memory model.
x86 CPU family has two major memory models:
* **Real Model of Addressing**
	* **Real Mode Flat Model** (**Oldest Memory Model**)
	* **Real Mode Segmented Model** (**Elderly Memory Model, Now retired**)
* **Protected Mode Flat Model** (**Newest**)

The difference between them lie (*mostly*) in the use of registers to
address memory.

Before we start the discussion let's ask a simple question, *What is the
Register?* A **Register** is a small storage space available as part of the
CPU.

<a name="HRealModeFlatModel"></a>
## How Memory Addressing Work Under Real Mode Flat Model
Real mode flat model converting *one* address value into a physically
meaningful location in the RAM.

**8080** CPU specification:
* 8-bit CPU, meaning it processed 8 bits of information at a time.
* 16 Address line, so it could address 64K bytes (**2^16**).

Important measure of a CPU's effectiveness:
* General-purpose register bits wide.
* Address Lines that CPUs could muster in one operation (**More important
  measurement unit**)

<a name="F_8080MemoryAddrScheme"></a>
```
            Input                                           Output
	+--------------------+                          +--------------------+
	|   16-bits Address  | ===== Address Line ===== |    8-bits value    |
	+--------------------+                          +--------------------+
```
**Figure**: 8080 Memory Addressing scheme. Address of returned 8-bit value,
are those 16-bit inputted address.

No necessary relation between the number of address lines in a memory
system and the size of the data stored at each location. 8080 stored 8-bit
at each location, but it could have stored 16 or 32 bit at each location.

OS used with the **8080 CPU** was **CP/M-80**. CP/M-80 OS was unusal, it
existed at top of memory, some time it could be contained in ROM, but
mostly just to get it out of the ways and allow a consistent memory
strating point for transient programs, those that were loded into memory
and run only when needed. CP/M-80 read program in from of disk to run it,
it would load the program into low memory, at address 0100H (256 byte from
the very bottom of memory).

The first 256 bytes of memory were called the **program segment prefix**
(**PSP**) and contained various odd bits of information as well as a
general-purpose memory buffer for the program's disk input/output (I/O).
The executable code itself did not begin until address 0100H.

<a name="F_8080MemoryModel"></a>
```
               0FFFFH +-------------------+ 64K
                      |                   |
                      |                   |
                      |  Address Without  |
                      |  Installed Memory |
                      |                   |
                      |                   |
                      |                   |
                      |                   |
                      |                   |
                      |                   |
     Top of Installed +-------------------+  Often 16K, 32K, or 48K
         Memory       |                   |
                      |      CP/M-80      |
                      | Operating System  |
                      |                   |
                      |                   |
                      |                   |
                      |                   |
                      +-------------------+
                      |                   |
                      |   Unused Memory   |
                      |                   |
                      +-------------------+
                      |                   |
                      |                   |
                      |    Transient      |
                      |   Program Code    |
                      |                   |
                      |                   |
                      |                   |
                0100H +-------------------+  <-- Code Execution Begins Here
                      |                   |
                      |  Program Segment  |
                      |      Prefix       |
                      |       (PSP)       |
                      |                   |
                0000H +-------------------+

```
**Figure**: 8080 Memory Model

<a name="HRealModeSegmentedModel"></a>
## How Memory Addressing Work Under Real Mode Segmented Model
**8086** was first 16-bit CPU of Intel Corporation. The translation method
used for translate older CP/M-80 sofware from the 8080 to the 8086 CPUs
called **porting**.

**8086** CPU specification:
* 16-bit CPU, meaning it processed 16 bits of information at a time.
* 20 Address line, so it could address 1MB (**2^20**).

For compatibility with older version of CPU, 8086 CPU let program take some
64K byte segment within that megabyte of memory and run entirely inside it
(Programs thought it were the smaller 8080 memroy system). This
compatibility done by **Segment Register**, which are basically memory
pointers located in CPU registers.

**Segment Register** (**CS**) point to a place to memory where things
begin.

**8088 8086** CPU has four *Segment Register*.

<a name="F_8080MemoryModelInsideAn8086"></a>
```
     20-Bit
  Memory Address
        0FFFFFH --- +-------------------+ ---- 1MB
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    +-------------------+
                    |        64K        |
                    |   Memory Segment  |
                    |                   |
     080000H ------ +-------------------+ <--- Segment Register
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
                    |                   |
      00000H ------ +-------------------+
```
**Figure**: The 8080 Memory Model Inside an 8086 Memory System.

The problem began when 8080 architectures dominate the architecture of 8086. 
When new programmers begin writing new programs from scratch and needing 
more than 64K of memory for their programs they had to use Segment Registers
for switching between 64K memory.

**Real Mode Segmented Model** Specification:<br />
* Can be used upto 1MB of directly addressable memory
* 20 Address Lines 

**Note**: Memory addresses are expressed in base 16 (Hexadecimal).

<a name="BackwardCompatibility"></a>
### Backward Compatibility
When a 32-bit CPU is operating in **Protected Mode Flat Model**, a segment
is 4GB -- so one segment is, for the most part, plenty.

CPU can limits themselvs to what the older chips could address and execute.
when newer CPU need to run ancient program **Real Mode Segmented Model**,
it pulls a neat trick that, temporarily, makes it **become** an 8086.
This is called **virtual-86 mode**.

<a name="16-BitBlinders"></a>
### 16-Bit blinders
In **Real Mode Segmented Model**, an x86 CPU can *see* a full megabyte of
memory. That is, the CPU chips set themselves up so that they can use 20 of
their 32 address pins and can pass a 20-bit address to the memory system.
The main problem of **Real Mode Segmented Model** is, as far as those CPUs
can see a full megabyte of memory, they are constrained to look at that
megabyte through 16-bit blinders.

<a name="F_SeeingAMBThrough64KBlinders"></a>
```

     +--------+ 0FFFFFH
     |        |
     |        | A full one megabyte (1,048,576 bytes)
     |        | of memory is at the CPU's disposal. However...
     +--------+
     |\       |\
     | \      | \
     +--\-----+  \
     |\  \    |\  \
     | \  \   | \  \
     |  \  \  |  \  \
     |   \  \ |   \  \
     |    \  \|    \  \
     |     \  \     \  \
     |      \ |\     \  \
     |       \| \     \  \
     |        \  \   +--------+ The CPU can slide this metaphorical pice of 
     |        |\  \  |        | cardboard up and down, at any one time it 
     |        | \  \ |        | can see 1K byte.
     +--------+  \  \|        |
00000H            \  |        |
                   \ |        |\
                    \|        | \
                     |        |  \
                     |        |\  \
                     +--------+ \  \
                        \   \    \  \
                         \   \    \  \
                          \   \    \  \
                          +------------+
                          |  x86 CPU   |
                          |  in Real   |
                          |   Mode     |
                          +------------+
```
**Figure**: Seeing a megabyte through 64K blinders

**Note**: CPU look at memory as chunks in **Real Mode Segmented Model**.

<a name="TheNatureofSegments"></a>
### The Nature of Segments
Segment is a chunk of memory within the larger memory space that the CPU
can see and use. In the context of real mode segmented model, a segment is
a region of memory that begins on a paragraph boundary and extends for some
number of bytes (less than or equal to 64K (65,536)).

A paragraph is a measure of memory equal to 16 bytes. It is one of numerous
technical terms used to describe various quantites of memory.

<a name="T_TermsOfMemory"></a>

**Table**: Terms of Memory

Size | Decimal | Hex
-----|---------|-----
Byte | 1 | 01H
Word | 2 | 02H
Double word | 4 | 04H
Quad word | 8 | 08H
Ten byte | 10 | 0AH
Paragraph | 16 | 10H
Page, or page frame, (almost never used) | 256 | 100H
Segment | 65,536 | 10000Hy

Any memory address evenly divisible by 16 is called a paragraph boundary.
The first paragraph boundary is address 0. The second is address 10H; the
third address 20H, and so on. (Remember that 10H is equal to decimal 16.)

Any paragraph bboundary may be considered the start of a segment. This
doesn't mean that a segment actually starts every 16 bytes up and down
throughout that megabyte of memory.

Paragraph boundaries are little slots at which a segment may be begun. In
real mode segmented model, a program may make us of only four or five
segments, but each of those segments may begin at any of the 65,536
paragraph boundaries existing in the megabyte of memory available in the
real mode segmented model.

There's that number again: 65,536 -- our beloved 64K. There are 64K
different paragraph boundaries where a segment may begin. Each paragraph
boundary has a number. As always, the numbers begin from 0, and go to 64K
minus one; in decimal 65,535, or in hex 0FFFFH. Because a segment may begin
at any paragraph boundary, the number of the pragraph boundary at which a
segment begins is called the segment address of that particular segment.

Segment address in real mode segmented model is 16 bytes (one paragraph).

**In Summary**: segments may begin at any segment address. There are 65,536
segment address evnly distributed across real mode's full megabyte of
memory, sixteen bytes apart. A segment address is more a permission than a
compulsion; for all the 64K possible segment addresses, only five or six
are ever actually used to begin segments at any one time. Think of segment
addresses as slots where segments may be placed.

So much for segment addresses;  now, what of segments themselves? The most
imporant thing to understand about a segment is that it may be up to 64K
bytes in size, but it doesn't have to be. A segment may be only one byte
long, or 256 bytes long, or 21,378 bytes long, or any length at all short
of 64K bytes.

<a name="F_MemoryAddrVersusSegmentAddr"></a>
```
                 +------------------+ ----- 0FFFFFH
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
      0FFFFH --- |------------------| ----- 0FFFF0H

   Segment addresses                  Memory addresses
   in the range                       in the range
   0000H - 0FFFFH                     00000H - 0FFFFFFH

                 |                  |
      0002H ---- |------------------| ----- 00020H
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
     0001H ----- |------------------| ----- 00010H
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------| ----- 00008H
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------|
                 |                  |
                 |------------------| ----- 00002H
                 |                  |
                 |------------------| ----- 00001H
                 |                  | 
     0000H ----- +------------------+ ----- 00000H
```
**Figure**: Memory Addresses versus segment addresses

You define a segment primarily by stating where it begins. What, then,
defines how long a segment is? Nothing, really.

A segment is more a horizon than a place. Once you define where a segment
begins, that segment can encompass any location in memory between that
starting place and the horizon—which is 65,536 bytes down the line.

Nothing dictates, of course, that a segment must use all of that memory. In
most cases, when a segment is defined at some segment address, a program
considers only the next few hundred or perhaps few thousand bytes as part
of that segment, unless it’s a really world-class program. Most beginners
reading about segments think of them as some kind of memory allocation, a
protected region of memory with walls on both sides, reserved for some
specific use.  

In real mode nothing is protected within a segment, and segments are not
reserved for any specific register or access method. Segments can overlap.
(People often don’t think about or realize this.) In a very real sense,
segments don’t really exist, except as horizons beyond which a certain type
of memory reference cannot go. It comes back to that set of 64K blinders
that the CPU wears.

Think of segment in this way: A segment is the location in memory at which
the CPU’s 64K blinders are positioned. In looking at memory through the
blinders, you can see bytes starting at the segment address and going on
until the blinders cut you off, 64K bytes down the way.

<a name="F_SegmentPositionInRealMode"></a>
**Figure**: Segment Position in Real Mode
```
                                                                                
    +---------+         +---------+         +---------+              +---------+
    |         |         |         |         |         |              |         |
    |         |         |         |         |         |              |         |
    |         |       +-------------+      +------------+            |         |
  +-------------+     | |         | |      ||         | |            |         |
  | |         | |     | |         | |     +------------+|        +-+-------------+-+
  | |         | |     +-------------+     |+-----------|+        | | |         | | |
  +-------------+       |         |       | |         ||         | | |         | | |
  | |         | |     +-------------+     +------------+         +-+-------------+-+
  | |         | |     | |         | |       |         |              |         |
  +-------------+     | |         | |       |         |              |         |
    |         |       +-------------+       |         |              |         |
    |         |         |         |         |         |              |         |
    |         |         |         |         |         |              |         |
    +---------+         +---------+         +---------+              +---------+
                                                                                        
     A: Adjacent        B: Disjoint         C: Partially Overlapped  D: Fully Overlapped
```

<a name="Making20-BitAddreeses"></a>
### Making 20-Bit Addresses out of 16-Bit Registers
A register is a memory location inside the CPU chip, rather than outside
the CPU in a memory bank somewhere. The 8088, 8086, and 80286 are often
called 16-bit CPUs because their internal registers are almost all 16 bits
in size.

Registers do many jobs, but perhaps their most important single job is
holding addresses of important locations in memory. If you recall, the 8086
and 8088 have 20 address pins, and heir megabyte of memory (*which is the
real mode segmented memory we're talking about*) requires addresses 20 bits
in size.

How do you put a 20-bit memory address in a 16-it register? you don't. You
put a 20-bit address in two 16-bit registers.

What happens is this: all memory locations in real mode’s megabyte of
memory have not one address but two. Every byte in memory is assumed to
reside in a segment. A byte’s complete address, then, consists of the
address of its segment, along with the distance of the byte from the start
of that segment.  Recall that the address of the segment is the byte’s
**segment address**. The byte’s distance from the start of the segment is
the byte’s **offset address**. Both addresses must be specified to
completely describe any single byte’s location within the full megabyte of
real mode memory. When written out, the segment address comes first,
followed by the offset address. The two are separated with a colon.
Segment:offset addresses are always written in hexadecimal.

<a name="F_Generating20BitPhysicalAddressInRealMode"></a>
**Figure**: Generating 20-bit physical address in real mode
```
    <---- 16 bits  ---->       
    +------------------+       
 CS | A | 0  | 0  | 0  |       
    +------------------+       
        <---- 16 bits  ---->   
        +------------------+   
+    IP |  5 | F  | 0  | 0 |   
        +------------------+   
_____________________________  
                               
    <------- 20 bits ------->  
    +-----------------------+  
    | A | 5  | F  | 0  |  0 |  
    +-----------------------+  
```

The universe is perverse, however, and clear eyes will perceive that MyByte
can have two other perfectly legal addresses: 0:0029 and 0002:0009. How so?
Keep in mind that a segment may start every 16 bytes throughout the full
megabyte of real memory. A segment, once begun, embraces all bytes from its
origin to 65,535 bytes further up in memory. There's nothing wrong with
segments overlapping. MyByte is 2DH bytes into the first segment, which
begins at segment address 0000H. MyByte is 1DH bytes into the second
segment, which begins at segment address 0001H. It's not that MyByte is in
two or threee places at once. Its in only one place, but that one place may
be described in any of three ways.

In summary: to express a 20-bit address in two 16-bit registers is to put
the segment address into one 16-bit register, and the offset address into
another 16-bit register. The two registers taken together identify one byte
among all 1,048,576 bytes in real mode’s megabyte of memory.

<a name="F_SegmentsAndOffsets"></a>
**Figure**: Segments and offsets
```
MyByte Could have any of theree possible addresses:
0000:0029
0001:0019
0002:0009

                             |                  |
                             |------------------|
                             |                  |
 --------------------------- |------------------| ----- MyByte
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     | 9H Bytes      |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |     |               |------------------|
 |     |     |               |                  |
 |     |      -- 0002H ----- |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     | 19H Bytes           |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |     |                     |------------------|
 |     |                     |                  |
 |      ------- 0001H ------ |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 | 29H Bytes                 |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
 |                           |------------------|
 |                           |                  |
  -------------- 0000H ----- +------------------+
```

<a name="16BitAnd32BitRegisters"></a>
## 16-Bit and 32-Bit Registers
Think of the segment address as the starting position of real mode’s 64K
blinders. Typically, you would move the blinders to encompass the location
where you wish to work, and then leave the blinders in one place while
moving around within their 64K limits.

This is exactly how registers tend to be used in real mode segmented model
assembly language. The 8088, 8086, and 80286 have exactly four segment
registers specifically designated as holders of segment addresses. The 386
and later CPUs have two more that can also be used in real mode. (You need
to be aware of the CPU model you’re running on if you intend to use the two
additional segment registers, because the older CPUs don’t have them at
all.)

Each segment register is a 16-bit memory location existing within the CPU
chip itself. No matter what the CPU is doing, if it’s addressing some
location in memory, then the segment address of that location is present in
one of the six segment registers.

The segment registers have names that reflect their general functions: CS,
DS, SS, ES, FS, and GS.

FS and GS exist only in the 386 and later Intel x86 CPUs—but are still 16
bits in size.

**All segment registers are 16 bits in size, irrespective of the CPU. This is
true even of the 32-bit CPUs**.

* Code Segment (**CS**):<br />
	Machine instructions exist at some offset into a code segment. The
	segment address of the code segment of the currently executing
	instruction is contained in **CS**.

* Data Segment (**DS**):<br />
	Variables and other data exist at some offset into a data segment.
	There may be many data segments, but the CPU may only use one at a
	time, by placing the segment address of that segment in register
	DS.

* Stack Segment (**SS**):<br />
	The stack is a very important component of the CPU used for
	temporary storage of data and addresses. Therefore, the stack has a
	segment address, which is contained in register SS.

* Extra Segment (**ES**):<br />
	The extra segment is exactly that: a spare segment that may be used
	for specifying a location in memory.

* Extra Segment **FS** and **GS**:<br />
	This two segment are clones of **ES**. These are just additional
	segments, no specialty here. Names **FS** and **GS** come from the
	fact that they were created after **ES**: **E**, **F**, **G**.
	These segment can be used for both data or code and they are exist
	only in the 386 and later x86 CPUs.

**In Summary**: very byte of memory, accessible by program, is assumed to
reside in a segment. Segment size varies and can range from 1 byte to 64
Kbytes.  Nothing is protected within a segment in Real Mode. Segments can
overlap.

Initializing the data segment register in 16-bit real mode:<br /> 
Numerical (immdeidate) values cannot be moved directly into the segment
register. It is a 2-step process.

<a name="F_InitializingTheDataSegmentRegister"></a>
**Figure**: Initializing the data segment register
```
mov ax, 1000h
mov ds, ax

                                    
   +---------+            +-------+ 
AX | 10 | 00 | <--------- | 1000H | 
   +---------+            +-------+ 
                                    
   +---------+           +---------+
DS | 10 | 00 |        AX | 10 | 00 |
   +---------+           +---------+
     ^     ^--------------------|   
     |                     |        
     |----------------------    
```

Recall that 8086 and 8088 CPUs had 20 address pins, limiting a program to 1
megabyte of memory.<br />
To express a 20-bit address, two 16-bit registers are used:
* **Segment address** in one 16-bit register.
* **Offset address** in another 16-bit register.

<a name="F_TheSixSegmentsOfTheMemorySystem"></a>
**Figure**: The six segments of the memory system.
```
                                            +-------------------+
                                            |                   |
                                            |                   |
                                            |-------------------|
                                            |                   |
                            --------------->|       CODE        |
                            |               |                   |
                            |               |-------------------|
                            |               |                   |
                            |               |                   |
    +-------------------+   |               |-------------------|
    |        CS         |---|               |                   |
    |-------------------|     ------------->|       STACK       |
    |        SS         |-----|             |                   |
    |-------------------|                   |-------------------|
    |        DS         |-------------|     |                   |
    |-------------------|             |     |                   |
    |        ES         |-----------| |     |-------------------|
    |-------------------|           | |     |                   |
    |        FS         |---------| | ----->|       DATA        |
    |-------------------|         | |       |                   |
    |        GS         |-------| | |       |-------------------|
    +-------------------+       | | |       |                   |
                                | | |       |                   |
                                | | |       |-------------------|
                                | | |       |                   |
                                | | ------->|       DATA        |
                                | |         |                   |
                                | |         |-------------------|
                                | |         |                   |
                                | |         |                   |
                                | |         |-------------------|
                                | |         |                   |
                                | --------->|       DATA        |
                                |           |                   |
                                |           |-------------------|
                                |           |                   |
                                |           |                   |
                                |           |-------------------|
                                |           |                   |
                                ----------->|       DATA        |
                                            |                   |
                                            +-------------------+
```

<a name="GeneralPurposeRegister"></a>
### General-Purpose Registers
The segment registers exist only to hold segment addresses.

The x86 CPUs have a crew of generalist registers to do the rest of the work
of assembly language computing. Among many other things, these
general-purpose registers are used to hold the offset addresses that must
be paired with segment addresses to pin down a single location in memory.
They also hold values for arithmetic manipulation, for bit-shifting (more
on this later) and many other things.

Like the segment registers, the general-purpose registers are memory
locations existing inside the CPU chip itself; and like the segment
registers, they all have names rather than numeric addresses. The
general-purpose registers really are generalists in that all of them share
a large suite of capabilities. How- ever, some of the general-purpose
registers also have what I call a ‘‘hidden agenda’’: a task or set of tasks
that only it can perform.

In our current 32-bit world, the general-purpose registers fall into three
general classes:
* The 32-bit extended general-purpose registers
* The 16-bit general-purpose registers
* The 8-bit general-purpose registers

These three classes do not represent three entirely distinct sets of
registers at all. The 16-bit and 8-bit registers are actually names of
regions inside the 32-bit registers. Register growth in the x86 CPU family
has come about by extending registers existing in older CPUs. Adding a room
to your house doesn’t make it two houses—just one bigger house.

There are eight 16-bit general-purpose registers: AX, BX, CX, DX, BP, SI,
DI, and SP. (SP is a little less general than the others, but we’ll get to
that.) They are all 16 bits in size, and you can place any value in them
that may be expressed in 16 bits or fewer. When Intel expanded the x86
architecture to 32 bits in 1986, it doubled the size of all eight registers
and gave them new names by prefixing an E in front of each register name,
resulting in EAX, EBX, ECX, EDX, EBP, ESI, EDI, and ESP.

<a name="F_Extending16BitGeneralPurposeRegisters"></a>
**Figure**: Extending 16-bit general-purpose registers.
```
32-Bit register names                    +----------------------------------+
                                         |      16-Bit register names       |
        Bit 31                           |                           Bit 0  |
          ^                              |              SI              ^   |
         +---------------------------------------------------------------+  |
ESI      | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |  |
         +---------------------------------------------------------------+  |
                                         |              DI                  |
         +---------------------------------------------------------------+  |
EDI      | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |  |
         +---------------------------------------------------------------+  |
                                         |              BP                  |
         +---------------------------------------------------------------+  |
EBP      | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |  |
         +---------------------------------------------------------------+  |
                                         |              SP                  |
         +---------------------------------------------------------------+  |
ESP      | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | |  |
         +---------------------------------------------------------------+  |
            The "High" 16 Bits           |         The "Low" 16 Bits        |
                                         +----------------------------------+
                                         This portion of the registers is
                                         what on the older 16-bit x86 CPUS:
                                         The 8086, 8088, and 80286.
```
Unfortunately, the high 16 bits of the 32-bit general-purpose registers do
not have their own names. You can access the low 16 bits of ESI as SI, but
to get at the high 16 bits, you must refer to ESI and get the whole 32-bit
shebang.

The low 16 bits are themselves divided into two 8-bit halves.

The 16-bit registers AX, BX, CX, and DX are present as the lower 16-bit
portions of EAX, EBX, ECX, and EDX; but AX, BX, CX, and DX are themselves
divided into 8-bit halves, and assemblers recognize special names for the
two halves. The A, B, C, and D are retained, but instead of the X, a half
is specified with an H (for high half) or an L (for low half). Each
register half is one byte (8 bits) in size.

<a name="F_8-16-32-BitRegisters"></a>
**Figure**: 8-Bit, 16-Bit, and 32-Bit registers
```
32-Bit: EAX
16-Bit: AX
8-Bit Low: AL
8-Bit High: AH 

+-------------------------------------------------------------------+
|                 E               +---------------------------------|
|                                 |               X                 |
|                                 |---------------+-----------------|
|                                 |       H       |       L         |
| +---------------------------------------------------------------+ |
| | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | A
| +---------------------------------------------------------------+ |
|                                 |               |                 |
| +---------------------------------------------------------------+ |
| | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | B
| +---------------------------------------------------------------+ |
|                                 |               |                 |
| +---------------------------------------------------------------+ |
| | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | C
| +---------------------------------------------------------------+ |
|                                 |               |                 |
| +---------------------------------------------------------------+ |
| | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | | D
| +---------------------------------------------------------------+ |
|                                 |               |                 |
|                                 +---------------------------------|
+-------------------------------------------------------------------+
```

One nice thing about the 8-bit register halves is that you can read and
change one half of a 16-bit number without disturbing the other half. This
means that if you place the word-sized hexadecimal value 76E9H into
register AX, you can read the byte-sized value 76H from register AH, and
0E9H from register AL.

Keep in mind that this dual nature involves only the 16-bit general-purpose
registers AX, BX, CX, and DX. The other 16-bit general-purpose registers,
SP, BP, SI, and DI, are not similarly equipped. There are no SIH and SIL
8-bit registers, for example, as convenient as that would sometimes be.

<a name="TheInstructionPointer"></a>
### The Instruction Pointer
Yet another type of register lives inside the x86 CPUs. The instruction
pointer (usually called IP or, in 32-bit protected mode, EIP) is in a class
by itself.

It can do only one thing: it contains the offset address of the next
machine instruction to be executed in the current code segment.

A code segment is an area of memory where machine instructions are stored.
The steps and tests of which a program is made are contained in code
segments.  Depending on the programming model you’re using (more on this
shortly) there may be many code segments in a program, or only one. The
current code segment is that code segment whose segment address is
currently stored in code segment register CS. At any given time, the
machine instruction currently being executed exists within the current code
segment. In real mode segmented model, the value in CS can change
frequently. In the two flat models, the value in CS (almost) never
changes—and certainly never changes at the bidding of an application
program. (As you’ll see later, in protected mode all the segment registers
‘‘belong’’ to the operating system and are not changeable by ordinary
programs.)

While executing a program, the CPU uses IP to keep track of where it is in
the current code segment. Each time an instruction is executed, IP is
incremented by some number of bytes. The number of bytes is the size of the
instruction just executed. The net result is to bump IP further into
memory, so that it points to the start of the next instruction to be
executed. Instructions come in different sizes, ranging typically from 1 to
6 bytes. (Some of the more arcane forms of the more arcane instructions may
be even larger.) The CPU is careful to increment IP by just the right
number of bytes, so that it does in fact end up pointing to the start of
the next instruction, and not merely into the middle of the last
instruction or some other instruction.

The nature of this address depends on what CPU you’re using, and the
programming model for which you’re using it.

IP is 16 bits in size. In the 386 and later CPUs, IP (like all the other
registers except the segment registers) graduates to 32 bits in size and
becomes EIP.

In real mode segmented model, CS and IP working together give you a 20-bit
address pointing to one of the 1,048,576 bytes in real-mode memory. In both
of the two flat models (more on which shortly), CS is set by the operating
system and held constant. IP does all the instruction pointing that you,
the programmer, have to deal with. In the 16-bit flat model (real mode flat
model), this means IP can follow instruction execution all across a full
64K segment of memory. The 32-bit flat model does far more than double
that; 32 bits can represent 4,294,967,290 different memory addresses.
Therefore, in 32-bit flat model (that is, protected mode flat model), IP
can follow instruction execution across over 4 gigabytes of memory.

IP is notable in being the only register that can be neither read from nor
written to directly. There are tricks that may be used to obtain the
current value in IP, but having IP’s value is not as useful as you might
think, and you won’t often have to do it.

<a name="TheFlagsRegister"></a>
### The Flags Register
It is 16 bits in size in the 8086, 8088, and 80286, and its formal name is
FLAGS. It is 32 bits in size in the 386 and later CPUs, and its formal name
in the 32-bit CPUs is EFLAGS. Most of the bits in the flags register are
used as single-bit registers called flags. Each of these individual flags
has a name, such as CF, DF, OF, and so on, and each has a very specific
meaning within the CPU.

The fixed bits at bit positions 1, 3 and 5, and carry, parity, adjust, zero
and sign flags are inherited from an even earlier architecture, 8080.

<a name="T_FlagsRegister"></a>
**Table**: FLAGS

Bit # | Mask | Abbreviation | Description | Category
------|------|--------------|-------------|----------
0 | 0x0001 | CF | Carry Flag | Status
1 | 0x0002 | | Reserved, always 1 in EFLAGS | 
2 | 0x0004 | PF | Parity flag | Status
3 | 0x0008 | | Reserved |
4 | 0x0010 | AF | Adjust Flag | Status
5 | 0x0020 | | Reserved |
6 | 0x0040 | ZF | Zero Flag | Status
7 | 0x0080 | SF | Sign Flag | Status
8 | 0x0100 | TF | Trap Flag (Single Step) | Control
9 | 0x0200 | IF | Interrupt enable flag | Control
10 | 0x0400 | DF | Direction Flag | Control
11 | 0x0800 | OF | Overflow flag | Status
12 - 13 | 0x3000 | IOPL | I/O Privilege Level (286+ Only), always 1 on 8086 and 186 | System
14 | 0x4000 | NT | Nested task flag (286+ Only), always 1 on 8086 and 186 | System
15 | 0x8000 | | Reserved, Always 1 on 8086 and 186, always 0 on later models |
16 | 0x0001 0000 | RF | Resume flag (386+ only) | System
17 | 0x0002 0000 | VM | Virtual 8086 mode flag (386+ only) | System
18 | 0x0004 0000 | AC | Alignment check (486SX+ Only) | System
19 | 0x0008 0000 | VIF | Virtual interrupt flag (Pentium+) | System
20 | 0x0010 0000 | VIP | Virtual interrupt pending (Pentium+) | System
21 | 0x0020 0000 | ID | Able to use CPUID instruction (Pentium+) | System
22 | 0xFFC0 0000 | ID | Able to use CPUID instruction (Pentium+) | System
23 - 31 | 0xFFCF 0000 | VAD | VAD Flag | System
32 - 63 | 0xFFFF FFFF ... 0000 0000 | | Reserved |

**NOTE**: 0 until 15 belong to *FLAGS* (16-bit), 16 until 31 belong to
*EFLAGS*, and 31 until 63 belong to *RFLAGS*.

<a name="RealModeFlatModel"></a>
## Real Mode Flat Model
In real mode, the CPU can see only one megabyte (1,048,576) of memory. You
can access every last one of those million-odd bytes by using the
segment:offset register trick shown earlier to form a 20-bit address out of
two 16-bit addresses contained in two registers. Or, you can be content
with 64K of memory, and not fool with segments at all.

In the real mode flat model, your program and all the data it works on must
exist within a single 64K block of memory. Sixty-four kilobytes.

<a name="F_RealModeFlatModel"></a>
```
 16-Bit                                                      
 Offset Addresses                                            
 0FFFFH - +-------------------+                              
          |     The Stack     |                              
          |                   |                              
          |-------------------| -- Stack Pointer (SP).       
          |                   |                              
          |                   |                              
          |      Unused       |                              
          |   Memory Space    |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |                   |                              
          |-------------------|                              
          |                   |                              
          |                   |                              
          | Your Program Data | -- BS. GP register like      
          |                   |        BX point to memory    
          |                   |        locations where data  
          |-------------------|        is stored.            
          |                   |                              
          |                   |                              
          |                   |                              
          | Your Program Code | -- Instruction Pointer (IP). 
          |                   |                              
          |                   |                              
          |                   |                              
  0100H - |-------------------|                              
          |  Program Segment  | -- Segment register are set  
          |    Prefix (PSP)   |    by OS, and you don't fool 
  0000H - +-------------------+    with them. CS, DS, SS, ES 
           The PSP is a holder                               
           from ancient CP/M-80!                             
```
**Figure**: Real Model Flat Model

There’s not much to it. The segment registers are all set to point to the
beginning of the 64K block of memory you can work with. (The operating
system sets them when it loads and runs your program.) They all point to
that same place and never change as long as your program is running. That
being the case, you can simply forget about them. Poof! No segment
registers, no fooling with segments, and none of the ugly complication that
comes with them.

Because a 16-bit register such as BX can hold any value from 0 to 65,535,
it can pinpoint any single byte within the full 64K your program has to
work with. Addressing memory can thus be done without the explicit use of
the segment registers. The segment registers are still functioning, of
course, from the CPU’s point of view. They don’t disappear and are still
there, but the operating system sets them to values of its own choosing
when it launches your program, and those values will be good as long as
your program runs.  You don’t have to access the segment registers in any
way to write your program.

Most of the general-purpose registers may contain addresses of locations in
memory. You use them in conjunction with machine instructions to bring data
in from memory and write it back out again.

<a name="RealModeSegmentedModel"></a>
## Real Mode Segmented Model
Under both flat models you can squint a little and pretend that segments
and segment registers don’t really exist, but they are both still there and
operating, and once you get into some of the more exotic styles of
programming, you will need to be aware of them and grasp how they work.

The CPU handles the combination of segments and offsets into a full 20-bit
address internally.  Your job is to tell the CPU where the two different
components of that 20-bit address are. The customary notation is to
separate the segment register and the offset register by a colon, as shown
in the following example:

SS:SP

SS:BP

ES:DI

DS:SI

CS:BX

A program written for real mode segmented model can see all of real mode
memory.

<a name="F_RealModeSegmentedModel"></a>
```
                                                                          
     16-Bit                                   20-Bit                      
Segment Addresses                        Memory Address                   
(Every 16 bytes                                                           
   in memory)                                                             
                  +-------------------+ 0FFFF (1MB)                       
                  |                   |                                   
                  |                   |                                   
                  |                   |                                   
          SS -->  |-------------------|                                   
                  |     The Stack     |                                   
                  |...................| <-- SP Address pointed to is SS:SP
                  |   Stack Segment   |                                   
                  |                   |                                   
                  |-------------------|                                   
                  |                   |                                   
                  |    Data Segment   |                                   
                  |                   |                                   
          ES -->  |-------------------|                                   
                  |                   |                                   
                  |    Data Segment   | <-- DI Address pointed to is ES:DI
                  |                   |                                   
          DS -->  |-------------------|                                   
                  |                   |                                   
                  |    Code Segment   | <-- SI Address pointed to is DS:SI
                  |                   |                                   
          CS -->  |-------------------|                                   
                  |                   |                                   
                  |    Code Segment   |                                   
                  |                   | <-- IP Next instruction executed  
                  |-------------------|     is at CS:IP                   
                  |                   |                                   
                  |                   |                                   
                  | Much of memory is |                                   
                  | taken up by the   |                                   
                  | OS and various    |                                   
                  | buffers and       |                                   
                  | tables dedicated  |                                   
                  | to its use.       |                                   
                  |                   |                                   
                  |                   |                                   
     Segment 0 -- +-------------------+  -- 00000H                        
```
**Figure**: Real mode segmented model

**Note**: Segment registers specify which paragraph boundary begins a
segment. Segment registers do not contain memory address per se!

**Note**: Segments need not be all the same size. and they may overlap.

**Note**: You the programmer do not change code segments directly. "Long
Jump" instructions alter CS as needed.

In contrast to real mode flat model , the diagram here shows all of memory,
not just the one little 64K chunk that your real mode flat model program is
allocated when it runs. A program written for real mode segmented model can
see all of real mode memory.

The diagram shows two code segments and two data segments. In practice you
can have any reasonable number of code and data segments, not just two of
each. You can access two data segments at the same time, because you have
two segment registers available to do the job: DS and ES.

In the 386 and later processors, you have two additional segment registers,
FS and GS. Each can specify a data segment, and you can move data from one
segment to another using any of several machine instructions. However, you
only have one code segment register, CS. CS always points to the current
code segment, and the next instruction to be executed is pointed to by the
IP register. You don’t load values directly into CS to change from one code
segment to another. Machine instructions called jumps change to another
code segment as necessary. Your program can span several code segments, and
when a jump instruction (of which there are several kinds) needs to take
execution into a different code segment, it changes the value in CS for
you.

There is only one stack segment for any single program, specified by the
stack segment register SS. The stack pointer register SP points to the
memory address (relative to SS, albeit in an upside-down direction) where
the next stack operation will take place.

You need to keep in mind that in real mode, there will be pieces of the
operating system (and if you’re using an 8086 or 8088, that will be the
whole operating system) in memory with your program, along with important
system data tables. You can destroy portions of the operating system by
careless use of segment registers, which will cause the operating system to
crash and take your program with it. This is the danger that prompted Intel
to build new features into its 80386 and later CPUs to support a
‘‘protected’’ mode. In protected mode, application programs (that is, the
programs that you write, as opposed to the operating system or device
drivers) cannot destroy the operating system or other application programs
that happen to be running elsewhere in memory via multitasking. That’s what
the protected means.

<a name="ProtectedModelFlatModel"></a>
## Protected Model Flat Model
Intel’s CPUs have implemented a very good protected mode architecture since
the 386 appeared in 1986. However, application programs cannot make use of
protected mode all by themselves. The operating system must set up and
manage a protected mode before application programs can run within it.

<a name="F_ProtectedModeFlatModel"></a>
```
                                                                  32-Bit                         
                                                             Flat Addresses                      
                               4GB -- +-------------------+  -- 0FFFFFFFFH                       
                                      |                   |                                      
                                      |                   |                                      
                                      |                   |                                      
                                      |                   |                                      
                                      |                   |                                      
                                      |-------------------|                                      
                                      |     The Stack     |                                      
                                      |-------------------| <-- ESP                              
                                      |                   |                                      
                                      |                   | <-- ESI                              
                                      | Your Program Data |          32-Bit GP registers point   
                                      |                   | <-- EDI  to memory locations where   
                                      |                   | <-- EBX  data is stored.             
                                      |-------------------|                                      
                                      |                   |                                      
                                      |                   | <-- EIP EIP points to the memory     
                                      | Your Program Code |         location of the next machine 
                                      |                   |         instruction to be executed   
                                      |                   |         by the CPU.                  
                                      |-------------------|                                      
     Segment registers have a new job |                   |                                      
     now. The locate your 4GB "flat"  | Some portions of  |                                      
     segment in system virtual        | your address      |                                      
     memory. The OS won't let you     | space may be      |                                      
     fool with them! They're          | "owned" by the OS |                                      
     "Protected"!                     | and not available |                                      
     +----------------------------+   | for your          |                                      
     |                            |   | program use.      |                                      
     | +----+ +----+ +----++----+ |   |                   |                                      
  ---| | CS | | CS | | CS || CS | |   |                   |                                      
 |   | +----+ +----+ +----++----+ |   |                   |                                      
 |   |                            |   |                   |                                      
 |   +----------------------------+   |                   |                                      
 |_______________________________>    +-------------------+  -- 0000H                            
 
```
**Figure**: Protected Model Flat Model

In Protected mode flat model program sees a single block of memory
addresses running from zero to a little over 4 gigabytes. Each address is a
32-bit quantity. All of the general-purpose registers are 32 bits in size,
so one GP register can point to any location in the full 4GB address space.
The instruction pointer is 32 bits in size as well, so EIP can indicate any
machine instruction anywhere in the 4GB of memory.

The segment registers still exist, but they work in a radically different
way.  Not only don’t you have to fool with them; you can’t. The segment
registers are now considered part of the operating system, and in almost
all cases you can neither read nor change them directly. Their new job is
to define where your 4GB memory space exists in physical or virtual memory.
Physical memory may be much larger than 4GB, and currently 4GB of memory is
not especially expensive. However, a 32-bit register can only express
4,294,967,296 different locations. If you have more than 4GB of memory in
your computer, the operating system must arrange a 4GB region within
memory, and your programs are limited to operating in this region. Defining
where in your larger memory system this 4GB region falls is the job of the
segment registers, and the operating system keeps them very close to its
vest.

Virtual memory is a system whereby a much larger memory space can be
‘‘mapped’’ onto disk storage, so that even with only 4GB of physical memory
in your machine, the CPU can address a ‘‘virtual’’ memory space millions of
bytes larger. Again, this is handled by the operating system, and handled
in a way that is almost completely transparent to the software that you
write.

It’s enough to understand that when your program runs, it receives a 4GB
address space in which to play, and any 32-bit register can potentially
address any of those 4 billion memory locations, all by itself. This is an
oversimplification, especially for ordinary Intel-based desktop PCs. Not
all of the 4GB is at your program’s disposal, and there are certain parts
of the memory space that you can’t use or even look at. Unfortunately, the
rules are specific to the operating system you’re running under.

The main difference between *Real mode flat model* and *Protect mode flat
model* is that in real mode flat model, your program owns the full 64K of
memory that the operating system hands it. In protected mode flat model,
you are given a portion of 4GB of memory as your own, while other portions
still belong to the operating system.  Apart from that, the similarities
are striking: a general-purpose (GP) register can by itself specify any
memory location in the full memory address space, and the segment registers
are really the tools of the operating system—not you, the programmer.
(Again, in protected mode flat model, a GP register can hold the address of
any location in its 4GB space, but attempting to actually read from or
write to certain locations will be forbidden by the OS and trigger a
runtime error.)

What difficulty exists in programming for protected mode flat model lies in
understanding the operating system, its requirements, and its restrictions.
