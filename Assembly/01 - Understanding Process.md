# Understanding Process

## Table of Conetents
1. [Introduction](#C_Introduction)
1. [Memory Layout of a Process](#C_MemoryLayoutOfAProcess)

<a name="C_Introduction"></a>
## Introduction
A **Program** is a file containing a range of information that describes
how to construct a process at run time. This information include the
following:

* **Binary Format Identification**: *ELF*, *a.out*, *COFF*
* **Machine-Language Instructions**: These encode the algorithm of the
  program.
* **Program Entry-Point Address**: This identifies the location of the
  instruction at which execution of the program should commence.
* **Data**: The program file contains values used to initialize variables
  and also literal constants used by the program
* **Symbol and Relocation Tables**: These describe the locations and names
  of functions and variables within the program. These tables are used for
  a variety of purposes, including debugging and run-time symbol resolution
  (dynamic linking).
* **Shared-Library and Dynamic-Linking Information**: The program file
  includes fields listing the shared libraries that the program needs to
  use at run time and the pathname of the dynamic linker that should be
  used to load these libraries.
* **Other Information**: The program file contains various other
  information that describes how to construct a process.

One program may be used to construct many processes, or, put conversely,
many processes may be running the same program.

A **process** is an instance of an **executing program**. To describe
precisely: a process is an abstract entity, defined by the kernel, to which
system resources are allocated in order to execute a program.

From the kernel’s point of view, a process consists of user-space memory
containing program code and variables used by that code, and a range of
kernel data structures that maintain information about the state of the
process. The information recorded in the kernel data structures includes
various identifier numbers (IDs) associated with the process, virtual
memory tables, the table of open file descriptors, information relating to
signal delivery and handling, process resource usages and limits, the
current working directory, and a host of other information.

<a name="C_MemoryLayoutOfAProcess"></a>
## Memory Layout of a Process
The memory allocated to each process is composed of a number of parts,
usually referred to as segments. These segments are as follows:

* **Text Segment**<br />
The text segment contains the machine-language instructions of the program
run by the process. The text segment is made read-only so that a process
doesn’t accidentally modify its own instructions via a bad pointer value.
Since many processes may be running the same program, the text segment is
made sharable so that a single copy of the program code can be mapped into
the virtual address space of all of the processes.
