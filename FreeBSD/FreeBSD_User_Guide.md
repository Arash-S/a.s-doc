# FreeBSD User Guide

## Table of Contents
1. [Introduction](#Installation)
    1. [Disks and Filesytems](#Disks-and-Filesytems)
    1. [Filesystem Encryption](#Filesystem-Encryption)
    1. [Disk Partitioning Methods](#Disk-Partitioning-Methods)
        1. [Partitioning with UFS](#Partitioning-with-UFS)
    1. [Swap Space](#Swap-Space)
1. [The Boot Process](#The-Boot-Process)
    1. [Power-On](#Power-On)
        1. [Unified Extensible Firmware Interface](#Unified-Extensible-Firmware-Interface)
        1. [Basic Input-Output System](#Basic-Input-Output-System)
    1. [The Loader](#The-Loader)
        1. [Boot Multi User](#Boot-Multi-User)
            1. [SYSRC](#SYSRC)
            1. [rc.conf.d Directory](#rc-conf-d-Directory)
            1. [Startup Options](#Startup-Options)
            1. [Filesystem Options](#Filesystem-Options)
            1. [Miscellaneous Network Daemons](#Miscellaneous-Network-Daemons)
            1. [Network Options](#Network-Options)
            1. [Network Routing Options](#Network-Routing-Options)
            1. [Console Options](#Console-Options)
            1. [Other Options](#Other-Options)
        1. [Boot FreeBSD in Single-User Mode](#Boot-FreeBSD-in-Single-User-Mode)
           1. [Disks in Single-User mode](#Disks-in-Single-User-mode)
           1. [UFS in Single-User Mode](#UFS-in-Single-User-Mode)
           1. [ZFS in Single-User Mode](#ZFS-in-Single-User-Mode)
           1. [Programs Available in Single-User Mode](#Programs-Available-in-Single-User-Mode)
           1. [The Network in Single-User Mode](#The-Network-in-Single-User-Mode)
           1. [Uses for Single-User Mode](#Uses-for-Single-User-Mode)
        1. [The Loader Prompt](#The-Loader-Prompt)
           1. [Viewing Disk](#Viewing-Disk)
           1. [Loader Variables](#Loader-Variables)
           1. [Booting from the Loader](#Booting-from-the-Loader)
           1. [Loader Configuration](#Loader-Configuration)
    1. [Boot Options](#Boot-Options)
    1. [Startup Messages](#Startup-Messages)
    1. [The rc.d Startup System](#The-rc-d-Startup-System)
        1. [The service Command](#The-service-Command)

## Introduction
There are two sort of configuration files in FreeBSD
* Default files
* Customization files

The default files contain variable assignments and aren’t intended to be
edited; instead, they’re designed to be overridden by another file of the same
name. Default configurations are kept in a directory called default . For
example, the boot loader configuration file is /boot/loader.conf , and the
default configuration file is /boot/defaults/loader.conf . If you want to see a
comprehensive list of loader variables, check the default configuration file.

During upgrades, the installer replaces the default configuration files but
doesn’t touch your local configuration files. This separation ensures that your
local changes remain intact while still allowing new values to be added to the
system.

**Note** keep your configuration files in a version control system. And
remember don't copy the default configuration file because it will eventually
bit you and made you a mistake.

### Disks and Filesytems
Using EFI permits FreeBSD to do some interesting things, like full-disk
encryption.

A base install of FreeBSD fits in about half a gigabyte of disk, but the
filesystem beneath those files dicates much of how the system behaves.

FreeBSD supports two major filesystems:
* Unix File System (UFS): is a direct descendant of the filesystem shipped with
  4.4 BSD and has been under continuous development for decades. UFS is
  designed to handle the most common situations effectively while reliably
  supporting unusual configurations. FreeBSD ships with UFS configured to be as
  widely useful as possible on modern hardware, but you can choose to optimize
  a partition for trillions of tiny files or a handful of 1TB files if you
  desire. UFS isn’t perfect either. A power failure or system crash can damage
  a UFS filesystem.  Repairing that filesystem takes time and system memory.
  Roughly speaking, repairing each terabyte in a UFS filesystem requires 700MB
  of RAM. If you create a 7TB filesystem on a system with 6GB of RAM, FreeBSD
  can’t automatically repair it.
* ZFS (Not an acronym): Introduced by Solaris in 2005. ZFS computes a checksum
  of every block of data or metadata and can use it for error correction.
  Storage is pooled, meaning that you can dynamically add more disks to an
  existing ZFS filesystem without recreating the filesystem. ZFS has a whole
  bunch of cool features, such as highly effective built-in replication and the
  ability to create and remove datasets (partitions) on the fly. ZFS cool
  features impose a performance cost, and ZFS can use a whole bunch of memory.
  I resist running ZFS on hosts with less than 4GB of RAM and refuse to run it
  on less than 2GB of RAM. UFS serves small and embedded systems better than
  ZFS can. ZFS makes a great storage system for a virtualization server, but it
  isn’t necessarily right for virtual machines that use disk images. Many
  virtual machines don’t get enough memory to effectively run ZFS.
  Additionally, I’ve seen more than one KVM-based virtualization system fail to
  migrate ZFS-based virtual machines. If you want to use ZFS on virtualized
  clients, be sure your virtualization system supports restoring and migrating
  ZFS disk images before installing a slew of hosts. 

**Note**: Never, never, never use a RAID controller with ZFS; using RAID
volumes as disks interferes with ZFS’s self-healing features. Many RAID
controllers claim to offer raw disks, but what they really offer are one-disk
RAID containers.

### Filesystem Encryption
FreeBSD supports two disk encryption systems:
* **GEOM-Based Disk Encryption** (GBDE): The gbde(8) encryption system is
  designed for use in situations where the mere existence of encrypted data can
  threaten the user’s life. It’s designed to protect a user who has a gun to
  their head.
* **GELI**: The geli(8) encryption system protects against more common risks.
  If your laptop is stolen, GELI prevents the thief from reading the hard
  drive. 4 If you store your company’s financial records on a GELI-encrypted
  partition, the service tech can’t read it during a service call.

### Disk Partitioning Methods
Older and smaller hardware uses master
boot record (MBR) partitioning and is always limited to disks of 2TB or
smaller. Newer and larger hardware uses the more flexible and generally better
GUID Partition Tables (GPT) scheme. FreeBSD manages both types of partition
with gpart(8).

#### Partitioning with UFS
At a minimum, separate your operating system from your data. If this host is
for user accounts, create a separate ```/home``` partition. If you’re running a
database, create a partition for the database. Web servers should have a
partition for web data and probably a second one for logs.  As an old Unix
hand, I usually create separate ```/usr```, ```/usr/local```, ```/var```,
```/var/log```, and ```/home``` partitions, as well as a partition for root (
```/```) and one for swap space, plus a separate partition for the server’s
application data. I’m told that I’m a fuddy-duddy, though, and that my concerns
about rogue processes and users filling up the hard drive are obsolete these
days. 5 A base install of modern FreeBSD fits in about half a gigabyte. That’s
trivial next to today’s hard drives. On a modern disk running on real hardware,
assigning 20GB for the operating system and related programs should be more
than sufficient.  If you’re running FreeBSD on modern hardware, though, you
probably want to use ZFS rather than UFS.

### Swap Space
Longrunning wisdom claimed that a host should have twice as much swap as it
has physical memory, but today that’s not only obsolete but dangerous

A modern host should have only enough swap space to perform its task.

Multiple hard drives let you increase the efficiency of swap space by splitting
it between disks on different drive controllers. Remember, though, that a crash
dump must fit entirely within a single swap partition. FreeBSD compresses crash
dumps so that they don’t take up as much room, but still, many small swap
partitions can be counterproductive. If you have a large number of drives,
don’t use the application drives for swap; restrain swap space to the operating
system drives.

The main use for swap on modern systems is to have a place to store a memory
dump should the system panic and crash. FreeBSD uses kernel minidumps, so they
dump only the kernel memory. A minidump is much smaller than a full dump: a
host with 8GB RAM has an average minidump size of about 250MB. Provisioning a
gigabyte of swap per 10GB of RAM should be sufficient for most situations.

If you have a truly intractable problem, though, you might need to dump the
entire contents of your RAM to swap. If I’m setting up an important production
system, I always create an unused partition larger than the host’s greatest
possible virtual memory space and tell the host to dump the kernel to that
partition. If my laptop has such a problem, I’ll just plug in a flash drive and
configure the system to dump on it instead.

## The Boot Process
The boot process itself can be divided into three main parts: the loader,
single-user startup, and multiuser startup.

### Power-On
A Computer for finding and loading its OS use BIOS or in newer systems use the
Unified Extensible Firmware Interface (UEFI).

#### Unified Extensible Firmware Interface
UEFI searches the boot drive for a partition marked as a UEFI boot partition.
Despite what the special mark might imply, that partition contains only a FAT
filesystem with a specific directory and file layout. UEFI executes the file
/EFI/BOOT/BOOTX64.EFI . That file might be a fancy multi-OS boot loader, or it
might dump you straight into an operating system. In FreeBSD, the UEFI boot
fires up the boot loader, /boot/loader.efi.

UEFI is comparatively new. If your new system has trouble booting FreeBSD, you
might try enabling a BIOS or “legacy” mode.

#### Basic Input-Output System
A BIOS searches for a disk partition marked active and then executes the first
section of that partition. For FreeBSD, that chunk of data is called the loader
. Every FreeBSD system has a reference copy of the loader as /boot/loader.

A BIOS has all sorts of limitations. The boot loader must reside in a very
specific section of the disk. BIOS can’t boot from disks larger than 2.2TB. The
target boot loader must be smaller than 512KB—huge by 1980 standards, yes, but
paltry today. The installed loader is a binary, not a filesystem, so even minor
changes require recompiling. UEFI has none of these limitations and offers
modern features, like mouse support.

### The Loader
The loader , or boot blocks , loads the FreeBSD kernel and presents you with a
menu before starting that kernel. The loader(8) program offers a menu of seven
options on the left. A new FreeBSD system presents these options:

1. **B**oot Multi User [Enter]
1. Boot **S**ingle User
1. **Esc**ape to loader prompt
1. Reboot
1. Kernel: default/kernel (1 of 2)
1. Configure Boot **O**ptions...
1. Select Boot Environment...

Each option highlights certain words or characters, such as S in “Boot Single
User” and ESC in “Escape to loader prompt.” Select an option by pressing the
highlighted character or the number.

#### Boot Multi User
This is a normal boot. Hit ENTER to boot immediately, skipping the 10-second
delay.

When FreeBSD finishes inspecting the hardware and attaching all the device
drivers appropriately, it runs the shell script /etc/rc . This script mounts
all filesystems, brings up the network interfaces, configures device nodes,
identifies available shared libraries, and does all the other work necessary to
make a system ready for normal work. Most systems have different startup
requirements; while almost every server needs to mount a hard drive, a web
server’s operating requirements are very different from those of a database
server, even if it’s running on absolutely identical hardware. This means that
/etc/rc must be extremely flexible. It achieves flexibility by delegating
everything to other shell scripts responsible for specific aspects of the
system.

The /etc/rc script is controlled by the files /etc/defaults/rc.conf and
/etc/rc.conf.

The configuration of /etc/rc is split between two files:
* /etc/defaults/rc.conf (**Default**)
* /etc/rc.conf (**Local**)

Settings in /etc/rc.conf override any values given in /etc/defaults/rc.conf,
exactly as with the loader. Almost everything in a standard FreeBSD system has
one or more rc.conf knobs (variables in /etc/defaults/rc.conf). For a complete,
up-to-date list, read rc.conf(5). To change rc.conf settings, you can either
use a text editor or sysrc(8).

##### SYSRC

If you must change dozens of servers, you can use ```sysrc```.
```sysrc``` can display information about your system's non-default settings.

```Bash
# sysrc -a
clear_tmp_enables: YES
defaulltrouter: 203.0.113.1
dumpdev: AUTO
keymap: us.dvorak.kbd
--snip--
```

To have sysrc(8) enable a service, give it the variable name, an equals sign,
and the new value.

```Bash
# sysrc rc_startmsgs=No
rc_startmsgs: YES -> No
```

Remember that sysrc(8) is a tool for changing rc.conf, not for configuring
FreeBSD. It does no validity checking.

Many FreeBSD configuration files closely resemble rc.conf. You can use sysrc(8)
to manage them by adding the -f flag and the file name.

```Bash
# sysrc -af /boot/loader.conf
```

##### rc.conf.d Directory
To manage a service in ```/etc/rc.conf.d/```, create a file named after the
service. That is, to manage bsnmpd(8) you'd create /etc/rc.conf.d/bsnmpd.
Enable or diable that service in this file.

```Bash
bsnmpd_enable=YES
```
##### Startup Options
The following rc.conf options control how FreeBSD configures itself and starts
other programs. These far-reaching settings affect how all other system
programs and services run.

If you’re having a problem with the startup scripts themselves, you might
enable debugging on /etc/rc and its subordinate scripts. This can provide
additional information about why a script is or isn’t starting.

```Bash
rc_debug="NO"
```

If you don’t need the full debugging output but would like some additional
information about the /etc/rc process , enable informational messages with
rc_info:

```Bash
rc_info="NO"
```

When the boot process hits multiuser startup, it prints out a message for each
daemon it starts. Remove those messages with the rc_startmsgs option.

```Bash
rc_startmsgs="NO"
```

##### Filesystem Options
FreeBSD can use memory as a filesystem. One common use for this feature is to
make ```/tmp``` really fast by using memory rather than a hard drive as its
backend. Variables in ```rc.conf``` let you enable a memory-backed ```/tmp```
and set its size transparently and painlessly. If you want to use a memory
filesystem ```/tmp```, set ```tmpmfs``` to ```YES``` and set tmpsize to the
desired size of your ```/tmp```.

```Bash
tmpmfs="AUTO"
tmpsize="20m"
tmpmfs_flags="-S" #Means disable soft updates.
```

When a booting FreeBSD attempts to mount its filesystems, it checks them for
internal consistency. If the kernel finds major filesystem problems, it can try
to fix them automatically with fsck -y . While this is necessary in certain
situations, it’s not entirely safe.

```Bash
fsck_y_enable="NO"
```

The kernel might also find minor filesystem problems, which it resolves on the
fly using a background fsck while the system is running in multiuser mode.
There are legitimate concerns about the safety of using this feature in
certaincircumstances. You can control the use of background fsck and set how
long the system will wait before beginning the background fsck.

```Bash
background_fsck="YES"
background_fsck_delay="60"
```

##### Miscellaneous Network Daemons
One popular daemon is syslogd(8). Once you’ve decided to run the logging
daemon, you can choose exactly how it’ll run by setting command line flags for
it. FreeBSD will use these flags when starting the daemon. For all the programs
included in rc.conf that can take command line flags, the flags are given in
this format:

```Bash
syslogd_enable="YES"
syslogd_flags="-s"
```

Another popular daemon is inetd(8), the server for small network services.

```Bash
inetd_enable="NO"
```

##### Network Options
These knobx control how FreeBSD configures its network facilities during boot.

You might be interested in failed attempts to connect to your system over the
network.  This will help detect port scans and network intrusion attempts, but
it’ll also collect a lot of garbage. It’s interesting to set this for a short
period of time just to see what really happens on your network. (Then again,
knowing what’s really going on tends to cause heartburn.) Set this to 1 to log
failed connection attempts.

```Bash
log_in_vain="0"
```

Routers use ICMP redirects to inform client machines of the proper network
gateways for particular routes. While this is completely legitimate, on some
networks intruders can use this to capture data. If you don’t need ICMP
redirects on your network, you can set this option for an extremely tiny
measure of added security. If you’re not sure whether you’re using them, ask
your network administrator.

```Bash
icmp_drop_redirect="NO"
```

If you are the network administrator and you’re not sure whether your network
uses ICMP redirects, there’s an easy way to find out—just log all redirects
received by your system to /var/log/messages . 3 Note that if your server is
under attack, this can fill your hard drive with redirect logs fairly quickly.

```Bash
icmp_log_redirect="NO"
```

To get on the network, you’ll need to assign each interface an IP address.
We’ll discuss this in some detail in Chapter 8. You can get a list of your
network interfaces with the ifconfig(8) command. List each network interface on
its own line, with its network configuration information in quotes. For
example, to give your em0 network card an IP address of 172.18.11.3 and a
netmask of 255.255.254.0, you would use:

```Bash
ifconfig_em0="inet 172.18.11.3 netmask 255.255.254.0"
```

##### Network Routing Options
FreeBSD’s network stack includes many features for routing internet traffic.
These start with the very basic, such as configuring an IP for your default
gateway. While assigning a valid IP address to a network interface gets you on
the local network, a default router will give you access to everything beyond
your LAN.

```Bash
defaultrouter=""
```

Network control devices, such as firewalls, must pass traffic between different
interfaces. While FreeBSD won’t do this by default, it’s simple to enable. Just
tell the system that it’s a gateway and it’ll connect multiple networks for
you.

```Bash
gateway_enable="NO"
```

##### Console Options

The console options control how the monitor and keyboard behave. You can change
the language of your keyboard, the monitor’s font size, or just about anything
else you like.

```Bash
keymap="NO"
```

FreeBSD turns the monitor dark when the keyboard has been idle for a time
specified in the blanktime knob. If you set this to NO , FreeBSD won’t dim the
screen. Mind you, new hardware will dim the monitor after some time as well, to
conserve power. If your screen goes blank even if you’ve set the blanktime knob
to NO , check your BIOS and your monitor manual.

```Bash
blanktime="300"
```

FreeBSD can also use a variety of fonts on the console. While the default font
is fine for servers, you might want a different font on your desktop or laptop.
My laptop has one of those 17-inch screens proportioned for watching movies,
and the default fonts look kind of silly at that size. You can choose a new
font from the directory /usr/share/syscons/fonts .  Try a few to see how they
look on your systems. The font’s name includes the size, so you can set the
appropriate variable. For example, the font swiss-8x8.fnt is the Swiss font, 8
pixels by 8 pixels. To use it, you would set the font8x8 knob.

```Bash
font8x16="NO"
font8x14="NO"
font8x8="YES"
```

You can use a mouse on the console, even without a GUI. moused(8).
```Bash
moused_enable="NO"
moused_type="AUTO"
```

##### Other Options
The sendmail(8) daemon manages transmission and receipt of email between
systems.  While almost all systems need to transmit email, most FreeBSD
machines don’t need to receive email. The sendmail_enable knob specifically
handles incoming mail, while sendmail_outbound_enable allows the machine to
transmit mail. See Chapter 20 for more details.

```Bash
sendmail_enable="NO"
sendmail_submit_enable="YES"
```

One of FreeBSD’s more interesting features is its ability to run software built
for Linux.

```Bash
linux_enable="NO"
```

A vital part of any Unix-like operating system is shared libraries. You can
control where FreeBSD looks for shared libraries. Although the default setting
is usually adequate, if you find yourself regularly setting the LD_LIBRARY_PATH
environment variable for your users, you should consider adjusting the library
path instead.

```Bash
ldconfig_paths="/usr/lib /usr/local/lib"
```

FreeBSD has a security profile system that allows the administrator to control
basic system features. You can globally disallow mounting hard disks, accessing
particular TCP/IP ports, and even changing files.

```Bash
kern_securelevel_enable="NO"
kern_securelevel="-1"
```

#### Boot FreeBSD in Single-User Mode
Single-user mode is a minimal startup mode that’s very useful on damaged
systems, especially when the damage was self-inflicted.

FreeBSD can perform a minimal boot, called single-user mode , that loads the
kernel and finds devices but doesn’t automatically set up your filesystems,
start the network, enable security, or run any standard Unix services.
Single-user mode is the earliest the system can possibly give you a command
prompt.

Why use single-user mode? If a badly configured daemon hangs the boot, you can
enter single-user mode to prevent it from starting. If you’ve lost your root
password, you can boot into single-user mode to change it. If you need to
shuffle critical filesystems around, again, single-user mode is the place to do
it.

When you choose a single-user mode boot, you’ll see the regular system startup
messages flow past. Before any programs start, however, the kernel offers you a
chance to choose a shell. You can enter any shell on the root partition; I
usually just take the default /bin/sh , but use /bin/tcsh if you prefer.

##### Disks in Single-User mode
In single-user mode, the root partition is mounted read-only and no other disks
are mounted.

Many of the programs that you’ll want to use are on partitions other than the
root, so you’ll want them all mounted read-write and available. The way to do
this varies depending on whether you’re using UFS or ZFS.

##### UFS in Single-User Mode
To make all the filesystems listed in the filesystem table /etc/fstab usable,
run the following commands:

```Bash
# fsck -p
# mount -o rw /
# mount -a
```
The fsck(8) program “cleans” the filesystems and confirms that they’re internally
consistent and that all the files that a disk thinks it has are actually present and accounted
for.

The root filesystem is mounted read-only. Whatever drove us to single-user mode
probably requires changing the root filesystem. Remount the root filesystem
read-write.  Finally, the -a flag to mount(8) activates every filesystem listed
in /etc/fstab.

##### ZFS in Single-User Mode
To make all of your ZFS datasets available, use zfs mount . You can either
mount individual datasets by name or mount everything that’s marked as
mountable with -a.

```Bash
# zfs mount -a
```
ZFS will perform its usual integrity checks before mounting the datasets.  Most
of the datasets will be exactly as accessible as in multiuser mode, but the
dataset mounted as root will still be read-only. Turn that off. Here, I’m
setting the root dataset to read-write on a default FreeBSD install.

```Bash
# zfs set readonly=off zroot/ROOT/default
```

You can now change the filesystem.

##### Programs Available in Single-User Mode
The commands available for your use depend on which partitions are mounted.
Some basic commands are available on the root partition in /bin and /sbin , and
they’re available even if root is mounted read-only. Others live in /usr and
are inaccessible until you mount that partition. (Take a look at /bin and /sbin
on your system to get an idea of what you’ll have to work with when things go
bad.)

If you’ve scrambled your shared library system, none of these programs will
work. If you’re that unlucky, FreeBSD provides statically linked versions of
many core utilities in the /rescue directory.

##### The Network in Single-User Mode
If you want to have network connectivity in single-user mode, use the shell
script /etc/netstart . This script calls the appropriate scripts to start the
network, gives IP addresses to interfaces, and enables packet filtering and
routing. If you want some, but not all, of these services, you’ll need to read
that shell script and execute the appropriate commands manually.

##### Uses for Single-User Mode
In single-user mode, your access to the system is limited only by your
knowledge of FreeBSD and Unix.

For example, if you’ve forgotten your root password, you can reset it from
single-user mode:

```Bash
# passwd
Changing local password for root
New Password:
Retype New Password:
#
```

Or, if you find that there’s a typo in /etc/fstab that confuses the system and
makes it unbootable, you can mount the root partition with the device name and
then edit /etc/fstab to resolve the issue.

If you have a program that panics the system on boot and you need to stop that
program from starting again, you can either edit /etc/rc.conf to disable the
program or set the permissions on the startup script so that it can’t execute.

```Bash
# chmod a-x /usr/local/etc/rc.d/program.sh
```

#### The Loader Prompt
The loader prompt allows you to make basic changes to your computer’s boot
environment and the variables that must be configured early in the boot
process. It’s not a Unix-like environment; it’s cramped and supports only a
minimal feature set. When you escape to a loader prompt (the third option in
the boot menu), you’ll see the following:

```Bash
Type '?' for a list of commands, 'help' for more detailed help.
OK
```

This isn’t a full-featured operating system; it’s a tool for configuring a
system boot that’s not intended for the ignorant nor the faint of heart. Any
changes you make at the loader prompt affect only the current boot. To undo
changes, reboot again.

##### Viewing Disk
To view the disks that the loader knows about, use ```lsdev```.

##### Loader Variables
The loader has variables set within the kernel and by a configuration file.
View these variables and their settings with the ```show``` command, and use
the spacebar to advance to the next page.

These values include low-level kernel tunables and information gleaned from the
hardware BIOS or UEFI.

You can show specific variables by name. Sadly, you can’t show all of a
keyword’s sub-variables. A command like show acpi.oem works, but show acpi or
show acpi.* doesn’t.  Change a value for a single boot with the ```set```
command.

The loader lets you change variables that really shouldn’t change. Setting
acpi.revision to 4 won’t suddenly upgrade your system to ACPI version 4, and
you can’t change hard drives with a software setting.

##### Booting from the Loader
Now that you’ve twiddled your system’s low-level settings, you probably want to
boot the system. Use the boot(8) command. You can adjust the boot further using
the boot flags discussed in the man page.

Once your system boots just the way you need it to, you’ll probably want to
make those settings permanent. FreeBSD lets you do this through the loader
configuration file.

##### Loader Configuration
Make loader setting changes permanent with the configuration file
/boot/loader.conf .  Settings in this file are fed directly into the boot
loader at system startup. Of course, if you enjoy being at your console every
time the system boots, then don’t bother with this!  The loader has a default
configuration file, /boot/defaults/loader.conf . We override many of the values
here.

If you look at the default loader configuration, you’ll see many options that
resemble variables listed in the loader.

Throughout the FreeBSD documentation, you’ll see references to boot-time
tunables and loader settings . All of these are set in loader.conf , which
includes many sysctl values that are read-only once the system is up and
kicking.

Some of these variables don’t have a specific value set in loader.conf ;
instead, they appear as empty quotes. This means that the loader normally lets
the kernel set this value, but if you want to override the kernel’s setting,
you can.

**Some commonly used loader values**

These values are affect the appearance and operation of the loader itself and
basic boot functionality.

* **boot_verbose="NO"**:

  This value toggles the verbose boot mode that you can reach through the boot
  menu.  In a standard boot, the kernel prints out a few basic notes about each
  device as it identifies system hardware. When you boot in verbose mode, the
  kernel tells each device driver to print out any and all information it can
  about each device as well as display assorted kernel-related setup details.
  Verbose mode is useful for debugging and development, but not generally for
  day-to-day use.

* **autoboot_delay="10"**:

  This value indicates the number of seconds between the display of the boot
  menu and the automatic boot. I frequently turn this down to 2 or 3 seconds,
  as I want my machines to come up as quickly as possible.

* **beastie_disable="NO"**:

  This value controls the appearance of the boot menu (originally, an ASCII art
  image of the BSD “Beastie” mascot decorated the boot menu). If set to YES ,
  the boot menu will not appear.

* **loader_logo="fbsdbw"**:
  
  This value allows you to choose which logo appears to the right of the boot
  menu. The fbsdbw option gives you the default FreeBSD logo in ASCII art.
  Other options include beastiebw (the original logo), beastie (the logo in
  color), and none (no logo).

### Boot Options
The boot menu also presents three options: choosing a kernel, setting boot
options, and selecting a boot environment. We’ll discuss each of these in an
appropriate section, but here’s a bit to orient you.

A host can have multiple kernels in its /boot directory. Hitting the Kernel
option tells the loader to cycle between the available options. To have a
kernel appear as an option, list it in loader.conf in the kernels variable.

```Bash
KERNELS="kernel kernel.old kernel.GENERIC"
```

The menu recognizes kernels only in directories beginning with /boot/kernel .
If you have a kernel in /boot/gerbil , you’ll have to load it from the loader
prompt.

FreeBSD supports a number of boot options. Selecting the Configure Boot Options
item brings up the most popular.

* Load System Default
  
  You mucked with your settings and want to undo all that? Choose this. You can
  at least boot the system to single-user mode and fix your loader.conf .

* ACPI Support
  
  ACPI is the Advanced Configuration and Power Interface, an
  Intel/Toshiba/Microsoft standard for hardware configuration. It replaces and
  subsumes a whole bunch of obscure standards. ACPI has been a standard for
  many years now, but if a particular piece of hardware has trouble running
  FreeBSD, you can turn it off and see what happens. If you even think of
  trying this option.

* Safe Mode

  FreeBSD’s safe mode turns on just about every conservative option in the
  operating system. It turns off DMA and write caching on hard disks, limiting
  their speed but increasing their reliability. It turns off ACPI. 32-bit
  systems disable SMP. USB keyboards no longer work in safe mode. This option
  is useful for debugging older hardware.

* Verbose
  
  The FreeBSD kernel probes every piece of hardware as it boots. Most of the
  information discovered is irrelevant to day-to-day use, so the boot loader
  doesn’t display it. When you boot in verbose mode, FreeBSD prints all the
  details it can about every system setting and attached device. The
  information will be available later in /var/run/dmesg.boot , as discussed in
  the next section. I encourage you to try verbose mode on new machines, just
  to glimpse the system’s complexity.

### Startup Messages
A booting FreeBSD system displays messages indicating the hardware attached to
the system, the operating system version, and the status of various programs
and services as they start. These messages are important when you first install
your system and when you do troubleshooting.

While the boot information is handy, chances are it’ll disappear from the
screen by the time you need it. For future reference, FreeBSD stores boot
messages in the file ```/var/run/dmesg.boot```. This means that you can inspect
your kernel’s hardware messages even after your system has been up and running
for months.

One key thing that the kernel displays in the boot messages is the device name
for each piece of hardware. This is critical information for managing your
system. Every piece of hardware has a device node name, and to configure it,
you’ll need to know that name.

### The rc.d Startup System
FreeBSD bridges the gap between single-user mode and multiuser mode via the
shell script /etc/rc . This script reads in the configuration files
/etc/defaults/rc.conf and /etc/rc.conf , and runs a collection of other scripts
based on what it finds there. For example, if you’ve enabled the network time
daemon, /etc/rc runs a script written specifically for starting that daemon.
FreeBSD includes scripts for starting services, mounting disks, configuring the
network, and setting security parameters.  These scripts live in /etc/rc.d and
/usr/local/etc/rc.d.

#### The service Command
All of the rc.d scripts are readable, and the way they fit together is pretty
straightforward.  When you have a problem, you can read the scripts to see how
they work and what they do. But that’s a lot like work, and most sysadmins have
more interesting work to do. The service(8) command provides a friendly
frontend to the rc.d scripts. You can use service(8) to see which scripts run
automatically; to stop, start, and restart services; to check the status of a
service; and more.

Use the -e flag to service(8) to see the full path of all scripts that’ll be
run at system boot, in the order they’ll be run.

```Bash
# service -e
```

The commands each service supports vary. The easiest way to get the full list
of commands a particular service supports is to give the service a bogus
argument. Something like “bert” is pretty bogus.

```Bash
# service sshd bert
/etc/rc.d/sshd: unknown directive 'bert'.
Usage: /etc/rc.d/sshd [fast|force|one|quiet](start|stop|restart|rcvar|enabled|
describe|extracommands|configtest|keygen|reload|status|poll)
```

The first group, in square brackets, contains options for the commands. Here
are the standard options. Use them as prefixes for the commands in the second
group. To determine exactly what a service’s extra commands do, you need to
read the service script.

Options       | Description
--------------|------------
fast          | Do no checking (used during startup).
force         | Try harder.
one           | Start this service despite not being enabled in rc.conf.
quiet         | Only print service name (used during startup).
start         | Start the service.
stop          | Stop the service.
restart       | Stop and restart the service.
rcvar         | Print the rc.conf variable for this service.
enabled       | Return true in shell if enabled (for script use).
describe      | Print service description.
extracommands | Show service-specific commands.
configtest    | Parse the service's configuration file and stop if there's an error.
reload        | Perform a soft reload (usually via SIGHUP) rather than a restart.
status        | Determine whether service is running.

#### System Shutdown
FreeBSD makes the rc.d startup system do double duty; not only must it handle
system startup, it must also shut all those programs down when it’s time to
power down.  Something has to unmount all those hard drives, shut down the
daemons, and clean up after doing all the work. Some programs don’t care
whether they’re unceremoniously killed when the system closes up for the
night—after all, after the system goes down, any clients connected over SSH
will be knocked off and any half-delivered web pages remain incomplete.
Database software, however, cares very much about how it’s turned off, and
unceremoniously killing the process will damage your data. Many other programs
that manage actual data are just as particular, and if you don’t let them clean
up after themselves, you’ll regret it.

When you shut down FreeBSD with the shutdown(8), halt(8), or reboot(8)
commands, the system calls the shell script /etc/rc.shutdown . This script
calls each rc.d script in turn with the stop option, reversing the order they
were called during startup, thereby allowing server programs to terminate
gracefully and disks to tidy themselves up before the power dies.
